using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using Json.Doodle;

namespace ActiveStore
{
	public abstract class  ASModel<T> : IASModel where T : IASModel
	{
		private int uid = -1;

        private IASStore<T> store;

		public int UID {
			get {
				return uid;
			}
			set {
				uid = value;
			}
		}

		public ASModel(){
            store = new PrefASStore<T>();
		}

		public static ASRepository<T> Repository{
			get{
				if(typeof(T).IsAbstract){
					throw new Exception();
				}
				return ASRepository<T>.Get();
			}
		}

		T self(){
			return (T)Convert.ChangeType(this, typeof(T));
		}

		public void Delete (){
			T t = self();
			Repository.Remove(t);
			store.Remove(t);
            store.SaveCount(ASRepositoryUtils.GenId<T>());
		}
		
		public void Save (){
			T t = self();
			if(UID < 0){
				UID = ASRepositoryUtils.GenId<T>();
				Repository.Add(t);
			}else{
                t = Repository.Where(e => e.UID == UID).FirstOrDefault();
                int i = Repository.ToList().IndexOf(t);
                Repository[i] = t;
			}
			store.Put(t);
            store.SaveCount(ASRepositoryUtils.GenId<T>());
		}
		
		public void Pull (){
            if (uid == -1) return;
			T t = self();
			store.Get(ref t);
		}

		public override string ToString ()
		{
			return string.Format ("[ID={0}]", UID);
		}
		
		public string GetStoreKey ()
		{
			return ASStoreUtils.GetStoreIdKey<T>(UID);
		}

		public string Serialize ()
		{
			return JsonConverter.Serialize(this);
		}

		public T Deserialize<T>(string s){
			T t = JsonConverter.Deserialize<T>(s);
			return t;
		}

	}
}


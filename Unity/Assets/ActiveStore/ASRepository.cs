using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
namespace ActiveStore{
	public class ASRepository{
		public static void Pull(Type type){
			Type t1 = typeof(ASRepository<>);
			Type[] typeArgs = { type };
			Type t2 = t1.MakeGenericType(typeArgs);
			MethodInfo m = t2.GetMethod("PullRepository", BindingFlags.Static | BindingFlags.Public);
			m.Invoke(null, null);
		}

        public static IEnumerable GetInternalValue(Type type)
        {
            Type t1 = typeof(ASRepository<>);
            Type[] typeArgs = { type };
            Type t2 = t1.MakeGenericType(typeArgs);
            MethodInfo m = t2.GetMethod("GetInternalValue", BindingFlags.Static | BindingFlags.Public);
            var ret = m.Invoke(null, null);
            IEnumerable ie = ret as IEnumerable;
            return ie;
        }
    }

	
	public class ASRepository<T> : IASRepository<T> where T : IASModel{
		private static ASRepository<T> sInstance;
        private static IASStore<T> store;
		public static ASRepository<T> Get(){
			if(sInstance == null){
				sInstance = new ASRepository<T>();
			}
			return sInstance;
		}

		private List<T> items;
		public ASRepository(){
            store = new PrefASStore<T>();
			items = new List<T>();
		}
		
		public T this[int index]{
			get {
				return items[index];
			}
			set{
				items[index] = value;
			}
		}

        public static List<T> GetInternalValue()
        {
            return Get().items;
        }

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return this.GetEnumerator();
		}
		
		public IEnumerator<T> GetEnumerator ()
		{
			return items.GetEnumerator();
		}
		
		public void Add (T item)
		{
			items.Add(item);
		}
		
		public void Clear ()
		{
			items.Clear();
		}
		
		public bool Contains (T item)
		{
			return items.Contains(item);
		}
		
		public void CopyTo (T[] array, int arrayIndex)
		{
			items.CopyTo(array, arrayIndex);
		}
		
		public void Insert(int index, T item){
			items.Insert(index, item);
		}
		
		public int Count {
			get {
				return items.Count;
			}
		}

		public bool Remove (T item)
		{
			return items.Remove(item);
		}

		public void Pull(){
			store.Get(ref items);
		}

		public static void PullRepository(){
			Get ().Pull();
		}
	}
}

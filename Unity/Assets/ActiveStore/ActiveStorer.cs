using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
namespace ActiveStore{
	public class ActiveStorer {

		static public void Pull(){
            TypeUtils.FindDerivedTypes<IASModel>().ForEach(e =>
            {
				ASRepository.Pull(e);
			});	
		}

		static public void ClearStore(){
			PlayerPrefs.DeleteAll();
		}
	}
}

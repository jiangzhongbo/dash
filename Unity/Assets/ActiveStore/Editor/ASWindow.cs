﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ActiveStore;
using System;
using System.Reflection;
public class ASWindow : EditorWindow
{
    [MenuItem("ActiveStore/ASWindow")]
    static void CreateEditorWindow()
    {
        EditorWindow.GetWindow(typeof(ASWindow));
    }

    public int selected;
    public Vector2 scrollPos;
    void OnGUI()
    {
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Refresh", GUILayout.Width(100)))
        {
            ActiveStorer.Pull();
        }
        if (GUILayout.Button("Save", GUILayout.Width(100)))
        {
            ActiveStorer.Pull();
        }
        if (GUILayout.Button("Add", GUILayout.Width(100)))
        {
            ActiveStorer.Pull();
        }
        GUILayout.EndHorizontal();
        selected = GUILayout.Toolbar(selected, TypeUtils.FindDerivedTypes<IASModel>().Select(e => e.Name).ToArray());
        Type t = TypeUtils.FindDerivedTypes<IASModel>().Where(e => !e.IsAbstract && !e.IsInterface).ToList()[selected];
        showProperties(t);
    }

    void showProperties(Type type)
    {
        var win = position.width;
        var height = position.height;
        var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public).Reverse().ToList();
        GUILayout.BeginHorizontal();
        foreach (PropertyInfo p in properties)
        {
            NGUIEditorTools.DrawHeader(p.Name, GUILayout.Width(win / properties.Count));
        }
        GUILayout.EndHorizontal();

        List<object> list = ASRepository.GetInternalValue(type).Cast<object>().ToList();
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, true, true, GUILayout.Width(win), GUILayout.Height(height));
        list.ForEach(i =>
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(6f);
            foreach (PropertyInfo p in properties)
            {
                if (p.PropertyType == typeof(int))
                {
                    p.SetValue(i, EditorGUILayout.IntField((int)p.GetValue(i, null), GUILayout.Width(win / properties.Count)), null);
                }
                else if (p.PropertyType == typeof(string))
                {
                    p.SetValue(i, EditorGUILayout.TextField((string)p.GetValue(i, null), GUILayout.Width(win / properties.Count)), null);
                }
                else if (p.PropertyType == typeof(bool))
                {
                    p.SetValue(i, EditorGUILayout.Toggle((bool)p.GetValue(i, null), GUILayout.Width(win / properties.Count)), null);
                }
                else if (p.PropertyType == typeof(float))
                {
                    p.SetValue(i, EditorGUILayout.FloatField((float)p.GetValue(i, null), GUILayout.Width(win / properties.Count)), null);
                }
                GUILayout.Space(4f);
            }
            
            GUILayout.EndHorizontal();
        });
        EditorGUILayout.EndScrollView();

    }

    
}
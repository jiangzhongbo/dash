using UnityEngine;
using System.Collections;
namespace ActiveStore{
	public interface IASModel : IASSerialize{

		void Save();
		void Delete();
		void Pull();
	}
}

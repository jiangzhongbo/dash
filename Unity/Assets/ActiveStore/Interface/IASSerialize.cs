using System;
namespace ActiveStore
{
    public interface IASSerialize
    {
		int UID {
			get; set;	
        }
        string GetStoreKey();
		string Serialize();
		T Deserialize<T>(string s);
    }
}


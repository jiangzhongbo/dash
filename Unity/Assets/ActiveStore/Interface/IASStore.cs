using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace ActiveStore{
	public interface IASStore<T> where T : IASSerialize{

		void Put(T t);
		void Get(ref T t);
		void Remove(T t);

		void Put(List<T> items);
		void Get(ref List<T> items);
		void Remove(List<T> items);

		int GetCount();
		void SaveCount(int count);
		void DeleteCount();
	}
}
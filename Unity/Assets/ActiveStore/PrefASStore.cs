using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;
using MiniJSON;
using Json.Doodle;
namespace ActiveStore{
	
	public class PrefASStore<T> : IASStore<T> where T : IASSerialize{
		public PrefASStore(){
			
		}
		
		public void Put (List<T> items)
		{
			items.ToList().ForEach(e => Put(e));
		}

		public void Get (ref List<T> items)
		{
			items.Clear();
			int count = GetCount();
			for(int i = 0; i < count; i++){
				T t = Activator.CreateInstance<T>();
				t.UID = i;
				string k = t.GetStoreKey();
				if(!PlayerPrefs.HasKey(k)){
					continue;
                }
                Get(ref t);
				items.Add(t);
			}
		}
		
		public void Put (T t)
		{
            try
            {
			    string json = t.Serialize();
                //Debug.Log(json);
			    PlayerPrefs.SetString(t.GetStoreKey(), json);
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }

		}
		
		public void Get (ref T t)
		{
            try
            {
                string key = t.GetStoreKey();
                string json = PlayerPrefs.GetString(key);
                t = t.Deserialize<T>(json);
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
		}

		public void SaveCount(int count){
			PlayerPrefs.SetInt(ASStoreUtils.GetStoreCountKey<T>(), count);
		}

		public int GetCount(){
			return PlayerPrefs.GetInt(ASStoreUtils.GetStoreCountKey<T>());
		}

		public void DeleteCount(){
			PlayerPrefs.DeleteKey(ASStoreUtils.GetStoreCountKey<T>());
		}

		public void Remove (T t)
		{
			PlayerPrefs.DeleteKey(t.GetStoreKey());
		}
		
		public void Remove (List<T> items)
		{
			items.ToList().ForEach(e => Remove(e));
		}
	}
}
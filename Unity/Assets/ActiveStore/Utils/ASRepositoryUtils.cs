using UnityEngine;
using System.Collections;
using System.Linq;
namespace ActiveStore{
	public static class ASRepositoryUtils {
		public static int GenId<T>() where T : IASModel{
            if (ASRepository<T>.Get().Count == 0)
            {
                return 0;
            }
            return ASRepository<T>.Get().Select(e => e.UID).Max() + 1;
		}
	}
}
using UnityEngine;
using System.Collections;
namespace ActiveStore{
	public static class ASStoreUtils {
		public static string GetStoreIdKey<T>(int i) where T : IASSerialize{
			string name = "AS#"+typeof(T).Name+"@"+i;
			return  name;
		}

		public static string GetStoreCountKey<T>() where T : IASSerialize{
			string name = "AS#"+typeof(T).Name+"@count";
			return  "AS#"+typeof(T).Name+"@count";
		}

		public static string GetStoreNamesKey(){
			string name = "AS#StoreNames";
            return name;
		}
	}
}
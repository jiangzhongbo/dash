﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Linq;
namespace ActiveStore
{
    public static class TypeUtils
    {

        public static List<Type> FindDerivedTypes<T>()
        {
            return FindDerivedTypes(typeof(T));
        }

        public static List<Type> FindDerivedTypes(Type baseType)
        {
            var derivedTypes = new List<Type>();
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var type in assembly.GetLoadableTypes())
                {
                    try
                    {
                        if (baseType.IsAssignableFrom(type) && !type.IsAbstract && !type.IsInterface)
                        {
                            derivedTypes.Add(type);
                        }
                    }
                    catch
                    {
                    }
                }
            }
            return derivedTypes;
        }

        public static IEnumerable<Type> GetLoadableTypes(this Assembly assembly)
        {
            if (assembly == null)
            {
                return Enumerable.Empty<Type>();
            }

            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                return e.Types.Where(t => t != null);
            }
        }
    }
}
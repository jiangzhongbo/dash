﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
    using UnityEditor;
#endif
public class SSConfiguration : ScriptableObject
{
    public string StaticDataPath;
    private static SSConfiguration _instance;

    public SSConfiguration()
    {
        StaticDataPath = "StaticStore/StaticData/";
    }

    public static SSConfiguration Instance
    {
        get
        {
            if (_instance == null)
            {
                SSConfiguration configuration = Resources.Load("StaticStore/Configuration/SSConfiguration", typeof(SSConfiguration)) as SSConfiguration;
#if UNITY_EDITOR
                if (configuration == null)
                {
                    System.IO.Directory.CreateDirectory("Assets/Resources/StaticStore/Configuration/");
                    configuration = ScriptableObject.CreateInstance<SSConfiguration>();
                    AssetDatabase.CreateAsset(configuration, "Assets/Resources/StaticStore/Configuration/SSConfiguration.asset");
                    AssetDatabase.Refresh();
                }
                if (!string.IsNullOrEmpty(configuration.StaticDataPath))
                {
                    System.IO.Directory.CreateDirectory("Assets/Resources/" + configuration.StaticDataPath);
                }
#endif
                _instance = configuration;
            }

            return _instance;
        }
    }
}


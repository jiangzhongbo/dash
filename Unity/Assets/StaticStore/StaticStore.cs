﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Reflection;
public abstract class StaticStore<T> : ScriptableObject where T : StaticStore<T>
{

    private static T store;

    public static T Load()
    {
        if (store == null)
        {
            store = Resources.Load(SSConfiguration.Instance.StaticDataPath + typeof(T).Name) as T;
#if UNITY_EDITOR
            if (store == null)
            {
                store = ScriptableObject.CreateInstance<T>() as T;
                AssetDatabase.CreateAsset(store, "Assets/Resources/" + SSConfiguration.Instance.StaticDataPath + typeof(T).Name + ".asset");
                AssetDatabase.Refresh();
            }
#endif
        }
        return store;
    }

    public void Save()
    {
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
#endif
    }
}
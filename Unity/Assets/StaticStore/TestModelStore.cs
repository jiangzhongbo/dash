﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class TestModel
{
    public string id;
    public string name;
}

public class TestModelStore : StaticStore<TestModelStore>
{
    public List<TestModel> Items = new List<TestModel>();
}

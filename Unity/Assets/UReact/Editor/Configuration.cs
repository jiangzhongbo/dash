﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEditor;
using UnityEngine;
#endregion

public class Configuration : ScriptableObject
{
    #region Fields

    public string UILayer;
    public List<string> ViewPaths;
    private static Configuration _instance;

    #endregion

    #region Constructor

    public Configuration()
    {
        ViewPaths = new List<string>();
        ViewPaths.Add("Assets/UReact/Views/");
        ViewPaths.Add("Assets/Views/");
        ViewPaths.Add("Assets/Themes/");
        ViewPaths.Add("Assets/UReact/Themes/");
        UILayer = "UI";
        SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
        SerializedProperty it = tagManager.GetIterator();
        bool created = false;
        while (it.NextVisible(true))
        {
            if (it.name.StartsWith("User Layer"))
            {
                if (it.type == "string" && it.stringValue == UILayer)
                {
                    created = true;
                    break;
                }
            }
        }
        it = tagManager.GetIterator();
        it.Reset();
        if (!created)
        {
            while (it.NextVisible(true))
            {
                if (it.name.StartsWith("User Layer"))
                {
                    if (it.type == "string")
                    {
                        if (string.IsNullOrEmpty(it.stringValue))
                        {
                            it.stringValue = UILayer;
                            tagManager.ApplyModifiedProperties();
                            return;
                        }
                    }
                }
            }
        }

    }

    #endregion

    #region Methods

    public static Configuration Load()
    {
        return null;
    }

    #endregion

    #region Properties

    public static Configuration Instance
    {
        get
        {
            if (_instance == null)
            {
                Configuration configuration = AssetDatabase.LoadAssetAtPath("Assets/UReact/Configuration/Configuration.asset", typeof(Configuration)) as Configuration;
                if (configuration == null)
                {
                    System.IO.Directory.CreateDirectory("Assets/UReact/Configuration/");
                    configuration = ScriptableObject.CreateInstance<Configuration>();
                    AssetDatabase.CreateAsset(configuration, "Assets/UReact/Configuration/Configuration.asset");
                    AssetDatabase.Refresh();
                }

                if (!configuration.ViewPaths.Any())
                {
                    Debug.LogError("[UReact.356] No view paths found. Using default configuration.");
                    configuration = ScriptableObject.CreateInstance<Configuration>();
                }
                else if (String.IsNullOrEmpty(configuration.UILayer))
                {
                    Debug.LogError("[UReact.357] UILayer not set. Using default configuration.");
                    configuration = ScriptableObject.CreateInstance<Configuration>();
                }
                else
                {
                    foreach (var viewPath in configuration.ViewPaths)
                    {
                        if (String.IsNullOrEmpty(viewPath) ||
                            !viewPath.StartsWith("Assets/") ||
                            !viewPath.EndsWith("/"))
                        {
                            Debug.LogError("[UReact.358] Invalid view path in configuration. The path must start with 'Assets/' and end with '/'. Using default configuration.");
                            Debug.LogError("This sometimes happens if Unity hasn't converted the configuration asset to correct serialization mode. To fix go to [Edit -> Project settings -> Editor] and change Asset Serialization Mode to another mode and back to the desired mode. If you inspect the Configuration asset at UReact/Configuration/Configuration.asset the values should be in plain text and the view paths should look like file path strings (not a bunch of numbers).");

                            configuration = ScriptableObject.CreateInstance<Configuration>();
                            break;
                        }
                    }
                }

                _instance = configuration;
            }

            return _instance;
        }
    }

    #endregion
}


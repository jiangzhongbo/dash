﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using UnityEngine;
#endregion

namespace UReact.Editor
{
    internal class ThemeElement
    {
        #region Fields

        private string id;
        private string style;
        private Type viewType;
        private Type parentViewType;
        private Dictionary<string, string> values;

        #endregion

        #region Properties

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Style
        {
            get { return style; }
            set { style = value; }
        }

        public Type ViewType
        {
            get { return viewType; }
            set { viewType = value; }
        }

        public Dictionary<string, string> Values
        {
            get { return values; }
            set { values = value; }
        }

        public Type ParentViewType
        {
            get { return parentViewType; }
            set { parentViewType = value; }
        }

        #endregion

        #region Constructor

        public ThemeElement()
        {
            values = new Dictionary<string, string>();
        }

        #endregion

    }
}

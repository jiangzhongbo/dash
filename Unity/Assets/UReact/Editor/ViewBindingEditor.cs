﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using UReact;
using System.Linq;
using System.Reflection;
using UReact.ValueConverters;
using System;
[CustomEditor(typeof(ViewBinding))]
public class ViewBindingEditor : Editor
{

    public override void OnInspectorGUI()
    {
        ViewBinding binding = (ViewBinding)target;
        GameObject go = binding.gameObject;
        View view = go.GetComponent<View>();
        List<string> fch = view.BindingFields;
        bool save = false;
        if (GUILayout.Button("Save"))
        {
            save = true;
        }
        for (int i = 0; i < fch.Count; i++)
        {
            string e = fch[i];
            FieldInfo fi = view.GetType().GetField(e);
            if (fi.FieldType == typeof(string))
            {
                object v = EditorGUILayout.TextField(e, fi.GetValue(view).ToString());
                view.SetValue(e, v);
                if (save) StaticData.Fetch(view.GetType().Name).Set(e, v);
            }
            else if (fi.FieldType == typeof(int))
            {
                int sv = int.Parse(fi.GetValue(view).ToString());
                object v = EditorGUILayout.IntField(e, sv);
                view.SetValue(e, v);
                if (save) StaticData.Fetch(view.GetType().Name).Set(e, v.ToString());

            }
            else if (fi.FieldType == typeof(Color))
            {
                object v = EditorGUILayout.ColorField(e, (Color)fi.GetValue(view));
                view.SetValue(e, v);
                if (save) StaticData.Fetch(view.GetType().Name).Set(e, ColorValueConverter.ColorToHex((Color)v));
            }
            else if (fi.FieldType == typeof(float))
            {
                float sv = float.Parse(fi.GetValue(view).ToString());
                object v = EditorGUILayout.FloatField(e, sv);
                view.SetValue(e, v);
                if (save) StaticData.Fetch(view.GetType().Name).Set(e, v.ToString());
            }
            else if (fi.FieldType == typeof(bool))
            {
                bool sv = bool.Parse(fi.GetValue(view).ToString());
                object v = EditorGUILayout.Toggle(e, sv);
                view.SetValue(e, v);
                if (save) StaticData.Fetch(view.GetType().Name).Set(e, v.ToString());
            }
        }
        if (save) EditorUtility.SetDirty(StaticData.Fetch(view.GetType().Name));
        
    }
}
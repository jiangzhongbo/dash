﻿#region Using Statements
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Reflection;
using System.Text;
using System.IO;
using UReact.Animation;
using UReact.Views;
using UReact.ValueConverters;
#endregion

namespace UReact
{

    public static class ViewCreator
    {

        public static void GenerateViews()
        {

            ViewPresenter.Instance.ViewData.Views.Clear();
            ViewPresenter.Instance.ViewData.Views.AddRange(ViewPresenter.Instance.ViewData.ViewTypeDataList.Where(y => !y.HideInPresenter).Select(x => x.ViewTypeName).OrderBy(x => x));

            ViewPresenter.Instance.ViewData.Themes.Clear();
            ViewPresenter.Instance.ViewData.Themes.AddRange(ViewPresenter.Instance.ViewData.ThemeData.Select(x => x.ThemeName).OrderBy(x => x));

            foreach (var viewType in ViewPresenter.Instance.ViewData.ViewTypeDataList)
            {
                viewType.Dependencies.Clear();
                foreach (var dependencyName in viewType.DependencyNames)
                {
                    var dependency = ViewPresenter.Instance.ViewData.ViewTypeDataList.Where(x =>
                        x.ViewNameAliases.Contains(dependencyName)).FirstOrDefault();
                    if (dependency == null)
                    {
                        Debug.LogError(String.Format("[UReact] {0}: View contains the child view \"{1}\" that could not be found.", viewType.ViewTypeName, dependencyName));
                        continue;
                    }

                    viewType.Dependencies.Add(dependency);
                }
            }


            try
            {
                ViewPresenter.Instance.ViewData.ViewTypeDataList = SortByDependency(ViewPresenter.Instance.ViewData.ViewTypeDataList);
            }
            catch (Exception e)
            {
                Debug.LogError(String.Format("[UReact] Unable to generate views. {0}", e.Message));
                return;
            }

            if (ViewPresenter.Instance.transform.childCount > 0)
            {
                for (int i = ViewPresenter.Instance.transform.childCount - 1; i >= 0; --i)
                {
                    var go = ViewPresenter.Instance.transform.GetChild(i).gameObject;
                    GameObject.DestroyImmediate(go);
                }
            }

            var layoutRootGo = CreateLayoutRoot(ViewPresenter.Instance);
            ViewPresenter.Instance.LayoutRoot = layoutRootGo;
            if (ViewPresenter.Instance.RootView != null)
            {
                GameObject.DestroyImmediate(ViewPresenter.Instance.RootView);
            }

            if (!String.IsNullOrEmpty(ViewPresenter.Instance.MainView))
            {
                var mainView = CreateView(ViewPresenter.Instance.MainView, null);
                mainView.IsLayoutRoot = true;
                if (mainView != null)
                {
                    ViewPresenter.Instance.RootView = mainView.gameObject;
                }
            }
            if (ViewPresenter.Instance.RootView)
            {
                ViewPresenter.Instance.RootView.gameObject.transform.parent = layoutRootGo.transform;
                ViewPresenter.Instance.RootView.gameObject.transform.localPosition = Vector3.zero;
                ViewPresenter.Instance.RootView.gameObject.transform.localScale = Vector3.one;
                ViewPresenter.Instance.RootView.gameObject.transform.localScale = Vector3.one;
                ViewPresenter.Instance.UIPanelRoot = NGUITools.FindInParents<UIPanel>(ViewPresenter.Instance.RootView.gameObject).gameObject;

            }


            ViewPresenter.Instance.Initialize();
        }

        public static void LoadAllXml(IEnumerable<TextAsset> xmlAssets)
        {
            var viewPresenter = ViewPresenter.Instance;

            viewPresenter.Clear();

            foreach (var xmlAsset in xmlAssets)
            {
                LoadXml(xmlAsset);
            }

            GenerateViews();
        }


        public static void LoadXml(TextAsset xmlAsset)
        {
            LoadXml(xmlAsset.text, xmlAsset.name);
        }


        public static void LoadXml(string xml, string xmlAssetName = "")
        {
            XElement xmlElement = null;
            try
            {
                xmlElement = XElement.Parse(xml);
            }
            catch (Exception e)
            {
                Debug.LogError(String.Format("[UReact] {0}: Error parsing XML. Exception thrown: {1}", xmlAssetName, Utils.GetError(e)));
                return;
            }

            if (String.Equals(xmlElement.Name.LocalName, "Theme", StringComparison.OrdinalIgnoreCase))
            {
                LoadThemeXml(xmlElement, xml, xmlAssetName);
            }
            else
            {
                LoadViewXml(xmlElement, xml);
            }
        }

        private static void LoadViewXml(XElement xmlElement, string xml)
        {
            ViewPresenter.Instance.ViewData.ViewTypeDataList.RemoveAll(x => String.Equals(x.ViewTypeName, xmlElement.Name.LocalName, StringComparison.OrdinalIgnoreCase));

            var viewTypeData = new ViewTypeData();
            ViewPresenter.Instance.ViewData.ViewTypeDataList.Add(viewTypeData);

            viewTypeData.Xml = xml;
            viewTypeData.XmlElement = xmlElement;
            viewTypeData.ViewTypeName = xmlElement.Name.LocalName;
            viewTypeData.ViewNameAliases.Add(viewTypeData.ViewTypeName);

            foreach (var descendant in xmlElement.Descendants())
            {
                if (!viewTypeData.DependencyNames.Contains(descendant.Name.LocalName, StringComparer.OrdinalIgnoreCase))
                {
                    viewTypeData.DependencyNames.Add(descendant.Name.LocalName);
                }
            }

            // set view type
            var type = GetViewType(viewTypeData.ViewTypeName);
            if (type == null)
            {
                type = typeof(View);
            }

            // set if view is internal
            viewTypeData.HideInPresenter = type.GetCustomAttributes(typeof(HideInPresenter), false).Any();

            // set view action fields
            var viewActionType = typeof(ViewAction);
            var actionFields = type.GetFields().Where(x => x.FieldType == viewActionType).Select(y => y.Name);
            viewTypeData.ViewActionFields.AddRange(actionFields);

            // set dependency fields
            var viewFieldBaseType = typeof(ViewFieldBase);
            var dependencyFields = type.GetFields().Where(x => viewFieldBaseType.IsAssignableFrom(x.FieldType)).Select(y => y.Name);
            viewTypeData.DependencyFields.AddRange(dependencyFields);

            // set component fields
            var componentType = typeof(Component);
            var baseViewType = typeof(View);
            var componentFields = type.GetFields().Where(x => componentType.IsAssignableFrom(x.FieldType) &&
                !baseViewType.IsAssignableFrom(x.FieldType)).Select(y => y.Name);
            viewTypeData.ComponentFields.AddRange(componentFields);

            // set reference fields
            var referenceFields = type.GetFields().Where(x => baseViewType.IsAssignableFrom(x.FieldType) &&
                x.Name != "ComponentParentView" && x.Name != "ComponentParentView").Select(y => y.Name);
            viewTypeData.ReferenceFields.AddRange(referenceFields);

            // set excluded component fields
            var excludedComponentFields = type.GetCustomAttributes(typeof(ExcludeComponent), true);
            viewTypeData.ExcludedComponentFields.AddRange(excludedComponentFields.Select(x => (x as ExcludeComponent).ComponentFieldName));

            // set view-model to view mappings
            var viewNameAliases = type.GetCustomAttributes(typeof(ViewNameAlias), false);
            viewTypeData.ViewNameAliases.AddRange(viewNameAliases.Select(x => (x as ViewNameAlias).ViewAlias));

            // set view-model replacements
            var replacesViewModel = type.GetCustomAttributes(typeof(ReplacesViewModel), false).FirstOrDefault();
            if (replacesViewModel != null)
            {
                viewTypeData.ReplacesViewModel = (replacesViewModel as ReplacesViewModel).ViewTypeName;
            }

            // set mapped fields and their converters and change handlers
            var mapFields = type.GetFields().SelectMany(x => x.GetCustomAttributes(typeof(MapViewField), true));
            var mapClassFields = type.GetCustomAttributes(typeof(MapViewField), true);
            viewTypeData.MapViewFields.AddRange(mapFields.Select(x => (x as MapViewField).MapFieldData));
            viewTypeData.MapViewFields.AddRange(mapClassFields.Select(x => (x as MapViewField).MapFieldData));

            // .. add mapped dependency fields
            foreach (var field in type.GetFields())
            {
                var mapTo = field.GetCustomAttributes(typeof(MapTo), true).FirstOrDefault() as MapTo;
                if (mapTo == null)
                    continue;

                mapTo.MapFieldData.From = field.Name;
                viewTypeData.MapViewFields.Add(mapTo.MapFieldData);
            }

            // ... see if any dependency fields are remapped
            var remappedViewFields = type.GetCustomAttributes(typeof(RemappedField), true).Cast<RemappedField>();
            foreach (var remappedViewField in remappedViewFields)
            {
                // add or update mapping of old dependency field so it points to the new one
                var mappedDependencyFieldData = viewTypeData.MapViewFields.FirstOrDefault(x => String.Equals(x.From, remappedViewField.MapFieldData.From, StringComparison.OrdinalIgnoreCase));
                if (mappedDependencyFieldData != null)
                {
                    // update so it maps to the new dependency field                           
                    mappedDependencyFieldData.To = remappedViewField.MapFieldData.To;
                    mappedDependencyFieldData.ValueConverterType = remappedViewField.MapFieldData.ValueConverterType;
                    mappedDependencyFieldData.ValueConverterTypeSet = remappedViewField.MapFieldData.ValueConverterTypeSet;
                    mappedDependencyFieldData.ChangeHandlerName = remappedViewField.MapFieldData.ChangeHandlerName;
                    mappedDependencyFieldData.ChangeHandlerNameSet = remappedViewField.MapFieldData.ChangeHandlerNameSet;
                    mappedDependencyFieldData.TriggerChangeHandlerImmediately = remappedViewField.MapFieldData.TriggerChangeHandlerImmediately;
                }
                else
                {
                    // add new mapped value
                    viewTypeData.MapViewFields.Add(remappedViewField.MapFieldData);
                }
            }

            //  .. init change handlers and value converters
            foreach (var mapField in viewTypeData.MapViewFields)
            {
                if (mapField.ValueConverterTypeSet)
                {
                    viewTypeData.ViewFieldConverters.Add(new ViewFieldConverterData { ValueConverterType = mapField.ValueConverterType, ViewField = mapField.To });
                }

                if (mapField.ChangeHandlerNameSet)
                {
                    viewTypeData.ViewFieldChangeHandlers.Add(new ViewFieldChangeHandler
                    {
                        ChangeHandlerName = mapField.ChangeHandlerName,
                        ViewField = mapField.To,
                        TriggerImmediately = mapField.TriggerChangeHandlerImmediately
                    });
                }
            }

            // set view field converters and change handlers
            foreach (var field in type.GetFields())
            {
                var valueConverter = field.GetCustomAttributes(typeof(ValueConverter), true).FirstOrDefault();
                if (valueConverter != null)
                {
                    viewTypeData.ViewFieldConverters.Add(new ViewFieldConverterData { ViewField = field.Name, ValueConverterType = valueConverter.GetType().Name });
                }

                var changeHandler = field.GetCustomAttributes(typeof(ChangeHandler), true).FirstOrDefault() as ChangeHandler;
                if (changeHandler != null)
                {
                    viewTypeData.ViewFieldChangeHandlers.Add(new ViewFieldChangeHandler { ViewField = field.Name, ChangeHandlerName = changeHandler.ChangeHandlerName, TriggerImmediately = changeHandler.TriggerImmediately });
                }

                var notNotSetFromXml = field.GetCustomAttributes(typeof(NotSetFromXml), true).FirstOrDefault() as NotSetFromXml;
                if (notNotSetFromXml != null)
                {
                    viewTypeData.FieldsNotSetFromXml.Add(field.Name);
                }

                var genericViewField = field.GetCustomAttributes(typeof(GenericViewField), true).FirstOrDefault();
                if (genericViewField != null)
                {
                    viewTypeData.GenericViewFields.Add(field.Name);
                }

                // see if any component fields are replaced
                var replacedComponentField = field.GetCustomAttributes(typeof(ReplacesComponentField), true).FirstOrDefault() as ReplacesComponentField;
                if (replacedComponentField != null)
                {
                    // when a component field is replaced we need to update the mappings of other fields so they point to the new component
                    foreach (var mappedField in viewTypeData.MapViewFields)
                    {
                        int indexOf = mappedField.To.IndexOf(replacedComponentField.ReplacedComponentField);
                        if (indexOf != 0)
                            continue;

                        mappedField.To = mappedField.To.ReplaceFirst(replacedComponentField.ReplacedComponentField, field.Name);
                    }
                }

                // see if any dependency fields are replaced
                var replacedViewField = field.GetCustomAttributes(typeof(ReplacesDependencyField), true).FirstOrDefault() as ReplacesDependencyField;
                if (replacedViewField != null)
                {
                    // add or update mapping of old dependency field so it points to the new one
                    var mappedDependencyFieldData = viewTypeData.MapViewFields.FirstOrDefault(x => String.Equals(x.From, field.Name, StringComparison.OrdinalIgnoreCase));
                    if (mappedDependencyFieldData != null)
                    {
                        var oldMappedDependencyFieldData = viewTypeData.MapViewFields.FirstOrDefault(x => String.Equals(x.From, replacedViewField.ReplacedDependencyField, StringComparison.OrdinalIgnoreCase));
                        if (oldMappedDependencyFieldData != null)
                        {
                            // update so it maps to the new dependency field                           
                            oldMappedDependencyFieldData.To = mappedDependencyFieldData.To;
                            oldMappedDependencyFieldData.ValueConverterType = mappedDependencyFieldData.ValueConverterType;
                            oldMappedDependencyFieldData.ValueConverterTypeSet = mappedDependencyFieldData.ValueConverterTypeSet;
                            oldMappedDependencyFieldData.ChangeHandlerName = mappedDependencyFieldData.ChangeHandlerName;
                            oldMappedDependencyFieldData.ChangeHandlerNameSet = mappedDependencyFieldData.ChangeHandlerNameSet;
                            oldMappedDependencyFieldData.TriggerChangeHandlerImmediately = mappedDependencyFieldData.TriggerChangeHandlerImmediately;
                        }
                        else
                        {
                            // add new mapped value
                            var mapViewField = new MapViewFieldData();
                            mapViewField.From = replacedViewField.ReplacedDependencyField;
                            mapViewField.To = mappedDependencyFieldData.To;
                            mapViewField.ValueConverterType = mappedDependencyFieldData.ValueConverterType;
                            mapViewField.ValueConverterTypeSet = mappedDependencyFieldData.ValueConverterTypeSet;
                            mapViewField.ChangeHandlerName = mappedDependencyFieldData.ChangeHandlerName;
                            mapViewField.ChangeHandlerNameSet = mappedDependencyFieldData.ChangeHandlerNameSet;
                            mapViewField.TriggerChangeHandlerImmediately = mappedDependencyFieldData.TriggerChangeHandlerImmediately;
                            viewTypeData.MapViewFields.Add(mapViewField);
                        }
                    }
                }
            }

            // get the normal fields that aren't mapped
            var fields = type.GetFields().Where(x =>
                !viewTypeData.FieldsNotSetFromXml.Contains(x.Name) &&
                !viewTypeData.ReferenceFields.Contains(x.Name) &&
                !viewTypeData.ComponentFields.Contains(x.Name) &&
                !viewTypeData.ViewActionFields.Contains(x.Name) &&
                !viewTypeData.DependencyFields.Contains(x.Name) &&
                !x.IsStatic
            ).Select(y => y.Name);
            var properties = type.GetProperties().Where(x =>
                !viewTypeData.FieldsNotSetFromXml.Contains(x.Name) &&
                !viewTypeData.ReferenceFields.Contains(x.Name) &&
                !viewTypeData.ComponentFields.Contains(x.Name) &&
                !viewTypeData.ViewActionFields.Contains(x.Name) &&
                !viewTypeData.DependencyFields.Contains(x.Name) &&
#if !UNITY_WINRT || UNITY_WINRT_10_0 || UNITY_EDITOR
 x.GetSetMethod() != null &&
                x.GetGetMethod() != null &&
#endif
 x.Name != "enabled" &&
                x.Name != "useGUILayout" &&
                x.Name != "tag" &&
                x.Name != "hideFlags" &&
                x.Name != "Name"
            ).Select(y => y.Name);
            viewTypeData.ViewFields.AddRange(fields);
            viewTypeData.ViewFields.AddRange(properties);



        }

        /// <summary>
        /// Loads XML to theme database.
        /// </summary>
        private static void LoadThemeXml(XElement xmlElement, string xml, string xmlAssetName)
        {

            var themeNameAttr = xmlElement.Attribute("Name");
            if (themeNameAttr == null)
            {
                Debug.LogError(String.Format("[UReact] {0}: Error parsing theme XML. Name attribute missing.", xmlAssetName));
            }

            ViewPresenter.Instance.ViewData.ThemeData.RemoveAll(x => String.Equals(x.ThemeName, themeNameAttr.Value, StringComparison.OrdinalIgnoreCase));

            var themeData = new ThemeData();
            ViewPresenter.Instance.ViewData.ThemeData.Add(themeData);

            themeData.Xml = xml;
            themeData.XmlElement = xmlElement;
            themeData.ThemeName = themeNameAttr.Value;
            
            foreach (var childElement in xmlElement.Elements())
            {
                var themeElement = new ThemeElementData();
                themeElement.ViewName = childElement.Name.LocalName;

                var idAttr = childElement.Attribute("Id");
                if (idAttr != null)
                {
                    themeElement.Id = idAttr.Value;
                }

                var styleAttr = childElement.Attribute("Style");
                if (styleAttr != null)
                {
                    themeElement.Style = styleAttr.Value;
                }

                var basedOnAttr = childElement.Attribute("BasedOn");
                if (basedOnAttr != null)
                {
                    themeElement.BasedOn = basedOnAttr.Value;
                }

                themeElement.XmlElement = childElement;
                themeElement.Xml = childElement.ToString();

                themeData.ThemeElementData.Add(themeElement);
            }
        }

        public static View CreateView(string viewName,View parent, string theme = "", string id = "", string style = "", IEnumerable<XElement> contentXml = null, View ComponentView = null)
        {
            // Creates the views in the following order:
            // CreateView(view)
            //   Foreach child
            //     CreateView(child)
            //     SetViewValues(child)
            //   Foreach contentView
            //      CreateView(contentView)
            //      SetViewValues(contentView)
            //   SetViewValues(view)
            //   SetThemeValues(view)

            // use default theme if no theme is specified
            if (String.IsNullOrEmpty(theme))
            {
                theme = ViewPresenter.Instance.DefaultTheme;
            }

            // create view from XML
            var viewTypeData = GetViewTypeData(viewName);
            if (viewTypeData == null)
            {
                return null;
            }

            // get view type
            var viewType = GetViewType(viewTypeData.ViewTypeName);
            if (viewType == null)
            {
                viewType = typeof(View);
            }
            
            var go = new GameObject(viewTypeData.ViewTypeName);
            if(parent) go.transform.parent = parent.transform;

            var view = go.AddComponent(viewType) as View;
            if (parent)
            {
                view.ParentGo = parent.gameObject;
                view.ParentView = parent;
            }
            if (ComponentView)
            {
                view.ComponentParentGo = ComponentView.gameObject;
                view.ComponentParentView = ComponentView;
            }
            if (!parent && !ComponentView)
            {
                view.ComponentParentGo = view.gameObject;
                view.ComponentParentView = view;
            }
            view.Id = id;
            view.Style = style;
            view.Theme = theme;
            view.ViewXmlName = viewTypeData.ViewTypeName;
            view.ViewTypeData = viewTypeData;

            foreach (AddComponent addComponentAttribute in viewType.GetCustomAttributes(typeof(AddComponent), true))
            {
                var component = go.GetComponent(addComponentAttribute.ComponentType);
                if (component == null)
                {
                    go.AddComponent(addComponentAttribute.ComponentType);
                }
            }

            foreach (RemoveComponent removeComponentAttribute in viewType.GetCustomAttributes(typeof(RemoveComponent), true))
            {
                var component = go.GetComponent(removeComponentAttribute.ComponentType);
                if (component != null)
                {
                    GameObject.DestroyImmediate(component);
                }
            }

            foreach (var componentField in viewTypeData.ComponentFields)
            {
                if (viewTypeData.ExcludedComponentFields.Contains(componentField))
                    continue;

                var componentFieldInfo = viewType.GetField(componentField);
                Component component = null;
                if (componentField == "Transform")
                {
                    component = go.transform;
                }
                else
                {
                    component = go.AddComponent(componentFieldInfo.FieldType);
                }
                componentFieldInfo.SetValue(view, component);
            }


            UIEventListener ul = null;
            foreach (var viewActionField in viewTypeData.ViewActionFields)
            {
                var viewActionFieldInfo = viewTypeData.GetViewField(viewActionField);
                var a = new ViewAction(viewActionField);
                viewActionFieldInfo.SetValue(view, a);
                if (ul == null && a.TriggeredByEventSystem)
                {
                    ul = go.AddComponent<UIEventListener>();
                    go.AddComponent<BoxCollider>();
                }
            }

            foreach (var dependencyField in viewTypeData.DependencyFields)
            {
                var dependencyFieldInfo = viewTypeData.GetViewField(dependencyField);
                var dependencyFieldInstance = TypeHelper.CreateViewField(dependencyFieldInfo.FieldType);
                dependencyFieldInfo.SetValue(view, dependencyFieldInstance);
                dependencyFieldInstance.TargetView = view;
                dependencyFieldInstance.ViewFieldPath = dependencyField;
                dependencyFieldInstance.IsMapped = !String.Equals(viewTypeData.GetMappedViewField(dependencyField), dependencyField);
            }

            foreach (var childElement in viewTypeData.XmlElement.Elements())
            {

                var childViewIdAttr = childElement.Attribute("Id");
                var childViewStyleAttr = childElement.Attribute("Style");
                var childThemeAttr = childElement.Attribute("Theme");

                var childView = CreateView(childElement.Name.LocalName, view,
                    childThemeAttr != null ? childThemeAttr.Value : theme,
                    childViewIdAttr != null ? childViewIdAttr.Value : String.Empty,
                    GetChildViewStyle(view.Style, childViewStyleAttr),
                    childElement.Elements(), view);
                SetViewValues(childView, childElement, view);
            }

            if (contentXml != null)
            {
                foreach (var contentElement in contentXml)
                {
                    var contentElementIdAttr = contentElement.Attribute("Id");
                    var contentElementStyleAttr = contentElement.Attribute("Style");
                    var contentThemeAttr = contentElement.Attribute("Theme");

                    var contentView = CreateView(contentElement.Name.LocalName, view,
                        contentThemeAttr != null ? contentThemeAttr.Value : theme,
                        contentElementIdAttr != null ? contentElementIdAttr.Value : String.Empty,
                        GetChildViewStyle(view.Style, contentElementStyleAttr),
                        contentElement.Elements(), ComponentView);
                    SetViewValues(contentView, contentElement, ComponentView);
                }



            }

            foreach (var referenceField in viewTypeData.ReferenceFields)
            {
                // is this a reference to a view?
                //var referencedView = view.Find<View>(x =>
                //{
                //    var TempV = String.Equals(x.Id, referenceField, StringComparison.OrdinalIgnoreCase);
                //    _.Log(referenceField, "==", x.Id, "==", TempV);
                //    return TempV;
                //},true, view);

                View referencedView = null;

                go.ForEachChild<View>(x =>
                {
                    if (x.ComponentParentGo == go && String.Equals(x.Id, referenceField, StringComparison.OrdinalIgnoreCase))
                    {
                        referencedView = x;
                    }
                });

                if (referencedView != null)
                {
                    var referenceFieldInfo = viewType.GetField(referenceField);
                    var referencedViewType = referencedView.GetType();

                    if (!referenceFieldInfo.FieldType.IsAssignableFrom(referencedViewType))
                    {
                        Debug.LogError(String.Format("[UReact] {0}: Unable to set view reference field \"{1}\". Referenced type \"{2}\" cannot be converted to target type \"{3}\".", viewTypeData.ViewTypeName, referenceField, referencedViewType.FullName, referenceFieldInfo.FieldType.FullName));
                        continue;
                    }

                    referenceFieldInfo.SetValue(view, referencedView);
                }
            }

            view.SetDefaultValues();

            SetViewValues(view, viewTypeData.XmlElement, view);

            var themeData = GetThemeData(theme);
            if (themeData != null)
            {
                var themeElements = new List<ThemeElementData>();
                if (!String.IsNullOrEmpty(viewTypeData.ReplacesViewModel))
                {
                    themeElements.AddRange(themeData.GetThemeElementData(viewTypeData.ReplacesViewModel, view.Id, view.Style));
                }
                var data = themeData.GetThemeElementData(view.ViewTypeName, view.Id, view.Style);
                themeElements.AddRange(data);

                foreach (var themeElement in themeElements)
                {
                    SetViewValues(view, themeElement.XmlElement, ComponentView);
                }
            }

            return view;
        }

        private static GameObject CreateLayoutRoot(ViewPresenter viewPresenter)
        {
            var root = NGUITools.CreateUI(false);
            root.name = "LayoutRoot";
            root.transform.parent = viewPresenter.transform;
            root.transform.localPosition = Vector3.zero;
            root.transform.localRotation = Quaternion.identity;
            var panel = NGUITools.AddChild<UIPanel>(root.gameObject.FindChild("Camera"));
            root.gameObject.FindChild("Camera").camera.clearFlags = CameraClearFlags.Depth;
            panel.gameObject.name = "PanelRoot";
            return panel.gameObject;
        }

        private static string GetChildViewStyle(string parentStyle, XAttribute childViewStyleAttr)
        {
            var childStyleName = childViewStyleAttr != null ? childViewStyleAttr.Value : String.Empty;
            return childStyleName == "*" ? parentStyle : childStyleName;
        }

        private static void SetViewValues(View view, XElement xmlElement, View contentParentView)
        {
            if (view == null)
                return;

            var viewTypeData = GetViewTypeData(view.ViewTypeName);
            foreach (var attribute in xmlElement.Attributes())
            {
                string viewFieldPath = attribute.Name.LocalName;
                string viewFieldValue = attribute.Value;

                if (String.Equals(viewFieldPath, "xmlns", StringComparison.OrdinalIgnoreCase))
                    continue;

                bool notAllowed = viewTypeData.FieldsNotSetFromXml.Contains(viewFieldPath);
                if (notAllowed)
                {
                    Debug.LogError(String.Format("[UReact] {0}: Unable to assign value \"{1}\" to view field \"{2}.{3}\". Field not allowed to be set from XML.", view.name, viewFieldValue, view.ViewTypeName, viewFieldPath));
                    continue;
                }

                if (ViewFieldBinding.ValueHasBindings(viewFieldValue))
                {
                    view.AddBinding(viewFieldPath, viewFieldValue);
                    continue;
                }


                var viewFieldData = view.GetViewFieldData(viewFieldPath);
                if (viewFieldData == null)
                {
                    Debug.LogError(String.Format("[UReact] {0}: Unable to assign value \"{1}\" to view field \"{2}\". View field not found.", view.name, viewFieldValue, viewFieldPath));
                    continue;
                }

                if (viewFieldData.ViewFieldTypeName == "ViewAction")
                {
                    viewFieldData.SourceView.AddViewActionEntry(viewFieldData.ViewFieldPath, viewFieldValue, contentParentView);
                    continue;
                }

                view.SetValue(attribute.Name.LocalName, attribute.Value, true, null, true);
            }
        }

        public static ViewTypeData GetViewTypeData(string viewTypeName)
        {
            return ViewPresenter.Instance.ViewData.GetViewTypeData(viewTypeName);
        }

        /// <summary>
        /// Gets theme data.
        /// </summary>
        public static ThemeData GetThemeData(string themeName)
        {
            return ViewPresenter.Instance.ViewData.GetThemeData(themeName);
        }


        public static Type GetViewType(string viewTypeName)
        {
            return ViewPresenter.Instance.ViewData.GetViewType(viewTypeName);
        }

        public static ValueConverter GetValueConverterForType(string viewFieldType)
        {
            return ViewPresenter.Instance.ViewData.GetValueConverterForType(viewFieldType);
        }

        public static ValueConverter GetValueConverter(string valueConverterTypeName)
        {
            return ViewPresenter.Instance.ViewData.GetValueConverter(valueConverterTypeName);
        }

        public static ValueInterpolator GetValueInterpolatorForType(string viewFieldType)
        {
            return ViewPresenter.Instance.ViewData.GetValueInterpolatorForType(viewFieldType);
        }

        private static List<ViewTypeData> SortByDependency(List<ViewTypeData> viewTypeDataList)
        {
            viewTypeDataList.ForEach(x =>
            {
                x.PermanentMark = false;
                x.TemporaryMark = false;
            });

            var sorted = new List<ViewTypeData>();
            while (viewTypeDataList.Any(x => !x.PermanentMark))
            {
                var viewTypeData = viewTypeDataList.First(x => !x.PermanentMark);
                Visit(viewTypeData, sorted, String.Empty);
            }

            return sorted;
        }

        private static void Visit(ViewTypeData viewTypeData, List<ViewTypeData> sorted, string dependencyChain)
        {
            if (viewTypeData.TemporaryMark)
            {
                throw new Exception(String.Format("Cyclical dependency {0}{1} detected.", dependencyChain, viewTypeData.ViewTypeName));
            }
            else if (!viewTypeData.PermanentMark)
            {
                viewTypeData.TemporaryMark = true;
                foreach (var dependency in viewTypeData.Dependencies)
                {
                    Visit(dependency, sorted, String.Format("{0}{1}->", dependencyChain, viewTypeData.ViewTypeName));
                }
                viewTypeData.TemporaryMark = false;
                viewTypeData.PermanentMark = true;

                sorted.Add(viewTypeData);
            }
        }
    }
}

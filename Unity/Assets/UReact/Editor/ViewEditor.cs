﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using UnityEditor;
using UnityEngine;
#endregion

[CustomEditor(typeof(View))]
public class ViewEditor : UnityEditor.Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        View view = (View)target;

        if (GUILayout.Button("Update View"))
        {
            //view.UpdateViews();
        }
    }

}


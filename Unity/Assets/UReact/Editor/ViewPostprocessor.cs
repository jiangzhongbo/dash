﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using System.Xml.Linq;
using System.Reflection;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;
#endregion

namespace UReact.Editor
{

    internal class ViewPostprocessor : AssetPostprocessor
    {
        #region Methods


        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            if (Application.isPlaying || ViewPresenter.Instance == null || ViewPresenter.Instance.DisableAutomaticReload)
            {
                return;
            }

            var configuration = Configuration.Instance;
            bool viewAssetsUpdated = false;
            foreach (var path in importedAssets.Concat(deletedAssets).Concat(movedAssets).Concat(movedFromAssetPaths))
            {
                if (configuration.ViewPaths.Any(x => path.IndexOf(x, StringComparison.OrdinalIgnoreCase) >= 0) &&
                    path.IndexOf(".xml", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    viewAssetsUpdated = true;
                    break;
                }
            }

            if (!viewAssetsUpdated)
            {
                return; 
            }

            ProcessViewAssets();
            

        }

        public static void ProcessViewAssets()
        {

            if (Application.isPlaying || ViewPresenter.Instance == null)
            {
                return;
            }

            HashSet<TextAsset> viewAssets = new HashSet<TextAsset>();
            foreach (var path in Configuration.Instance.ViewPaths)
            {
                string localPath = path.StartsWith("Assets/") ? path.Substring(7) : path;
                foreach (var asset in GetXmlAssetsAtPath(localPath))
                {
                    viewAssets.Add(asset);
                }
            }

            ViewCreator.LoadAllXml(viewAssets);

            Utils.Log("[UReact] Views processed. {0}", DateTime.Now);
        }

        private static List<TextAsset> GetXmlAssetsAtPath(string path)
        {
            var assets = new List<TextAsset>();
            string searchPath = Application.dataPath + "/" + path;

            if (Directory.Exists(searchPath))
            {
                string[] fileEntries = Directory.GetFiles(searchPath, "*.xml", SearchOption.AllDirectories);
                foreach (string fileName in fileEntries)
                {
                    string localPath = "Assets/" + path + fileName.Substring(searchPath.Length);
                    var textAsset = AssetDatabase.LoadAssetAtPath(localPath, typeof(TextAsset)) as TextAsset;
                    if (textAsset != null)
                    {
                        assets.Add(textAsset);
                    }
                }
            }

            return assets;
        }

        /// <summary>
        /// Generates XSD schema from view type data.
        /// </summary>
        //public static void GenerateXsdSchema()
        //{
        //    if (ViewPresenter.Instance == null)
        //    {
        //        Debug.LogError("[UReact] Unable to generate XSD schema. View presenter can't be found in scene. Make sure the view presenter is enabled.");
        //        return;
        //    }

        //    var sb = new StringBuilder();
        //    sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        //    sb.AppendLine("<xs:schema id=\"UReact\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" targetNamespace=\"UReact\" xmlns=\"UReact\" attributeFormDefault=\"unqualified\" elementFormDefault=\"qualified\">");

        //    // create temporary root view where instantiate each view to get info about view fields
        //    if (ViewPresenter.Instance.RootView == null)
        //    {
        //        ViewPresenter.Instance.RootView = ViewData.CreateView<View>(ViewPresenter.Instance, ViewPresenter.Instance).gameObject;
        //    }
        //    var layoutRoot = ViewPresenter.Instance.RootView.GetComponent<View>();
        //    var temporaryRootView = ViewData.CreateView<View>(layoutRoot, layoutRoot);
        //    var enums = new HashSet<Type>();

        //    Utils.SuppressLogging = true;

        //    // generate XSD schema based on view type data
        //    foreach (var viewType in ViewPresenter.Instance.ViewTypeDataList)
        //    {
        //        sb.AppendLine();
        //        sb.AppendFormat("  <xs:element Name=\"{0}\" type=\"{0}\" />{1}", viewType.ViewTypeName, Environment.NewLine);
        //        sb.AppendFormat("  <xs:complexType Name=\"{0}\">{1}", viewType.ViewTypeName, Environment.NewLine);
        //        sb.AppendFormat("    <xs:sequence>{0}", Environment.NewLine);
        //        sb.AppendFormat("      <xs:any processContents=\"lax\" minOccurs=\"0\" maxOccurs=\"unbounded\" />{0}", Environment.NewLine);
        //        sb.AppendFormat("    </xs:sequence>{0}", Environment.NewLine);

        //        // instantiate view to get detailed information about each view field
        //        var view = ViewData.CreateView(viewType.ViewTypeName, temporaryRootView, temporaryRootView);
        //        view.InitializeViews();

        //        var viewFields = new List<string>(viewType.ViewFields);
        //        viewFields.AddRange(viewType.DependencyFields);
        //        viewFields.AddRange(viewType.MapViewFields.Select(x => x.From));
        //        viewFields.AddRange(viewType.ViewActionFields);
        //        viewFields = viewFields.Distinct().ToList();

        //        // create attributes
        //        foreach (var viewField in viewFields)
        //        {
        //            bool isEnum = false;
        //            var viewFieldData = view.GetViewFieldData(viewField);
        //            if (viewFieldData.ViewFieldType != null && viewFieldData.ViewFieldType.IsEnum)
        //            {
        //                isEnum = true;
        //                enums.Add(viewFieldData.ViewFieldType);
        //            }

        //            sb.AppendFormat("    <xs:attribute Name=\"{0}\" type=\"{1}\" />{2}", viewField, isEnum ? "Enum" + viewFieldData.ViewFieldTypeName : "xs:string", Environment.NewLine);
        //        }

        //        sb.AppendFormat("    <xs:anyAttribute processContents=\"skip\" />{0}", Environment.NewLine);

        //        sb.AppendFormat("  </xs:complexType>{0}", Environment.NewLine);
        //    }

        //    Utils.SuppressLogging = false;

        //    // destroy temporary root view
        //    GameObject.DestroyImmediate(temporaryRootView.gameObject);

        //    // add enums
        //    foreach (var enumType in enums)
        //    {
        //        sb.AppendLine();
        //        sb.AppendFormat("  <xs:simpleType Name=\"{0}\">{1}", "Enum" + enumType.Name, Environment.NewLine);
        //        sb.AppendFormat("    <xs:restriction base=\"xs:string\">{0}", Environment.NewLine);

        //        foreach (var enumTypeName in Enum.GetNames(enumType))
        //        {
        //            sb.AppendFormat("      <xs:enumeration value=\"{0}\" />{1}", enumTypeName, Environment.NewLine);
        //        }

        //        sb.AppendFormat("    </xs:restriction>{0}", Environment.NewLine);
        //        sb.AppendFormat("  </xs:simpleType>{0}", Environment.NewLine);
        //    }

        //    // add theme element
        //    sb.AppendLine();
        //    sb.AppendFormat("  <xs:element Name=\"{0}\" type=\"{0}\" />{1}", "Theme", Environment.NewLine);
        //    sb.AppendFormat("  <xs:complexType Name=\"{0}\">{1}", "Theme", Environment.NewLine);
        //    sb.AppendFormat("    <xs:sequence>{0}", Environment.NewLine);
        //    sb.AppendFormat("      <xs:any processContents=\"lax\" minOccurs=\"0\" maxOccurs=\"unbounded\" />{0}", Environment.NewLine);
        //    sb.AppendFormat("    </xs:sequence>{0}", Environment.NewLine);
        //    sb.AppendFormat("    <xs:attribute Name=\"{0}\" type=\"{1}\" />{2}", "BaseDirectory", "xs:string", Environment.NewLine);
        //    sb.AppendFormat("    <xs:attribute Name=\"{0}\" type=\"{1}\" />{2}", "Name", "xs:string", Environment.NewLine);
        //    sb.AppendFormat("    <xs:attribute Name=\"{0}\" type=\"{1}\" />{2}", "UnitSize", "xs:string", Environment.NewLine);
        //    sb.AppendFormat("  </xs:complexType>{0}", Environment.NewLine);

        //    // add resource dictionary element
        //    sb.AppendLine();
        //    sb.AppendFormat("  <xs:element Name=\"{0}\" type=\"{0}\" />{1}", "ResourceDictionary", Environment.NewLine);
        //    sb.AppendFormat("  <xs:complexType Name=\"{0}\">{1}", "ResourceDictionary", Environment.NewLine);
        //    sb.AppendFormat("    <xs:sequence minOccurs=\"0\" maxOccurs=\"unbounded\">{0}", Environment.NewLine);
        //    sb.AppendFormat("      <xs:element Name=\"Resource\" type=\"Resource\" minOccurs=\"0\" maxOccurs=\"unbounded\" />{0}", Environment.NewLine);
        //    sb.AppendFormat("      <xs:element Name=\"ResourceGroup\" type=\"ResourceGroup\" minOccurs=\"0\" maxOccurs=\"unbounded\" />{0}", Environment.NewLine);
        //    sb.AppendFormat("    </xs:sequence>{0}", Environment.NewLine);
        //    sb.AppendFormat("    <xs:attribute Name=\"{0}\" type=\"{1}\" />{2}", "Name", "xs:string", Environment.NewLine);
        //    sb.AppendFormat("  </xs:complexType>{0}", Environment.NewLine);

        //    // add resource element
        //    sb.AppendLine();
        //    sb.AppendFormat("  <xs:element Name=\"{0}\" type=\"{0}\" />{1}", "Resource", Environment.NewLine);
        //    sb.AppendFormat("  <xs:complexType Name=\"{0}\">{1}", "Resource", Environment.NewLine);
        //    sb.AppendFormat("    <xs:attribute Name=\"{0}\" type=\"{1}\" />{2}", "Key", "xs:string", Environment.NewLine);
        //    sb.AppendFormat("    <xs:attribute Name=\"{0}\" type=\"{1}\" />{2}", "Value", "xs:string", Environment.NewLine);
        //    sb.AppendFormat("    <xs:attribute Name=\"{0}\" type=\"{1}\" />{2}", "Language", "xs:string", Environment.NewLine);
        //    sb.AppendFormat("    <xs:attribute Name=\"{0}\" type=\"{1}\" />{2}", "Platform", "xs:string", Environment.NewLine);
        //    sb.AppendFormat("  </xs:complexType>{0}", Environment.NewLine);

        //    // add resource group element
        //    sb.AppendLine();
        //    sb.AppendFormat("  <xs:element Name=\"{0}\" type=\"{0}\" />{1}", "ResourceGroup", Environment.NewLine);
        //    sb.AppendFormat("  <xs:complexType Name=\"{0}\">{1}", "ResourceGroup", Environment.NewLine);
        //    sb.AppendFormat("    <xs:sequence minOccurs=\"0\" maxOccurs=\"unbounded\">{0}", Environment.NewLine);
        //    sb.AppendFormat("      <xs:element Name=\"Resource\" type=\"Resource\" minOccurs=\"0\" maxOccurs=\"unbounded\" />{0}", Environment.NewLine);
        //    sb.AppendFormat("      <xs:element Name=\"ResourceGroup\" type=\"ResourceGroup\" minOccurs=\"0\" maxOccurs=\"unbounded\" />{0}", Environment.NewLine);
        //    sb.AppendFormat("    </xs:sequence>{0}", Environment.NewLine);
        //    sb.AppendFormat("    <xs:attribute Name=\"{0}\" type=\"{1}\" />{2}", "Key", "xs:string", Environment.NewLine);
        //    sb.AppendFormat("    <xs:attribute Name=\"{0}\" type=\"{1}\" />{2}", "Value", "xs:string", Environment.NewLine);
        //    sb.AppendFormat("    <xs:attribute Name=\"{0}\" type=\"{1}\" />{2}", "Language", "xs:string", Environment.NewLine);
        //    sb.AppendFormat("    <xs:attribute Name=\"{0}\" type=\"{1}\" />{2}", "Platform", "xs:string", Environment.NewLine);
        //    sb.AppendFormat("  </xs:complexType>{0}", Environment.NewLine);

        //    sb.AppendLine("</xs:schema>");

        //    // save file
        //    var path = Configuration.Instance.SchemaFile;
        //    string localPath = path.StartsWith("Assets/") ? path.Substring(7) : path;
        //    File.WriteAllText(String.Format("{0}/{1}", Application.dataPath, localPath), sb.ToString());

        //    // print result
        //    Debug.Log(String.Format("[UReact] Schema generated at \"{0}\"", Configuration.Instance.SchemaFile));
        //}

        #endregion
    }
}


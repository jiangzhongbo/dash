﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using UnityEditor;
using UnityEngine;
using UReact;
using UReact.Editor;
#endregion

/// <summary>
/// Custom inspector for ViewPresenter components.
/// </summary>
[CustomEditor(typeof(ViewPresenter))]
public class ViewPresenterInspector : UnityEditor.Editor
{
    #region Methods

    /// <summary>
    /// Called when inspector GUI is to be rendered.
    /// </summary>
    public override void OnInspectorGUI()
    {
        //DrawDefaultInspector();

        var viewPresenter = target as ViewPresenter;
        // main view selection
        int selectedViewIndex = viewPresenter.ViewData.Views.IndexOf(viewPresenter.MainView) + 1;
        // .. add empty selection
        var mainViewOptions = new List<string>(viewPresenter.ViewData.Views);
        mainViewOptions.Insert(0, "-- none --");

        // .. add drop-down logic
        int newSelectedViewIndex = EditorGUILayout.Popup("Main View", selectedViewIndex, mainViewOptions.ToArray());
        viewPresenter.MainView = newSelectedViewIndex > 0 ? viewPresenter.ViewData.Views[newSelectedViewIndex - 1] : String.Empty;
        
        if (newSelectedViewIndex != selectedViewIndex)
        {
            // .. trigger reload on view presenter
            {
                ViewCreator.GenerateViews();
                //ViewPostprocessor.ProcessViewAssets();
            }
        }

        // default theme selection
        int selectedThemeIndex = viewPresenter.ViewData.Themes.IndexOf(viewPresenter.DefaultTheme) + 1;

        // .. add empty selection
        var themeOptions = new List<string>(viewPresenter.ViewData.Themes);
        themeOptions.Insert(0, "-- none --");

        // .. add drop-down logic            
        int newSelectedThemeIndex = EditorGUILayout.Popup("Default Theme", selectedThemeIndex, themeOptions.ToArray());
        viewPresenter.DefaultTheme = newSelectedThemeIndex > 0 ? viewPresenter.ViewData.Themes[newSelectedThemeIndex - 1] : String.Empty;
        if (newSelectedThemeIndex != selectedThemeIndex)
        {
            // .. trigger reload on view presenter
            {
                ViewCreator.GenerateViews();
                //ViewPostprocessor.ProcessViewAssets();
            }
        }


        viewPresenter.ElementSize = EditorGUILayout.FloatField("Element Size", viewPresenter.ElementSize);
        viewPresenter.ScaleSize = EditorGUILayout.Vector2Field("Scale Size", viewPresenter.ScaleSize);

        GUIContent reloadViewsContent = new GUIContent("Reload Views");
        if (GUILayout.Button(reloadViewsContent))
        {
            viewPresenter.GetComponentsInChildren<ViewBinding>().ToList().ForEach(e => e.Load());
            ViewPostprocessor.ProcessViewAssets();
        }
        viewPresenter.ViewData.ElementSize = viewPresenter.ElementSize;
        viewPresenter.ViewData.ScaleSize = viewPresenter.ScaleSize;
    }

    #endregion
}


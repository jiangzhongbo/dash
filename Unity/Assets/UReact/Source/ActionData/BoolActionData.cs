﻿using UnityEngine;
using System.Collections;

namespace UReact.Views
{
    public class BoolActionData : ActionData
    {
        public GameObject go;
        public bool state;
    }
}
﻿#region Using Statements
using UReact.Views;
using System;
#endregion

namespace UReact.Views
{
    public class EnabledChangedActionData : ActionData
    {
        public bool Enabled;
    }
}

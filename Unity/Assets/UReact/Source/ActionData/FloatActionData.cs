﻿using UnityEngine;
using System.Collections;

namespace UReact.Views
{
    public class FloatActionData : ActionData
    {
        public GameObject go;
        public float delta;
    }
}
﻿using UnityEngine;
using System.Collections;

namespace UReact.Views
{
    public class GameObjectActionData : ActionData
    {
        public GameObject go;
        public GameObject draggedObject;
    }
}
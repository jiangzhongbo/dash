﻿using UnityEngine;
using System.Collections;

namespace UReact.Views
{
    public class KeyCodeActionData : ActionData
    {
        public GameObject go;
        public KeyCode key;
    }
}
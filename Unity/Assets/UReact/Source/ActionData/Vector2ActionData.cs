﻿using UnityEngine;
using System.Collections;

namespace UReact.Views
{
    public class Vector2ActionData : ActionData
    {
        public GameObject go;
        public Vector2 delta;
    }
}
﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using UnityEngine;
#endregion

namespace UReact.Animation
{
    public class ValueInterpolator
    {
        #region Fields

        protected Type type;

        #endregion

        #region Constructor

        public ValueInterpolator()
        {
            type = null;
        }

        #endregion

        #region Methods

        public virtual object Interpolate(object from, object to, float weight)
        {
            return weight < 1f ? from : to;
        }

        public static float Lerp(float from, float to, float weight)
        {
            return (1f - weight) * from + weight * to;
        }

        #endregion

        #region Properties

        public Type Type
        {
            get
            {
                return type;
            }
        }

        #endregion        
    }
}

﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using UnityEngine;
#endregion

namespace UReact.Animation
{

    public class BoolValueInterpolator : ValueInterpolator
    {
        #region Constructor

        public BoolValueInterpolator()
        {
            type = typeof(bool);
        }

        #endregion

        #region Methods

        public override object Interpolate(object from, object to, float weight)
        {
            return weight < 1f ? from : to;
        }

        #endregion
    }
}

﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using UnityEngine;
#endregion

namespace UReact.Animation
{
    public class ElementSizeValueInterpolator : ValueInterpolator
    {
        #region Constructor
        public ElementSizeValueInterpolator()
        {
            type = typeof(ElementSize);
        }

        #endregion

        #region Methods
        public override object Interpolate(object from, object to, float weight)
        {
            ElementSize a = (ElementSize)from;
            ElementSize b = (ElementSize)to;

            ElementSize result = ElementSize.GetPercents(Lerp(a.Value, b.Value, weight));

            return result;

        }

        #endregion
    }
}
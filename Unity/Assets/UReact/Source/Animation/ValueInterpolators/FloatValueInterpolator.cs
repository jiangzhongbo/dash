﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using UnityEngine;
#endregion

namespace UReact.Animation
{

    public class FloatValueInterpolator : ValueInterpolator
    {
        #region Constructor

        public FloatValueInterpolator()
        {
            type = typeof(float);
        }

        #endregion

        #region Methods

        public override object Interpolate(object from, object to, float weight)
        {
            return Lerp((float)from, (float)to, weight);
        }

        #endregion
    }
}

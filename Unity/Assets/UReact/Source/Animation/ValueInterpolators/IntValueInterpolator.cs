﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using UnityEngine;
#endregion

namespace UReact.Animation
{

    public class IntValueInterpolator : ValueInterpolator
    {
        #region Constructor

        public IntValueInterpolator()
        {
            type = typeof(int);
        }

        #endregion

        #region Methods

        public override object Interpolate(object from, object to, float weight)
        {
            return Lerp((int)from, (int)to, weight);
        }

        #endregion
    }
}

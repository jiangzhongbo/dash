﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using UnityEngine;
#endregion

namespace UReact.Animation
{
    public class Vector2ValueInterpolator : ValueInterpolator
    {
        #region Constructor
        public Vector2ValueInterpolator()
        {
            type = typeof(Vector2);
        }

        #endregion

        #region Methods

        public override object Interpolate(object from, object to, float weight)
        {
            Vector2 v1 = (Vector2)from;
            Vector2 v2 = (Vector2)to;

            return new Vector2(
                Lerp(v1.x, v2.x, weight),
                Lerp(v1.y, v2.y, weight));
        }

        #endregion
    }
}

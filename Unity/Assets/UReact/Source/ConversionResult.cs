﻿#region Using Statements
using System;
#endregion

namespace UReact
{
    public class ConversionResult
    {
        #region Fields
                
        private object convertedObject;
        private bool success;
        private string errorMessage;

        #endregion

        #region Constructor

        public ConversionResult()
        {
            success = true;
        }

        public ConversionResult(object convertedObject)
        {
            this.convertedObject = convertedObject;
            success = true;
        }

        #endregion

        #region Properties

        public object ConvertedObject
        {
            get 
            {
                return convertedObject;
            }
            set 
            {
                convertedObject = value;
            }
        }

        public bool Success
        {
            get 
            {
                return success;
            }
            set 
            {
                success = value;
            }
        }

        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                errorMessage = value;
            }
        }

        #endregion
    }
}

﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
#endregion
using UReact.ValueConverters;
namespace UReact
{
    /// <summary>
    /// 统一px,pm,百分比
    /// </summary>
    [Serializable]
    public class ElementSize
    {
        #region Fields

        [SerializeField]
        private float value;

        [SerializeField]
        private ElementSizeUnit unit;

        [SerializeField]
        private bool fill;

        #endregion

        #region Constructor

        public ElementSize()
        {
            value = 0f;
            unit = ElementSizeUnit.Pixels;
        }

        public ElementSize(float value, ElementSizeUnit unit)
        {
            this.value = value;
            this.unit = unit;
        }

        public ElementSize(ElementSize elementSize)
        {
            value = elementSize.Value;
            unit = elementSize.Unit;
            fill = elementSize.Fill;
        }

        #endregion

        #region Methods

        public static ElementSize GetPixels(float pixels)
        {
            return new ElementSize(pixels, ElementSizeUnit.Pixels);
        }

        public static ElementSize GetElements(float elements)
        {
            return new ElementSize(elements, ElementSizeUnit.Elements);
        }

        public static ElementSize GetPercents(float percents)
        {
            return new ElementSize(percents, ElementSizeUnit.Percents);
        }

        public static float ElementsToPixels(float elements)
        {
            return ViewPresenter.Instance.ViewData.ElementSize * elements;
        }

        public static float PixelsToElements(float pixels)
        {
            return pixels / ViewPresenter.Instance.ViewData.ElementSize;
        }

        public static ElementSize Parse(string value)
        {
            ElementSize elementSize = new ElementSize();
            string trimmedValue = value.Trim();
            if (trimmedValue == "*")
            {
                elementSize.Value = 1;
                elementSize.Unit = ElementSizeUnit.Percents;
                elementSize.Fill = true;
            }
            else if (trimmedValue.EndsWith("em", StringComparison.OrdinalIgnoreCase))
            {
                int lastIndex = trimmedValue.LastIndexOf("em", StringComparison.OrdinalIgnoreCase);
                elementSize.Value = System.Convert.ToSingle(trimmedValue.Substring(0, lastIndex), CultureInfo.InvariantCulture);
                elementSize.Unit = ElementSizeUnit.Elements;
            }
            else if (trimmedValue.EndsWith("%"))
            {
                int lastIndex = trimmedValue.LastIndexOf("%", StringComparison.OrdinalIgnoreCase);
                elementSize.Value = System.Convert.ToSingle(trimmedValue.Substring(0, lastIndex), CultureInfo.InvariantCulture) / 100.0f;
                elementSize.Unit = ElementSizeUnit.Percents;
            }
            else if (trimmedValue.EndsWith("px"))
            {
                int lastIndex = trimmedValue.LastIndexOf("px", StringComparison.OrdinalIgnoreCase);
                elementSize.Value = System.Convert.ToSingle(trimmedValue.Substring(0, lastIndex), CultureInfo.InvariantCulture);
                elementSize.Unit = ElementSizeUnit.Pixels;
            }
            else if (trimmedValue.EndsWith("sc"))
            {
                int lastIndex = trimmedValue.LastIndexOf("sc", StringComparison.OrdinalIgnoreCase);
                elementSize.Value = System.Convert.ToSingle(trimmedValue.Substring(0, lastIndex), CultureInfo.InvariantCulture);
                elementSize.Unit = ElementSizeUnit.Scale;
            }
            else if (trimmedValue.EndsWith("sh"))
            {
                int lastIndex = trimmedValue.LastIndexOf("sh", StringComparison.OrdinalIgnoreCase);
                elementSize.Value = System.Convert.ToSingle(trimmedValue.Substring(0, lastIndex), CultureInfo.InvariantCulture);
                elementSize.Unit = ElementSizeUnit.ScaleBasedHeight;
            }
            else
            {
                elementSize.Value = System.Convert.ToSingle(trimmedValue, CultureInfo.InvariantCulture);
                elementSize.Unit = ElementSizeUnit.Pixels;
            }

            return elementSize;
        }

        #endregion

        #region Properties

        public float Value
        {
            get
            {
                return value;
            }
            set
            {
                this.value = value;
            }
        }

        public float PercentOfOffsetBasedParent(bool isWidth, View layoutParentView, UIAnchor.Side side)
        {
            if (unit == ElementSizeUnit.Percents)
            {
                return value;
            }
            else if (unit == ElementSizeUnit.Scale)
            {
                if (layoutParentView)
                {
                    if (isWidth)
                    {
                        float x = (value / ViewPresenter.Instance.ViewData.ScaleSize.x * Screen.width - layoutParentView.ActualOffset(UIAnchor.Side.TopLeft).x + layoutParentView.ActualWidth / 2) / layoutParentView.ActualWidth;
                        return UIAnchorSideValueConverter.Convert(new Vector2(x, 0), UIAnchor.Side.TopLeft, side).x;
                    }
                    else
                    {
                        float y = (value / ViewPresenter.Instance.ViewData.ScaleSize.y * Screen.height - (layoutParentView.ActualOffset(UIAnchor.Side.TopLeft).y + layoutParentView.ActualHeight / 2)) / layoutParentView.ActualHeight;
                        return UIAnchorSideValueConverter.Convert(new Vector2(0, y), UIAnchor.Side.TopLeft, side).y;
                    }
                }
                else
                {
                    if (isWidth)
                    {
                        return value / ViewPresenter.Instance.ViewData.ScaleSize.x;
                    }
                    else
                    {
                        return value / ViewPresenter.Instance.ViewData.ScaleSize.y;
                    }
                }



            }
            else if (unit == ElementSizeUnit.ScaleBasedHeight)
            {
                if (layoutParentView)
                {
                    float y = (value / ViewPresenter.Instance.ViewData.ScaleSize.y * Screen.height - (layoutParentView.ActualOffset(UIAnchor.Side.TopLeft).y + layoutParentView.ActualHeight / 2)) / layoutParentView.ActualHeight;
                    return UIAnchorSideValueConverter.Convert(new Vector2(0, y), UIAnchor.Side.TopLeft, side).y;
                }
                else
                {
                    return value / ViewPresenter.Instance.ViewData.ScaleSize.y;
                }

            }
            return 0f;
        }

        public float PercentOfSizeBasedParent(bool isWidth, View layoutParentView)
        {
            if (unit == ElementSizeUnit.Percents)
            {
                return value;
            }
            else if (unit == ElementSizeUnit.Scale)
            {
                if (layoutParentView)
                {
                    if (isWidth)
                    {
                        return value / ViewPresenter.Instance.ViewData.ScaleSize.x * Screen.width / layoutParentView.ActualWidth;
                    }
                    else
                    {
                        return value / ViewPresenter.Instance.ViewData.ScaleSize.y * Screen.height / layoutParentView.ActualHeight;
                    }
                }
                else
                {
                    if (isWidth)
                    {
                        return value / ViewPresenter.Instance.ViewData.ScaleSize.x;
                    }
                    else
                    {
                        return value / ViewPresenter.Instance.ViewData.ScaleSize.y;
                    }
                }



            }
            else if (unit == ElementSizeUnit.ScaleBasedHeight)
            {
                if (layoutParentView)
                {
                    return value / ViewPresenter.Instance.ViewData.ScaleSize.y * Screen.height / layoutParentView.ActualHeight;
                }
                else
                {
                    return value / ViewPresenter.Instance.ViewData.ScaleSize.y;
                }

            }
            return 0f;
        }

        public float Percent(bool isWidth = true)
        {
                if (unit == ElementSizeUnit.Percents)
                {
                    return value;
                }
                else if(unit == ElementSizeUnit.Scale)
                {
                    {
                        if (isWidth)
                        {
                            return value / ViewPresenter.Instance.ViewData.ScaleSize.x;
                        }
                        else
                        {
                            return value / ViewPresenter.Instance.ViewData.ScaleSize.y;
                        }
                    }



                }
                else if (unit == ElementSizeUnit.ScaleBasedHeight)
                {
                    return value / ViewPresenter.Instance.ViewData.ScaleSize.y;                   
                }
                return 0f;
        }

        public ElementSizeUnit Unit
        {
            get
            {
                return unit;
            }
            set
            {
                unit = value;
            }
        }

        public bool Fill
        {
            get
            {
                return fill;
            }
            set
            {
                fill = value;
            }
        }

        #endregion
    }
}

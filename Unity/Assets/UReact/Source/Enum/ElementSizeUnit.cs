﻿#region Using Statements
using System;
#endregion

namespace UReact
{
    public enum ElementSizeUnit
    {
        Pixels = 0,
        Percents = 1,
        Elements = 2,
        Scale = 3,
        ScaleBasedHeight = 4,
    }
}

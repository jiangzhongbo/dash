﻿#region Using Statements
using System;
#endregion

namespace UReact
{
    public enum SearchAlgorithm
    {
        Default = 0,
        DepthFirst = 0,
        BreadthFirst = 1,
        ReverseDepthFirst = 2,
        ReverseBreadthFirst = 3
    }
}

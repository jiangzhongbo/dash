﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
public class StaticData : ScriptableObject
{
    #if UNITY_EDITOR
    public List<KeyValue> KV;
    private static StaticData instance;

    private static Dictionary<string, StaticData> cache = new Dictionary<string, StaticData>();

    public static StaticData Fetch(string name)
    {
        if (!cache.ContainsKey(name) || !cache[name])
        {
            StaticData staticData = AssetDatabase.LoadAssetAtPath("Assets/_UReactStaticData/" + name + ".asset", typeof(StaticData)) as StaticData;
            if (staticData == null)
            {
                System.IO.Directory.CreateDirectory("Assets/_UReactStaticData/");
                staticData = ScriptableObject.CreateInstance<StaticData>();
                AssetDatabase.CreateAsset(staticData, "Assets/_UReactStaticData/" + name + ".asset");
                AssetDatabase.Refresh();
            }
            cache[name] = staticData;
        }
        return cache[name];
    }

    public bool Have(string key)
    {
        return KV.Where(e => e.Key == key).Count() > 0;
    }

    public KeyValue Get(string key)
    {
        return KV.Where(e => e.Key == key).First();
    }

    public void Set(string key, string value)
    {
        if (Have(key))
        {
            Get(key).Value = value;
        }
        else
        {
            KV.Add(new KeyValue() { Key = key, Value = value});
        }
    }
    public void Set(string key, object value)
    {
        if (Have(key))
        {
            Get(key).Value = value.ToString();
        }
        else
        {
            KV.Add(new KeyValue() { Key = key, Value = value.ToString() });
        }
    }
#endif
}

[Serializable]
public class KeyValue
{
    public string Key;
    public string Value;
}

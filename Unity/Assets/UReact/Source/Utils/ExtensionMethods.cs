﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Reflection;
#endregion

namespace UReact
{
    public static class ExtensionMethods
    {

        /// <summary>
        /// Clamps a value to specified range [min, max].
        /// </summary>
        public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0) return min;
            else if (val.CompareTo(max) > 0) return max;
            else return val;
        }

        public static GameObject GetLayoutRoot(this GameObject gameObject)
        {
            var viewComponent = gameObject.GetComponent<View>();
            if (viewComponent != null && viewComponent.IsLayoutRoot)
            {
                return gameObject;
            }

            var parent = gameObject.transform.parent;
            if (parent == null)
                return null;

            return parent.gameObject.GetLayoutRoot();
        }

        public static View GetLayoutRoot(this View view)
        {
            var go = view.gameObject.GetLayoutRoot();
            return go != null ? go.GetComponent<View>() : null;
        }

        public static View FindView(this View view, string id, bool recursive = true, View parent = null)
        {
            return view.FindView<View>(id, recursive, parent);
        }

        public static T FindView<T>(this View view, string id, bool recursive = true, View parent = null) where T : View
        {
            var go = view.gameObject.FindView<T>(id, recursive, parent);
            return go != null ? go.GetComponent<T>() : null;
        }

        public static GameObject FindView(this GameObject gameObject, string id, bool recursive = true, View parent = null)
        {
            return gameObject.FindView<View>(id, recursive, parent);
        }

        public static GameObject FindView<T>(this GameObject gameObject, string id, bool recursive = true, View parent = null) where T : View
        {
            return gameObject.FindView<T>(id, true, recursive, parent);
        }

        public static GameObject FindView<T>(this GameObject gameObject, bool recursive, View parent = null) where T : View
        {
            return gameObject.FindView<T>(null, false, recursive, parent);
        }

        public static GameObject FindView<T>(this GameObject gameObject, string id, bool filterById, bool recursive, View parent) where T : View
        {
            foreach (Transform child in gameObject.transform)
            {
                var viewComponent = child.GetComponent<T>();
                if (viewComponent != null)
                {
                    if ((!filterById || String.Equals(id, viewComponent.Id, StringComparison.OrdinalIgnoreCase)) &&
                        (parent == null || viewComponent.ComponentParentGo == parent.gameObject))
                    {
                        return child.gameObject;
                    }
                }

                if (recursive)
                {
                    var result = child.gameObject.FindView<T>(id, recursive, parent);
                    if (result != null)
                    {
                        return result;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Traverses the view object tree and performs an action on each child until the action returns false.
        /// </summary>
        public static void DoUntil<T>(this View view, Func<T, bool> action, bool recursive = true, View parent = null, SearchAlgorithm traversalAlgorithm = SearchAlgorithm.DepthFirst) where T : View
        {
            switch (traversalAlgorithm)
            {
                default:
                case SearchAlgorithm.DepthFirst:
                    foreach (Transform child in view.gameObject.transform)
                    {
                        bool skipChild = false;
                        var childView = child.GetComponent<View>();
                        if (childView == null)
                        {
                            continue;
                        }

                        if (parent != null)
                        {
                            if (childView.ParentView != parent)
                                skipChild = true;
                        }
                        if (!skipChild)
                        {
                            var component = child.GetComponent<T>();
                            if (component != null)
                            {
                                var result = action(component);
                                if (!result)
                                {
                                    // done traversing
                                    return;
                                }
                            }
                        }

                        if (recursive)
                        {
                            childView.DoUntil<T>(action, recursive, parent, traversalAlgorithm);
                        }
                    }
                    break;

                case SearchAlgorithm.BreadthFirst:
                    Queue<View> queue = new Queue<View>();
                    foreach (Transform child in view.gameObject.transform)
                    {
                        bool skipChild = false;
                        var childView = child.GetComponent<View>();
                        if (childView == null)
                        {
                            continue;
                        }

                        if (parent != null)
                        {
                            if (childView.ParentGo != parent.gameObject)
                                skipChild = true;
                        }

                        if (!skipChild)
                        {
                            var component = child.GetComponent<T>();
                            if (component != null)
                            {
                                var result = action(component);
                                if (!result)
                                {
                                    // done traversing
                                    return;
                                }
                            }
                        }

                        if (recursive)
                        {
                            // add children to queue
                            queue.Enqueue(childView);
                        }
                    }

                    foreach (var queuedView in queue)
                    {
                        queuedView.DoUntil<T>(action, recursive, parent, traversalAlgorithm);
                    }
                    break;

                case SearchAlgorithm.ReverseDepthFirst:
                    foreach (Transform child in view.gameObject.transform)
                    {
                        var childView = child.GetComponent<View>();
                        if (childView == null)
                        {
                            continue;
                        }

                        if (recursive)
                        {
                            childView.DoUntil<T>(action, recursive, parent, traversalAlgorithm);
                        }

                        if (parent != null)
                        {
                            if (childView.ParentGo != parent.gameObject)
                                continue;
                        }

                        var component = child.GetComponent<T>();
                        if (component != null)
                        {
                            var result = action(component);
                            if (!result)
                            {
                                // done traversing
                                return;
                            }
                        }
                    }
                    break;

                case SearchAlgorithm.ReverseBreadthFirst:
                    Stack<T> componentStack = new Stack<T>();
                    Stack<View> childStack = new Stack<View>();
                    foreach (Transform child in view.gameObject.transform)
                    {
                        var childView = child.GetComponent<View>();
                        if (childView == null)
                        {
                            continue;
                        }

                        if (recursive)
                        {
                            childStack.Push(childView);
                        }

                        if (parent != null)
                        {
                            if (childView.ParentGo != parent.gameObject)
                                continue;
                        }

                        var component = child.GetComponent<T>();
                        if (component != null)
                        {
                            componentStack.Push(component);
                        }
                    }

                    foreach (var childStackView in childStack)
                    {
                        childStackView.DoUntil<T>(action, recursive, parent, traversalAlgorithm);
                    }

                    foreach (T component in componentStack)
                    {
                        var result = action(component);
                        if (!result)
                        {
                            // done traversing
                            return;
                        }
                    }

                    break;
            }
        }

        public static void ForEachChild<T>(this View view, Action<T> action, bool recursive = true, View parent = null, SearchAlgorithm searchAlgorithm = SearchAlgorithm.DepthFirst) where T : View
        {
            view.gameObject.ForEachChild<T>(action, recursive, parent, searchAlgorithm);
        }


        /// <summary>
        /// Performs an action on view children of a game object.
        /// </summary>
        /// <typeparam Name="T">Types of views to do action on.</typeparam>
        /// <param Name="gameObject">Root game object.</param>
        /// <param Name="action">Action to perform.</param>
        /// <param Name="recursive">Boolean indicating if children of children should be traversed.</param>
        /// <param Name="contentChild">Boolean indicating if views must be content children (i.e. appearing as content in the view).</param>
        /// <param Name="searchAlgorithm">Search algorithm to use.</param>
        public static void ForEachChild<T>(this GameObject gameObject, Action<T> action, bool recursive = true, View parent = null, SearchAlgorithm searchAlgorithm = SearchAlgorithm.DepthFirst) where T : View
        {
            switch (searchAlgorithm)
            {
                default:
                case SearchAlgorithm.DepthFirst:
                    foreach (Transform child in gameObject.transform)
                    {
                        bool skipChild = false;
                        if (parent != null)
                        {
                            var view = child.GetComponent<View>();
                            if (view.ComponentParentGo != parent.gameObject)
                                skipChild = true;
                        }

                        if (!skipChild)
                        {
                            var component = child.GetComponent<T>();
                            if (component != null)
                            {
                                action(component);
                            }
                        }

                        if (recursive)
                        {
                            child.gameObject.ForEachChild(action, recursive);
                        }
                    }
                    break;

                case SearchAlgorithm.BreadthFirst:
                    Queue<GameObject> queue = new Queue<GameObject>();
                    foreach (Transform child in gameObject.transform)
                    {
                        bool skipChild = false;
                        if (parent != null)
                        {
                            var view = child.GetComponent<View>();
                            if (view.ComponentParentGo != parent.gameObject)
                                skipChild = true;
                        }

                        if (!skipChild)
                        {
                            var component = child.GetComponent<T>();
                            if (component != null)
                            {
                                action(component);
                            }
                        }

                        if (recursive)
                        {
                            queue.Enqueue(child.gameObject);
                        }
                    }

                    foreach (GameObject go in queue)
                    {
                        go.ForEachChild(action, recursive, parent, searchAlgorithm);
                    }
                    break;

                case SearchAlgorithm.ReverseDepthFirst:
                    foreach (Transform child in gameObject.transform)
                    {
                        if (recursive)
                        {
                            child.gameObject.ForEachChild(action, recursive);
                        }

                        if (parent != null)
                        {
                            var view = child.GetComponent<View>();
                            if (view.ComponentParentGo != parent.gameObject)
                                continue;
                        }

                        var component = child.GetComponent<T>();
                        if (component != null)
                        {
                            action(component);
                        }
                    }
                    break;

                case SearchAlgorithm.ReverseBreadthFirst:
                    Stack<T> componentStack = new Stack<T>();
                    Stack<GameObject> childStack = new Stack<GameObject>();
                    foreach (Transform child in gameObject.transform)
                    {
                        if (recursive)
                        {
                            childStack.Push(child.gameObject);
                        }

                        if (parent != null)
                        {
                            var view = child.GetComponent<View>();
                            if (view.ComponentParentGo != parent.gameObject)
                                continue;
                        }

                        var component = child.GetComponent<T>();
                        if (component != null)
                        {
                            componentStack.Push(component);
                        }
                    }

                    foreach (var childObject in childStack)
                    {
                        childObject.ForEachChild(action, recursive, parent, searchAlgorithm);
                    }

                    foreach (T component in componentStack)
                    {
                        action(component);
                    }

                    break;
            }
        }

        public static void ForEachParent<T>(this GameObject gameObject, Action<T> action, bool recursive = true) where T : UnityEngine.Component
        {
            var parent = gameObject.transform.parent;
            if (parent != null)
            {
                var component = parent.GetComponent<T>();
                if (component != null)
                {
                    action(component);
                }

                if (recursive)
                {
                    parent.gameObject.ForEachParent(action, recursive);
                }
            }
        }

        /// <summary>
        /// Gets value from a view field.
        /// </summary>
        public static object GetFieldValue(this MemberInfo memberInfo, object typeObject)
        {
            var fieldInfo = memberInfo as FieldInfo;
            if (fieldInfo != null)
                return fieldInfo.GetValue(typeObject);

            var propertyInfo = memberInfo as PropertyInfo;
            return propertyInfo.GetValue(typeObject, null);
        }

        /// <summary>
        /// Sets view field value.
        /// </summary>
        public static void SetFieldValue(this MemberInfo memberInfo, object typeObject, object value)
        {
            var fieldInfo = memberInfo as FieldInfo;
            if (fieldInfo != null)
            {
                fieldInfo.SetValue(typeObject, value);
                return;
            }

            var propertyInfo = memberInfo as PropertyInfo;
            if (propertyInfo != null)
            {
                propertyInfo.SetValue(typeObject, value, null);
            }
        }

        /// <summary>
        /// Traverses the view object tree and returns the first view that matches the predicate.
        /// </summary>
        public static T Find<T>(this View view, Predicate<T> predicate, bool recursive = true, View parent = null, SearchAlgorithm traversalAlgorithm = SearchAlgorithm.DepthFirst) where T : View
        {
            T result = null;
            view.DoUntil<T>(x =>
            {
                if (predicate(x))
                {
                    result = x;
                    return false;
                }
                return true;
            }, recursive, parent, traversalAlgorithm);
            return result;
        }

        /// <summary>
        /// Returns first view of _type T found.
        /// </summary>
        public static T Find<T>(this View view, bool recursive = true, View parent = null, SearchAlgorithm traversalAlgorithm = SearchAlgorithm.DepthFirst) where T : View
        {
            return view.Find<T>(x => true, recursive, parent, traversalAlgorithm);
        }

        /// <summary>
        /// Returns first view of _type T found.
        /// </summary>
        public static T Find<T>(this GameObject gameObject, Predicate<T> predicate, bool recursive = true, View parent = null, SearchAlgorithm traversalAlgorithm = SearchAlgorithm.DepthFirst) where T : View
        {
            var view = gameObject.GetComponent<View>();
            if (view == null)
            {
                return null;
            }

            return view.Find<T>(predicate, recursive, parent, traversalAlgorithm);
        }

        /// <summary>
        /// Returns first view of _type T found.
        /// </summary>
        public static T Find<T>(this GameObject gameObject, bool recursive = true, View parent = null, SearchAlgorithm traversalAlgorithm = SearchAlgorithm.DepthFirst) where T : View
        {
            var view = gameObject.GetComponent<View>();
            if (view == null)
            {
                return null;
            }

            return view.Find<T>(x => true, recursive, parent, traversalAlgorithm);
        }

        /// <summary>
        /// Returns first view of _type T with the specified ID.
        /// </summary>
        public static T Find<T>(this View view, string id, bool recursive = true, View parent = null, SearchAlgorithm traversalAlgorithm = SearchAlgorithm.DepthFirst) where T : View
        {
            return view.Find<T>(x => String.Equals(x.Id, id, StringComparison.OrdinalIgnoreCase), recursive, parent, traversalAlgorithm);
        }

        /// <summary>
        /// Gets view field info from a _type.
        /// </summary>
        public static MemberInfo GetFieldInfo(this Type type, string field, BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance)
        {
            var fieldInfo = type.GetField(field, bindingFlags);
            if (fieldInfo != null)
                return fieldInfo;

            var propertyInfo = type.GetProperty(field, bindingFlags);
            return propertyInfo;
        }

        /// <summary>
        /// Gets view field _type from view field info.
        /// </summary>
        public static Type GetFieldType(this MemberInfo memberInfo)
        {
            var fieldInfo = memberInfo as FieldInfo;
            if (fieldInfo != null)
            {
                return fieldInfo.FieldType;
            }

            var propertyInfo = memberInfo as PropertyInfo;
            if (propertyInfo != null)
            {
                return propertyInfo.PropertyType;
            }

            return null;
        }

        /// <summary>
        /// Gets value from dictionary and returns null if it doesn't exist.
        /// </summary>
        public static TValue Get<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key)
        {
            TValue value;
            if (!dict.TryGetValue(key, out value))
            {
                return default(TValue);
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// Traverses the view object tree and performs an action on this view and its children until the action returns false.
        /// </summary>
        public static void ForThisAndEachChild<T>(this View view, Action<T> action, bool recursive = true, View parent = null, SearchAlgorithm traversalAlgorithm = SearchAlgorithm.DepthFirst) where T : View
        {
            var thisView = view.gameObject.GetComponent<T>();
            if (thisView != null)
            {
                action(thisView);
            }
            view.ForEachChild<T>(action, recursive, parent, traversalAlgorithm);
        }


        /// <summary>
        /// Gets a list of all descendants. 
        /// </summary>
        public static List<T> GetChildren<T>(this View view, bool recursive = true, View parent = null, SearchAlgorithm traversalAlgorithm = SearchAlgorithm.DepthFirst) where T : View
        {
            return view.GetChildren<T>(x => true, recursive, parent, traversalAlgorithm);
        }

        /// <summary>
        /// Gets a list of all descendants matching the predicate. 
        /// </summary>
        public static List<T> GetChildren<T>(this View view, Func<T, bool> predicate = null, bool recursive = true, View parent = null, SearchAlgorithm traversalAlgorithm = SearchAlgorithm.DepthFirst) where T : View
        {
            var children = new List<T>();
            if (predicate == null)
            {
                predicate = x => true;
            }

            view.ForEachChild<T>(x =>
            {
                if (predicate(x))
                {
                    children.Add(x);
                }
            }, recursive, parent, traversalAlgorithm);

            return children;
        }

        /// <summary>
        /// Gets a list of all descendants matching the predicate. 
        /// </summary>
        public static List<T> GetChildren<T>(this GameObject gameObject, Func<T, bool> predicate = null, bool recursive = true, View parent = null, SearchAlgorithm traversalAlgorithm = SearchAlgorithm.DepthFirst) where T : View
        {
            var view = gameObject.GetComponent<View>();
            if (view == null)
            {
                return new List<T>();
            }

            return view.GetChildren<T>(predicate, recursive, parent, traversalAlgorithm);
        }

        /// <summary>
        /// Replaces the first occurance of a string.
        /// </summary>
        public static string ReplaceFirst(this string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }
    }
}


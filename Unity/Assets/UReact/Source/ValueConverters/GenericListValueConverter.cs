﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using System.Globalization;
using System.Collections;
#endregion

namespace UReact.ValueConverters
{
    public class GenericListValueConverter : ValueConverter
    {
        #region Constructor

        public GenericListValueConverter()
        {
            _type = typeof(List<object>);
        }

        #endregion

        #region Methods

        public override ConversionResult Convert(object value, ValueConverterContext context)
        {
            if (value == null)
            {
                return new ConversionResult(new List<object>());
            }
            Type valueType = value.GetType();
            if (valueType == _type)
            {
                return base.Convert(value, context);
            }
            try
            {
                var list = new List<object>();
                if (value is IEnumerable)
                {                    
                    foreach (var e in (value as IEnumerable))
                    {
                        list.Add(e);
                    }

                    return new ConversionResult(list);
                }
                else
                {
                    list.Add(value);
                    return new ConversionResult(list);
                }
            }
            catch (Exception e)
            {
                return ConversionFailed(value, e);
            }
        }

        #endregion
    }
}

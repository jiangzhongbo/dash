﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using System.IO;
#endregion

namespace UReact.ValueConverters
{
    public class MaterialValueConverter : AssetValueConverter
    {
        #region Constructor

        public MaterialValueConverter()
        {
            _type = typeof(Material);
        }

        #endregion
    }
}

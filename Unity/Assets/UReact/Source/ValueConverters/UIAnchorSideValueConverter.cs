﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UReact;
#endregion

namespace UReact.ValueConverters
{
    public class UIAnchorSideValueConverter : ValueConverter
    {
        #region Constructor

        public UIAnchorSideValueConverter()
        {
            _type = typeof(UIAnchor.Side);
        }

        #endregion

        #region Methods

        public override ConversionResult Convert(object value, ValueConverterContext context)
        {
            if (value == null)
            {
                return base.Convert(value, context);
            }
            Type valueType = value.GetType();
            if (valueType == _type)
            {
                return base.Convert(value, context);
            }
            if (value.GetType() == typeof(string))
            {
                var stringValue = (string)value;
                try
                {
                    var convertedValue = Enum.Parse(typeof(UIAnchor.Side), stringValue, true);
                    return new ConversionResult(convertedValue);
                }
                catch (Exception e)
                {
                    return ConversionFailed(value, e);
                }
            }

            return ConversionFailed(value);
        }
        public static Vector2 Convert(Vector2 value, UIAnchor.Side source, UIAnchor.Side target){
            switch (source)
            {
                case UIAnchor.Side.TopLeft:
                    {
                        break;
                    }
                case UIAnchor.Side.Top:
                    {
                        value += new Vector2(0.5f, 0);
                        break;
                    }
                case UIAnchor.Side.TopRight:
                    {
                        value += new Vector2(1f, 0);
                        break;
                    }
                case UIAnchor.Side.Left:
                    {
                        value += new Vector2(0, -0.5f);
                        break;
                    }
                case UIAnchor.Side.Center:
                    {
                        value += new Vector2(0.5f, -0.5f);
                        break;
                    }
                case UIAnchor.Side.Right:
                    {
                        value += new Vector2(1f, -0.5f);
                        break;
                    }
                case UIAnchor.Side.BottomLeft:
                    {
                        value += new Vector2(0, -1f);
                        break;
                    }
                case UIAnchor.Side.Bottom:
                    {
                        value += new Vector2(0.5f, -1f);
                        break;
                    }
                case UIAnchor.Side.BottomRight:
                    {
                        value += new Vector2(1, -1f);
                        break;
                    }
            }
            switch (target)
            {
                case UIAnchor.Side.TopLeft:
                    {
                        break;
                    }
                case UIAnchor.Side.Top:
                    {
                        value -= new Vector2(0.5f, 0);
                        break;
                    }
                case UIAnchor.Side.TopRight:
                    {
                        value -= new Vector2(1f, 0);
                        break;
                    }
                case UIAnchor.Side.Left:
                    {
                        value -= new Vector2(0, -0.5f);
                        break;
                    }
                case UIAnchor.Side.Center:
                    {
                        value -= new Vector2(0.5f, -0.5f);
                        break;
                    }
                case UIAnchor.Side.Right:
                    {
                        value -= new Vector2(1f, -0.5f);
                        break;
                    }
                case UIAnchor.Side.BottomLeft:
                    {
                        value -= new Vector2(0, -1f);
                        break;
                    }
                case UIAnchor.Side.Bottom:
                    {
                        value -= new Vector2(0.5f, -1f);
                        break;
                    }
                case UIAnchor.Side.BottomRight:
                    {
                        value -= new Vector2(1, -1f);
                        break;
                    }
            }
            return value;
        }
        #endregion
    }

    
}

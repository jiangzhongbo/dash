﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
#endregion

namespace UReact
{
    public class ValueConverterContext
    {
        #region Fields

        public static ValueConverterContext Empty = new ValueConverterContext();

        #endregion

        #region Constructor

        public ValueConverterContext()
        {
        }

        #endregion
    }
}

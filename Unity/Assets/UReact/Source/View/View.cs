﻿using UReact.Animation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UReact;
using UReact.Views;
using UReact.ValueConverters;
[InternalView]
[AddComponent(typeof(UIWidget))]
[AddComponent(typeof(UIAnchor))]
[AddComponent(typeof(UIStretch))]
public class View : MonoBehaviour
{

    public ViewAction EnabledChanged;

    [ChangeHandler("UpdateLayouts")]
    public ElementSize Width;

    [ChangeHandler("UpdateLayouts")]
    public ElementSize Height;

    [ChangeHandler("UpdateLayouts")]
    public ElementSize X;

    [ChangeHandler("UpdateLayouts")]
    public ElementSize Y;

    [ChangeHandler("UpdateLayout")]
    public UIWidget.Pivot Pivot;

    [ChangeHandler("UpdateLayout")]
    public UIAnchor.Side Anchor;

    [ChangeHandler("UpdateBehavior")]
    public UIStretch.Style Fit = UIStretch.Style.Both;

    [ChangeHandler("UpdateLayouts")]
    public string AnchorParent = "$Parent";

    [ChangeHandler("UpdateLayouts")]
    public string FitParent = "$Parent";

    [ChangeHandler("UpdateBehavior")]
    public float Alpha = 1;

    [ChangeHandler("UpdateBehavior")]
    public int Depth = 0;

    [ChangeHandler("UpdateBehavior")]
    public string Id;

    [ChangeHandler("UpdateBehavior")]
    public Vector3 Rotation;

    [ChangeHandler("UpdateBehavior")]
    public Vector3 Position;

    [ChangeHandler("UpdateBehavior")]
    public Vector3 Scale;

    [NotSetFromXml]
    public string ViewTypeName;

    [NotSetFromXml]
    [ChangeHandler("UpdateEnable")]
    public bool Enabled;

    [NotSetFromXml]
    public GameObject ComponentParentGo;

    [NotSetFromXml]
    public View ComponentParentView;

    [NotSetFromXml]
    public GameObject ParentGo;

    [NotSetFromXml]
    public View ParentView;

    [NotSetFromXml]
    public bool IsLayoutRoot;

    [NotSetFromXml]
    public string ViewXmlName;

    public string Style;

    public string BasedOn;

    public string Theme;

    [NotSetFromXml]
    public List<ViewFieldBinding> ViewFieldBindings;

    [NotSetFromXml]
    public List<string> BindingFields;

    [NotSetFromXml]
    public List<ViewActionEntry> ViewActionEntries;

    [NotSetFromXml]
    public List<string> SetViewFieldNames;

    [NotSetFromXml]
    public ViewTypeData ViewTypeData;

    protected UIAnchor _uiAnchor;
    protected UIStretch _uiStretch;
    protected UIWidget _uiWidget;

    private bool _isInitialized;

    public bool IsInitialized
    {
        get
        {
            return _isInitialized;
        }
        set
        {
            _isInitialized = value;
        }
    }

    private Dictionary<string, ViewFieldData> _viewFieldData;
    private HashSet<string> _changeHandlers;
    private Dictionary<string, MethodInfo> _changeHandlerMethods;
    private HashSet<string> _setViewFields;
    private Dictionary<string, string> _expressionViewField;
    private List<ValueObserver> _valueObservers;
    private List<ViewAction> _eventSystemViewActions;



    public View()
    {
        ViewTypeName = GetType().Name;
        Enabled = true;

        ViewFieldBindings = new List<ViewFieldBinding>();
        ViewActionEntries = new List<ViewActionEntry>();
        SetViewFieldNames = new List<string>();
        BindingFields = new List<string>();

        Pivot = UIWidget.Pivot.Center;
        Anchor = UIAnchor.Side.Center;
        Width = new ElementSize(1.0f, ElementSizeUnit.Percents);
        Height = new ElementSize(1.0f, ElementSizeUnit.Percents);
        X = new ElementSize(0, ElementSizeUnit.Percents);
        Y = new ElementSize(0, ElementSizeUnit.Percents);

        Scale = Vector3.one;
        
    }

    public void UpdateView()
    {
#if UNITY_EDITOR
        _uiAnchor = gameObject.GetComponent<UIAnchor>();
        _uiStretch = gameObject.GetComponent<UIStretch>();
        _uiWidget = gameObject.GetComponent<UIWidget>();
#endif

        UpdateBehavior();
        UpdateLayout();
        UpdateEnable();
        TriggerChangeHandlers();
    }

    /// <summary>
    /// Gets view field data.
    /// </summary>
    public ViewFieldData GetViewFieldData(string viewField, int depth = int.MaxValue)
    {
        // get mapped view field
        //var viewTypeData = ViewPresenter.Instance.ViewData.GetViewTypeData(ViewTypeName);
        var viewTypeData = ViewTypeData;
        var mappedViewField = viewTypeData.GetMappedViewField(viewField);

        if (_viewFieldData == null)
        {
            _viewFieldData = new Dictionary<string, ViewFieldData>();
        }

        // is there data for this field?
        var fieldData = _viewFieldData.Get(mappedViewField);
        if (fieldData == null)
        {
            // no. create new field data
            fieldData = ViewFieldData.FromViewFieldPath(this, mappedViewField);
            if (fieldData != null)
            {
                _viewFieldData.Add(mappedViewField, fieldData);
            }
        }

        // are we the owner of this field?
        if (fieldData != null && !fieldData.IsOwner && depth > 0)
        {
            // no. go deeper if we can
            var targetView = fieldData.GetTargetView();
            if (targetView != null)
            {
                fieldData = targetView.GetViewFieldData(fieldData.TargetViewFieldPath, --depth);
            }
        }

        return fieldData;
    }

    public void EnabledHasChanged()
    {
        if (Enabled)
        {
            OnViewEnabled();
        }
        else
        {
            OnViewDisabled();
        }
    }

    public virtual void OnViewEnabled()
    {

    }

    public virtual void OnViewDisabled()
    {

    }

    public virtual void Initialize()
    {
    }

    public Vector2 GetRelativeOffset()
    {
        if (!_uiAnchor) return Vector2.zero;
        return _uiAnchor.relativeOffset;
    }

    public Vector2 GetRelativeSize()
    {
        if (!_uiStretch) return Vector2.zero;
        return _uiStretch.relativeSize;
    }

    /// <summary>
    /// Initializes internal values to default values. Called once before InitializeInternal(). Called in depth-first order.
    /// </summary>
    public virtual void InitializeInternalDefaultValues()
    {
        // initialize lists and dictionaries
        _viewFieldData = new Dictionary<string, ViewFieldData>();
        _setViewFields = new HashSet<string>();
        _valueObservers = new List<ValueObserver>();
        _eventSystemViewActions = new List<ViewAction>();
        _changeHandlers = new HashSet<string>();
        _changeHandlerMethods = new Dictionary<string, MethodInfo>();
        _expressionViewField = new Dictionary<string, string>();
    }

    internal void InitializeInternal()
    {
        if (ParentView)
        {
            Depth += ParentView.Depth;
        }
        _uiAnchor = gameObject.GetComponent<UIAnchor>();
        _uiStretch = gameObject.GetComponent<UIStretch>();
        _uiWidget = gameObject.GetComponent<UIWidget>();

        if (_uiAnchor) _uiAnchor.runOnlyOnce = false;
        if (_uiStretch) _uiStretch.runOnlyOnce = false;

        if (!string.IsNullOrEmpty(AnchorParent))
        {
            GameObject go = null;
            if (AnchorParent == "$Componet")
            {
                if(ComponentParentView){
                    go = ComponentParentView.gameObject;
                }else{
                    go = ParentGo;
                }
            }
            else if (AnchorParent == "$Parent")
            {
                go = ParentGo;
            }
            else if (AnchorParent == "$Root")
            {
                go = GetUIPanelRoot();
            }
            else if(this.FindView(AnchorParent))
            {
                go = this.FindView(AnchorParent).gameObject;
            }
            if (_uiAnchor) _uiAnchor.container = go;
        }

        if(!string.IsNullOrEmpty(FitParent))
        {
            GameObject go = null;
            if (FitParent == "$Componet")
            {
                if (ComponentParentView)
                {
                    go = ComponentParentView.gameObject;
                }
                else
                {
                    go = ParentGo;
                }
            }
            else if (FitParent == "$Parent")
            {
                go = ParentGo;
            }
            else if (FitParent == "$Root")
            {
                go = GetUIPanelRoot();
            }
            else if (this.FindView(FitParent))
            {
                go = this.FindView(FitParent).gameObject;
            }
            if (_uiStretch) _uiStretch.container = go;
        }


        if(string.IsNullOrEmpty(AnchorParent))
        {
            if (ParentGo)
            {
                if (_uiAnchor) _uiAnchor.container = ParentGo;
            }
            else if (ComponentParentGo)
            {
                if (_uiAnchor) _uiAnchor.container = ComponentParentGo;
            }
            else
            {
                if (_uiAnchor) _uiAnchor.container = NGUITools.FindInParents<UIPanel>(gameObject).gameObject;
            }
        }
        if (string.IsNullOrEmpty(FitParent))
        {
            if (ParentGo)
            {
                if (_uiStretch) _uiStretch.container = ParentGo;
            }
            else if (ComponentParentGo)
            {
                if (_uiStretch) _uiStretch.container = ComponentParentGo;
            }
            else
            {
                if (_uiStretch) _uiStretch.container = NGUITools.FindInParents<UIPanel>(gameObject).gameObject;
            }
        }


        if (_uiAnchor) _uiAnchor.side = Anchor;
        if (_uiStretch) _uiStretch.style = Fit;
        if (_uiWidget)
        {
            _uiWidget.pivot = Pivot;
            _uiWidget.depth = Depth;
            _uiWidget.autoResizeBoxCollider = true;
        }

        if (_uiAnchor) _uiAnchor.relativeOffset = new Vector2(0, 0);
        if (_uiStretch) _uiStretch.relativeSize = new Vector2(1, 1);

        // initialize bindings
        foreach (var binding in ViewFieldBindings)
        {
            SetBinding(binding.ViewField, binding.BindingString);
        }

        // initialize set-fields
        foreach (var setField in SetViewFieldNames)
        {
            SetViewFieldSetValue(setField);
        }

        // initialize action handlers
        foreach (var actionEntry in ViewActionEntries)
        {
            SetViewActionEntry(actionEntry);
        }

        // initialize change handlers
        var viewTypeData = ViewPresenter.Instance.ViewData.GetViewTypeData(ViewTypeName);
        foreach (var changeHandler in viewTypeData.ViewFieldChangeHandlers)
        {
            SetChangeHandler(changeHandler);
        }

        // initialize system event triggers            

        for (int i = 0; i < this.ViewTypeData.ViewActionFields.Count; i++)
        {
            this._eventSystemViewActions.Add(GetType().GetField(this.ViewTypeData.ViewActionFields[i]).GetValue(this) as ViewAction);
        }


        InitEventSystemTriggers();

        IsInitialized = true;
    }

    internal void InitEventSystemTriggers()
    {
        // hook up event system triggers
        var eventListner = GetComponent<UIEventListener>();
        if (eventListner != null)
        {
            _eventSystemViewActions.ForEach(e =>
            {
                switch (e.EventTriggerType)
                {
                    case EventTriggerType.Click:
                        eventListner.onClick += go =>
                        {
                            e.Trigger(new VoidActionData { go = go });
                        };
                        break;
                    case EventTriggerType.DoubleClick:
                        eventListner.onDoubleClick += go =>
                        {
                            e.Trigger(new VoidActionData { go = go });
                        };
                        break;
                    case EventTriggerType.Drag:
                        eventListner.onDrag += (go, delta) =>
                        {
                            e.Trigger(new Vector2ActionData { go = go, delta = delta });
                        };
                        break;
                    case EventTriggerType.DragOut:
                        eventListner.onDragOut += (go) =>
                        {
                            e.Trigger(new VoidActionData { go = go });
                        };
                        break;
                    case EventTriggerType.DragOver:
                        eventListner.onDragOver += (go) =>
                        {
                            e.Trigger(new VoidActionData { go = go });
                        };
                        break;
                    case EventTriggerType.DragStart:
                        eventListner.onDragStart += (go) =>
                        {
                            e.Trigger(new VoidActionData { go = go });
                        };
                        break;
                    case EventTriggerType.Drop:
                        eventListner.onDrop += (go, dragGo) =>
                        {
                            e.Trigger(new GameObjectActionData { go = go, draggedObject = dragGo });
                        };
                        break;
                    case EventTriggerType.Hover:
                        eventListner.onHover += (go, state) =>
                        {
                            e.Trigger(new BoolActionData { go = go, state = state });
                        };
                        break;
                    case EventTriggerType.Key:
                        eventListner.onKey += (go, key) =>
                        {
                            e.Trigger(new KeyCodeActionData { go = go, key = key });
                        };
                        break;
                    case EventTriggerType.Press:
                        eventListner.onPress += (go, state) =>
                        {
                            e.Trigger(new BoolActionData { go = go, state = state });
                        };
                        break;
                    case EventTriggerType.Scroll:
                        eventListner.onScroll += (go, delta) =>
                        {
                            e.Trigger(new FloatActionData { go = go, delta = delta });
                        };
                        break;
                    case EventTriggerType.Select:
                        eventListner.onSelect += (go, state) =>
                        {
                            e.Trigger(new BoolActionData { go = go, state = state });
                        };
                        break;
                    case EventTriggerType.Touch:
                        eventListner.onTouch += go =>
                        {
                            e.Trigger(new VoidActionData { go = go });
                        };
                        break;
                }
            });

        }
    }

    /// <summary>
    /// Sets view action entry.
    /// </summary>
    public void SetViewActionEntry(ViewActionEntry entry)
    {
        // get view field data for binding target
        var viewFieldData = GetViewFieldData(entry.ViewActionFieldName);
        if (viewFieldData == null)
        {
            Debug.LogError(String.Format("[UReact] {0}: Unable to assign view action handler \"{1}.{2}()\" to view action \"{3}\". View action not found.", name, ParentView.ViewTypeName, entry.ViewActionHandlerName, entry.ViewActionFieldName));
            return;
        }

        bool hasValue;
        entry.SourceView = viewFieldData.SourceView;
        ViewAction viewAction = viewFieldData.GetValue(out hasValue) as ViewAction;
        if (hasValue)
        {
            viewAction.AddEntry(entry);
        }
    }

    public void TriggerChangeHandlers()
    {
        if (_changeHandlers.Count > 0)
        {
            var triggeredChangeHandlers = new List<string>(_changeHandlers);
            _changeHandlers.Clear();

            foreach (var changeHandler in triggeredChangeHandlers)
            {
                try
                {
                    _changeHandlerMethods[changeHandler].Invoke(this, null);
                }
                catch (Exception e)
                {
                    Debug.LogError(String.Format("[UReact] {0}: Exception thrown in change handler \"{1}\": {2}", name, changeHandler, Utils.GetError(e)));
                }
            }
        }
    }


    public GameObject GetUIPanelRoot()
    {
        return ViewPresenter.Instance.UIPanelRoot;
    }


    public void InitializeViews()
    {
        ViewPresenter.Instance.InitializeViews(this);
    }


    public void UpdateLayouts()
    {
        QueueChangeHandler("UpdateLayout");

        // inform parents of update
        NotifyUpdateLayout();
    }

    public virtual void UpdateLayout()
    {
        //Debug.Log(String.Format("{0}.UpdateLayout called", Name));

        if (_uiAnchor) _uiAnchor.side = Anchor;
        if (_uiStretch) _uiStretch.style = Fit;
        if (_uiWidget)
        {
            _uiWidget.pivot = Pivot;
            _uiWidget.depth = Depth;
        }
        if (_uiAnchor) _uiAnchor.relativeOffset = new Vector2(X.Percent(), Y.Percent(false));
        if (_uiStretch) _uiStretch.relativeSize = new Vector2(Width.Percent(), Height.Percent(false));
    }

    public void NotifyUpdateLayout()
    {

    }

    /// <summary>
    /// Updates the behavior and visual appearance of the view.
    /// </summary>
    public virtual void UpdateBehavior()
    {
        //Debug.Log(String.Format("{0}.UpdateBehavior called", Name));

        if (!String.IsNullOrEmpty(Id))
        {
            name = Id;
        }
        
        transform.localScale = Scale;
        transform.localRotation = Quaternion.Euler(Rotation);
        transform.localPosition = Position;
        if (_uiWidget)
        {
            _uiWidget.alpha = Alpha;
        }
        if (_uiAnchor) _uiAnchor.enabled = true;
        if (_uiStretch) _uiStretch.enabled = true;
    }

    public virtual void UpdateEnable()
    {

        EnabledHasChanged();

        if (ParentView && !ParentView.Enabled)
        {
            Enabled = false;
        }
        if (Enabled)
        {
            if (_uiWidget)
            {
                _uiWidget.alpha = Alpha;
                _uiWidget.enabled = true;
            }


            bool b = NGUITools.FindInParents<List>(gameObject) != null;

            if (_uiAnchor) _uiAnchor.runOnlyOnce = b;
            if (_uiStretch) _uiStretch.runOnlyOnce = b;
        }
        else
        {

            if (_uiWidget)
            {
                _uiWidget.alpha = 0f;
                _uiWidget.enabled = false;
            }


            if (_uiAnchor) _uiAnchor.runOnlyOnce = true;
            if (_uiStretch) _uiStretch.runOnlyOnce = true;
        }
        if (_uiAnchor) _uiAnchor.enabled = true;
        if (_uiStretch) _uiStretch.enabled = true;
        if (_uiWidget) _uiWidget.ResizeCollider();
        UpdateCollider();
        this.ForEachChild<View>(x => x.UpdateEnable(), false, null, SearchAlgorithm.BreadthFirst);
    }

    public virtual void UpdateColliders()
    {
        this.ForEachChild<View>(x => x.UpdateColliders(), false, null, SearchAlgorithm.BreadthFirst);
        UpdateCollider();
    }

    public virtual void UpdateCollider()
    {
        if (collider)
        {
            bool b = Enabled;
            gameObject.ForEachParent<View>(v =>
            {
                if (!v.Enabled)
                {
                    b = false;
                }
            });
            if (!b && !Application.isPlaying && Application.isEditor)
            {
                if(_uiWidget) _uiWidget.enabled = true;
                if (_uiWidget) _uiWidget.alpha = 0.01f;
                collider.enabled = true;
            }
            else
            {
                collider.enabled = b;
            }
        }
    }

    public virtual void LateUpdate()
    {
        TriggerChangeHandlers();
    }

    internal void AddIsSetField(string viewFieldPath)
    {
        if (!SetViewFieldNames.Contains(viewFieldPath))
        {
            SetViewFieldNames.Add(viewFieldPath);
        }

        if (_setViewFields != null)
        {
            _setViewFields.Add(viewFieldPath);
        }
    }

    internal bool GetIsSetFieldValue(string viewFieldPath)
    {
        if (_setViewFields != null)
        {
            return _setViewFields.Contains(viewFieldPath);
        }

        return false;
    }

    public void SetIsSet(string viewField)
    {
        // get view field data
        var viewFieldData = GetViewFieldData(viewField);
        if (viewFieldData == null)
        {
            Debug.LogError(String.Format("[UReact] {0}: Unable to set is-set indicator on view field \"{1}\". View field not found.", name, viewField));
            return;
        }

        viewFieldData.SetIsSet();
    }

    /// <summary>
    /// Gets boolean indicating if value has been set on view field.
    /// </summary>
    public bool IsSet<TField>(Expression<Func<TField>> expression)
    {
        return IsSet(GetMappedViewField(expression));
    }

    /// <summary>
    /// Gets boolean indicating if value has been set on view field.
    /// </summary>
    public bool IsSet(string viewField)
    {
        // get view field data
        var viewFieldData = GetViewFieldData(viewField);
        if (viewFieldData == null)
        {
            Debug.LogError(String.Format("[UReact] {0}: Unable to get set-value from view field \"{1}\". View field not found.", name, viewField));
            return false;
        }

        return viewFieldData.IsSet();
    }

    private void SetViewFieldSetValue(string viewFieldPath)
    {
        _setViewFields.Add(viewFieldPath);
    }

    private void SetChangeHandler(ViewFieldChangeHandler changeHandler)
    {
        // get view field data for change handler
        var viewFieldData = GetViewFieldData(changeHandler.ViewField);
        if (viewFieldData == null)
        {
            return;
        }

        // create change handler observer
        var changeHandlerObserver = new ChangeHandlerValueObserver(this, changeHandler.ChangeHandlerName, changeHandler.TriggerImmediately);
        if (changeHandlerObserver.IsValid)
        {
            viewFieldData.RegisterValueObserver(changeHandlerObserver);
            AddValueObserver(changeHandlerObserver);
        }
        else
        {
            Debug.LogError(String.Format("[UReact] {0}: Unable to assign view change handler \"{1}()\" for view field \"{2}\". Change handler method not found.", name, changeHandler.ChangeHandlerName, changeHandler.ViewField));
            return;
        }
    }

    public void SetBinding(string viewField, string viewFieldBinding)
    {
        // get view field data for binding target
        var viewFieldData = GetViewFieldData(viewField);
        if (viewFieldData == null)
        {
            Debug.LogError(String.Format("[UReact] {0}: Unable to assign binding \"{1}\" to view field \"{2}\". View field not found.", name, viewFieldBinding, viewField));
            return;
        }

        // create BindingValueObserver and add it as observer to source view fields
        var bindingValueObserver = new BindingValueObserver();
        bindingValueObserver.Target = viewFieldData;

        // parse view field binding string
        char[] delimiterChars = { ' ', ',', '$', '(', ')', '{', '}' };
        string trimmedBinding = viewFieldBinding.Trim();
        //if (trimmedBinding.StartsWith("$"))
        //{
        //    // transformed multi-binding
        //    string[] bindings = trimmedBinding.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
        //    if (bindings.Length < 1)
        //    {
        //        Debug.LogError(String.Format("[UReact] {0}: Unable to assign binding \"{1}\" to view field \"{2}\". Improperly formatted binding string.", name, viewFieldBinding, viewField));
        //        return;
        //    }

        //    bindingValueObserver.BindingType = BindingType.MultiBindingTransform;
        //    bindingValueObserver.ComponentParentView = ComponentParentView;

        //    // get transformation method
        //    string transformMethodName = bindings[0];
        //    Type transformMethodViewType = ComponentParentView.GetType();
        //    string[] transformStr = bindings[0].Split('.');
        //    if (transformStr.Length == 2)
        //    {
        //        transformMethodViewType = ViewData.GetViewType(transformStr[0]);
        //        transformMethodName = transformStr[1];

        //        if (transformMethodViewType == null)
        //        {
        //            Debug.LogError(String.Format("[UReact] {0}: Unable to assign binding \"{1}\" to view field \"{2}\". View \"{3}\" not found.", name, viewFieldBinding, viewField, transformStr[0]));
        //            return;
        //        }
        //    }

        //    bindingValueObserver.TransformMethod = transformMethodViewType.GetMethod(transformMethodName, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
        //    if (bindingValueObserver.TransformMethod == null)
        //    {
        //        Debug.LogError(String.Format("[UReact] {0}: Unable to assign binding \"{1}\" to view field \"{2}\". Transform method \"{3}\" not found in view type \"{4}\".", name, viewFieldBinding, viewField, bindings[0], ComponentParentView.ViewTypeName));
        //        return;
        //    }

        //    foreach (var binding in bindings.Skip(1))
        //    {
        //        bool isLocalField, isNegatedField, isOneWay, isResource;
        //        var sourceFieldName = ParseBindingString(binding, out isLocalField, out isNegatedField, out isOneWay, out isResource);


        //        // if the binding is defined as a local field (through the '#' notation) we are binding to a field on this view 
        //        // otherwise we are binding to our parent view
        //        var bindingView = isLocalField ? this : ComponentParentView;

        //        // get view field data for binding
        //        var sourceViewFieldData = bindingView.GetViewFieldData(sourceFieldName);
        //        if (sourceViewFieldData == null)
        //        {
        //            Debug.LogError(String.Format("[UReact] {0}: Unable to assign binding \"{1}\" to view field \"{2}\". Source binding view field \"{3}\" not found.", name, viewFieldBinding, viewField, binding));
        //            return;
        //        }
        //        //Debug.Log(String.Format("Creating binding {0} <-> {1}", sourceViewFieldData.ViewFieldPath, viewFieldData.ViewFieldPath));

        //        bindingValueObserver.Sources.Add(new ViewFieldBindingSource(sourceViewFieldData, isNegatedField));
        //        sourceViewFieldData.RegisterValueObserver(bindingValueObserver);
        //    }
        //}
        //else
        {
            // check for bindings in string
            string formatString = String.Empty;
            List<Match> matches = new List<Match>();
            foreach (Match match in ViewFieldBinding.BindingRegex.Matches(viewFieldBinding))
            {
                matches.Add(match);
            }

            if (matches.Count <= 0)
            {
                // no bindings found
                Debug.LogError(String.Format("[UReact] {0}: Unable to assign binding \"{1}\" to view field \"{2}\". String contains no binding.", name, viewFieldBinding, viewField));
                return;
            }

            // is the binding a format string?
            bool formatStringBinding = false;
            if (matches.Count > 1 || (matches[0].Value.Length != viewFieldBinding.Length) || !String.IsNullOrEmpty(matches[0].Groups["format"].Value))
            {
                // yes. 
                int matchCount = 0;
                formatString = ViewFieldBinding.BindingRegex.Replace(viewFieldBinding, x =>
                {
                    string matchCountString = matchCount.ToString();
                    ++matchCount;
                    return String.Format("{{{0}{1}}}", matchCountString, x.Groups["format"]);
                });

                formatStringBinding = true;
                bindingValueObserver.BindingType = BindingType.MultiBindingFormatString;
                bindingValueObserver.FormatString = formatString;
            }
            // parse view fields for binding source(s)
            foreach (var match in matches)
            {
                var binding = match.Groups["field"].Value.Trim();
                bool isLocalField, isNegatedField, isOneWay, isResource;
                var sourceFieldName = ParseBindingString(binding, out isLocalField, out isNegatedField, out isOneWay, out isResource);

                // if the binding is defined as a local field (through the '#' notation) we are binding to a field on this view 
                // otherwise we are binding to our parent view
                var bindingView = isLocalField ? this : ComponentParentView;

                // get view field data for binding
                var sourceViewFieldData = bindingView.GetViewFieldData(sourceFieldName);
                bindingView.BindingFields.Add(sourceFieldName);
                if (sourceViewFieldData == null)
                {
                    Debug.LogError(String.Format("[UReact] {0}: Unable to assign binding \"{1}\" to view field \"{2}\". Source binding view field \"{3}\" not found.", name, viewFieldBinding, viewField, sourceFieldName));
                    return;
                }
                //Debug.Log(String.Format("Creating binding {0} <-> {1}", sourceViewFieldData.ViewFieldPath, viewFieldData.ViewFieldPath));

                bindingValueObserver.Sources.Add(new ViewFieldBindingSource(sourceViewFieldData, isNegatedField));
                sourceViewFieldData.RegisterValueObserver(bindingValueObserver);

                // handle two-way bindings
                if (!formatStringBinding && !isOneWay)
                {
                    bindingValueObserver.BindingType = BindingType.SingleBinding;

                    // create value observer for target
                    var targetBindingValueObserver = new BindingValueObserver();
                    targetBindingValueObserver.BindingType = BindingType.SingleBinding;
                    targetBindingValueObserver.Target = sourceViewFieldData;
                    targetBindingValueObserver.Sources.Add(new ViewFieldBindingSource(viewFieldData, isNegatedField));

                    viewFieldData.RegisterValueObserver(targetBindingValueObserver);
                    AddValueObserver(targetBindingValueObserver);

                    // if this is a local binding and target view is the same as source view 
                    // we need to make sure value propagation happens in an intuitive order
                    // so that if we e.g. bind Text="{#Item.Score}" that Item.Score propagates to Text first. 
                    if (isLocalField && viewFieldData.TargetView == bindingView)
                    {
                        sourceViewFieldData.PropagateFirst = true;
                    }
                }
            }
        }

        AddValueObserver(bindingValueObserver);
    }

    private string ParseBindingString(string binding, out bool isLocalField, out bool isNegatedField, out bool isOneWay, out bool isResource)
    {
        isLocalField = false;
        isNegatedField = false;
        isOneWay = false;
        isResource = false;

        var viewField = binding;
        while (viewField.Length > 0)
        {
            if (viewField.StartsWith("#"))
            {
                isLocalField = true;
                viewField = viewField.Substring(1);
            }
            else if (viewField.StartsWith("!"))
            {
                isNegatedField = true;
                viewField = viewField.Substring(1);
            }
            else if (viewField.StartsWith("="))
            {
                isOneWay = true;
                viewField = viewField.Substring(1);
            }
            else if (viewField.StartsWith("@"))
            {
                isResource = true;
                viewField = viewField.Substring(1);
            }
            else
            {
                break;
            }
        }

        return viewField;
    }

    internal void QueueChangeHandler(string name)
    {
        _changeHandlers.Add(name);

        if (!_changeHandlerMethods.ContainsKey(name))
        {
            _changeHandlerMethods.Add(name, GetType().GetMethod(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic));
        }
    }

    public void NotifyDependentValueObservers(string viewFieldPath, bool includeViewField = false)
    {
        foreach (var viewFieldData in _viewFieldData.Values)
        {
            if (!viewFieldData.IsOwner)
                continue;

            if (includeViewField && viewFieldData.ViewFieldPath == viewFieldPath)
            {
                viewFieldData.NotifyValueObservers(new HashSet<ViewFieldData>());
            }

            if (viewFieldData.ViewFieldPathInfo.Dependencies.Count > 0 &&
                viewFieldData.ViewFieldPathInfo.Dependencies.Contains(viewFieldPath))
            {
                viewFieldData.NotifyValueObservers(new HashSet<ViewFieldData>());
            }
        }
    }

    /// <summary>
    /// Sets view value.
    /// </summary>
    public object SetValue(string viewField, object value)
    {
        return SetValue(viewField, value, true, null, true);
    }

    /// <summary>
    /// Sets view field value. 
    /// </summary>
    /// <param Name="viewField">View field path.</param>
    /// <param Name="value">Value to be set.</param>
    /// <param Name="updateDefaultState">Boolean indicating if the default state should be updated (if the view is in the default state).</param>
    /// <param Name="callstack">Callstack used to prevent cyclical SetValue calls.</param>
    /// <param Name="context">Value converter context.</param>
    /// <returns>Returns the converted value set.</returns>
    public object SetValue(string viewField, object value, bool updateDefaultState, HashSet<ViewFieldData> callstack, bool notifyObservers)
    {
        callstack = callstack ?? new HashSet<ViewFieldData>();

        // Debug.Log(String.Format("{0}: {1} = {2}", GameObjectName, viewField, value));

        // get view field data
        var viewFieldData = GetViewFieldData(viewField);
        if (viewFieldData == null)
        {
            Debug.LogError(String.Format("[UReact] {0}: Unable to assign value \"{1}\" to view field \"{2}\". View field not found.", name, value, viewField));
            return null;
        }

        // set view field value
        try
        {
            return viewFieldData.SetValue(value, callstack, updateDefaultState, notifyObservers);
        }
        catch (Exception e)
        {
            Debug.LogError(String.Format("[UReact] {0}: Unable to assign value \"{1}\" to view field \"{2}\". Exception thrown: {3}", name, value, viewField, Utils.GetError(e)));
            return null;
        }
    }

    /// <summary>
    /// Sets the value of a field utilizing the binding and change tracking system.
    /// </summary>
    protected object SetValue<TField>(Expression<Func<TField>> expression, object value, bool updateDefaultState, bool notifyObservers)
    {
        return SetValue(GetMappedViewField(expression), value, updateDefaultState, null, notifyObservers);
    }

    /// <summary>
    /// Sets the value of a field utilizing the binding and change tracking system.
    /// </summary>
    protected object SetValue<TField>(Expression<Func<TField>> expression, object value)
    {
        return SetValue(GetMappedViewField(expression), value, true, null, true);
    }

    /// <summary>
    /// Gets mapped view field from expression.
    /// </summary>
    protected string GetMappedViewField<TField>(Expression<Func<TField>> expression)
    {
        // get mapped view field           
        string expressionString = expression.ToString();
        string mappedViewField = _expressionViewField.Get(expressionString);
        if (mappedViewField == null)
        {
            // add expression
            mappedViewField = expressionString.Substring(expressionString.IndexOf(").") + 2);
            _expressionViewField.Add(expressionString, mappedViewField);
        }

        return mappedViewField;
    }

    /// <summary>
    /// Gets value from view field.
    /// </summary>
    protected object GetValue<TField>(Expression<Func<TField>> expression)
    {
        return GetValue(GetMappedViewField(expression));
    }

    /// <summary>
    /// Gets value from view field.
    /// </summary>
    public object GetValue(string viewField)
    {
        bool hasValue;
        return GetValue(viewField, out hasValue);
    }

    /// <summary>
    /// Gets value from view field.
    /// </summary>
    public object GetValue(string viewField, out bool hasValue)
    {
        // get view field data            
        var viewFieldData = GetViewFieldData(viewField);
        if (viewFieldData == null)
        {
            hasValue = false;
            Debug.LogError(String.Format("[UReact] {0}: Unable to get value from view field \"{1}\". View field not found.", name, viewField));
            return null;
        }

        try
        {
            return viewFieldData.GetValue(out hasValue);
        }
        catch (Exception e)
        {
            hasValue = false;
            Debug.LogError(String.Format("[UReact] {0}: Unable to get value from view field \"{1}\". Exception thrown: {2}", name, viewField, Utils.GetError(e)));
            return null;
        }
    }

    /// <summary>
    /// Adds a value observer to the view.
    /// </summary>
    public void AddValueObserver(ValueObserver valueObserver)
    {
        _valueObservers.Add(valueObserver);
    }

    /// <summary>
    /// Adds a binding to the view field that will be processed when the view is initialized.
    /// </summary>
    public void AddBinding(string viewField, string bindingString)
    {
        ViewFieldBindings.Add(new ViewFieldBinding { ViewField = viewField, BindingString = bindingString });
    }

    public void PropagateBindings()
    {
        foreach (var viewFieldData in _viewFieldData.Values.OrderByDescending(x => x.PropagateFirst))
        {
            viewFieldData.NotifyBindingValueObservers(new HashSet<ViewFieldData>());
        }
    }

    public void QueueAllChangeHandlers()
    {
        var _viewFieldDataList = new List<ViewFieldData>(_viewFieldData.Values);
        foreach (var viewFieldData in _viewFieldDataList)
        {
            viewFieldData.NotifyChangeHandlerValueObservers(new HashSet<ViewFieldData>());
        }
    }

    /// <summary>
    /// Adds a view action handler for a certain view action.
    /// </summary>
    public void AddViewActionEntry(string viewActionFieldName, string viewActionHandlerName, View parent)
    {
        ViewActionEntries.Add(new ViewActionEntry { ComponentParentView = parent, ViewActionFieldName = viewActionFieldName, ViewActionHandlerName = viewActionHandlerName });
    }

    /// <summary>
    /// Called after the view is initialized but before any XML values are set. Used to set default values on the view.
    /// </summary>
    public virtual void SetDefaultValues()
    {
        ViewTypeName = GetType().Name;
    }


    public void Activate(bool immediate = false)
    {
        if (immediate)
        {
            EnabledChanged.Trigger(new EnabledChangedActionData { Enabled = true });
        }

        SetValue(() => Enabled, true);
        this.ForEachChild<View>(x => x.Activate(immediate), true, null, SearchAlgorithm.ReverseBreadthFirst);
    }

    public void Deactivate(bool immediate = false)
    {
        if (immediate)
        {
            EnabledChanged.Trigger(new EnabledChangedActionData { Enabled = false });
        }

        SetValue(() => Enabled, false);
        this.ForEachChild<View>(x => x.Deactivate(immediate), true, null, SearchAlgorithm.ReverseBreadthFirst);
    }

    public bool HasQueuedChangeHandlers
    {
        get
        {
            return _changeHandlers != null ? _changeHandlers.Count > 0 : false;
        }
    }
    public List<string> QueuedChangeHandlers
    {
        get
        {
            return _changeHandlers != null ? _changeHandlers.ToList() : new List<string>();
        }
    }

    public float ActualWidth
    {
        get
        {
            if (!_uiStretch) return 0;
            if (ParentView)
            {
                return _uiStretch.relativeSize.x * ParentView.ActualWidth;
            }
            else
            {
                return ScreenWidth;
            }
            
        }
    }

    public float ActualHeight
    {
        get
        {
            if (!_uiStretch) return 0;
            if (ParentView)
            {
                return _uiStretch.relativeSize.y * ParentView.ActualHeight;
            }
            else
            {
                return ScreenHeight;
            }
        }
    }

    public float ScreenWidth
    {
        get
        {
#if UNITY_EDITOR
            return 480;
#else
            return Screen.width;
#endif
        }
    }

    public float ScreenHeight
    {
        get
        {
#if UNITY_EDITOR
            return 800;
#else
            return Screen.height;
#endif
        }
    }

    /// <summary>
    /// Gets view field data of the view.
    /// </summary>
    public Dictionary<string, ViewFieldData> ViewFieldDataDictionary
    {
        get
        {
            return _viewFieldData;
        }
    }

    public Vector2 ActualOffset(UIAnchor.Side side)
    {
        if (!_uiAnchor) return Vector2.zero;
        if (ParentView)
        {
            var offset = UIAnchorSideValueConverter.Convert(_uiAnchor.relativeOffset, _uiAnchor.side, UIAnchor.Side.TopLeft);
            var parentOffset = ParentView.ActualOffset(UIAnchor.Side.TopLeft);
            offset.x = offset.x * ParentView.ActualWidth + (parentOffset.x - ParentView.ActualWidth / 2);
            offset.y = offset.y * ParentView.ActualHeight + (parentOffset.y + ParentView.ActualHeight / 2);
            return UIAnchorSideValueConverter.Convert(offset, UIAnchor.Side.TopLeft, side);
        }
        else
        {
            var offset = UIAnchorSideValueConverter.Convert(_uiAnchor.relativeOffset, _uiAnchor.side, side);
            offset.x *= ScreenWidth;
            offset.y *= ScreenHeight;
            return offset;
        }
    }

    /// <summary>
    /// Calls InitializeInternalDefaultValues() and catches and prints any exception thrown.
    /// </summary>
    internal void TryInitializeInternalDefaultValues()
    {
#if !DISABLE_INIT_TRYCATCH
        try
        {
            InitializeInternalDefaultValues();
        }
        catch (Exception e)
        {
            Debug.LogError(String.Format("[UReact] {0}: InitializeInternalDefaultValues() failed. Exception thrown: {1}", name, Utils.GetError(e)));
        }
#else
            InitializeInternalDefaultValues();
#endif
    }

    /// <summary>
    /// Calls InitializeInternalDefaultValues() and catches and prints any exception thrown if define is set.
    /// </summary>
    internal void TryInitializeInternal()
    {
#if !DISABLE_INIT_TRYCATCH
        try
        {
            InitializeInternal();
        }
        catch (Exception e)
        {
            Debug.LogError(String.Format("[UReact] {0}: InitializeInternal() failed. Exception thrown: {1}", name, Utils.GetError(e)));
        }
#else
            InitializeInternal();
#endif
    }

    /// <summary>
    /// Calls Initialize() and catches and prints any exception thrown.
    /// </summary>
    internal void TryInitialize()
    {
#if !DISABLE_INIT_TRYCATCH
        try
        {
            Initialize();
        }
        catch (Exception e)
        {
            Debug.LogError(String.Format("[UReact] {0}: Initialize() failed. Exception thrown: {1}", name, Utils.GetError(e)));
        }
#else
            Initialize();
#endif
    }

    /// <summary>
    /// Calls PropagateBindings() and catches and prints any exception thrown.
    /// </summary>
    internal void TryPropagateBindings()
    {
#if !DISABLE_INIT_TRYCATCH
        try
        {
            PropagateBindings();
        }
        catch (Exception e)
        {
            Debug.LogError(String.Format("[UReact] {0}: PropagateBindings() failed. Exception thrown: {1}", name, Utils.GetError(e)));
        }
#else
            PropagateBindings();
#endif
    }

    /// <summary>
    /// Calls QueueAllChangeHandlers() and catches and prints any exception thrown.
    /// </summary>
    internal void TryQueueAllChangeHandlers()
    {
#if !DISABLE_INIT_TRYCATCH
        try
        {
            QueueAllChangeHandlers();
        }
        catch (Exception e)
        {
            Debug.LogError(String.Format("[UReact] {0}: QueueAllChangeHandlers() failed. Exception thrown: {1}", name, Utils.GetError(e)));
        }
#else
            QueueAllChangeHandlers();
#endif
    }

    /// <summary>
    /// Calls TriggerChangeHandlers() and catches and prints any exception thrown.
    /// </summary>
    internal void TryTriggerChangeHandlers()
    {
#if !DISABLE_INIT_TRYCATCH
        try
        {
            TriggerChangeHandlers();
        }
        catch (Exception e)
        {
            Debug.LogError(String.Format("[UReact] {0}: TriggerChangeHandlers() failed. Exception thrown: {1}", name, Utils.GetError(e)));
        }
#else
            TriggerChangeHandlers();
#endif
    }

    /// <summary>
    /// Returns string based on format string and parameters.
    /// </summary>
    public static string Format(string format, object arg)
    {
        return String.Format(format, arg ?? String.Empty);
    }

    /// <summary>
    /// Returns string based on format string and parameters.
    /// </summary>
    public static string Format1(string format, object arg)
    {
        return String.Format(format, arg ?? String.Empty);
    }

    /// <summary>
    /// Returns string based on format string and parameters.
    /// </summary>
    public static string Format2(string format, object arg1, object arg2)
    {
        return String.Format(format, arg1 ?? String.Empty, arg2 ?? String.Empty);
    }

    /// <summary>
    /// Returns string based on format string and parameters.
    /// </summary>
    public static string Format3(string format, object arg1, object arg2, object arg3)
    {
        return String.Format(format, arg1 ?? String.Empty, arg2 ?? String.Empty, arg3 ?? String.Empty);
    }
}

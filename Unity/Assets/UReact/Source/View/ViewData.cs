﻿#region Using Statements
using System.Collections;
using System;
using System.Xml;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using UnityEngine;
using UReact.Animation;
#endregion
using UReact.ValueConverters;
namespace UReact
{
    [Serializable]
    public class ViewData
    {
        public List<ViewTypeData> ViewTypeDataList;
        public List<ThemeData> ThemeData;
        public List<string> Views;
        public List<string> Themes;

        public float ElementSize;
        public Vector2 ScaleSize;

        private Dictionary<string, ValueConverter> _cachedValueConverters;
        private Dictionary<string, Type> _viewTypes;
        private Dictionary<string, ViewTypeData> _viewTypeDataDictionary;
        private Dictionary<string, ThemeData> _themeDataDictionary;
        private Dictionary<string, ValueConverter> _valueConvertersForType;
        private Dictionary<string, ValueConverter> _valueConverters;
        private Dictionary<string, ValueInterpolator> _valueInterpolatorsForType;

        public ViewData()
        {
            ViewTypeDataList = new List<ViewTypeData>();
            ThemeData = new List<ThemeData>();
            Views = new List<string>();
            Themes = new List<string>();
        }

        public void Clear()
        {
            ThemeData.Clear();

            ViewTypeDataList.Clear();

            _viewTypeDataDictionary = null;
            _themeDataDictionary = null;
            _viewTypes = null;
        }

        public ViewTypeData GetViewTypeData(string viewTypeName)
        {
            if (_viewTypeDataDictionary == null)
            {
                LoadViewTypeDataDictionary();
            }

            ViewTypeData viewTypeData;
            if (!_viewTypeDataDictionary.TryGetValue(viewTypeName, out viewTypeData))
            {
                Debug.LogError(String.Format("[UReact] Can't find view type \"{0}\".", viewTypeName));
                return null;
            }

            return viewTypeData;
        }

        private void LoadViewTypeDataDictionary()
        {
            _viewTypeDataDictionary = new Dictionary<string, ViewTypeData>();
            foreach (var viewTypeData in ViewTypeDataList)
            {
                foreach (var viewName in viewTypeData.ViewNameAliases)
                {
                    if (_viewTypeDataDictionary.ContainsKey(viewName))
                    {
                        Debug.LogError(String.Format("[UReact] Can't map view-model \"{0}\" to view \"{1}\" because it is already mapped to view-model \"{2}\". If you want to replace another view-model use the ReplaceViewModel class attribute. Otherwise choose a different view Name that is available.", viewTypeData.ViewTypeName, viewName, _viewTypeDataDictionary[viewName].ViewTypeName));
                        continue;
                    }

                    _viewTypeDataDictionary.Add(viewName, viewTypeData);
                }
            }

            // check if view-models should be replaced
            foreach (var viewTypeData in ViewTypeDataList.Where(x => !String.IsNullOrEmpty(x.ReplacesViewModel)))
            {
                // find the view type it replaces
                var replacedViewTypeData = ViewTypeDataList.FirstOrDefault(x => String.Equals(x.ViewTypeName, viewTypeData.ReplacesViewModel, StringComparison.OrdinalIgnoreCase));
                if (replacedViewTypeData == null)
                    continue;

                // replace the view-model
                foreach (var kv in _viewTypeDataDictionary.ToList())
                {
                    if (kv.Value == replacedViewTypeData)
                    {
                        _viewTypeDataDictionary[kv.Key] = viewTypeData;
                    }
                }
            }
        }


        public ThemeData GetThemeData(string themeName)
        {
            if (_themeDataDictionary == null)
            {
                _themeDataDictionary = new Dictionary<string, ThemeData>();
                foreach (var themeData in ThemeData)
                {
                    _themeDataDictionary.Add(themeData.ThemeName, themeData);
                }
            }

            return _themeDataDictionary.Get(themeName);
        }

        public Type GetViewType(string viewTypeName)
        {
            if (_viewTypes == null)
            {
                _viewTypes = new Dictionary<string, Type>();
                foreach (var viewType in TypeHelper.FindDerivedTypes(typeof(View)))
                {
                    _viewTypes.Add(viewType.Name, viewType);
                }
            }

            return _viewTypes.Get(viewTypeName);
        }

        public ValueConverter GetValueConverterForType(string viewFieldType)
        {
            if (_valueConvertersForType == null)
            {
                _valueConvertersForType = new Dictionary<string, ValueConverter>();

                // cache standard converters to improve load performance
                _valueConvertersForType.Add("Object", new ValueConverter());
                _valueConvertersForType.Add("Single", new FloatValueConverter());
                _valueConvertersForType.Add("Int32", new IntValueConverter());
                _valueConvertersForType.Add("Boolean", new BoolValueConverter());
                _valueConvertersForType.Add("Color", new ColorValueConverter());
                _valueConvertersForType.Add("ElementSize", new ElementSizeValueConverter());
                _valueConvertersForType.Add("Enum", new EnumValueConverter());
                _valueConvertersForType.Add("Component", new ComponentValueConverter());
                _valueConvertersForType.Add("Font", new FontValueConverter());
                _valueConvertersForType.Add("Material", new MaterialValueConverter());
                _valueConvertersForType.Add("Quaternion", new QuaternionValueConverter());
                _valueConvertersForType.Add("String", new StringValueConverter());
                _valueConvertersForType.Add("Vector2", new Vector2ValueConverter());
                _valueConvertersForType.Add("Vector3", new Vector3ValueConverter());
                _valueConvertersForType.Add("Vector4", new Vector4ValueConverter());

                foreach (var valueConverterType in TypeHelper.FindDerivedTypes(typeof(ValueConverter)))
                {
                    if (CachedValueConverters.ContainsKey(valueConverterType.Name))
                        continue;

                    var valueConverter = TypeHelper.CreateInstance(valueConverterType) as ValueConverter;
                    if (valueConverter.Type != null)
                    {
                        var valueTypeName = valueConverter.Type.Name;
                        if (!_valueConvertersForType.ContainsKey(valueTypeName))
                        {
                            _valueConvertersForType.Add(valueTypeName, valueConverter);
                        }
                    }
                }
            }

            return _valueConvertersForType.Get(viewFieldType);
        }

        public ValueConverter GetValueConverter(string valueConverterTypeName)
        {
            if (_valueConverters == null)
            {
                _valueConverters = new Dictionary<string, ValueConverter>();

                // cache standard converters to improve load performance
                foreach (var cachedConverter in CachedValueConverters)
                {
                    _valueConverters.Add(cachedConverter.Key, cachedConverter.Value);
                }

                foreach (var valueConverterType in TypeHelper.FindDerivedTypes(typeof(ValueConverter)))
                {
                    if (_valueConverters.ContainsKey(valueConverterType.Name))
                        continue;

                    var valueConverter = TypeHelper.CreateInstance(valueConverterType) as ValueConverter;
                    _valueConverters.Add(valueConverterType.Name, valueConverter);
                }
            }

            return _valueConverters.Get(valueConverterTypeName);
        }

        public ValueInterpolator GetValueInterpolatorForType(string viewFieldType)
        {
            if (_valueInterpolatorsForType == null)
            {
                _valueInterpolatorsForType = new Dictionary<string, ValueInterpolator>();
                foreach (var valueInterpolatorType in TypeHelper.FindDerivedTypes(typeof(ValueInterpolator)))
                {
                    var valueInterpolator = TypeHelper.CreateInstance(valueInterpolatorType) as ValueInterpolator;
                    if (valueInterpolator.Type != null)
                    {
                        var valueTypeName = valueInterpolator.Type.Name;
                        if (!_valueInterpolatorsForType.ContainsKey(valueTypeName))
                        {
                            _valueInterpolatorsForType.Add(valueTypeName, valueInterpolator);
                        }
                    }
                }
            }

            return _valueInterpolatorsForType.Get(viewFieldType);
        }


        private Dictionary<string, ValueConverter> CachedValueConverters
        {
            get
            {
                if (_cachedValueConverters == null)
                {
                    _cachedValueConverters = new Dictionary<string, ValueConverter>();
                    _cachedValueConverters.Add("ValueConverter", new ValueConverter());
                    _cachedValueConverters.Add("FloatValueConverter", new FloatValueConverter());
                    _cachedValueConverters.Add("IntValueConverter", new IntValueConverter());
                    _cachedValueConverters.Add("BoolValueConverter", new BoolValueConverter());
                    _cachedValueConverters.Add("ColorValueConverter", new ColorValueConverter());
                    _cachedValueConverters.Add("ElementSizeValueConverter", new ElementSizeValueConverter());
                    _cachedValueConverters.Add("ComponentValueConverter", new ComponentValueConverter());
                    _cachedValueConverters.Add("EnumValueConverter", new EnumValueConverter());
                    _cachedValueConverters.Add("FontValueConverter", new FontValueConverter());
                    _cachedValueConverters.Add("MaterialValueConverter", new MaterialValueConverter());
                    _cachedValueConverters.Add("QuaternionValueConverter", new QuaternionValueConverter());
                    _cachedValueConverters.Add("AssetValueConverter", new AssetValueConverter());
                    _cachedValueConverters.Add("StringValueConverter", new StringValueConverter());
                    _cachedValueConverters.Add("Vector2ValueConverter", new Vector2ValueConverter());
                    _cachedValueConverters.Add("Vector3ValueConverter", new Vector3ValueConverter());
                    _cachedValueConverters.Add("Vector4ValueConverter", new Vector4ValueConverter());
                }

                return _cachedValueConverters;
            }
        }
    }
}

﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
#endregion

namespace UReact
{
    public enum EventTriggerType
    {
        Click,
        DoubleClick,
        Hover,
        Press,
        Select,
        Scroll,
        Drag,
        Drop,
        DragStart,
        DragOver,
        DragOut,
        Key,
        Touch,
    }

    [Serializable]
    public class ViewAction
    {
        #region Fields

        public string Name;
        public bool TriggeredByEventSystem;
        public EventTriggerType EventTriggerType;
        public bool IsDisabled;
        public static Dictionary<string, EventTriggerType> EventTriggerTypes;

        private List<ViewActionEntry> _viewActionEntries;

        #endregion

        #region Constructor

        static ViewAction()
        {
            EventTriggerTypes = new Dictionary<string, EventTriggerType>();
            EventTriggerTypes.Add("Click", EventTriggerType.Click);
            EventTriggerTypes.Add("DoubleClick", EventTriggerType.DoubleClick);
            EventTriggerTypes.Add("Hover", EventTriggerType.Hover);
            EventTriggerTypes.Add("Press", EventTriggerType.Press);
            EventTriggerTypes.Add("Select", EventTriggerType.Select);
            EventTriggerTypes.Add("Scroll", EventTriggerType.Scroll);
            EventTriggerTypes.Add("Drag", EventTriggerType.Drag);
            EventTriggerTypes.Add("DragStart", EventTriggerType.DragStart);
            EventTriggerTypes.Add("DragOver", EventTriggerType.DragOver);
            EventTriggerTypes.Add("DragOut", EventTriggerType.DragOut);
            EventTriggerTypes.Add("Drop", EventTriggerType.Drop);
            EventTriggerTypes.Add("Key", EventTriggerType.Key);
            EventTriggerTypes.Add("Touch", EventTriggerType.Touch);
        }

        public ViewAction(string name = "")
        {
            Name = name;
            if (EventTriggerTypes.ContainsKey(Name))
            {
                TriggeredByEventSystem = true;
                EventTriggerType = EventTriggerTypes[Name];
            }

            _viewActionEntries = new List<ViewActionEntry>();
        }

        #endregion

        #region Methods

        public void Trigger()
        {
            if (IsDisabled)
                return;

            // go through the entries and call them
            if (_viewActionEntries != null)
            {
                foreach (var entry in _viewActionEntries)
                {
                    entry.Invoke();
                }
            }
        }

        public void Trigger(ActionData actionData)
        {
            if (IsDisabled)
                return;

            // go through the entries and call them
            if (_viewActionEntries != null)
            {
                foreach (var entry in _viewActionEntries)
                {
                    entry.Invoke(actionData);
                }
            }
        }

        public void Trigger(object customData)
        {
            if (IsDisabled)
                return;

            // go through the entries and call them
            if (_viewActionEntries != null)
            {
                foreach (var entry in _viewActionEntries)
                {
                    entry.Invoke(customData);
                }
            }
        }

        public void Trigger(ActionData actionData, object customData)
        {
            if (IsDisabled)
                return;

            if (_viewActionEntries != null)
            {
                foreach (var entry in _viewActionEntries)
                {
                    entry.Invoke(actionData, customData);
                }
            }
        }

        public void AddEntry(ViewActionEntry viewActionEntry)
        {
            if (_viewActionEntries == null)
            {
                _viewActionEntries = new List<ViewActionEntry>();
            }

            _viewActionEntries.Add(viewActionEntry);
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets number of view action entries attached to this view action.
        /// </summary>
        public int Entries
        {
            get
            {
                return _viewActionEntries != null ? _viewActionEntries.Count : 0;
            }
        }

        /// <summary>
        /// Gets boolean indicating if the view action has any entries attached to it. 
        /// </summary>
        public bool HasEntries
        {
            get
            {
                return _viewActionEntries != null ? _viewActionEntries.Count > 0 : false;
            }
        }
        #endregion
    }
}

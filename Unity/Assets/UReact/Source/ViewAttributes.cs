﻿#region Using Statements
using System;
#endregion

namespace UReact
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public class NotSetFromXml : Attribute
    {
    }
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class AddComponent : Attribute
    {
        #region Fields

        private Type componentType;

        #endregion

        #region Constructor

        public AddComponent(Type componentType)
        {
            this.componentType = componentType;
        }

        #endregion

        #region Properties

        public Type ComponentType
        {
            get 
            {
                return componentType;
            }
            set
            {
                componentType = value;
            }
        }

        #endregion
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class RemoveComponent : Attribute
    {
        #region Fields

        private Type componentType;

        #endregion

        #region Constructor

        public RemoveComponent(Type componentType)
        {
            this.componentType = componentType;
        }

        #endregion

        #region Properties

        public Type ComponentType
        {
            get
            {
                return componentType;
            }
            set
            {
                componentType = value;
            }
        }

        #endregion
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public class ChangeHandler : Attribute
    {
        #region Fields

        private string changeHandlerName;
        public bool TriggerImmediately;

        #endregion

        #region Constructor

        public ChangeHandler(string changeHandlerName)
        {
            this.changeHandlerName = changeHandlerName;
        }

        #endregion

        #region Properties

        public string ChangeHandlerName
        {
            get
            {
                return changeHandlerName;
            }
            set
            {
                changeHandlerName = value;
            }
        }

        #endregion
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class InternalView : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class CreatesView : Attribute
    {
        #region Fields

        protected Type type;
        protected string style;

        #endregion

        #region Constructor

        public CreatesView(Type type, string style = "")
        {
            this.type = type;
            this.style = style;
        }

        #endregion

        #region Properties

        public Type Type
        {
            get
            {
                return type;
            }
        }

        public string Style
        {
            get
            {
                return style;
            }
        }

        #endregion
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class HideInPresenter : Attribute
    {
    }

    /// <summary>
    /// Attribute indicating that component isn't to be automatically added to the view.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class ExcludeComponent : Attribute
    {
        #region Fields

        public string ComponentFieldName;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public ExcludeComponent(string componentFieldName)
        {
            ComponentFieldName = componentFieldName;
        }

        #endregion
    }

    /// <summary>
    /// Attribute indicating additional view Name alias that will map to this view-model.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class ViewNameAlias : Attribute
    {
        #region Fields

        public string ViewAlias;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public ViewNameAlias(string viewName)
        {
            ViewAlias = viewName;
        }

        #endregion
    }

    /// <summary>
    /// Attribute indicating that this view-model replaces another with the specified Name.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ReplacesViewModel : Attribute
    {
        #region Fields

        public string ViewTypeName;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public ReplacesViewModel(string viewTypeName)
        {
            ViewTypeName = viewTypeName;
        }

        #endregion
    }

    /// <summary>
    /// Maps one field to another.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class MapViewField : Attribute
    {
        #region Fields

        public MapViewFieldData MapFieldData;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public MapViewField(string from, string to, string changeHandlerName = null, Type valueConverterType = null, bool triggerChangeHandlerImmediately = false)
        {
            MapFieldData = new MapViewFieldData();
            MapFieldData.From = from;
            MapFieldData.To = to;
            MapFieldData.TriggerChangeHandlerImmediately = triggerChangeHandlerImmediately;
            if (valueConverterType != null)
            {
                MapFieldData.ValueConverterType = valueConverterType.Name;
                MapFieldData.ValueConverterTypeSet = true;
            }

            if (changeHandlerName != null)
            {
                MapFieldData.ChangeHandlerName = changeHandlerName;
                MapFieldData.ChangeHandlerNameSet = true;
            }
        }

        #endregion

        #region Methods
        #endregion
    }

    /// <summary>
    /// Maps a dependency field to another field.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class RemappedField : Attribute
    {
        #region Fields

        public MapViewFieldData MapFieldData;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public RemappedField(string field, string to, string changeHandlerName = null, Type valueConverterType = null, bool triggerChangeHandlerImmediately = false)
        {
            MapFieldData = new MapViewFieldData();
            MapFieldData.From = field;
            MapFieldData.To = to;
            MapFieldData.TriggerChangeHandlerImmediately = triggerChangeHandlerImmediately;
            if (valueConverterType != null)
            {
                MapFieldData.ValueConverterType = valueConverterType.Name;
                MapFieldData.ValueConverterTypeSet = true;
            }

            if (changeHandlerName != null)
            {
                MapFieldData.ChangeHandlerName = changeHandlerName;
                MapFieldData.ChangeHandlerNameSet = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Maps a dependency field to another field.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class MapTo : Attribute
    {
        #region Fields

        public MapViewFieldData MapFieldData;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public MapTo(string to, string changeHandlerName = null, Type valueConverterType = null, bool triggerChangeHandlerImmediately = false)
        {
            MapFieldData = new MapViewFieldData();
            MapFieldData.To = to;
            MapFieldData.TriggerChangeHandlerImmediately = triggerChangeHandlerImmediately;
            if (valueConverterType != null)
            {
                MapFieldData.ValueConverterType = valueConverterType.Name;
                MapFieldData.ValueConverterTypeSet = true;
            }

            if (changeHandlerName != null)
            {
                MapFieldData.ChangeHandlerName = changeHandlerName;
                MapFieldData.ChangeHandlerNameSet = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Specifies that a view field is generic.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class GenericViewField : Attribute
    {
        #region Fields
        #endregion

        #region Constructor
        #endregion
    }

    /// <summary>
    /// Attribute for replacing one component field with another.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ReplacesComponentField : Attribute
    {
        #region Fields

        public string ReplacedComponentField;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public ReplacesComponentField(string replacedComponentField)
        {
            ReplacedComponentField = replacedComponentField;
        }

        #endregion
    }

    /// <summary>
    /// Attribute for replacing one dependency field with another.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ReplacesDependencyField : Attribute
    {
        #region Fields

        public string ReplacedDependencyField;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public ReplacesDependencyField(string replacedDependencyField)
        {
            ReplacedDependencyField = replacedDependencyField;
        }

        #endregion
    }
}

﻿using UnityEngine;
using System.Collections.Generic;
using UReact;
using System.Linq;
using System.Reflection;
public class ViewBinding : MonoBehaviour
{
#if UNITY_EDITOR
    public void Load()
    {
        if (Application.isPlaying) return;
        View view = gameObject.GetComponent<View>();
        List<string> fch = view.BindingFields;
        fch.ForEach(e =>
        {
            
            if (StaticData.Fetch(view.GetType().Name).Have(e))
            {
                view.SetValue(e, StaticData.Fetch(view.GetType().Name).Get(e).Value);
            }
        });
        //view.UpdateViews();
    }
#endif
}

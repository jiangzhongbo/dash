﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using UnityEngine;
#endregion

namespace UReact.Editor
{
    internal class ViewElement
    {
        #region Fields

        private string assetName;
        private string name;
        private XElement element;
        private List<ViewElement> dependencies;
        private List<string> dependencyNames;
        private bool permanentMark;
        private bool temporaryMark;
        private Type viewType;
        private List<FieldInfo> viewActionFields;
        private bool isInternal;

        #endregion

        #region Properties

        public string AssetName
        {
            get { return assetName; }
            set { assetName = value; }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public XElement Element
        {
            get
            {
                return element;
            }
            set
            {
                element = value;
            }
        }

        public List<ViewElement> Dependencies
        {
            get
            {
                return dependencies;
            }
            set
            {
                dependencies = value;
            }
        }

        public List<string> DependencyNames
        {
            get
            {
                return dependencyNames;
            }
            set
            {
                dependencyNames = value;
            }
        }

        public bool PermanentMark
        {
            get
            {
                return permanentMark;
            }
            set
            {
                permanentMark = value;
            }
        }

        public bool TemporaryMark
        {
            get
            {
                return temporaryMark;
            }
            set
            {
                temporaryMark = value;
            }
        }

        public Type ViewType
        {
            get
            {
                return viewType;
            }
            set
            {
                viewType = value;
            }
        }

        public List<FieldInfo> ViewActionFields
        {
            get
            {
                return viewActionFields;
            }
            set
            {
                viewActionFields = value;
            }
        }

        public bool IsInternal
        {
            get { return isInternal; }
            set { isInternal = value; }
        }

        #endregion

        #region Constructor

        public ViewElement()
        {
            dependencies = new List<ViewElement>();
            dependencyNames = new List<string>();
            viewActionFields = new List<FieldInfo>();
        }

        #endregion
    }
}

﻿#region Using Statements
using System;
using System.Collections.Generic;
using UnityEngine;
#endregion

namespace UReact
{
    [Serializable]
    public class _float : ViewField<float>
    {
        public static implicit operator float(_float value) { return value.Value; }
    }

    [Serializable]
    public class _string : ViewField<string>
    {
        public static implicit operator string(_string value) { return value.Value; }
    }

    [Serializable]
    public class _int : ViewField<int>
    {
        public static implicit operator int(_int value) { return value.Value; }
    }

    [Serializable]
    public class _bool : ViewField<bool>
    {
        public static implicit operator bool(_bool value) { return value.Value; }
    }

    [Serializable]
    public class _char : ViewField<char>
    {
        public static implicit operator char(_char value) { return value.Value; }
    }

    [Serializable]
    public class _Color : ViewField<Color>
    {
        public static implicit operator Color(_Color value) { return value.Value; }
    }

    [Serializable]
    public class _ElementSize : ViewField<ElementSize> { }

    [Serializable]
    public class _Font : ViewField<Font> { }


    [Serializable]
    public class _Material : ViewField<Material> { }

    [Serializable]
    public class _Quaternion : ViewField<Quaternion> { }
    [Serializable]
    public class _Vector2 : ViewField<Vector2> { }

    [Serializable]
    public class _Vector3 : ViewField<Vector3> { }

    [Serializable]
    public class _Vector4 : ViewField<Vector4> { }

    [Serializable]
    public class _FontStyle : ViewField<FontStyle>
    {
        public static implicit operator FontStyle(_FontStyle value) { return value.Value; }
    }


    [Serializable]
    public class _ImageType : ViewField<UISprite.Type>
    {
        public static implicit operator UISprite.Type(_ImageType value) { return value.Value; }
    }


    [Serializable]
    public class _TouchScreenKeyboardType : ViewField<UnityEngine.TouchScreenKeyboardType>
    {
        public static implicit operator UnityEngine.TouchScreenKeyboardType(_TouchScreenKeyboardType value) { return value.Value; }
    }

    [Serializable]
    public class _MaterialArray : ViewField<UnityEngine.Material[]>
    {
        public static implicit operator UnityEngine.Material[](_MaterialArray value) { return value.Value; }
    }


    [Serializable]
    public class _Transform : ViewField<Transform>
    {
        public static implicit operator Transform(_Transform value) { return value.Value; }
    }


    [Serializable]
    public class _CameraComponent : ViewField<UnityEngine.Camera>
    {
        public static implicit operator UnityEngine.Camera(_CameraComponent value) { return value.Value; }
    }

    [Serializable]
    public class _GameObject : ViewField<UnityEngine.GameObject>
    {
        public static implicit operator UnityEngine.GameObject(_GameObject value) { return value.Value; }
    }

    [Serializable]
    public class _HideFlags : ViewField<UnityEngine.HideFlags>
    {
        public static implicit operator UnityEngine.HideFlags(_HideFlags value) { return value.Value; }
    }


    [Serializable]
    public class _Mesh : ViewField<Mesh>
    {
        public static implicit operator Mesh(_Mesh value) { return value.Value; }
    }

    [Serializable]
    public class _object : ViewField<object> { }

    [Serializable]
    public class _Color32 : ViewField<Color32>
    {
        public static implicit operator Color32(_Color32 value) { return value.Value; }
    }
}

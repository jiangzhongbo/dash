﻿#region Using Statements
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Reflection;
using System.Text;
using System.IO;
using UReact;
using UReact.Animation;
using UReact.ValueConverters;
#endregion

[AddComponentMenu("UReact/View Presenter")]
public class ViewPresenter : MonoBehaviour
{
    public ViewData ViewData;
    public string MainView;
    public string DefaultTheme;
    public GameObject RootView;
    public GameObject LayoutRoot;
    public GameObject UIPanelRoot;
    public bool DisableAutomaticReload;
    public bool UpdateXsdSchema;

    public float ElementSize;
    public Vector2 ScaleSize;

    private static ViewPresenter _instance;
    private static string _currentScene;

    public ViewPresenter()
    {

    }

    public void Awake()
    {
        Initialize();
    }

    public void Initialize()
    {
        UpdateInstance();

        InitializeViews(RootView);
        ViewData.ElementSize = ElementSize;
        ViewData.ScaleSize = ScaleSize;
    }


    public void InitializeViews(GameObject rootView)
    {
        if (rootView == null)
            return;

        InitializeViews(rootView.GetComponent<View>());
    }

    public void InitializeViews(View rootView)
    {
        if (rootView == null || rootView.IsInitialized)
        {
            Debug.LogError("[UReact] Error initializing views. rootView == null");
            return;
        }


        rootView.ForThisAndEachChild<View>(x => x.TryInitializeInternalDefaultValues());
        rootView.ForThisAndEachChild<View>(x => x.TryInitializeInternal());
        rootView.ForThisAndEachChild<View>(x => x.TryInitialize(), true, null, SearchAlgorithm.ReverseBreadthFirst);
        rootView.ForThisAndEachChild<View>(x => x.TryPropagateBindings(), true, null, SearchAlgorithm.BreadthFirst);
        rootView.ForThisAndEachChild<View>(x => x.TryQueueAllChangeHandlers(), true, null, SearchAlgorithm.ReverseBreadthFirst);

        int pass = 0;
        while (rootView.Find<View>(x => x.HasQueuedChangeHandlers) != null)
        {
            if (pass >= 1000)
            {
                PrintTriggeredChangeHandlerOverflowError(pass, rootView);
                break;
            }

            rootView.ForThisAndEachChild<View>(x => x.TryTriggerChangeHandlers(), true, null, SearchAlgorithm.ReverseBreadthFirst);
            ++pass;
        }


    }

    private void PrintTriggeredChangeHandlerOverflowError(int pass, View rootView)
    {
        var sb = new StringBuilder();
        var triggeredViews = rootView.GetChildren<View>(x => x.HasQueuedChangeHandlers);
        foreach (var triggeredView in triggeredViews)
        {
            sb.AppendFormat("{0}: ", triggeredView.name);
            sb.AppendLine();
            foreach (var triggeredChangeHandler in triggeredView.QueuedChangeHandlers)
            {
                sb.AppendFormat("\t{0}", triggeredChangeHandler);
                sb.AppendLine();
            }
        }

        Debug.LogError(String.Format("[UReact] Error initializing views. Stack overflow when triggering change handlers. Make sure your change handlers doesn't trigger each other in a loop. The following change handlers were still triggered after {0} passes:{1}{2}", pass, Environment.NewLine, sb.ToString()));
    }

    public void Clear()
    {
        ViewData.Clear();
        if (RootView != null)
        {
            GameObject.DestroyImmediate(RootView);
        }

        if (LayoutRoot != null)
        {
            GameObject.DestroyImmediate(LayoutRoot);
        }

        if (UIPanelRoot != null)
        {
            GameObject.DestroyImmediate(UIPanelRoot);
        }
    }


    public static void UpdateInstance()
    {
        var sceneName = Application.loadedLevelName;
        if (Application.isPlaying)
        {
            if (_instance == null || sceneName != _currentScene)
            {            
                _instance = GameObject.FindObjectOfType(typeof(ViewPresenter)) as ViewPresenter;
                _currentScene = sceneName;
            }
        }
        else
        {
            if (!GameObject.FindWithTag("ViewPresenter"))
            {
                _instance = null;
                return;
            }
            _instance = GameObject.FindWithTag("ViewPresenter").GetComponent<ViewPresenter>();
            _currentScene = sceneName;
        }

    }


    public static ViewPresenter Instance
    {
        get
        {
            UpdateInstance();
            return _instance;
        }
    }

}


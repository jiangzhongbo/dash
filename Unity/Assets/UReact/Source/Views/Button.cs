﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UReact.Animation;
using UReact;
using UReact.Views;
#endregion

/// <summary>
/// Button view.
/// </summary>
[InternalView]
public class Button : View
{
    #region Fields

    public ViewAction Click;
    public ViewAction Hover;
    public ViewAction Press;

    public Label ButtonLabel;
    public Image ButtonImage;

    [ChangeHandler("UpdateBehavior")]
    public bool Disabled;

    [ChangeHandler("UpdateBehavior")]
    public bool IsToggleButton;

    [ChangeHandler("UpdateBehavior")]
    public bool ToggleValue;

    [ChangeHandler("UpdateBehavior")]
    public Color BgColor;

    [ChangeHandler("UpdateBehavior")]
    public string BgSrc;

    // button label
    public string Text;
    public string Font;
    public FontStyle FontStyle;
    public int Size;
    public Color FontColor;
    public NGUIText.Alignment TextAlign;
    public Color EffectColor;
    public Vector2 EffectDistance;
    public UILabel.Effect FontEffect;

    // animation
    // color tint animation
    [ChangeHandler("UpdateBehavior")]
    public Color HoverColor;

    [ChangeHandler("UpdateBehavior")]
    public Color PressedColor;

    [ChangeHandler("UpdateBehavior")]
    public Color DisabledColor;

    public ViewAnimation HoverColorAnimation;
    public ViewAnimation PressedColorAnimation;

    public string Goto;

    #endregion

    #region Constructor

    public Button()
    {
        Disabled = false;
        ToggleValue = false;
        IsToggleButton = false;

        // button label
        Text = String.Empty;
        FontStyle = UnityEngine.FontStyle.Normal;
        FontColor = Color.white;
        TextAlign = NGUIText.Alignment.Center;
        EffectColor = Color.clear;
        EffectDistance = Vector2.zero;
        FontEffect = UILabel.Effect.None;

        // animation
        // color init animation
        HoverColor = Color.blue;
        PressedColor = Color.red;
        DisabledColor = Color.white;
        BgColor = Color.green;
        Text = "";
    }

    #endregion

    #region Methods

    public override void UpdateBehavior()
    {
        //SetChanged(() => HoverColor);
        //SetChanged(() => PressedColor);
        if (Disabled)
        {
            SetValue(() => BgColor, DisabledColor);
        }
        else if (IsToggleButton)
        {
            if (ToggleValue)
            {
                SetValue(() => BgColor, PressedColor);
            }
            else
            {
                SetValue(() => BgColor, BgColor);
            }
        }
        //if (string.IsNullOrEmpty(Text))
        //{
        //    SetValue(() => ButtonLabel.Enabled, false);
        //}
        base.UpdateBehavior();
    }

    public void ButtonMouseClick()
    {
        if (IsToggleButton)
        {
            SetValue(() => ToggleValue, !ToggleValue);
        }
        else
        {
            if (!string.IsNullOrEmpty(Goto) && Goto.Contains(":"))
            {
                string[] strs = Goto.Split(':');
                string switcher = strs[0];
                string method = strs[1];
                string view = strs[2];
                if (method.Equals("SwitchTo"))
                {
                    if (view.Equals("Prev"))
                    {
                        if (ViewSwitcher.GetSwitcher(switcher)) ViewSwitcher.GetSwitcher(switcher).SwitchToPrev();
                    }
                    else
                    {
                        if (ViewSwitcher.GetSwitcher(switcher)) ViewSwitcher.GetSwitcher(switcher).SwitchTo(view);
                    }
                }
                else if (method.Equals("Show"))
                {
                    if (ViewSwitcher.GetSwitcher(switcher)) ViewSwitcher.GetSwitcher(switcher).Show(view);
                }
            }
        }

    }

    public void ButtonMousePress(BoolActionData actionData)
    {
        //if (!Disabled)
        //{
        //    if (actionData.state)
        //    {
        //        PressedColorAnimation.StartAnimation();
        //    }
        //    else
        //    {
        //        PressedColorAnimation.ReverseAnimation();
        //    }

        //}
    }

    public void ButtonMouseHover(BoolActionData actionData)
    {
        //if (!Disabled)
        //{
        //    if (actionData.state)
        //    {
        //        HoverColorAnimation.StartAnimation();
        //    }
        //    else
        //    {
        //        HoverColorAnimation.ReverseAnimation();
        //    }

        //}
    }


    #endregion
}


﻿using UnityEngine;
using System.Collections;
using UReact;
[InternalView]
[RemoveComponent(typeof(UIAnchor))]
[RemoveComponent(typeof(UIStretch))]
[RemoveComponent(typeof(UIWidget))]
public class Empty : View
{
    public string Value;

    public override void Initialize()
    {
        base.Initialize();
        Width = new ElementSize(0.0f, ElementSizeUnit.Percents);
        Height = new ElementSize(0.0f, ElementSizeUnit.Percents);
        X = new ElementSize(0, ElementSizeUnit.Percents);
        Y = new ElementSize(0, ElementSizeUnit.Percents);
    }
}
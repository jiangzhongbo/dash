﻿using UnityEngine;
using System.Collections;
using UReact;
[InternalView]
public class Group : View
{
    public ViewAction Click;
    public bool UseCollider;
    public override void UpdateCollider()
    {
        base.UpdateCollider();
        if (UseCollider)
        {
            collider.enabled = true;
        }
        else
        {
            collider.enabled = false;
        }

    }
}

﻿using UnityEngine;
using System.Collections;
using UReact;
[InternalView]
[AddComponent(typeof(UISprite))]
public class Image : View
{
    public bool UseCollider;
    public ViewAction Click;
    public ViewAction Drag;
    public ViewAction Press;
    public string Altas;
    [ChangeHandler("UpdateBehavior")]
    public string Src;
    [ChangeHandler("UpdateBehavior")]
    public UISprite.Type Type = UISprite.Type.Simple;
    [ChangeHandler("UpdateBehavior")]
    public UISprite.FillDirection FillDirection = UISprite.FillDirection.Horizontal;
    [ChangeHandler("UpdateBehavior")]
    public UISprite.Flip Flip = UISprite.Flip.Nothing;
    [ChangeHandler("UpdateBehavior")]
    public Color Color = Color.white;
    [ChangeHandler("UpdateBehavior")]
    public float FillAmount = 1;
    [ChangeHandler("UpdateBehavior")]
    public UISprite.FillDirection FillDir = UISprite.FillDirection.Horizontal;

    private UISprite uiSprite;

    public override void Initialize()
    {
        base.Initialize();
        uiSprite = gameObject.GetComponent<UISprite>();
        if (!string.IsNullOrEmpty(Altas))
        {
            uiSprite.atlas = Resources.Load(Altas, typeof(UIAtlas)) as UIAtlas;
        }

        uiSprite.spriteName = Src;
        uiSprite.type = Type;
        uiSprite.fillDirection = FillDirection;
        uiSprite.flip = Flip;
        uiSprite.color = Color;
    }

    public override void UpdateBehavior()
    {
        uiSprite.color = Color;
        base.UpdateBehavior();
#if UNITY_EDITOR
        uiSprite = gameObject.GetComponent<UISprite>();
#endif
        uiSprite.spriteName = Src;
        uiSprite.type = Type;
        uiSprite.fillDirection = FillDirection;
        uiSprite.flip = Flip;
        
        uiSprite.fillAmount = FillAmount;
        uiSprite.fillDirection = FillDir;
    }

    public override void UpdateLayout()
    {
        base.UpdateLayout();
    }

    public override void UpdateCollider()
    {
        base.UpdateCollider();
        if (UseCollider)
        {
            collider.enabled = true;
        }
        else
        {
            if (Click == null)
            {
                collider.enabled = false;
            }
            else if (Click != null && Click.Entries == 0)
            {
                collider.enabled = false;
            }
        }

    }
}

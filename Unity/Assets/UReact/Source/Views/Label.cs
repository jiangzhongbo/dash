﻿using UnityEngine;
using System.Collections;
using UReact;
[InternalView]
[AddComponent(typeof(UILabel))]
public class Label : View
{
    public string Font;
    [ChangeHandler("UpdateBehavior")]
    public string Text;
    [ChangeHandler("UpdateBehavior")]
    public FontStyle FontStyle;
    [ChangeHandler("UpdateBehavior")]
    public int Size = -1;
    [ChangeHandler("UpdateBehavior")]
    public Color Color;
    [ChangeHandler("UpdateBehavior")]
    public NGUIText.Alignment TextAlign;
    [ChangeHandler("UpdateBehavior")]
    public Color EffectColor;
    [ChangeHandler("UpdateBehavior")]
    public Vector2 EffectDistance;
    [ChangeHandler("UpdateBehavior")]
    public UILabel.Effect FontEffect;

    private UILabel uiLabel;

    public Label()
    {
        Color = Color.white;
        FontStyle = FontStyle.Normal;
        TextAlign = NGUIText.Alignment.Automatic;
        FontEffect = UILabel.Effect.None;
    }

    public override void Initialize()
    {
        base.Initialize();
        uiLabel = gameObject.GetComponent<UILabel>();
        if (!string.IsNullOrEmpty(Font))
        {
            uiLabel.bitmapFont = Resources.Load(Font, typeof(UIFont)) as UIFont;
        }
        uiLabel.effectStyle = FontEffect;
        uiLabel.text = Text;
        uiLabel.fontSize = Size == -1 ? uiLabel.bitmapFont.defaultSize : Size;
        uiLabel.fontStyle = FontStyle;
        uiLabel.color = Color;
        uiLabel.alignment = TextAlign;
        uiLabel.effectColor = EffectColor;
        uiLabel.effectDistance = EffectDistance;
    }

    public override void UpdateBehavior()
    {
        base.UpdateBehavior();
#if UNITY_EDITOR
        uiLabel = gameObject.GetComponent<UILabel>();
#endif
        uiLabel.effectStyle = FontEffect;
        uiLabel.text = Text;
        uiLabel.fontSize = Size == -1 ? uiLabel.bitmapFont.defaultSize : Size;
        uiLabel.fontStyle = FontStyle;
        uiLabel.color = Color;
        uiLabel.alignment = TextAlign;
        uiLabel.effectColor = EffectColor;
        uiLabel.effectDistance = EffectDistance;
    }

    public override void UpdateLayout()
    {
        base.UpdateLayout();
    }
}

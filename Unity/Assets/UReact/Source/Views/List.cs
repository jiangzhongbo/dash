﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UReact.Animation;
using UReact;
using UReact.Views;
#endregion

[InternalView]
[AddComponent(typeof(UIPanel))]
[AddComponent(typeof(UIScrollView))]
[RemoveComponent(typeof(UIWidget))]
public class List : View
{
    private UIPanel uiPanel;
    private UIScrollView uiScrollView;
    public UIScrollView.Movement Movement;
    public override void Initialize()
    {
#if UNITY_EDITOR
        gameObject.layer = LayerMask.NameToLayer("UI");
#endif
        base.Initialize();
        uiPanel = gameObject.GetComponent<UIPanel>();
        uiScrollView = gameObject.GetComponent<UIScrollView>();
        Movement = UIScrollView.Movement.Vertical;
    }

    public override void UpdateLayout()
    {
        base.UpdateLayout();
        uiPanel.clipping = UIDrawCall.Clipping.SoftClip;
        uiPanel.depth = NGUITools.FindInParents<UIPanel>(gameObject).depth + 1;
        uiScrollView.movement = Movement;
        uiScrollView.iOSDragEmulation = false;
        uiScrollView.restrictWithinPanel = true;
    }

    public override void UpdateEnable()
    {
        base.UpdateEnable();
        if (Enabled)
        {
            uiPanel.clipOffset = new Vector2(0, 0);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UReact;
using UReact.Views;


[InternalView]
public class ProgressBar : View
{
    public float Progress = 1;
    public string ProgressRate;
    public string ProgressBg;
    public Color ProgressRateColor;
    public Color ProgressBgColor;
    public override void Initialize()
    {
        base.Initialize();
    }

    public override void UpdateBehavior()
    {
        base.UpdateBehavior();
    }

    public override void UpdateLayout()
    {
        base.UpdateLayout();
    }

    public override void UpdateColliders()
    {
        base.UpdateColliders();
    }
}

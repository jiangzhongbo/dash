﻿#region Using Statements
using UReact.ValueConverters;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using UnityEngine;
using UReact.Views;
using UReact;
#endregion

[InternalView]
public class ViewSwitcher : View
{
    private static Dictionary<string, ViewSwitcher> cache = new Dictionary<string, ViewSwitcher>();

    public static ViewSwitcher GetSwitcher(string id)
    {
        if (cache.ContainsKey(id))
        {
            return cache[id];
        }
        return null;
    }

    #region Fields

    public string StartView;
    public bool StartViewSet = true;

    public bool SwitchToDefault;

    public string TransitionIn;

    public string TransitionOut;

    public ViewAnimation TransitionInAnimation;

    public ViewAnimation TransitionOutAnimation;

    [NotSetFromXml]
    public int ViewCount;

    #endregion

    #region Constructor

    public ViewSwitcher()
    {
        SwitchToDefault = true;
        StartViewSet = true;
    }

    #endregion

    #region Methods

    public override void Initialize()
    {
        base.Initialize();
        if (!string.IsNullOrEmpty(Id))
        {
            cache[Id] = this;
        }

        if (!StartViewSet)
        {
            if (SwitchToDefault)
            {
                SwitchTo(0, false);
            }
            else
            {
                gameObject.ForEachChild<View>(x => x.Deactivate(), false);
            }
        }
        else
        {
            SwitchTo(StartView, false);
        }
    }


    public void SwitchToPrev()
    {
        if (stack.Count > 1)
        {
            stack.ToList().ForEach(e =>
            {
                Debug.Log(stack.ToList().IndexOf(e)+":"+e);
            });
            stack.Pop();
            View v = stack.Pop();
            SwitchTo(v);
        }
    }

    public void SwitchTo(View view, bool animate = true)
    {
        gameObject.ForEachChild<View>(x => SwitchTo(x, x == view, animate), false);
    }

    public void SwitchTo(string id, bool animate = true)
    {
        gameObject.ForEachChild<View>(x => {
            SwitchTo(x, String.Equals(x.Id, id, StringComparison.OrdinalIgnoreCase), animate);
        }, false);
    }

    public void SwitchTo(int index, bool animate = true)
    {
        int i = 0;
        gameObject.ForEachChild<View>(x =>
        {
            SwitchTo(x, index == i, animate);
            ++i;
        }, false);
    }

    public void Show(string id, bool animate = true)
    {
        gameObject.ForEachChild<View>(x =>
        {
            if (String.Equals(x.Id, id, StringComparison.OrdinalIgnoreCase))
                SwitchTo(x, true, animate);
        }, false);
    }

    private static Stack<View> stack = new Stack<View>();

    private void SwitchTo(View view, bool active, bool animate)
    {
        if (!active && view.Enabled && animate)
        {
            if (TransitionOutAnimation)
            {
                TransitionOutAnimation.SetAnimationTarget(view);
                TransitionOutAnimation.StartAnimation();
            }
            else
            {
                view.Deactivate(true);
            }
        }

        if (!animate)
        {
            if (active)
            {
                view.Activate();
            }
            else
            {
                view.Deactivate(true);
            }
        }

        if (active && animate)
        {
            if (TransitionInAnimation != null)
            {
                TransitionInAnimation.SetAnimationTarget(view);
                TransitionInAnimation.StartAnimation();
            }
            view.Activate();
        }

        if (active)
        {
            if (!stack.Contains(view))
            {
                stack.Push(view);
            }
            else
            {
                while (stack.Peek() != view)
                {
                    stack.Pop();
                }
            }
            stack.ToList().ForEach(e =>
            {
                Debug.Log(stack.ToList().IndexOf(e)+"::"+e);
            });
        }
    }

    public override void UpdateBehavior()
    {
        base.UpdateBehavior();

        if (!String.IsNullOrEmpty(TransitionIn))
        {
            TransitionInAnimation = this.GetLayoutRoot().FindView<ViewAnimation>(TransitionIn);
        }

        if (!String.IsNullOrEmpty(TransitionOut))
        {
            TransitionOutAnimation = this.GetLayoutRoot().FindView<ViewAnimation>(TransitionOut);
        }

        ViewCount = 0;
        gameObject.ForEachChild<View>(x =>
        {
            ++ViewCount;
        }, false);
    }

    #endregion
}


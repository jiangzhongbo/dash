﻿using UnityEngine;
using System.Collections;
using UReact;
using UReact.Views;
[RequireComponent(typeof(ViewBinding))]
public class Ach : View
{
    public AchItem Item1;
    public AchItem Item2;
    public AchItem Item3;
    public AchItem Item4;
    public AchItem Item5;
    public AchItem Item6;
    public AchItem Item7;
    public AchItem Item8;
    public AchItem Item9;
    public AchItem Item10;
    public AchItem Item11;
    public AchItem Item12;
    public AchItem Item13;
    public AchItem Item14;
    public AchItem Item15;
    public AchItem Item16;
    public AchItem Item17;
    public AchItem Item18;

    public override void Initialize()
    {
        base.Initialize();
        for (int i = 0; i < 18; i++)
        {
            SetContent(i, AchInfo.Load().Infos[i]);
        }
    }

    public override void OnViewEnabled()
    {
        for (int i = 0; i < 18; i++)
        {
            SetContent(i, AchInfo.Load().Infos[i]);
        }
    }

    void SetContent(int i, AchInfoPart ai)
    {
        (GetType().GetField("Item" + (i + 1)).GetValue(this) as AchItem).SetContent(ai);
    }
}

﻿using UnityEngine;
using System.Collections;
using UReact;
using UReact.Views;
public class AchItem : ListItem
{
    public _string ItemTitle;

    public _string ItemContent;

    public _string ItemState;

    public int ItemValue;

    public float ItemAmount;

    public Animate FillAnim;
    public Animate TextAnim;

    public Label ItemValueLabel;

    public override void Initialize()
    {
        base.Initialize();
        ItemTitle.Value = "name";
        ItemContent.Value = "description";
        ItemState.Value = "ach_check";
        ItemValue = 1;
        ItemAmount = 1;
    }


    public void SetContent(AchInfoPart info)
    {
        ItemTitle.Value = info.name;
        ItemContent.Value = info.description;
        ItemState.Value = info.gc_value > GameContext.Instance.GetPropertyValueByName<int>(info.gc_key) ? "ach_circle" : "ach_check";
        int value = Mathf.CeilToInt(GameContext.Instance.GetPropertyValueByName<int>(info.gc_key) * 100.0f / info.gc_value);
        float amount = GameContext.Instance.GetPropertyValueByName<int>(info.gc_key) * 1.0f / info.gc_value;
        SetValue(() => ItemValue, value);
        SetValue(() => ItemAmount, amount);
        if (info.gc_value > GameContext.Instance.GetPropertyValueByName<int>(info.gc_key))
        {
            ItemValueLabel.Activate();
        }
        else
        {
            ItemValueLabel.Deactivate();
        }

        if (info.gc_value > GameContext.Instance.GetPropertyValueByName<int>(info.gc_key))
        {
            FillAnim.Duration = TextAnim.Duration = ItemAmount;

            FillAnim.SetAnimationTarget(this);
            FillAnim.To = ItemAmount;
            FillAnim.ResetAnimation();
            FillAnim.StartAnimation();

            TextAnim.SetAnimationTarget(this);
            TextAnim.To = ItemValue;
            TextAnim.ResetAnimation();
            TextAnim.StartAnimation();

        }

    }
}

﻿using UReact;
using UReact.Views;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;
public class Cube : View
{
    public static float STEP = 2.2f;
    public ViewAction Press;
    public ViewAction Drag;
    public Empty Cubes;
    public Empty CubeParent;
    public _int CubeIndex;
    public _string Rank;
    public _string Name;
    public Button Play;
    public Button Money;
    public int index = 0;
    public _string MoneyValue;
    public List<GameObject> CubeList = new List<GameObject>();
    public override void Initialize()
    {
        base.Initialize();
        if (Cubes.transform.childCount == 0)
        {
            CubeList.Clear();
            for (int i = 0; i < CubeInfo.Load().Infos.Count; i++)
            {
                GameObject cubePrefab = Resources.Load("Cube/"+CubeInfo.Load().Infos[i].model) as GameObject;
                GameObject cube = GameObject.Instantiate(cubePrefab) as GameObject;
                cube.transform.parent = Cubes.gameObject.transform;
                cube.transform.localScale = Vector3.one;
                cube.transform.localRotation = Quaternion.identity;
                cube.transform.localPosition = new Vector3(0, 0, i * STEP);
                CubeList.Add(cube);
            }
        
            
            OnViewEnabled();
            CubeList.ForEach(e => e.Hide());
        }
        if (CubeModel.Repository.Count() == 0)
        {
            for (int i = 0; i < 40; i++)
            {
                var c = new CubeModel();
                c.ID = CubeInfo.Load().Infos[i].id;
                c.Save();
            }
        }

    }

    public override void OnViewEnabled()
    {
        Vector3 lp = Cubes.transform.localPosition;
        lp.z = 0;
        Cubes.transform.localPosition = lp;
        index = 0;

        HandleUI();

        CubeList.ForEach(e => e.Show());

    }

    public override void OnViewDisabled()
    {
        CubeList.ForEach(e => e.Hide());
    }

    private bool mDraging = false;

    public void _OnDrag(Vector2ActionData data)
    {
        mDraging = true;
        Vector3 lp = Cubes.transform.localPosition;
        lp.z += data.delta.x / 60;
        Cubes.transform.localPosition = lp;
        if (lp.z > 0)
        {
            lp.z = 0;
        }
        else if (lp.z < -STEP * 39)
        {
            lp.z = -STEP * 39;
        }
        float a = lp.z / STEP;
        index = -Mathf.RoundToInt(a);
        HandleUI();
    }

    void Buy()
    {
        CubeModel.Repository[index].Bought = true;
        CubeModel.Repository[index].Save();
        HandleUI();
    }

    void HandleUI()
    {
        CubeIndex.Value = index + 1;
        Rank.Value = CubeInfo.Load().Infos[index].rank;
        Name.Value = CubeInfo.Load().Infos[index].name;
        MoneyValue.Value = CubeInfo.Load().Infos[index].buy_price;

        if (CubeModel.Repository[index].Bought)
        {
            Play.Activate();
            Money.Deactivate();
        }
        else
        {
            Play.Deactivate();
            Money.Activate();
        }
    }

    public Vector3 GetCubeParentPos(GameObject cube)
    {
        return CubeParent.transform.InverseTransformPoint(cube.transform.position);
    }

    Vector3 target = Vector3.zero;

    public override void LateUpdate()
    {
        base.LateUpdate();
        if (!mDraging && (Cubes.transform.localPosition - target).sqrMagnitude > 0.0001f)
        {
            Cubes.transform.localPosition = Vector3.Lerp(Cubes.transform.localPosition, target, Time.deltaTime * 30);
        }
        HandleCubes();
    }

    void HandleCubes()
    {
        for (int i = 0; i < CubeList.Count; i++)
        {
            Vector3 v = GetCubeParentPos(CubeList[i]);
            float z = Mathf.Abs(v.z);
            if (z < STEP)
            {
                CubeList[i].transform.localScale = Vector3.one * ((STEP - z) * 0.2f+1);
            }
            else
            {
                CubeList[i].transform.localScale = Vector3.one;
            }
        }
    }


    public void _Press()
    {
        mDraging = false;
        Vector3 lp = Cubes.transform.localPosition;
        if (lp.z > 0)
        {
            lp.z = 0;
        }
        else if (lp.z < -STEP * 39)
        {
            lp.z = -STEP * 39;
        }
        float a = lp.z / STEP;
        lp.z = Mathf.RoundToInt(a) * STEP;
        target = lp;
        index = -Mathf.RoundToInt(a);
        GameContext.Instance.CubeId = CubeInfo.Load().Infos[index].id;
        HandleUI();
    }

}

﻿using UnityEngine;
using System.Collections;
using UReact;

public class Daily : View
{
    public ViewAction Click;
    public DailyRewardPart DRP1;
    public DailyRewardPart DRP2;
    public DailyRewardPart DRP3;
    public DailyRewardPart DRP4;
    public DailyRewardPart DRP5;

    public Image OK1;
    public Image OK2;
    public Image OK3;
    public Image OK4;
    public Image OK5;

    public override void Initialize()
    {
        base.Initialize();
        DRP1 = DailyReward.Load().Infos[0];
        DRP2 = DailyReward.Load().Infos[1];
        DRP3 = DailyReward.Load().Infos[2];
        DRP4 = DailyReward.Load().Infos[3];
        DRP5 = DailyReward.Load().Infos[4];
    }

    public void OnClick()
    {
        ViewSwitcher.GetSwitcher("VSwitcher").SwitchTo("Level");
    }
}

﻿using UnityEngine;
using System.Collections;
using UReact;
using UReact.Views;
public class End : View
{
    public void Revive()
    {
        LevelManager.Instance.Select();
        CubeManager.Instance.SetRevive();
        ViewSwitcher.GetSwitcher("VSwitcher").SwitchTo("Game");
    }

    public void Replay()
    {
        LevelManager.Instance.Select();
    }

    public override void OnViewEnabled()
    {
        SoundManager.Instance.PlayOneShot(SoundManager.Instance.fx_win);
    }

    public void PrevRevivePoint()
    {
        CubeManager.Instance.PrevRevivePoint();
    }

    public void Next()
    {
        if (!LevelManager.Instance)
        {
            return;
        }
        LevelManager.Instance.Next();
        LevelManager.Instance.Select();
        ViewSwitcher.GetSwitcher("VSwitcher").SwitchTo("Game");
    }

    public void GoLevel()
    {
        ViewSwitcher.GetSwitcher("VSwitcher").SwitchTo("Level");
        CubeManager.Instance.Stop();
        Time.timeScale = 1;
        LevelManager.Instance.HideAll();
    }
}

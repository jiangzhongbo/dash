﻿using UnityEngine;
using System.Collections;
using UReact;
using UReact.Views;
using System;
[RequireComponent(typeof(ViewBinding))]
public class Game : View
{
    public static Game Current;
    public ViewAction Touch;
    public Button PauseBtn;
    public Label DistanceLabel;
    public ShowMoney SM;
    public Button DelPointBtn;

    public _float DistanceFillAmount;
    public _string Mode;
    public _string Distance;

    public Color ProgressColor = Color.gray;

    private bool isPlaying = false;

    public Image GoldIcon1;
    public Image GoldIcon2;
    public Image GoldIcon3;
    public Image GoldIcon4;

    public bool IsGoldIcon1 = false;
    public bool IsGoldIcon2 = false;
    public bool IsGoldIcon3 = false;
    public bool IsGoldIcon4 = false;

    public void PlayAnimGold(Vector2 from)
    {
        if (!IsGoldIcon1)
        {
            IsGoldIcon1 = true;
            StartCoroutine(animLoop(GoldIcon1, from, SM.GetComponent<UIAnchor>().relativeOffset, () => IsGoldIcon1 = false));
        }
        else if (!IsGoldIcon2)
        {
            IsGoldIcon2 = true;
            StartCoroutine(animLoop(GoldIcon2, from, SM.GetComponent<UIAnchor>().relativeOffset, () => IsGoldIcon2 = false));
        }
        else if (!IsGoldIcon3)
        {
            IsGoldIcon3 = true;
            StartCoroutine(animLoop(GoldIcon3, from, SM.GetComponent<UIAnchor>().relativeOffset, () => IsGoldIcon3 = false));
        }
        else if (!IsGoldIcon4)
        {
            IsGoldIcon4 = true;
            StartCoroutine(animLoop(GoldIcon4, from, SM.GetComponent<UIAnchor>().relativeOffset, () => IsGoldIcon4 = false));
        }
    }

    public override void OnViewEnabled()
    {
        GoldIcon1.Deactivate();
        GoldIcon2.Deactivate();
        GoldIcon3.Deactivate();
        GoldIcon4.Deactivate();

        Current = this;
        Distance.Value = "0";
        Mode.Value = "";
        DistanceFillAmount.Value = 0;
        if (CubeManager.Instance && !CubeManager.Instance.IsPractice)
        {
            DelPointBtn.Deactivate();
            Mode.Value = "NORMAL";
        }
        else
        {
            DelPointBtn.Activate();
            Mode.Value = "PRACTICE";
        }
        PlayGame();
    }

    void Start()
    {
        if (CubeManager.Instance)
        {
            CubeManager.Instance.Failed += () =>
            {
                lateCall();
            };

            CubeManager.Instance.Success += () =>
            {
                lateCall();
            };
        }

    }

    public void Jump()
    {
        CubeManager.Instance.Jump();
    }


    public void PlayGame()
    {
        isPlaying = true;
        DistanceLabel.Activate();
        if (CubeManager.Instance) CubeManager.Instance.Play();
        if (SoundManager.Instance) SoundManager.Instance.PlayLoop(SoundManager.Instance.m_scene1);
    }


    public void PauseGame()
    {
        ViewSwitcher.GetSwitcher("VSwitcher").SwitchTo("Pause");
    }

    public void ResumeGame()
    {
        ViewSwitcher.GetSwitcher("VSwitcher").SwitchTo("Game");
    }

    void Update()
    {
        if (!isPlaying) return;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            CubeManager.Instance.Jump();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 0)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }

        float v = (CubeManager.Instance.transform.position.z / EndTrigger.Instance.transform.position.z) * 100;

        if (MapManager.Instance) Distance.Value = v.ToString("0");
        if (MapManager.Instance) DistanceFillAmount.Value = v / 100;
    }


    public void PlayBlow()
    {
        GameObject blow = Resources.Load("Effect/FX_blow") as GameObject;
        var go = GameObject.Instantiate(blow) as GameObject;
        go.transform.position = CubeManager.Instance.gameObject.transform.position;
    }

    public void DelRevivePoint()
    {
        CubeManager.Instance.PrevRevivePoint();
    }


    void lateCall()
    {
        StartCoroutine(call());
    }

    IEnumerator call()
    {
        PlayBlow();
        yield return new WaitForSeconds(1);

        isPlaying = false;
        SoundManager.Instance.StopLoop(SoundManager.Instance.m_scene1);
        if (CubeManager.Instance.IsPractice && CubeManager.Instance.State != GameState.Success)
        {
            CubeManager.Instance.SetRevive();
            OnViewEnabled();
        }
        else
        {
            ViewSwitcher.GetSwitcher("VSwitcher").SwitchTo("End");

        }
    }

    IEnumerator animLoop(Image gold, Vector2 from, Vector2 to, Action action)
    {
        float t = 0;
        gold.Activate();
        while (true)
        {
            gold.GetComponent<UIAnchor>().relativeOffset = Vector2.Lerp(from, to, t);
            t += Time.deltaTime;
            if (t > 1)
            {
                break;
            }
            yield return null;
        }
        gold.Deactivate();
        action();
    }
}

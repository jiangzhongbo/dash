﻿using UnityEngine;
using System.Collections;
using UReact;
using UReact.Views;

public class Index : View
{
    public Label FPSLabel;
    void Start()
    {
        FPSLabel.gameObject.AddComponent<TestFPS>().Label = FPSLabel.gameObject;
    }
}

﻿using UReact;
using UReact.Views;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(ViewBinding))]
public class Level : View
{
    public ViewAction Drag;
    public ViewAction Press;
    public int LevelNum = 1;
    public int LevelMax = 0;
    public Color NormalBarColor = Color.gray;
    public Color PracticeBarColor = Color.gray;

    public ViewAnimation N100ToZ0Animation;
    public ViewAnimation Z0ToP100Animation;

    public ViewAnimation P100ToZ0Animation;
    public ViewAnimation Z0ToN100Animation;

    public Group Card1;
    public Group Card2;

    public Group MainCard;
    public Group LevelCards;

    public bool InMainCard = true;

    public string LockTitle1;
    public string LockTitle2;

    public float NormalProgress1;
    public float PracticeProgress1;

    public int NormalProgressValue1;
    public int PracticeProgressValue1;

    public float NormalProgress2;
    public float PracticeProgress2;

    public int NormalProgressValue2;
    public int PracticeProgressValue2;

    public Group UnlockCard1;
    public Group LockCard1;

    public Group UnlockCard2;
    public Group LockCard2;

    public Image New1;
    public Image New2;

    public ShowMoney SM;

    public Image Lock1;
    public Image Lock2;

    public override void Initialize()
    {
        base.Initialize();
        SetValue(() => LevelMax, LevelManager.Instance.MAX);
        LevelModel.Repository.Pull();
        if (LevelModel.Repository.Count() == 0)
        {
            for (int i = 0; i < LevelManager.Instance.MAX; i++)
            {
                var l = new LevelModel();
                l.Bought = LevelInfo.Load().Infos[i].locked == 0;
                l.Save();
            }
        }
        GameContext.Instance.st_time_played = 4;
        GameContext.Instance.Save();
        if (GameSceneManager.Instance) GameSceneManager.Instance.Select(GetSceneId(LevelManager.Instance.GetLevelInfo().scene));

    }

    public void MainPlay()
    {
        if (!LevelManager.Instance) return;
        if (P100ToZ0Animation.IsAnimationRunning) return;
        if (Z0ToN100Animation.IsAnimationRunning) return;

        SetValue(() => Card1.X, "0%");
        SetValue(() => Card2.X, "100%");

        P100ToZ0Animation.SetAnimationTarget(LevelCards);
        Z0ToN100Animation.SetAnimationTarget(MainCard);

        P100ToZ0Animation.ResetAnimation();
        Z0ToN100Animation.ResetAnimation();
        P100ToZ0Animation.StartAnimation();
        Z0ToN100Animation.StartAnimation();

        UpdateCard1(LevelModel.Repository[LevelManager.Instance.Level - 1]);
        if (LevelManager.Instance.Level < LevelManager.Instance.MAX)
        {
            UpdateCard2(LevelModel.Repository[LevelManager.Instance.Level]);
        }

        InMainCard = false;
        if (GameSceneManager.Instance) GameSceneManager.Instance.Select(GetSceneId(LevelManager.Instance.GetLevelInfo().scene));
    }

    public void Back()
    {
        if (N100ToZ0Animation.IsAnimationRunning) return;
        if (Z0ToP100Animation.IsAnimationRunning) return;
        if (InMainCard) return;

        SetValue(() => Card1.X, "0%");
        SetValue(() => Card2.X, "100%");

        N100ToZ0Animation.SetAnimationTarget(MainCard);
        Z0ToP100Animation.SetAnimationTarget(LevelCards);
        InMainCard = true;

        N100ToZ0Animation.ResetAnimation();
        Z0ToP100Animation.ResetAnimation();
        N100ToZ0Animation.StartAnimation();
        Z0ToP100Animation.StartAnimation();
    }

    public void Left()
    {
        if (!LevelManager.Instance) return;

        if (N100ToZ0Animation.IsAnimationRunning) return;
        if (Z0ToP100Animation.IsAnimationRunning) return;
        if (InMainCard) return;
        if (LevelManager.Instance.Level == 1)
        {
            N100ToZ0Animation.SetAnimationTarget(MainCard);
            Z0ToP100Animation.SetAnimationTarget(LevelCards);
            InMainCard = true;
        }
        else
        {
            N100ToZ0Animation.SetAnimationTarget(Card2);
            Z0ToP100Animation.SetAnimationTarget(Card1);
            InMainCard = false;

            UpdateCard1(LevelModel.Repository[LevelManager.Instance.Level - 1]);
            if (LevelManager.Instance.Level - 2 >= 0)
            {
                UpdateCard2(LevelModel.Repository[LevelManager.Instance.Level - 2]);
            }
        }

        SetValue(() => LevelNum, LevelManager.Instance.Prev());
        if (GameSceneManager.Instance) GameSceneManager.Instance.Select(GetSceneId(LevelManager.Instance.GetLevelInfo().scene));

        N100ToZ0Animation.ResetAnimation();
        Z0ToP100Animation.ResetAnimation();
        N100ToZ0Animation.StartAnimation();
        Z0ToP100Animation.StartAnimation();
    }

    void UpdateCard1(LevelModel lm)
    {
        if (lm.Bought)
        {
            UnlockCard1.Activate();
            LockCard1.Deactivate();
            Lock1.Deactivate();
        }
        else
        {
            UnlockCard1.Deactivate();
            LockCard1.Activate();
            Lock1.Activate();
        }


        if (lm.Bought && !lm.IsNotFirsPlay)
        {
            New1.Activate();
        }
        else
        {
            New1.Deactivate();
        }

        SetValue(() => NormalProgress1, lm.NormalProgress);
        SetValue(() => PracticeProgress1, lm.PracticeProgress);
        SetValue(() => LockTitle1, lm.UID+1);
    }

    void UpdateCard2(LevelModel lm)
    {
        if (lm.Bought)
        {
            UnlockCard2.Activate();
            LockCard2.Deactivate();
            Lock2.Deactivate();
        }
        else
        {
            UnlockCard2.Deactivate();
            LockCard2.Activate();
            Lock2.Activate();
        }

        if (lm.Bought && !lm.IsNotFirsPlay)
        {
            New2.Activate();
        }
        else
        {
            New2.Deactivate();
        }

        SetValue(() => NormalProgress2, lm.NormalProgress);
        SetValue(() => PracticeProgress2, lm.PracticeProgress);

        SetValue(() => LockTitle2, lm.UID + 1);
    }

    public void Right()
    {
        if (!LevelManager.Instance) return;

        if (P100ToZ0Animation.IsAnimationRunning) return;
        if (Z0ToN100Animation.IsAnimationRunning) return;

        if (LevelManager.Instance.Level == LevelManager.Instance.MAX)
        {
            InMainCard = false;
            return;
        }
        else
        {
            P100ToZ0Animation.SetAnimationTarget(Card2);
            Z0ToN100Animation.SetAnimationTarget(Card1);

            UpdateCard1(LevelModel.Repository[LevelManager.Instance.Level - 1]);
            if (LevelManager.Instance.Level < LevelManager.Instance.MAX)
            {
                UpdateCard2(LevelModel.Repository[LevelManager.Instance.Level]);
            }

            P100ToZ0Animation.ResetAnimation();
            Z0ToN100Animation.ResetAnimation();
            P100ToZ0Animation.StartAnimation();
            Z0ToN100Animation.StartAnimation();
            InMainCard = false;
        }

        SetValue(() => LevelNum, LevelManager.Instance.Next());
        if (GameSceneManager.Instance) GameSceneManager.Instance.Select(GetSceneId(LevelManager.Instance.GetLevelInfo().scene));

    }

    public void Play()
    {
        if (LevelManager.Instance) LevelManager.Instance.SelectLevel(LevelManager.Instance.Level);
        ViewSwitcher.GetSwitcher("VSwitcher").SwitchTo("Game");
    }

    public void UnlockButton()
    {
        if (P100ToZ0Animation.IsAnimationRunning) return;
        if (Z0ToN100Animation.IsAnimationRunning) return;
        if (N100ToZ0Animation.IsAnimationRunning) return;
        if (Z0ToP100Animation.IsAnimationRunning) return;

        GameContext.Instance.SpendGolds(100);

        LevelModel.Repository[LevelManager.Instance.Level - 1].Bought = true;
        LevelModel.Repository[LevelManager.Instance.Level - 1].NormalProgress  =0.4f;
        LevelModel.Repository[LevelManager.Instance.Level - 1].PracticeProgress = 0.8f;
        LevelModel.Repository[LevelManager.Instance.Level - 1].Save();

        SetValue(() => Card1.X, "0%");
        SetValue(() => Card2.X, "100%");
        SM.UpdateCount();

        UpdateCard1(LevelModel.Repository[LevelManager.Instance.Level - 1]);
    }



    float sum = 0;

    public void _OnDrag(Vector2ActionData data)
    {
        sum += data.delta.x;
        if (sum < -100)
        {
            sum = 0;
            if (InMainCard) MainPlay();
            else Right();
        }
        else if (sum > 100)
        {
            sum = 0;
            Left();
        }
    }

    public void _Press()
    {
        sum = 0;
    }

    public int GetSceneId(string scene)
    {
        switch (scene)
        {
            case "scene_1":
                return 1;
            case "scene_2":
                return 2;
            case "scene_3":
                return 3;
            case "scene_4":
                return 4;
        }
        return 1;
    }
}

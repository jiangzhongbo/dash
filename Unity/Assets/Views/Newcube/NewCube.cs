﻿using UnityEngine;
using System.Collections;
using UReact;
using UReact.Views;
using System.Linq;
public class Newcube : View
{
    public ViewAction Click;

    public Empty Cube;

    public GameObject cube;

    public _string Name;

    public _string Type;

    public Label NewCubeLabel;

    public override void Initialize()
    {
        base.Initialize();
        if (cube)
        {
            return;
        }
        GameObject prefab = Resources.Load("Cube/Cube") as GameObject;
        var go = Instantiate(prefab) as GameObject;
        go.transform.parent = Cube.transform;
        go.transform.localPosition = Vector3.zero;
        go.transform.localRotation = Quaternion.identity;
        go.transform.localScale = Vector3.one;
        cube = go;
        cube.Hide();
    }

    public override void OnViewEnabled()
    {
        if (CubeModel.Repository.Count() == 0)
        {
            for (int i = 0; i < 40; i++)
            {
                var c = new CubeModel();
                c.ID = CubeInfo.Load().Infos[i].id;
                c.Save();
            }
        }
        if (cube) cube.Show();

        var ItemInfo = CubeInfo.Load().Infos[Random.Range(0, CubeInfo.Load().Infos.Count())];
        CubeModel cm = CubeModel.Repository.Where(e => e.ID == ItemInfo.id).First();
        if (cm.Bought)
        {
            NewCubeLabel.Deactivate();
        }
        else
        {
            cm.Bought = true;
            cm.Save();
            NewCubeLabel.Activate();
        }
        Name.Value = ItemInfo.name;
        Type.Value = ItemInfo.rank;
    }

    public override void OnViewDisabled()
    {
        if(cube) cube.Hide();
    }

    public void OnClick()
    {
        cube.animation.Play("cube_rotate");
    }

}

﻿using UnityEngine;
using System.Collections;
using UReact;
using UReact.Views;
public class Pause : View
{
    public Image PracticeBanImage;
    public string LevelName;
    public override void OnViewEnabled()
    {
        if (CubeManager.Instance && CubeManager.Instance.IsPractice) PracticeBanImage.Deactivate();
        else PracticeBanImage.Activate();
        Time.timeScale = 0;
        LevelName = LevelManager.Instance.GetLevelInfo().name;
        if (CubeManager.Instance) CubeManager.Instance.Pause();
    }

    public override void OnViewDisabled()
    {
    }

    public void PlayGame()
    {
        CubeManager.Instance.SetRevive();
        ViewSwitcher.GetSwitcher("VSwitcher").SwitchTo("Game");
        Time.timeScale = GameContext.Instance.TimeScale;
    }

    public void SetPractice()
    {
        CubeManager.Instance.IsPractice = !CubeManager.Instance.IsPractice;
        PlayGame();
    }

    public void GoLevel()
    {
        ViewSwitcher.GetSwitcher("VSwitcher").SwitchTo("Level");
        CubeManager.Instance.Stop();
        Time.timeScale = GameContext.Instance.TimeScale;
        LevelManager.Instance.HideAll();
    }

    public void GoBuyNew()
    {

    }
}

﻿using UnityEngine;
using System.Collections;
using UReact;
using UReact.Views;
[RequireComponent(typeof(ViewBinding))]
public class Shop : View
{
    public string DoubleBoxContent = "Double Income In Game";
    public int Golds_1_Num = 100;
    public int Golds_2_Num = 100;
    public int Golds_3_Num = 100;
    public int Golds_4_Num = 100;
    public int Golds_5_Num = 100;
    public int Golds_6_Num = 100;

    public float Golds_Double = 2.99f;
    public float Golds_1 = 2.99f;
    public float Golds_2 = 2.99f;
    public float Golds_3 = 2.99f;
    public float Golds_4 = 2.99f;
    public float Golds_5 = 2.99f;
    public float Golds_6 = 2.99f;

    public ShowMoney SM;

    public override void Initialize()
    {
        base.Initialize();
        for (int i = 0; i < 6; i++)
        {
            GetType().GetField("Golds_" + (i + 1) + "_Num").SetValue(this, Currency.Load().Infos[i].count);
            GetType().GetField("Golds_" + (i + 1)).SetValue(this, Currency.Load().Infos[i].price_dollars);
        }
    }

    public void BuyDouble()
    {
        GameContext.Instance.SetDoubleIncome(true);
    }

    public void Buy1()
    {
        GameContext.Instance.AddGolds(Golds_1_Num);
        SM.UpdateCount();
    }

    public void Buy2()
    {
        GameContext.Instance.AddGolds(Golds_2_Num);
        SM.UpdateCount();
    }

    public void Buy3()
    {
        GameContext.Instance.AddGolds(Golds_3_Num);
        SM.UpdateCount();
    }

    public void Buy4()
    {
        GameContext.Instance.AddGolds(Golds_4_Num);
        SM.UpdateCount();
    }

    public void Buy5()
    {
        GameContext.Instance.AddGolds(Golds_5_Num);
        SM.UpdateCount();
    }

    public void Buy6()
    {
        GameContext.Instance.AddGolds(Golds_6_Num);
        SM.UpdateCount();
    }
}

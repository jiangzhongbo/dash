﻿using UReact;
using UReact.Views;
public class ShowMoney : View
{
    public static ShowMoney Current;

    public ViewAction Click; 
    public string Icon;
    public _int Count;

    public override void Initialize()
    {
        base.Initialize();
        UpdateCount();
    }

    public override void OnViewEnabled()
    {
        Current = this;
        UpdateCount();
    }


    public void GoStore()
    {
        ViewSwitcher.GetSwitcher("VSwitcher").SwitchTo("Shop");
    }

    public void UpdateCount()
    {
        Count.Value = GameContext.Instance.Golds;
    }
}
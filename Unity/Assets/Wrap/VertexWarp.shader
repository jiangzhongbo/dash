
Shader "ZHH/shadowProject" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_StrengthRadius ("StrengthRadius" , Vector) = (1,1,1,1)
		}
	
	SubShader {
		Tags { "Queue" = "Transparent+1" }
		
		Pass {
	
			Program "vp" {
// Vertex combos: 1
//   opengl - ALU: 73 to 73
//   d3d9 - ALU: 73 to 73
//   d3d11 - ALU: 29 to 29, TEX: 0 to 0, FLOW: 1 to 1
//   d3d11_9x - ALU: 29 to 29, TEX: 0 to 0, FLOW: 1 to 1
SubProgram "opengl " {
Keywords { }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Matrix 1 [_Object2World]
Matrix 5 [unity_MatrixVP]
Vector 17 [p_vVertexWarpModelPosition]
Matrix 9 [vTransform]
Matrix 13 [vInvTransform]
Vector 18 [_StrengthRadius]
"!!ARBvp1.0
# 73 ALU
PARAM c[23] = { { 1.0063684, 0, 0.33333334, 1 },
		program.local[1..18],
		{ 1.5, 6, 5 },
		{ 0.25, 0.5, 0.75, 1 },
		{ 1, -1, -24.980801, 60.145809 },
		{ -85.453789, 64.939346, -19.73921, 1 } };
TEMP R0;
TEMP R1;
TEMP R2;
TEMP R3;
TEMP R4;
TEMP R5;
DP4 R4.z, vertex.position, c[4];
MOV R0.w, R4.z;
DP4 R0.z, vertex.position, c[3];
DP4 R0.x, vertex.position, c[1];
DP4 R0.y, vertex.position, c[2];
DP4 R3.x, R0, c[13];
DP4 R3.y, R0, c[14];
MUL R1.xy, R3, R3;
ADD R1.x, R1, R1.y;
RSQ R1.x, R1.x;
RCP R4.w, c[18].y;
RCP R1.x, R1.x;
MUL R1.x, R1, R4.w;
MIN R1.x, R1, c[0].w;
MAX R5.x, R1, c[0].y;
MAD R1.x, -R5, R5, c[0].w;
MAD R1.x, R1, c[19].y, -c[19].z;
POW R1.x, c[19].x, R1.x;
MAX R1.y, R1.x, c[0];
ADD R1.x, -R5, c[0].w;
MUL R1.x, R1, R1.y;
MUL R1.x, R1, c[18];
MUL R1.x, R1, c[0].z;
MUL R1.x, R1, R1;
MUL R1.x, R1, c[0];
FRC R2.w, R1.x;
SLT R1, R2.w, c[20];
ADD R2.xyz, R1.yzww, -R1;
MOV R1.yzw, R2.xxyz;
DP4 R3.w, R1, c[20].xxzz;
DP3 R3.z, R2, c[20].yyww;
ADD R2.xy, R2.w, -R3.zwzw;
MUL R3.zw, R2.xyxy, R2.xyxy;
MUL R4.xy, R3.zwzw, R3.zwzw;
MUL R2, R4.xxyy, c[21].zwzw;
ADD R2, R2, c[22].xyxy;
MAD R2, R2, R4.xxyy, c[22].zwzw;
MAD R2.xy, R2.xzzw, R3.zwzw, R2.ywzw;
DP4 R3.z, R0, c[15];
DP4 R2.w, R1, c[21].xxyy;
DP4 R2.z, R1, c[21].xyyx;
MUL R2.xy, R2.zwzw, R2;
MOV R0.xyz, c[17];
MOV R1.x, -R2.y;
MOV R1.y, R2.x;
MOV R1.z, c[0].y;
DP3 R1.w, R3, R1;
MOV R0.w, c[0];
DP4 R1.y, R0, c[14];
DP4 R1.x, R0, c[13];
MOV R1.z, c[0].y;
DP3 R0.x, R1, R1;
MOV R2.z, c[0].y;
RSQ R0.x, R0.x;
RCP R0.y, R0.x;
MUL R0.z, R4.w, R0.y;
MOV R0.w, R4.z;
MOV R2.w, c[0];
DP3 R0.x, R3, c[0].yyww;
POW R0.y, R5.x, c[19].y;
MUL R1.y, R0.x, R0;
MIN R0.z, R0, c[0].w;
MAX R0.x, R0.z, c[0].y;
DP3 R1.x, R3, R2;
MUL R2.xyz, R1.xwyw, R0.x;
DP4 R0.z, R2, c[11];
DP4 R0.y, R2, c[10];
DP4 R0.x, R2, c[9];
DP4 result.position.w, R0, c[8];
DP4 result.position.z, R0, c[7];
DP4 result.position.y, R0, c[6];
DP4 result.position.x, R0, c[5];
MOV result.texcoord[0].xy, vertex.texcoord[0];
END
# 73 instructions, 6 R-regs
"
}

SubProgram "d3d9 " {
Keywords { }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Matrix 0 [_Object2World]
Matrix 4 [unity_MatrixVP]
Vector 16 [p_vVertexWarpModelPosition]
Matrix 8 [vTransform]
Matrix 12 [vInvTransform]
Vector 17 [_StrengthRadius]
"vs_2_0
; 73 ALU
def c18, 0.00000000, 1.00000000, 6.00000000, 1.50000000
def c19, 6.00000000, -5.00000000, 0.33333334, 0
def c20, 1.00636840, 0.50000000, 6.28318501, -3.14159298
def c21, -0.00000155, -0.00002170, 0.00260417, 0.00026042
def c22, -0.02083333, -0.12500000, 1.00000000, 0.50000000
dcl_position0 v0
dcl_texcoord0 v1
dp4 r2.w, v0, c3
mov r1.w, r2
dp4 r1.z, v0, c2
dp4 r1.x, v0, c0
dp4 r1.y, v0, c1
dp4 r2.x, r1, c12
dp4 r2.y, r1, c13
mul r0.xy, r2, r2
add r0.x, r0, r0.y
rsq r0.x, r0.x
rcp r3.w, c17.y
rcp r0.x, r0.x
mul r0.x, r0, r3.w
min r0.x, r0, c18.y
max r4.x, r0, c18
mad r0.x, -r4, r4, c18.y
mad r2.z, r0.x, c19.x, c19.y
pow r0, c18.w, r2.z
max r0.y, r0.x, c18.x
add r0.x, -r4, c18.y
mul r0.x, r0, r0.y
mul r0.x, r0, c17
mul r0.x, r0, c19.z
mul r0.x, r0, r0
mad r0.x, r0, c20, c20.y
frc r0.x, r0
mad r2.z, r0.x, c20, c20.w
sincos r0.xy, r2.z, c21.xyzw, c22.xyzw
dp4 r2.z, r1, c14
mov r0.z, c18.x
mov r3.x, -r0.y
mov r3.y, r0.x
dp3 r1.x, r2, r0
mov r3.z, c18.x
dp3 r1.y, r2, r3
mov r0.xyz, c16
mov r0.w, c18.y
dp4 r3.y, r0, c13
dp4 r3.x, r0, c12
mov r3.z, c18.x
dp3 r0.x, r3, r3
rsq r1.w, r0.x
pow r0, r4.x, c18.z
rcp r0.y, r1.w
dp3 r1.z, r2, c18.xxyw
mul r0.y, r3.w, r0
mul r1.z, r1, r0.x
min r0.y, r0, c18
max r0.x, r0.y, c18
mul r0.xyz, r1, r0.x
mov r0.w, c18.y
dp4 r1.z, r0, c10
dp4 r1.y, r0, c9
mov r1.w, r2
dp4 r1.x, r0, c8
dp4 oPos.w, r1, c7
dp4 oPos.z, r1, c6
dp4 oPos.y, r1, c5
dp4 oPos.x, r1, c4
mov oT0.xy, v1
"
}

SubProgram "xbox360 " {
Keywords { }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Matrix 0 [_Object2World] 4
Vector 17 [_StrengthRadius]
Vector 8 [p_vVertexWarpModelPosition]
Matrix 4 [unity_MatrixVP] 4
Matrix 13 [vInvTransform] 4
Matrix 9 [vTransform] 4
// Shader Timing Estimate, in Cycles/64 vertex vector:
// ALU: 54.67 (41 instructions), vertex: 32, texture: 0,
//   sequencer: 22,  4 GPRs, 31 threads,
// Performance (if enough threads): ~54 cycles per vector
// * Vertex cycle estimates are assuming 3 vfetch_minis for every vfetch_full,
//     with <= 32 bytes per vfetch_full group.

"vs_360
backbbabaaaaabmeaaaaacjiaaaaaaaaaaaaaaceaaaaabgeaaaaabimaaaaaaaa
aaaaaaaaaaaaabdmaaaaaabmaaaaabcppppoadaaaaaaaaagaaaaaabmaaaaaaaa
aaaaabciaaaaaajeaaacaaaaaaaeaaaaaaaaaakeaaaaaaaaaaaaaaleaaacaabb
aaabaaaaaaaaaameaaaaaaaaaaaaaaneaaacaaaiaaabaaaaaaaaaapaaaaaaaaa
aaaaabaaaaacaaaeaaaeaaaaaaaaaakeaaaaaaaaaaaaabapaaacaaanaaaeaaaa
aaaaaakeaaaaaaaaaaaaabbnaaacaaajaaaeaaaaaaaaaakeaaaaaaaafpepgcgk
gfgdhedcfhgphcgmgeaaklklaaadaaadaaaeaaaeaaabaaaaaaaaaaaafpfdhehc
gfgoghhegifcgbgegjhfhdaaaaabaaadaaabaaacaaabaaaaaaaaaaaahafphgfg
gfhchegfhifhgbhchaengpgegfgmfagphdgjhegjgpgoaaklaaabaaadaaabaaad
aaabaaaaaaaaaaaahfgogjhehjfpengbhehcgjhifgfaaahgejgohgfehcgbgohd
gggphcgnaahgfehcgbgohdgggphcgnaahghdfpddfpdaaadccodacodcdadddfdd
codaaaklaaaaaaaaaaaaaaabaaaaaaaaaaaaaaaaaaaaaabeaapmaabaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaeaaaaaacfiaaabaaadaaaaaaaaaaaaaaaa
aaaaaicbaaaaaaabaaaaaaacaaaaaaabaaaaacjaaabaaaagaadafaahaaaadafa
aaaabadaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaadpaaaaaadpianakpdpbfmabk
maejapnldokkkkkldpiaaaaaeagakacheamjapnlaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaadaafcaagaaaabcaamcaaaaaaaaaagaaigaaobcaabcaaaaaaaaaagabe
gabkbcaabcaaaaaaaaaagacagacgbcaabcaaaaaaaaaaeacmaaaabcaameaaaaaa
aaaabadaaaaaccaaaaaaaaaaafpicaaaaaaaagiiaaaaaaaaafpiaaaaaaaaaohi
aaaaaaaamiakaaaaaagbmggbalapaibamiakaaaaaagblbbbclaoaiaamiapaaab
aabliiaakbacadaamiapaaabaamgiiaaklacacabmiakaaaaaagbgmbbclanaiaa
miapaaabaalbdejeklacababmiapaaabaagmnajeklacaaabaiboacacaamgpmlb
kbabbaaamiaoaaacaablpmabklabapacmiaoaaacaalbpmabklabaoacmiahaaad
aagmmabfklabanacaicmacacaakmkmblobadadaaembkacaaaammbblbmaacacbb
kacaaaaaaaaaaalbocaaaaiakaiaaaaaaaaaaablocaaaaiamjajaaacaagmldaa
obacaaaaaieaacaaaaaaaagmocaaaaacmiacaaaaaemgmgmgilacpopndiciaaaa
aegmlblbkaacpoaamiacaaaaaalbgmaakcaappaakicaaaaaaaaaaaebmcaaaapo
miacaaaaaalbblaaobaaaaaakicaacaaaaaaaaebmcaaaabbmiakaaaaaalglgaa
obacacaamiacaaaaaalbmgaaobaaacaamiaiaaaaaabllbgmilaapnpncmicaaaa
aalbmgblobaaadaamiaiaaaaaablblblilaapopnmaccaaacaalbblblobaaacaa
miahaaacaalbleleilacalammeiaaaaaaaaaaablocaaaaaabeapaaabaamgiigm
kbabahadamihadadabgbbjlbobadaaaamiakaaaaaalmbgaaoaadadaamiakaaaa
aablbbaaobacaaaamiahaaacaablmaleklaaakacmiahaaacaalbleleklaaajac
miapaaabaalbiiaaklacagabmiapaaabaamgdejeklacafabmiapiadoaagmaade
klacaeabmiadiaaaaamemeaaocaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
}

SubProgram "ps3 " {
Keywords { }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Matrix 256 [_Object2World]
Matrix 260 [unity_MatrixVP]
Vector 467 [p_vVertexWarpModelPosition]
Matrix 264 [vTransform]
Matrix 268 [vInvTransform]
Vector 466 [_StrengthRadius]
"sce_vp_rsx // 48 instructions using 5 registers
[Configuration]
8
0000003001010500
[Defaults]
2
465 4
3f80000040c000003f15c01a40a00000
464 3
000000003eaaaaab40ca57a8
[Microcode]
768
00011c6c0050c00d8186c0836041fffc00009c6c0050d00d8186c0836041fffc
00001c6c01d0200d8106c0c360405ffc00001c6c01d0100d8106c0c360409ffc
00019c6c01d0300d8106c0c360403ffc00001c6c01d0000d8106c0c360411ffc
00001c6c0040007f8686c08360403ffc00021c6c01d0d00d8086c0c360409ffc
00021c6c01d0c00d8086c0c360411ffc00019c6c019d300c0186c14360409ffc
00009c6c008000080884044360419ffc00009c6c00c000000286c08aa0a11ffc
00019c6c019d300c0186c24360411ffc001f9c6c2000000d8106c08000a800fc
00011c6c115d200c0686034aa07100fc401f9c6c104008080106c08aa0a9809c
00009c6c205d00000186c080013041fc04009c6c0080002a8280014360409ffc
00011c6c011d10aa8295414000611ffc00011c6c105d00000186c08001a240fc
00011c6c011d1000049540ffe0611ffc04009c6c0080000002bfc14360403ffc
00009c6c009d100004aa80c360411ffc00021c6c71d0e00d8086c0c000b040fc
00001c6c00dd10000186c0aaa0a11ffc00001c6c029d0000028000c360409ffc
00001c6c008000000095404360411ffc00001c6c009d2000008000c360411ffc
00009c6c009d0000009540c360411ffc00011c6c688000000280014aa0b1007c
00009c6c009d000004aa80c360411ffc00001c6c789d1055088000c000b0417c
00001c6c809d1000009540c000b100fc00009c6c004000000486c08360409ffc
00011c6c004000800486c08360411ffc00011c6c704000000286c0800022807c
00001c6c0140000c0486044360409ffc00001c6c0140000c0886014360411ffc
00001c6c0080005500bfc04360405ffc00009c6c0080000c00bfc1436041dffc
00001c6c0190a00c0286c0c360405ffc00001c6c0190900c0286c0c360409ffc
00001c6c0190800c0286c0c360411ffc00001c6c0040007f8686c08360403ffc
401f9c6c01d0700d8086c0c360403f80401f9c6c01d0600d8086c0c360405f80
401f9c6c01d0500d8086c0c360409f80401f9c6c01d0400d8086c0c360411f81
"
}

SubProgram "d3d11 " {
Keywords { }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
ConstBuffer "$Globals" 176 // 168 used size, 5 vars
Vector 0 [p_vVertexWarpModelPosition] 3
Matrix 32 [vTransform] 4
Matrix 96 [vInvTransform] 4
Vector 160 [_StrengthRadius] 2
ConstBuffer "UnityPerDraw" 336 // 256 used size, 6 vars
Matrix 192 [_Object2World] 4
ConstBuffer "UnityPerFrame" 208 // 208 used size, 4 vars
Matrix 144 [unity_MatrixVP] 4
BindCB "$Globals" 0
BindCB "UnityPerDraw" 1
BindCB "UnityPerFrame" 2
// 49 instructions, 5 temp regs, 0 temp arrays:
// ALU 29 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0
eefiecednankdbmbhljjedohjhomkahblnkbnibeabaaaaaaemahaaaaadaaaaaa
cmaaaaaaiaaaaaaaniaaaaaaejfdeheoemaaaaaaacaaaaaaaiaaaaaadiaaaaaa
aaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapapaaaaebaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaadadaaaafaepfdejfeejepeoaafeeffiedepepfceeaaklkl
epfdeheofaaaaaaaacaaaaaaaiaaaaaadiaaaaaaaaaaaaaaabaaaaaaadaaaaaa
aaaaaaaaapaaaaaaeeaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaadamaaaa
fdfgfpfagphdgjhegjgpgoaafeeffiedepepfceeaaklklklfdeieefcgmagaaaa
eaaaabaajlabaaaafjaaaaaeegiocaaaaaaaaaaaalaaaaaafjaaaaaeegiocaaa
abaaaaaabaaaaaaafjaaaaaeegiocaaaacaaaaaaanaaaaaafpaaaaadpcbabaaa
aaaaaaaafpaaaaaddcbabaaaabaaaaaaghaaaaaepccabaaaaaaaaaaaabaaaaaa
gfaaaaaddccabaaaabaaaaaagiaaaaacafaaaaaadiaaaaajdcaabaaaaaaaaaaa
fgifcaaaaaaaaaaaaaaaaaaaegiacaaaaaaaaaaaahaaaaaadcaaaaaldcaabaaa
aaaaaaaaegiacaaaaaaaaaaaagaaaaaaagiacaaaaaaaaaaaaaaaaaaaegaabaaa
aaaaaaaadcaaaaaldcaabaaaaaaaaaaaegiacaaaaaaaaaaaaiaaaaaakgikcaaa
aaaaaaaaaaaaaaaaegaabaaaaaaaaaaaaaaaaaaidcaabaaaaaaaaaaaegaabaaa
aaaaaaaaegiacaaaaaaaaaaaajaaaaaaapaaaaahbcaabaaaaaaaaaaaegaabaaa
aaaaaaaaegaabaaaaaaaaaaaelaaaaafbcaabaaaaaaaaaaaakaabaaaaaaaaaaa
aocaaaaibcaabaaaaaaaaaaaakaabaaaaaaaaaaabkiacaaaaaaaaaaaakaaaaaa
diaaaaaipcaabaaaabaaaaaafgbfbaaaaaaaaaaaegiocaaaabaaaaaaanaaaaaa
dcaaaaakpcaabaaaabaaaaaaegiocaaaabaaaaaaamaaaaaaagbabaaaaaaaaaaa
egaobaaaabaaaaaadcaaaaakpcaabaaaabaaaaaaegiocaaaabaaaaaaaoaaaaaa
kgbkbaaaaaaaaaaaegaobaaaabaaaaaadcaaaaakpcaabaaaabaaaaaaegiocaaa
abaaaaaaapaaaaaapgbpbaaaaaaaaaaaegaobaaaabaaaaaadiaaaaaiocaabaaa
aaaaaaaafgafbaaaabaaaaaaagijcaaaaaaaaaaaahaaaaaadcaaaaakocaabaaa
aaaaaaaaagijcaaaaaaaaaaaagaaaaaaagaabaaaabaaaaaafgaobaaaaaaaaaaa
dcaaaaakocaabaaaaaaaaaaaagijcaaaaaaaaaaaaiaaaaaakgakbaaaabaaaaaa
fgaobaaaaaaaaaaadcaaaaakocaabaaaaaaaaaaaagijcaaaaaaaaaaaajaaaaaa
pgapbaaaabaaaaaafgaobaaaaaaaaaaaapaaaaahbcaabaaaabaaaaaajgafbaaa
aaaaaaaajgafbaaaaaaaaaaaelaaaaafbcaabaaaabaaaaaaakaabaaaabaaaaaa
aocaaaaibcaabaaaabaaaaaaakaabaaaabaaaaaabkiacaaaaaaaaaaaakaaaaaa
dcaaaaakccaabaaaabaaaaaaakaabaiaebaaaaaaabaaaaaaakaabaaaabaaaaaa
abeaaaaaaaaaiadpdcaaaaajccaabaaaabaaaaaabkaabaaaabaaaaaaabeaaaaa
aaaamaeaabeaaaaaaaaakamadiaaaaahccaabaaaabaaaaaabkaabaaaabaaaaaa
abeaaaaabkmabfdpbjaaaaafccaabaaaabaaaaaabkaabaaaabaaaaaaaaaaaaai
ecaabaaaabaaaaaaakaabaiaebaaaaaaabaaaaaaabeaaaaaaaaaiadpdiaaaaah
bcaabaaaabaaaaaaakaabaaaabaaaaaaakaabaaaabaaaaaadiaaaaahccaabaaa
abaaaaaabkaabaaaabaaaaaackaabaaaabaaaaaadiaaaaaiccaabaaaabaaaaaa
bkaabaaaabaaaaaaakiacaaaaaaaaaaaakaaaaaadiaaaaahccaabaaaabaaaaaa
bkaabaaaabaaaaaaabeaaaaaklkkkkdodiaaaaahccaabaaaabaaaaaabkaabaaa
abaaaaaabkaabaaaabaaaaaadiaaaaahccaabaaaabaaaaaabkaabaaaabaaaaaa
abeaaaaakifhmkeaenaaaaahbcaabaaaacaaaaaabcaabaaaadaaaaaabkaabaaa
abaaaaaadgaaaaafecaabaaaaeaaaaaaakaabaaaacaaaaaadgaaaaafccaabaaa
aeaaaaaaakaabaaaadaaaaaadgaaaaagbcaabaaaaeaaaaaaakaabaiaebaaaaaa
acaaaaaaapaaaaahccaabaaaacaaaaaaegaabaaaaeaaaaaajgafbaaaaaaaaaaa
apaaaaahbcaabaaaacaaaaaajgafbaaaaeaaaaaajgafbaaaaaaaaaaadiaaaaah
ccaabaaaaaaaaaaaakaabaaaabaaaaaaakaabaaaabaaaaaadiaaaaahccaabaaa
aaaaaaaabkaabaaaaaaaaaaaakaabaaaabaaaaaadiaaaaahecaabaaaacaaaaaa
bkaabaaaaaaaaaaadkaabaaaaaaaaaaadiaaaaahhcaabaaaaaaaaaaaagaabaaa
aaaaaaaaegacbaaaacaaaaaadiaaaaaihcaabaaaabaaaaaafgafbaaaaaaaaaaa
egiccaaaaaaaaaaaadaaaaaadcaaaaaklcaabaaaaaaaaaaaegiicaaaaaaaaaaa
acaaaaaaagaabaaaaaaaaaaaegaibaaaabaaaaaadcaaaaakhcaabaaaaaaaaaaa
egiccaaaaaaaaaaaaeaaaaaakgakbaaaaaaaaaaaegadbaaaaaaaaaaaaaaaaaai
hcaabaaaaaaaaaaaegacbaaaaaaaaaaaegiccaaaaaaaaaaaafaaaaaadiaaaaai
pcaabaaaacaaaaaafgafbaaaaaaaaaaaegiocaaaacaaaaaaakaaaaaadcaaaaak
pcaabaaaacaaaaaaegiocaaaacaaaaaaajaaaaaaagaabaaaaaaaaaaaegaobaaa
acaaaaaadcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaalaaaaaakgakbaaa
aaaaaaaaegaobaaaacaaaaaadcaaaaakpccabaaaaaaaaaaaegiocaaaacaaaaaa
amaaaaaapgapbaaaabaaaaaaegaobaaaaaaaaaaadgaaaaafdccabaaaabaaaaaa
egbabaaaabaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { }
"!!GLES


#ifdef VERTEX

varying highp vec2 xlv_TEXCOORD0;
uniform mediump vec2 _StrengthRadius;
uniform mediump mat4 vInvTransform;
uniform mediump mat4 vTransform;
uniform highp vec3 p_vVertexWarpModelPosition;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 _Object2World;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesVertex;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = (_Object2World * _glesVertex);
  highp vec4 vVertexWS_2;
  vVertexWS_2.w = tmpvar_1.w;
  highp float fScalar_3;
  highp vec3 vModelPos_4;
  highp vec3 vVertex_5;
  highp vec3 tmpvar_6;
  tmpvar_6 = (vInvTransform * tmpvar_1).xyz;
  highp vec4 tmpvar_7;
  tmpvar_7.w = 1.0;
  tmpvar_7.xyz = p_vVertexWarpModelPosition;
  vModelPos_4.xy = (vInvTransform * tmpvar_7).xy;
  vModelPos_4.z = 0.0;
  highp float tmpvar_8;
  tmpvar_8 = clamp ((sqrt(dot (tmpvar_6.xy, tmpvar_6.xy)) / _StrengthRadius.y), 0.0, 1.0);
  mediump float tmpvar_9;
  tmpvar_9 = _StrengthRadius.x;
  fScalar_3 = tmpvar_9;
  highp float tmpvar_10;
  tmpvar_10 = ((((1.0 - tmpvar_8) * max (0.0, pow (1.5, (((1.0 - (tmpvar_8 * tmpvar_8)) * 6.0) - 5.0)))) * fScalar_3) / 3.0);
  mediump float fAngle_11;
  fAngle_11 = (6.3232 * (tmpvar_10 * tmpvar_10));
  mediump float tmpvar_12;
  tmpvar_12 = sin(fAngle_11);
  mediump float tmpvar_13;
  tmpvar_13 = cos(fAngle_11);
  mediump mat3 tmpvar_14;
  tmpvar_14[0].x = tmpvar_13;
  tmpvar_14[0].y = -(tmpvar_12);
  tmpvar_14[0].z = 0.0;
  tmpvar_14[1].x = tmpvar_12;
  tmpvar_14[1].y = tmpvar_13;
  tmpvar_14[1].z = -0.0;
  tmpvar_14[2].x = -0.0;
  tmpvar_14[2].y = 0.0;
  tmpvar_14[2].z = ((1.0 - tmpvar_13) + tmpvar_13);
  highp vec3 tmpvar_15;
  tmpvar_15 = (tmpvar_14 * tmpvar_6);
  vVertex_5.xy = tmpvar_15.xy;
  vVertex_5.z = (tmpvar_15.z * pow (tmpvar_8, 6.0));
  highp vec3 tmpvar_16;
  tmpvar_16 = (vVertex_5 * clamp ((sqrt(dot (vModelPos_4, vModelPos_4)) / _StrengthRadius.y), 0.0, 1.0));
  vVertex_5 = tmpvar_16;
  highp vec4 tmpvar_17;
  tmpvar_17.w = 1.0;
  tmpvar_17.xyz = tmpvar_16;
  vVertexWS_2.xyz = (vTransform * tmpvar_17).xyz;
  gl_Position = (unity_MatrixVP * vVertexWS_2);
  xlv_TEXCOORD0 = _glesMultiTexCoord0.xy;
}



#endif
#ifdef FRAGMENT

varying highp vec2 xlv_TEXCOORD0;
uniform highp vec4 _Color;
uniform sampler2D _MainTex;
void main ()
{
  highp vec4 color_1;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD0);
  color_1 = tmpvar_2;
  gl_FragData[0] = (color_1 * _Color);
}



#endif"
}

SubProgram "glesdesktop " {
Keywords { }
"!!GLES


#ifdef VERTEX

varying highp vec2 xlv_TEXCOORD0;
uniform mediump vec2 _StrengthRadius;
uniform mediump mat4 vInvTransform;
uniform mediump mat4 vTransform;
uniform highp vec3 p_vVertexWarpModelPosition;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 _Object2World;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesVertex;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = (_Object2World * _glesVertex);
  highp vec4 vVertexWS_2;
  vVertexWS_2.w = tmpvar_1.w;
  highp float fScalar_3;
  highp vec3 vModelPos_4;
  highp vec3 vVertex_5;
  highp vec3 tmpvar_6;
  tmpvar_6 = (vInvTransform * tmpvar_1).xyz;
  highp vec4 tmpvar_7;
  tmpvar_7.w = 1.0;
  tmpvar_7.xyz = p_vVertexWarpModelPosition;
  vModelPos_4.xy = (vInvTransform * tmpvar_7).xy;
  vModelPos_4.z = 0.0;
  highp float tmpvar_8;
  tmpvar_8 = clamp ((sqrt(dot (tmpvar_6.xy, tmpvar_6.xy)) / _StrengthRadius.y), 0.0, 1.0);
  mediump float tmpvar_9;
  tmpvar_9 = _StrengthRadius.x;
  fScalar_3 = tmpvar_9;
  highp float tmpvar_10;
  tmpvar_10 = ((((1.0 - tmpvar_8) * max (0.0, pow (1.5, (((1.0 - (tmpvar_8 * tmpvar_8)) * 6.0) - 5.0)))) * fScalar_3) / 3.0);
  mediump float fAngle_11;
  fAngle_11 = (6.3232 * (tmpvar_10 * tmpvar_10));
  mediump float tmpvar_12;
  tmpvar_12 = sin(fAngle_11);
  mediump float tmpvar_13;
  tmpvar_13 = cos(fAngle_11);
  mediump mat3 tmpvar_14;
  tmpvar_14[0].x = tmpvar_13;
  tmpvar_14[0].y = -(tmpvar_12);
  tmpvar_14[0].z = 0.0;
  tmpvar_14[1].x = tmpvar_12;
  tmpvar_14[1].y = tmpvar_13;
  tmpvar_14[1].z = -0.0;
  tmpvar_14[2].x = -0.0;
  tmpvar_14[2].y = 0.0;
  tmpvar_14[2].z = ((1.0 - tmpvar_13) + tmpvar_13);
  highp vec3 tmpvar_15;
  tmpvar_15 = (tmpvar_14 * tmpvar_6);
  vVertex_5.xy = tmpvar_15.xy;
  vVertex_5.z = (tmpvar_15.z * pow (tmpvar_8, 6.0));
  highp vec3 tmpvar_16;
  tmpvar_16 = (vVertex_5 * clamp ((sqrt(dot (vModelPos_4, vModelPos_4)) / _StrengthRadius.y), 0.0, 1.0));
  vVertex_5 = tmpvar_16;
  highp vec4 tmpvar_17;
  tmpvar_17.w = 1.0;
  tmpvar_17.xyz = tmpvar_16;
  vVertexWS_2.xyz = (vTransform * tmpvar_17).xyz;
  gl_Position = (unity_MatrixVP * vVertexWS_2);
  xlv_TEXCOORD0 = _glesMultiTexCoord0.xy;
}



#endif
#ifdef FRAGMENT

varying highp vec2 xlv_TEXCOORD0;
uniform highp vec4 _Color;
uniform sampler2D _MainTex;
void main ()
{
  highp vec4 color_1;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD0);
  color_1 = tmpvar_2;
  gl_FragData[0] = (color_1 * _Color);
}



#endif"
}

SubProgram "flash " {
Keywords { }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
Matrix 0 [_Object2World]
Matrix 4 [unity_MatrixVP]
Vector 16 [p_vVertexWarpModelPosition]
Matrix 8 [vTransform]
Matrix 12 [vInvTransform]
Vector 17 [_StrengthRadius]
"agal_vs
c18 0.0 1.0 6.0 1.5
c19 6.0 -5.0 0.333333 0.0
c20 1.006368 0.5 6.283185 -3.141593
c21 -0.000002 -0.000022 0.002604 0.00026
c22 -0.020833 -0.125 1.0 0.5
[bc]
bdaaaaaaacaaaiacaaaaaaoeaaaaaaaaadaaaaoeabaaaaaa dp4 r2.w, a0, c3
aaaaaaaaabaaaiacacaaaappacaaaaaaaaaaaaaaaaaaaaaa mov r1.w, r2.w
bdaaaaaaabaaaeacaaaaaaoeaaaaaaaaacaaaaoeabaaaaaa dp4 r1.z, a0, c2
bdaaaaaaabaaabacaaaaaaoeaaaaaaaaaaaaaaoeabaaaaaa dp4 r1.x, a0, c0
bdaaaaaaabaaacacaaaaaaoeaaaaaaaaabaaaaoeabaaaaaa dp4 r1.y, a0, c1
bdaaaaaaacaaabacabaaaaoeacaaaaaaamaaaaoeabaaaaaa dp4 r2.x, r1, c12
bdaaaaaaacaaacacabaaaaoeacaaaaaaanaaaaoeabaaaaaa dp4 r2.y, r1, c13
adaaaaaaaaaaadacacaaaafeacaaaaaaacaaaafeacaaaaaa mul r0.xy, r2.xyyy, r2.xyyy
abaaaaaaaaaaabacaaaaaaaaacaaaaaaaaaaaaffacaaaaaa add r0.x, r0.x, r0.y
akaaaaaaaaaaabacaaaaaaaaacaaaaaaaaaaaaaaaaaaaaaa rsq r0.x, r0.x
aaaaaaaaadaaapacbbaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov r3, c17
afaaaaaaadaaaiacadaaaaffacaaaaaaaaaaaaaaaaaaaaaa rcp r3.w, r3.y
afaaaaaaaaaaabacaaaaaaaaacaaaaaaaaaaaaaaaaaaaaaa rcp r0.x, r0.x
adaaaaaaaaaaabacaaaaaaaaacaaaaaaadaaaappacaaaaaa mul r0.x, r0.x, r3.w
agaaaaaaaaaaabacaaaaaaaaacaaaaaabcaaaaffabaaaaaa min r0.x, r0.x, c18.y
ahaaaaaaaeaaabacaaaaaaaaacaaaaaabcaaaaoeabaaaaaa max r4.x, r0.x, c18
bfaaaaaaaaaaabacaeaaaaaaacaaaaaaaaaaaaaaaaaaaaaa neg r0.x, r4.x
adaaaaaaaaaaabacaaaaaaaaacaaaaaaaeaaaaaaacaaaaaa mul r0.x, r0.x, r4.x
abaaaaaaaaaaabacaaaaaaaaacaaaaaabcaaaaffabaaaaaa add r0.x, r0.x, c18.y
adaaaaaaacaaaeacaaaaaaaaacaaaaaabdaaaaaaabaaaaaa mul r2.z, r0.x, c19.x
abaaaaaaacaaaeacacaaaakkacaaaaaabdaaaaffabaaaaaa add r2.z, r2.z, c19.y
alaaaaaaaaaaapacbcaaaappabaaaaaaacaaaakkacaaaaaa pow r0, c18.w, r2.z
ahaaaaaaaaaaacacaaaaaaaaacaaaaaabcaaaaaaabaaaaaa max r0.y, r0.x, c18.x
bfaaaaaaaaaaabacaeaaaaaaacaaaaaaaaaaaaaaaaaaaaaa neg r0.x, r4.x
abaaaaaaaaaaabacaaaaaaaaacaaaaaabcaaaaffabaaaaaa add r0.x, r0.x, c18.y
adaaaaaaaaaaabacaaaaaaaaacaaaaaaaaaaaaffacaaaaaa mul r0.x, r0.x, r0.y
adaaaaaaaaaaabacaaaaaaaaacaaaaaabbaaaaoeabaaaaaa mul r0.x, r0.x, c17
adaaaaaaaaaaabacaaaaaaaaacaaaaaabdaaaakkabaaaaaa mul r0.x, r0.x, c19.z
adaaaaaaaaaaabacaaaaaaaaacaaaaaaaaaaaaaaacaaaaaa mul r0.x, r0.x, r0.x
adaaaaaaaaaaabacaaaaaaaaacaaaaaabeaaaaoeabaaaaaa mul r0.x, r0.x, c20
abaaaaaaaaaaabacaaaaaaaaacaaaaaabeaaaaffabaaaaaa add r0.x, r0.x, c20.y
aiaaaaaaaaaaabacaaaaaaaaacaaaaaaaaaaaaaaaaaaaaaa frc r0.x, r0.x
adaaaaaaacaaaeacaaaaaaaaacaaaaaabeaaaaoeabaaaaaa mul r2.z, r0.x, c20
abaaaaaaacaaaeacacaaaakkacaaaaaabeaaaappabaaaaaa add r2.z, r2.z, c20.w
apaaaaaaaaaaabacacaaaakkacaaaaaaaaaaaaaaaaaaaaaa sin r0.x, r2.z
baaaaaaaaaaaacacacaaaakkacaaaaaaaaaaaaaaaaaaaaaa cos r0.y, r2.z
bdaaaaaaacaaaeacabaaaaoeacaaaaaaaoaaaaoeabaaaaaa dp4 r2.z, r1, c14
aaaaaaaaaaaaaeacbcaaaaaaabaaaaaaaaaaaaaaaaaaaaaa mov r0.z, c18.x
bfaaaaaaadaaabacaaaaaaffacaaaaaaaaaaaaaaaaaaaaaa neg r3.x, r0.y
aaaaaaaaadaaacacaaaaaaaaacaaaaaaaaaaaaaaaaaaaaaa mov r3.y, r0.x
bcaaaaaaabaaabacacaaaakeacaaaaaaaaaaaakeacaaaaaa dp3 r1.x, r2.xyzz, r0.xyzz
aaaaaaaaadaaaeacbcaaaaaaabaaaaaaaaaaaaaaaaaaaaaa mov r3.z, c18.x
bcaaaaaaabaaacacacaaaakeacaaaaaaadaaaakeacaaaaaa dp3 r1.y, r2.xyzz, r3.xyzz
aaaaaaaaaaaaahacbaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov r0.xyz, c16
aaaaaaaaaaaaaiacbcaaaaffabaaaaaaaaaaaaaaaaaaaaaa mov r0.w, c18.y
bdaaaaaaadaaacacaaaaaaoeacaaaaaaanaaaaoeabaaaaaa dp4 r3.y, r0, c13
bdaaaaaaadaaabacaaaaaaoeacaaaaaaamaaaaoeabaaaaaa dp4 r3.x, r0, c12
aaaaaaaaadaaaeacbcaaaaaaabaaaaaaaaaaaaaaaaaaaaaa mov r3.z, c18.x
bcaaaaaaaaaaabacadaaaakeacaaaaaaadaaaakeacaaaaaa dp3 r0.x, r3.xyzz, r3.xyzz
akaaaaaaabaaaiacaaaaaaaaacaaaaaaaaaaaaaaaaaaaaaa rsq r1.w, r0.x
alaaaaaaaaaaapacaeaaaaaaacaaaaaabcaaaakkabaaaaaa pow r0, r4.x, c18.z
afaaaaaaaaaaacacabaaaappacaaaaaaaaaaaaaaaaaaaaaa rcp r0.y, r1.w
bcaaaaaaabaaaeacacaaaakeacaaaaaabcaaaanaabaaaaaa dp3 r1.z, r2.xyzz, c18.xxyw
adaaaaaaaaaaacacadaaaappacaaaaaaaaaaaaffacaaaaaa mul r0.y, r3.w, r0.y
adaaaaaaabaaaeacabaaaakkacaaaaaaaaaaaaaaacaaaaaa mul r1.z, r1.z, r0.x
agaaaaaaaaaaacacaaaaaaffacaaaaaabcaaaaoeabaaaaaa min r0.y, r0.y, c18
ahaaaaaaaaaaabacaaaaaaffacaaaaaabcaaaaoeabaaaaaa max r0.x, r0.y, c18
adaaaaaaaaaaahacabaaaakeacaaaaaaaaaaaaaaacaaaaaa mul r0.xyz, r1.xyzz, r0.x
aaaaaaaaaaaaaiacbcaaaaffabaaaaaaaaaaaaaaaaaaaaaa mov r0.w, c18.y
bdaaaaaaabaaaeacaaaaaaoeacaaaaaaakaaaaoeabaaaaaa dp4 r1.z, r0, c10
bdaaaaaaabaaacacaaaaaaoeacaaaaaaajaaaaoeabaaaaaa dp4 r1.y, r0, c9
aaaaaaaaabaaaiacacaaaappacaaaaaaaaaaaaaaaaaaaaaa mov r1.w, r2.w
bdaaaaaaabaaabacaaaaaaoeacaaaaaaaiaaaaoeabaaaaaa dp4 r1.x, r0, c8
bdaaaaaaaaaaaiadabaaaaoeacaaaaaaahaaaaoeabaaaaaa dp4 o0.w, r1, c7
bdaaaaaaaaaaaeadabaaaaoeacaaaaaaagaaaaoeabaaaaaa dp4 o0.z, r1, c6
bdaaaaaaaaaaacadabaaaaoeacaaaaaaafaaaaoeabaaaaaa dp4 o0.y, r1, c5
bdaaaaaaaaaaabadabaaaaoeacaaaaaaaeaaaaoeabaaaaaa dp4 o0.x, r1, c4
aaaaaaaaaaaaadaeadaaaaoeaaaaaaaaaaaaaaaaaaaaaaaa mov v0.xy, a3
aaaaaaaaaaaaamaeaaaaaaoeabaaaaaaaaaaaaaaaaaaaaaa mov v0.zw, c0
"
}

SubProgram "d3d11_9x " {
Keywords { }
Bind "vertex" Vertex
Bind "texcoord" TexCoord0
ConstBuffer "$Globals" 176 // 168 used size, 5 vars
Vector 0 [p_vVertexWarpModelPosition] 3
Matrix 32 [vTransform] 4
Matrix 96 [vInvTransform] 4
Vector 160 [_StrengthRadius] 2
ConstBuffer "UnityPerDraw" 336 // 256 used size, 6 vars
Matrix 192 [_Object2World] 4
ConstBuffer "UnityPerFrame" 208 // 208 used size, 4 vars
Matrix 144 [unity_MatrixVP] 4
BindCB "$Globals" 0
BindCB "UnityPerDraw" 1
BindCB "UnityPerFrame" 2
// 49 instructions, 5 temp regs, 0 temp arrays:
// ALU 29 float, 0 int, 0 uint
// TEX 0 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"vs_4_0_level_9_1
eefiecedaidkboplkmloiknjhalkjnbkibpcohfnabaaaaaadeamaaaaaeaaaaaa
daaaaaaabeafaaaaiialaaaanmalaaaaebgpgodjnmaeaaaanmaeaaaaaaacpopp
ieaeaaaafiaaaaaaaeaaceaaaaaafeaaaaaafeaaaaaaceaaabaafeaaaaaaaaaa
abaaabaaaaaaaaaaaaaaacaaajaaacaaaaaaaaaaabaaamaaaeaaalaaaaaaaaaa
acaaajaaaeaaapaaaaaaaaaaaaaaaaaaaaacpoppfbaaaaafbdaaapkaaaaaaaaa
aaaaiadpaaaamaeaaaaakamafbaaaaafbeaaapkabkmabfdpklkkkkdokpnaiadp
aaaaaadpfbaaaaafbfaaapkanlapmjeanlapejmaaaaaaaaaaaaaaaaafbaaaaaf
bgaaapkaaaaaialpaaaaiadpaaaaaaaaaaaaaaaafbaaaaafbhaaapkaabannalf
gballglhklkkckdlijiiiidjfbaaaaafbiaaapkaklkkkklmaaaaaaloaaaaiadp
aaaaaadpbpaaaaacafaaaaiaaaaaapjabpaaaaacafaaabiaabaaapjaabaaaaac
aaaaahiaabaaoekaafaaaaadaaaaakiaaaaaffiaahaagakaaeaaaaaeaaaaadia
agaaoekaaaaaaaiaaaaaoniaaeaaaaaeaaaaadiaaiaaoekaaaaakkiaaaaaoeia
acaaaaadaaaaadiaaaaaoeiaajaaoekaafaaaaadaaaaadiaaaaaoeiaaaaaoeia
acaaaaadaaaaabiaaaaaffiaaaaaaaiaahaaaaacaaaaabiaaaaaaaiaagaaaaac
aaaaabiaaaaaaaiaagaaaaacaaaaaciaakaaffkaafaaaaadaaaaabiaaaaaffia
aaaaaaiaalaaaaadaaaaabiaaaaaaaiabdaaaakaafaaaaadabaaapiaaaaaffja
amaaoekaaeaaaaaeabaaapiaalaaoekaaaaaaajaabaaoeiaaeaaaaaeabaaapia
anaaoekaaaaakkjaabaaoeiaaeaaaaaeabaaapiaaoaaoekaaaaappjaabaaoeia
afaaaaadacaaahiaabaaffiaahaaoekaaeaaaaaeacaaahiaagaaoekaabaaaaia
acaaoeiaaeaaaaaeabaaahiaaiaaoekaabaakkiaacaaoeiaaeaaaaaeabaaahia
ajaaoekaabaappiaabaaoeiaafaaaaadaaaaamiaabaaeeiaabaaeeiaacaaaaad
aaaaaeiaaaaappiaaaaakkiaahaaaaacaaaaaeiaaaaakkiaagaaaaacaaaaaeia
aaaakkiaafaaaaadaaaaaciaaaaaffiaaaaakkiaalaaaaadaaaaaciaaaaaffia
bdaaaakaakaaaaadaaaaadiaaaaaoeiabdaaffkaaeaaaaaeaaaaaeiaaaaaffia
aaaaffibbdaaffkaaeaaaaaeaaaaaeiaaaaakkiabdaakkkabdaappkaafaaaaad
aaaaaeiaaaaakkiabeaaaakaaoaaaaacaaaaaeiaaaaakkiaacaaaaadaaaaaiia
aaaaffibbdaaffkaafaaaaadaaaaagiaaaaaoeiaaaaapeiaafaaaaadaaaaaeia
aaaakkiaakaaaakaafaaaaadaaaaaeiaaaaakkiabeaaffkaafaaaaadaaaaaeia
aaaakkiaaaaakkiaaeaaaaaeaaaaaeiaaaaakkiabeaakkkabeaappkabdaaaaac
aaaaaeiaaaaakkiaaeaaaaaeaaaaaeiaaaaakkiabfaaaakabfaaffkacfaaaaae
acaaadiaaaaakkiabhaaoekabiaaoekaafaaaaadadaaahiaacaameiabdaamfka
afaaaaadacaaahiaacaambiabgaaoekaaiaaaaadacaaaciaacaaoeiaabaaoeia
aiaaaaadacaaabiaadaaoeiaabaaoeiaafaaaaadaaaaaeiaaaaaffiaaaaaffia
afaaaaadaaaaaciaaaaakkiaaaaaffiaafaaaaadacaaaeiaaaaaffiaabaakkia
afaaaaadaaaaahiaaaaaaaiaacaaoeiaafaaaaadabaaahiaaaaaffiaadaaoeka
aeaaaaaeaaaaaliaacaakekaaaaaaaiaabaakeiaaeaaaaaeaaaaahiaaeaaoeka
aaaakkiaaaaapeiaacaaaaadaaaaahiaaaaaoeiaafaaoekaafaaaaadacaaapia
aaaaffiabaaaoekaaeaaaaaeacaaapiaapaaoekaaaaaaaiaacaaoeiaaeaaaaae
aaaaapiabbaaoekaaaaakkiaacaaoeiaaeaaaaaeaaaaapiabcaaoekaabaappia
aaaaoeiaaeaaaaaeaaaaadmaaaaappiaaaaaoekaaaaaoeiaabaaaaacaaaaamma
aaaaoeiaabaaaaacaaaaadoaabaaoejappppaaaafdeieefcgmagaaaaeaaaabaa
jlabaaaafjaaaaaeegiocaaaaaaaaaaaalaaaaaafjaaaaaeegiocaaaabaaaaaa
baaaaaaafjaaaaaeegiocaaaacaaaaaaanaaaaaafpaaaaadpcbabaaaaaaaaaaa
fpaaaaaddcbabaaaabaaaaaaghaaaaaepccabaaaaaaaaaaaabaaaaaagfaaaaad
dccabaaaabaaaaaagiaaaaacafaaaaaadiaaaaajdcaabaaaaaaaaaaafgifcaaa
aaaaaaaaaaaaaaaaegiacaaaaaaaaaaaahaaaaaadcaaaaaldcaabaaaaaaaaaaa
egiacaaaaaaaaaaaagaaaaaaagiacaaaaaaaaaaaaaaaaaaaegaabaaaaaaaaaaa
dcaaaaaldcaabaaaaaaaaaaaegiacaaaaaaaaaaaaiaaaaaakgikcaaaaaaaaaaa
aaaaaaaaegaabaaaaaaaaaaaaaaaaaaidcaabaaaaaaaaaaaegaabaaaaaaaaaaa
egiacaaaaaaaaaaaajaaaaaaapaaaaahbcaabaaaaaaaaaaaegaabaaaaaaaaaaa
egaabaaaaaaaaaaaelaaaaafbcaabaaaaaaaaaaaakaabaaaaaaaaaaaaocaaaai
bcaabaaaaaaaaaaaakaabaaaaaaaaaaabkiacaaaaaaaaaaaakaaaaaadiaaaaai
pcaabaaaabaaaaaafgbfbaaaaaaaaaaaegiocaaaabaaaaaaanaaaaaadcaaaaak
pcaabaaaabaaaaaaegiocaaaabaaaaaaamaaaaaaagbabaaaaaaaaaaaegaobaaa
abaaaaaadcaaaaakpcaabaaaabaaaaaaegiocaaaabaaaaaaaoaaaaaakgbkbaaa
aaaaaaaaegaobaaaabaaaaaadcaaaaakpcaabaaaabaaaaaaegiocaaaabaaaaaa
apaaaaaapgbpbaaaaaaaaaaaegaobaaaabaaaaaadiaaaaaiocaabaaaaaaaaaaa
fgafbaaaabaaaaaaagijcaaaaaaaaaaaahaaaaaadcaaaaakocaabaaaaaaaaaaa
agijcaaaaaaaaaaaagaaaaaaagaabaaaabaaaaaafgaobaaaaaaaaaaadcaaaaak
ocaabaaaaaaaaaaaagijcaaaaaaaaaaaaiaaaaaakgakbaaaabaaaaaafgaobaaa
aaaaaaaadcaaaaakocaabaaaaaaaaaaaagijcaaaaaaaaaaaajaaaaaapgapbaaa
abaaaaaafgaobaaaaaaaaaaaapaaaaahbcaabaaaabaaaaaajgafbaaaaaaaaaaa
jgafbaaaaaaaaaaaelaaaaafbcaabaaaabaaaaaaakaabaaaabaaaaaaaocaaaai
bcaabaaaabaaaaaaakaabaaaabaaaaaabkiacaaaaaaaaaaaakaaaaaadcaaaaak
ccaabaaaabaaaaaaakaabaiaebaaaaaaabaaaaaaakaabaaaabaaaaaaabeaaaaa
aaaaiadpdcaaaaajccaabaaaabaaaaaabkaabaaaabaaaaaaabeaaaaaaaaamaea
abeaaaaaaaaakamadiaaaaahccaabaaaabaaaaaabkaabaaaabaaaaaaabeaaaaa
bkmabfdpbjaaaaafccaabaaaabaaaaaabkaabaaaabaaaaaaaaaaaaaiecaabaaa
abaaaaaaakaabaiaebaaaaaaabaaaaaaabeaaaaaaaaaiadpdiaaaaahbcaabaaa
abaaaaaaakaabaaaabaaaaaaakaabaaaabaaaaaadiaaaaahccaabaaaabaaaaaa
bkaabaaaabaaaaaackaabaaaabaaaaaadiaaaaaiccaabaaaabaaaaaabkaabaaa
abaaaaaaakiacaaaaaaaaaaaakaaaaaadiaaaaahccaabaaaabaaaaaabkaabaaa
abaaaaaaabeaaaaaklkkkkdodiaaaaahccaabaaaabaaaaaabkaabaaaabaaaaaa
bkaabaaaabaaaaaadiaaaaahccaabaaaabaaaaaabkaabaaaabaaaaaaabeaaaaa
kifhmkeaenaaaaahbcaabaaaacaaaaaabcaabaaaadaaaaaabkaabaaaabaaaaaa
dgaaaaafecaabaaaaeaaaaaaakaabaaaacaaaaaadgaaaaafccaabaaaaeaaaaaa
akaabaaaadaaaaaadgaaaaagbcaabaaaaeaaaaaaakaabaiaebaaaaaaacaaaaaa
apaaaaahccaabaaaacaaaaaaegaabaaaaeaaaaaajgafbaaaaaaaaaaaapaaaaah
bcaabaaaacaaaaaajgafbaaaaeaaaaaajgafbaaaaaaaaaaadiaaaaahccaabaaa
aaaaaaaaakaabaaaabaaaaaaakaabaaaabaaaaaadiaaaaahccaabaaaaaaaaaaa
bkaabaaaaaaaaaaaakaabaaaabaaaaaadiaaaaahecaabaaaacaaaaaabkaabaaa
aaaaaaaadkaabaaaaaaaaaaadiaaaaahhcaabaaaaaaaaaaaagaabaaaaaaaaaaa
egacbaaaacaaaaaadiaaaaaihcaabaaaabaaaaaafgafbaaaaaaaaaaaegiccaaa
aaaaaaaaadaaaaaadcaaaaaklcaabaaaaaaaaaaaegiicaaaaaaaaaaaacaaaaaa
agaabaaaaaaaaaaaegaibaaaabaaaaaadcaaaaakhcaabaaaaaaaaaaaegiccaaa
aaaaaaaaaeaaaaaakgakbaaaaaaaaaaaegadbaaaaaaaaaaaaaaaaaaihcaabaaa
aaaaaaaaegacbaaaaaaaaaaaegiccaaaaaaaaaaaafaaaaaadiaaaaaipcaabaaa
acaaaaaafgafbaaaaaaaaaaaegiocaaaacaaaaaaakaaaaaadcaaaaakpcaabaaa
acaaaaaaegiocaaaacaaaaaaajaaaaaaagaabaaaaaaaaaaaegaobaaaacaaaaaa
dcaaaaakpcaabaaaaaaaaaaaegiocaaaacaaaaaaalaaaaaakgakbaaaaaaaaaaa
egaobaaaacaaaaaadcaaaaakpccabaaaaaaaaaaaegiocaaaacaaaaaaamaaaaaa
pgapbaaaabaaaaaaegaobaaaaaaaaaaadgaaaaafdccabaaaabaaaaaaegbabaaa
abaaaaaadoaaaaabejfdeheoemaaaaaaacaaaaaaaiaaaaaadiaaaaaaaaaaaaaa
aaaaaaaaadaaaaaaaaaaaaaaapapaaaaebaaaaaaaaaaaaaaaaaaaaaaadaaaaaa
abaaaaaaadadaaaafaepfdejfeejepeoaafeeffiedepepfceeaaklklepfdeheo
faaaaaaaacaaaaaaaiaaaaaadiaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaa
apaaaaaaeeaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaadamaaaafdfgfpfa
gphdgjhegjgpgoaafeeffiedepepfceeaaklklkl"
}

SubProgram "gles3 " {
Keywords { }
"!!GLES3#version 300 es


#ifdef VERTEX

#define gl_Vertex _glesVertex
in vec4 _glesVertex;
#define gl_MultiTexCoord0 _glesMultiTexCoord0
in vec4 _glesMultiTexCoord0;
void xll_sincos_f_f_f( float x, out float s, out float c) {
  s = sin(x); 
  c = cos(x); 
}
void xll_sincos_vf2_vf2_vf2( vec2 x, out vec2 s, out vec2 c) {
  s = sin(x); 
  c = cos(x); 
}
void xll_sincos_vf3_vf3_vf3( vec3 x, out vec3 s, out vec3 c) {
  s = sin(x); 
  c = cos(x); 
}
void xll_sincos_vf4_vf4_vf4( vec4 x, out vec4 s, out vec4 c) {
  s = sin(x); 
  c = cos(x); 
}
void xll_sincos_mf2x2_mf2x2_mf2x2( mat2 x, out mat2 s, out mat2 c) {
  s = mat2( sin ( x[0] ), sin ( x[1] ) ); 
  c = mat2( cos ( x[0] ), cos ( x[1] ) ); 
}
void xll_sincos_mf3x3_mf3x3_mf3x3( mat3 x, out mat3 s, out mat3 c) {
  s = mat3( sin ( x[0] ), sin ( x[1] ), sin ( x[2] ) ); 
  c = mat3( cos ( x[0] ), cos ( x[1] ), cos ( x[2] ) ); 
}
void xll_sincos_mf4x4_mf4x4_mf4x4( mat4 x, out mat4 s, out mat4 c) {
  s = mat4( sin ( x[0] ), sin ( x[1] ), sin ( x[2] ), sin ( x[3] ) ); 
  c = mat4( cos ( x[0] ), cos ( x[1] ), cos ( x[2] ), cos ( x[3] ) ); 
}
float xll_saturate_f( float x) {
  return clamp( x, 0.0, 1.0);
}
vec2 xll_saturate_vf2( vec2 x) {
  return clamp( x, 0.0, 1.0);
}
vec3 xll_saturate_vf3( vec3 x) {
  return clamp( x, 0.0, 1.0);
}
vec4 xll_saturate_vf4( vec4 x) {
  return clamp( x, 0.0, 1.0);
}
mat2 xll_saturate_mf2x2(mat2 m) {
  return mat2( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0));
}
mat3 xll_saturate_mf3x3(mat3 m) {
  return mat3( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0));
}
mat4 xll_saturate_mf4x4(mat4 m) {
  return mat4( clamp(m[0], 0.0, 1.0), clamp(m[1], 0.0, 1.0), clamp(m[2], 0.0, 1.0), clamp(m[3], 0.0, 1.0));
}
#line 68
struct VertexWarp {
    mediump mat4 vTransform;
    mediump mat4 vInvTransform;
    mediump vec2 vStrengthRadius;
};
#line 105
struct VertexOutput {
    highp vec4 position_;
    highp vec2 uv;
};
#line 99
struct VertexInput {
    highp vec4 vertex;
    highp vec2 texcoord;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform highp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[4];
uniform highp vec4 unity_LightPosition[4];
uniform highp vec4 unity_LightAtten[4];
#line 19
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
uniform highp vec4 unity_SHBr;
#line 23
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
#line 27
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
uniform highp vec4 _LightSplitsNear;
#line 31
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
#line 35
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
uniform highp mat4 _Object2World;
#line 39
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
uniform highp mat4 glstate_matrix_texture0;
#line 43
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
uniform highp mat4 glstate_matrix_projection;
#line 47
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 75
uniform highp vec3 p_vVertexWarpModelPosition;
uniform sampler2D _MainTex;
#line 95
uniform highp vec4 _Color;
uniform mediump mat4 vTransform;
uniform mediump mat4 vInvTransform;
uniform mediump vec2 _StrengthRadius;
#line 111
#line 124
#line 50
mediump mat3 MakeRotation( in mediump float fAngle, in mediump vec3 vAxis ) {
    #line 52
    mediump float fS;
    mediump float fC;
    xll_sincos_f_f_f( fAngle, fS, fC);
    mediump float fXX = (vAxis.x * vAxis.x);
    #line 56
    mediump float fYY = (vAxis.y * vAxis.y);
    mediump float fZZ = (vAxis.z * vAxis.z);
    mediump float fXY = (vAxis.x * vAxis.y);
    mediump float fYZ = (vAxis.y * vAxis.z);
    #line 60
    mediump float fZX = (vAxis.z * vAxis.x);
    mediump float fXS = (vAxis.x * fS);
    mediump float fYS = (vAxis.y * fS);
    mediump float fZS = (vAxis.z * fS);
    #line 64
    mediump float fOneC = (1.0 - fC);
    mediump mat3 result = mat3( ((fOneC * fXX) + fC), ((fOneC * fXY) - fZS), ((fOneC * fZX) + fYS), ((fOneC * fXY) + fZS), ((fOneC * fYY) + fC), ((fOneC * fYZ) - fXS), ((fOneC * fZX) - fYS), ((fOneC * fYZ) + fXS), ((fOneC * fZZ) + fC));
    return result;
}
#line 76
highp vec4 ApplyWarp( in highp vec4 vVertexWS, in VertexWarp warp ) {
    highp vec3 vVertex = vec3( (warp.vInvTransform * vVertexWS));
    #line 79
    highp vec3 vModelPos = vec3( (warp.vInvTransform * vec4( p_vVertexWarpModelPosition, 1.0)));
    vModelPos.z = 0.0;
    highp float fModelDistance = xll_saturate_f((length(vModelPos) / warp.vStrengthRadius.y));
    highp float fOriginalLength = length(vVertex.xy);
    #line 83
    highp float fGradient = xll_saturate_f((fOriginalLength / warp.vStrengthRadius.y));
    highp float fSquaredGradient = (1.0 - (fGradient * fGradient));
    highp float fScalar = warp.vStrengthRadius.x;
    highp float fR = ((((1.0 - fGradient) * max( 0.0, pow( 1.5, ((fSquaredGradient * 6.0) - 5.0)))) * fScalar) / 3.0);
    #line 87
    mediump mat3 rotation = MakeRotation( (((fR * fR) * 3.1616) * 2.0), vec3( 0.0, 0.0, 1.0));
    vVertex = (rotation * vVertex);
    vVertex.z = (vVertex.z * pow( fGradient, 6.0));
    vVertex = (vVertex * fModelDistance);
    #line 91
    vVertexWS.xyz = vec3( (warp.vTransform * vec4( vVertex, 1.0)));
    return vVertexWS;
}
#line 111
VertexOutput VertexProgram( in VertexInput xlat_var_input ) {
    VertexOutput xlat_var_output;
    VertexWarp warp;
    #line 115
    warp.vTransform = vTransform;
    warp.vInvTransform = vInvTransform;
    warp.vStrengthRadius = _StrengthRadius;
    highp vec4 worldPos = (_Object2World * xlat_var_input.vertex);
    #line 119
    xlat_var_input.vertex = ApplyWarp( worldPos, warp);
    xlat_var_output.position_ = (unity_MatrixVP * xlat_var_input.vertex);
    xlat_var_output.uv = xlat_var_input.texcoord;
    return xlat_var_output;
}
out highp vec2 xlv_TEXCOORD0;
void main() {
    VertexOutput xl_retval;
    VertexInput xlt_xlat_var_input;
    xlt_xlat_var_input.vertex = vec4(gl_Vertex);
    xlt_xlat_var_input.texcoord = vec2(gl_MultiTexCoord0);
    xl_retval = VertexProgram( xlt_xlat_var_input);
    gl_Position = vec4(xl_retval.position_);
    xlv_TEXCOORD0 = vec2(xl_retval.uv);
}


#endif
#ifdef FRAGMENT

#define gl_FragData _glesFragData
layout(location = 0) out mediump vec4 _glesFragData[4];

#line 68
struct VertexWarp {
    mediump mat4 vTransform;
    mediump mat4 vInvTransform;
    mediump vec2 vStrengthRadius;
};
#line 105
struct VertexOutput {
    highp vec4 position_;
    highp vec2 uv;
};
#line 99
struct VertexInput {
    highp vec4 vertex;
    highp vec2 texcoord;
};
uniform highp vec4 _Time;
uniform highp vec4 _SinTime;
#line 3
uniform highp vec4 _CosTime;
uniform highp vec4 unity_DeltaTime;
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _ProjectionParams;
#line 7
uniform highp vec4 _ScreenParams;
uniform highp vec4 _ZBufferParams;
uniform highp vec4 unity_CameraWorldClipPlanes[6];
uniform highp vec4 _WorldSpaceLightPos0;
#line 11
uniform highp vec4 _LightPositionRange;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
#line 15
uniform highp vec4 unity_4LightAtten0;
uniform highp vec4 unity_LightColor[4];
uniform highp vec4 unity_LightPosition[4];
uniform highp vec4 unity_LightAtten[4];
#line 19
uniform highp vec4 unity_SHAr;
uniform highp vec4 unity_SHAg;
uniform highp vec4 unity_SHAb;
uniform highp vec4 unity_SHBr;
#line 23
uniform highp vec4 unity_SHBg;
uniform highp vec4 unity_SHBb;
uniform highp vec4 unity_SHC;
uniform highp vec3 unity_LightColor0;
uniform highp vec3 unity_LightColor1;
uniform highp vec3 unity_LightColor2;
uniform highp vec3 unity_LightColor3;
#line 27
uniform highp vec4 unity_ShadowSplitSpheres[4];
uniform highp vec4 unity_ShadowSplitSqRadii;
uniform highp vec4 unity_LightShadowBias;
uniform highp vec4 _LightSplitsNear;
#line 31
uniform highp vec4 _LightSplitsFar;
uniform highp mat4 unity_World2Shadow[4];
uniform highp vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
#line 35
uniform highp mat4 glstate_matrix_mvp;
uniform highp mat4 glstate_matrix_modelview0;
uniform highp mat4 glstate_matrix_invtrans_modelview0;
uniform highp mat4 _Object2World;
#line 39
uniform highp mat4 _World2Object;
uniform highp vec4 unity_Scale;
uniform highp mat4 glstate_matrix_transpose_modelview0;
uniform highp mat4 glstate_matrix_texture0;
#line 43
uniform highp mat4 glstate_matrix_texture1;
uniform highp mat4 glstate_matrix_texture2;
uniform highp mat4 glstate_matrix_texture3;
uniform highp mat4 glstate_matrix_projection;
#line 47
uniform highp vec4 glstate_lightmodel_ambient;
uniform highp mat4 unity_MatrixV;
uniform highp mat4 unity_MatrixVP;
#line 75
uniform highp vec3 p_vVertexWarpModelPosition;
uniform sampler2D _MainTex;
#line 95
uniform highp vec4 _Color;
uniform mediump mat4 vTransform;
uniform mediump mat4 vInvTransform;
uniform mediump vec2 _StrengthRadius;
#line 111
#line 124
#line 124
highp vec4 FragmentProgram( in VertexOutput xlat_var_input ) {
    highp vec4 color = texture( _MainTex, xlat_var_input.uv);
    return (color * _Color);
}
in highp vec2 xlv_TEXCOORD0;
void main() {
    highp vec4 xl_retval;
    VertexOutput xlt_xlat_var_input;
    xlt_xlat_var_input.position_ = vec4(0.0);
    xlt_xlat_var_input.uv = vec2(xlv_TEXCOORD0);
    xl_retval = FragmentProgram( xlt_xlat_var_input);
    gl_FragData[0] = vec4(xl_retval);
}


#endif"
}

}
Program "fp" {
// Fragment combos: 1
//   opengl - ALU: 2 to 2, TEX: 1 to 1
//   d3d9 - ALU: 2 to 2, TEX: 1 to 1
//   d3d11 - ALU: 1 to 1, TEX: 1 to 1, FLOW: 1 to 1
//   d3d11_9x - ALU: 1 to 1, TEX: 1 to 1, FLOW: 1 to 1
SubProgram "opengl " {
Keywords { }
Vector 0 [_Color]
SetTexture 0 [_MainTex] 2D
"!!ARBfp1.0
# 2 ALU, 1 TEX
PARAM c[1] = { program.local[0] };
TEMP R0;
TEX R0, fragment.texcoord[0], texture[0], 2D;
MUL result.color, R0, c[0];
END
# 2 instructions, 1 R-regs
"
}

SubProgram "d3d9 " {
Keywords { }
Vector 0 [_Color]
SetTexture 0 [_MainTex] 2D
"ps_2_0
; 2 ALU, 1 TEX
dcl_2d s0
dcl t0.xy
texld r0, t0, s0
mul r0, r0, c0
mov oC0, r0
"
}

SubProgram "xbox360 " {
Keywords { }
Vector 0 [_Color]
SetTexture 0 [_MainTex] 2D
// Shader Timing Estimate, in Cycles/64 pixel vector:
// ALU: 1.33 (1 instructions), vertex: 0, texture: 4,
//   sequencer: 6, interpolator: 8;    1 GPR, 63 threads,
// Performance (if enough threads): ~8 cycles per vector
// * Texture cycle estimates are assuming an 8bit/component texture with no
//     aniso or trilinear filtering.

"ps_360
backbbaaaaaaaaniaaaaaadmaaaaaaaaaaaaaaceaaaaaaaaaaaaaaleaaaaaaaa
aaaaaaaaaaaaaaimaaaaaabmaaaaaahpppppadaaaaaaaaacaaaaaabmaaaaaaaa
aaaaaahiaaaaaaeeaaacaaaaaaabaaaaaaaaaaemaaaaaaaaaaaaaafmaaadaaaa
aaabaaaaaaaaaagiaaaaaaaafpedgpgmgphcaaklaaabaaadaaabaaaeaaabaaaa
aaaaaaaafpengbgjgofegfhiaaklklklaaaeaaamaaabaaabaaabaaaaaaaaaaaa
hahdfpddfpdaaadccodacodcdadddfddcodaaaklaaaaaaaaaaaaaadmbaaaaaaa
aaaaaaaeaaaaaaaaaaaaaicbaaabaaabaaaaaaabaaaadafaaaabbaacaaaabcaa
meaaaaaaaaaabaadaaaaccaaaaaaaaaabaaiaaabbpbppgiiaaaaeaaamiapiaaa
aaaaaaaakbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
}

SubProgram "ps3 " {
Keywords { }
Vector 0 [_Color]
SetTexture 0 [_MainTex] 2D
"sce_fp_rsx // 3 instructions using 2 registers
[Configuration]
24
ffffffff000040200001ffff000000000000844002000000
[Offsets]
1
_Color 1 0
00000020
[Microcode]
48
9e001700c8011c9dc8000001c8003fe11e010200c8001c9dc8020001c8000001
00000000000000000000000000000000
"
}

SubProgram "d3d11 " {
Keywords { }
ConstBuffer "$Globals" 176 // 32 used size, 5 vars
Vector 16 [_Color] 4
BindCB "$Globals" 0
SetTexture 0 [_MainTex] 2D 0
// 3 instructions, 1 temp regs, 0 temp arrays:
// ALU 1 float, 0 int, 0 uint
// TEX 1 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0
eefiecednebkikcecllbiobmbdijbogcghpenmmgabaaaaaafmabaaaaadaaaaaa
cmaaaaaaieaaaaaaliaaaaaaejfdeheofaaaaaaaacaaaaaaaiaaaaaadiaaaaaa
aaaaaaaaabaaaaaaadaaaaaaaaaaaaaaapaaaaaaeeaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaabaaaaaaadadaaaafdfgfpfagphdgjhegjgpgoaafeeffiedepepfcee
aaklklklepfdeheocmaaaaaaabaaaaaaaiaaaaaacaaaaaaaaaaaaaaaaaaaaaaa
adaaaaaaaaaaaaaaapaaaaaafdfgfpfegbhcghgfheaaklklfdeieefcjmaaaaaa
eaaaaaaachaaaaaafjaaaaaeegiocaaaaaaaaaaaacaaaaaafkaaaaadaagabaaa
aaaaaaaafibiaaaeaahabaaaaaaaaaaaffffaaaagcbaaaaddcbabaaaabaaaaaa
gfaaaaadpccabaaaaaaaaaaagiaaaaacabaaaaaaefaaaaajpcaabaaaaaaaaaaa
egbabaaaabaaaaaaeghobaaaaaaaaaaaaagabaaaaaaaaaaadiaaaaaipccabaaa
aaaaaaaaegaobaaaaaaaaaaaegiocaaaaaaaaaaaabaaaaaadoaaaaab"
}

SubProgram "gles " {
Keywords { }
"!!GLES"
}

SubProgram "glesdesktop " {
Keywords { }
"!!GLES"
}

SubProgram "flash " {
Keywords { }
Vector 0 [_Color]
SetTexture 0 [_MainTex] 2D
"agal_ps
[bc]
ciaaaaaaaaaaapacaaaaaaoeaeaaaaaaaaaaaaaaafaababb tex r0, v0, s0 <2d wrap linear point>
adaaaaaaaaaaapacaaaaaaoeacaaaaaaaaaaaaoeabaaaaaa mul r0, r0, c0
aaaaaaaaaaaaapadaaaaaaoeacaaaaaaaaaaaaaaaaaaaaaa mov o0, r0
"
}

SubProgram "d3d11_9x " {
Keywords { }
ConstBuffer "$Globals" 176 // 32 used size, 5 vars
Vector 16 [_Color] 4
BindCB "$Globals" 0
SetTexture 0 [_MainTex] 2D 0
// 3 instructions, 1 temp regs, 0 temp arrays:
// ALU 1 float, 0 int, 0 uint
// TEX 1 (0 load, 0 comp, 0 bias, 0 grad)
// FLOW 1 static, 0 dynamic
"ps_4_0_level_9_1
eefiecedenfolfjgaohhdfhdmnhehlpmlacghapgabaaaaaaoiabaaaaaeaaaaaa
daaaaaaaliaaaaaafmabaaaaleabaaaaebgpgodjiaaaaaaaiaaaaaaaaaacpppp
emaaaaaadeaaaaaaabaaciaaaaaadeaaaaaadeaaabaaceaaaaaadeaaaaaaaaaa
aaaaabaaabaaaaaaaaaaaaaaaaacppppbpaaaaacaaaaaaiaaaaaadlabpaaaaac
aaaaaajaaaaiapkaecaaaaadaaaaapiaaaaaoelaaaaioekaafaaaaadaaaaapia
aaaaoeiaaaaaoekaabaaaaacaaaiapiaaaaaoeiappppaaaafdeieefcjmaaaaaa
eaaaaaaachaaaaaafjaaaaaeegiocaaaaaaaaaaaacaaaaaafkaaaaadaagabaaa
aaaaaaaafibiaaaeaahabaaaaaaaaaaaffffaaaagcbaaaaddcbabaaaabaaaaaa
gfaaaaadpccabaaaaaaaaaaagiaaaaacabaaaaaaefaaaaajpcaabaaaaaaaaaaa
egbabaaaabaaaaaaeghobaaaaaaaaaaaaagabaaaaaaaaaaadiaaaaaipccabaaa
aaaaaaaaegaobaaaaaaaaaaaegiocaaaaaaaaaaaabaaaaaadoaaaaabejfdeheo
faaaaaaaacaaaaaaaiaaaaaadiaaaaaaaaaaaaaaabaaaaaaadaaaaaaaaaaaaaa
apaaaaaaeeaaaaaaaaaaaaaaaaaaaaaaadaaaaaaabaaaaaaadadaaaafdfgfpfa
gphdgjhegjgpgoaafeeffiedepepfceeaaklklklepfdeheocmaaaaaaabaaaaaa
aiaaaaaacaaaaaaaaaaaaaaaaaaaaaaaadaaaaaaaaaaaaaaapaaaaaafdfgfpfe
gbhcghgfheaaklkl"
}

SubProgram "gles3 " {
Keywords { }
"!!GLES3"
}

}

#LINE 79


		}
	}
}
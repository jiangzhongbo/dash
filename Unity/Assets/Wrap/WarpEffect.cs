﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]

public class WarpEffect : MonoBehaviour {

	// Use this for initialization
	void Start () {
        
	}
	
	// Pull is called once per frame
	void Update () {
        Shader.SetGlobalMatrix("vTransform", transform.localToWorldMatrix);
        Shader.SetGlobalMatrix("vInvTransform", transform.localToWorldMatrix.inverse);
	}
}

Shader "MADFINGER/Environment/Scroll 2 Layers Multiplicative - Skybox" {
Properties {
	_MainTex ("Base layer (RGB)", 2D) = "white" {}
	_DetailTex ("2nd layer (RGB)", 2D) = "white" {}
	_ScrollX ("Base layer Scroll speed X", Float) = 1.0
	_ScrollY ("Base layer Scroll speed Y", Float) = 0.0
	_Scroll2X ("2nd layer Scroll speed X", Float) = 1.0
	_Scroll2Y ("2nd layer Scroll speed Y", Float) = 0.0
	_AMultiplier ("Layer Multiplier", Range(0, 1)) = 0.5
	_Color ("Color", Color) = (1, 1, 1, 1)
}

SubShader {
	Tags { "Queue"="Background" "RenderType"="Opaque" }
	Lighting Off 
	ZWrite Off
	Fog { Mode Off }
	LOD 100
			
	CGINCLUDE
	#include "UnityCG.cginc"
	sampler2D _MainTex;
	sampler2D _DetailTex;
	
	half _ScrollX;
	half _ScrollY;
	half _Scroll2X;
	half _Scroll2Y;
	half _AMultiplier;
	fixed4 _Color;

	struct appdata{
		float4 vertex : POSITION;
		float2 texcoord : TEXCOORD0;
	};

	struct v2f {
		float4 pos : SV_POSITION;
		float4 uv : TEXCOORD0;
	};
	
	v2f vert (appdata v)
	{
		v2f o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
		o.uv.xy = v.texcoord.xy + frac(float2(_ScrollX, _ScrollY) * _Time.x);
		o.uv.zw = v.texcoord.xy + frac(float2(_Scroll2X, _Scroll2Y) * _Time.x);
		return o;
	}
	ENDCG


	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma fragmentoption ARB_precision_hint_fastest		
		fixed4 frag (v2f i) : COLOR
		{
			fixed4 o;
			fixed4 tex = tex2D (_MainTex, i.uv.xy);
			fixed4 tex2 = tex2D (_DetailTex, i.uv.zw);
			
			o = lerp(tex, tex2, _AMultiplier);
			o *= _Color;
			
			return o;
		}
		ENDCG
	}
}
}

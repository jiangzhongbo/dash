﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

public class CreateMesh
{

    [MenuItem("Tools/create")]
    static void Start1()
    {
        GameObject root = Selection.activeGameObject;

        if (root)
        {
            MeshRenderer[] meshRenderers = root.GetComponentsInChildren<MeshRenderer>();

            GameObject[] gos = new GameObject[meshRenderers.Length];
            for (int i = 0; i < meshRenderers.Length; i++)
            {
                gos[i] = meshRenderers[i].gameObject;
            }
            StaticBatchingUtility.Combine(gos, root);
        }
    }
}
Shader "AE/AEVertAO (Pixel shaded)" {
	
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_MainColor ("Main Color", Color) = (0, 0, 0, 0)
		_AOColor ("AO Color", Color) = (0,0,0,1)
		_AOIntensity ("AO Intensity", Range(0, 1)) = 1.0
		_AOPower ("AO Power", Range(1, 10)) = 1.0
		_Illum ("Illumin (A)", 2D) = "white" {}
		_EmissionLM ("Emission (Lightmapper)", Float) = 0
	}
	
	SubShader {
		
		Tags {
			"RenderType" = "Opaque"
		}
		
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
		
		sampler2D _MainTex;
		sampler2D _Illum;
		half4 _AOColor;
		half4 _MainColor;
		float _AOIntensity;
		float _AOPower;
		
		struct Input {
			float2 uv_MainTex : TEXCOORD0;
			float2 uv_Illum;
			float4 color      : COLOR;
		};
		
		
		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
			fixed4 c = tex * _MainColor;
			half ao = pow((1-IN.color.a)*_AOIntensity, _AOPower );
			o.Emission = c.rgb * tex2D(_Illum, IN.uv_Illum).a;
			o.Albedo = lerp(c.rgb , _AOColor, ao);
			//o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		
		ENDCG
	}
	
	FallBack "Diffuse"
	
}

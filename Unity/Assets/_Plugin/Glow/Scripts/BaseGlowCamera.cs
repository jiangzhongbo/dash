﻿using UnityEngine;
using System.Collections;

public class BaseGlowCamera : MonoBehaviour
{
    internal IBlur _blur;

    public Camera parentCamera;

    public Settings settings;

    public Glow glow;

    public bool highPrecision;

    private Shader _glowOnly;

    internal IBlur blur
    {
        get
        {
            return this._blur;
        }
        set
        {
            this._blur = value;
        }
    }

    protected Shader glowOnly
    {
        get
        {
            if (!this._glowOnly)
            {
                this._glowOnly = Shader.Find("Hidden/Glow 11/GlowObjects");
            }
            return this._glowOnly;
        }
    }

    internal virtual void Init()
    {
    }
}

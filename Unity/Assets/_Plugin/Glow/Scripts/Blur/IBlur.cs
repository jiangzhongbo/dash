﻿using UnityEngine;
using System.Collections;

public interface IBlur
{
    void BlurAndBlitBuffer(RenderTexture rbuffer, RenderTexture destination, Settings settings, bool highPrecision);
}

﻿public enum DownsampleBlendMode
{
    Additive,
    Screen,
    Max = 100
}

﻿public enum DownsampleResolution
{
    Half = 2,
    Quarter = 4
}

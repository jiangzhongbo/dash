﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("")]
public class GlowCameraRerender : BaseGlowCamera
{
    private void OnPreCull()
    {
        camera.CopyFrom(this.parentCamera);
        camera.backgroundColor = Color.black;
        camera.clearFlags = CameraClearFlags.Color;
        camera.SetReplacementShader(base.glowOnly, "RenderType");
        camera.renderingPath = 0;
        camera.depth = this.parentCamera.depth + 0.1f;
    }

    private void OnPreRender()
    {
        camera.projectionMatrix = this.parentCamera.projectionMatrix;
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        base.blur.BlurAndBlitBuffer(source, destination, this.settings, this.highPrecision);
    }
}

﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("")]
internal class GlowCameraRerenderOnly : BaseGlowCamera
{
    private void OnPreCull()
    {
        RenderTexture targetTexture = camera.targetTexture;
        camera.CopyFrom(this.parentCamera);
        camera.backgroundColor = Color.black;
        camera.clearFlags = CameraClearFlags.Color;
        camera.SetReplacementShader(base.glowOnly, "RenderType");
        camera.renderingPath = RenderingPath.VertexLit;
        camera.targetTexture = targetTexture;
    }

    private void OnPreRender()
    {
        camera.projectionMatrix = this.parentCamera.projectionMatrix;
    }
}

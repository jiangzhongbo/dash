﻿using UnityEngine;
using System.Collections;
[AddComponentMenu(""), ExecuteInEditMode]
public class GlowCameraReuse : BaseGlowCamera
{
    private GlowCameraReuseHelper helper;

    public RenderTexture screenRt;

    private RenderTexture tmpRt;

    private void ActivateHelper()
    {
        if (this.parentCamera && !this.helper)
        {
            this.helper = this.parentCamera.gameObject.AddComponent<GlowCameraReuseHelper>();
            this.helper.hideFlags = HideFlags.HideInInspector;
            this.helper.glowCam = this;
        }
    }

    private void OnDisable()
    {
        if (!Application.isEditor)
        {
            this.helper.glowCam = null;
            UnityEngine.Object.Destroy(this.helper);
        }
        else
        {
            this.helper.glowCam = null;
        }
    }

    private void OnEnable()
    {
        this.ActivateHelper();
    }

    internal override void Init()
    {
        this.ActivateHelper();
    }

    private void OnPreCull()
    {
        this.ActivateHelper();
        camera.CopyFrom(this.parentCamera);
        camera.backgroundColor = Color.black;
        camera.clearFlags = CameraClearFlags.Nothing;
        camera.SetReplacementShader(base.glowOnly, "RenderEffect");
        camera.renderingPath = RenderingPath.VertexLit;
        this.tmpRt = RenderTexture.GetTemporary(this.screenRt.width, this.screenRt.height);
        RenderTexture.active = this.tmpRt;
        GL.Clear(false, true, Color.black);
        base.GetComponent<Camera>().targetTexture = this.tmpRt;
        //base.GetComponent<Camera>().SetTargetBuffers(this.tmpRt.colorBuffer, this.screenRt.depthBuffer);
    }

    private void OnPreRender()
    {
        base.GetComponent<Camera>().projectionMatrix = this.parentCamera.projectionMatrix;
    }

    private void OnPostRender()
    {
        base.blur.BlurAndBlitBuffer(this.tmpRt, this.screenRt, this.settings, this.highPrecision);
        RenderTexture.ReleaseTemporary(this.tmpRt);
    }
}

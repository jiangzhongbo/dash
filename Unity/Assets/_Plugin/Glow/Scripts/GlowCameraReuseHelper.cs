﻿using UnityEngine;
using System.Collections;

[AddComponentMenu(""), ExecuteInEditMode]
public class GlowCameraReuseHelper : MonoBehaviour
{
    internal GlowCameraReuse glowCam;

    private void OnPreCull()
    {
        if (!this.glowCam)
        {
            if (Application.isPlaying)
            {
                UnityEngine.Object.Destroy(this);
            }
            else
            {
                UnityEngine.Object.DestroyImmediate(this);
            }
        }
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (!this.glowCam)
        {
            this.OnPreCull();
            return;
        }
        if (!this.glowCam.glow.CheckSupport())
        {
            return;
        }
        this.glowCam.screenRt = source;
        this.glowCam.gameObject.GetComponent<Camera>().Render();
        Graphics.Blit(source, destination);
    }
}

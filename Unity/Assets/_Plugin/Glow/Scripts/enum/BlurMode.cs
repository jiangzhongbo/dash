﻿public enum BlurMode
{
    Default,
    Advanced = 5,
    HighQuality = 10,
    UnityBlur = 100
}

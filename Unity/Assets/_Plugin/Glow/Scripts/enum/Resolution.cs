﻿
public enum Resolution
{
    Full = 1,
    Half,
    Quarter = 4
}
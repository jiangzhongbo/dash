using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
public static class ConverterUtils {
	public static bool IsDictionary(object source){
		return source is IDictionary;
	}

	public static bool IsDictionary(Type type){
		return typeof(IDictionary).IsAssignableFrom(type);
	}

	public static bool IsList(object source){
		return source is IList;
	}

	public static bool IsList(Type type){
		return typeof(IList).IsAssignableFrom(type);
	}

	public static bool IsPrimitiveType(object value){
        if (value == null) return false;
		Type t = value.GetType();
		if(t.IsPrimitive || t == typeof(decimal) || t == typeof(string)){
			return true;
		}
		return false;
	}

	public static bool IsOfType<T>(object value)
	{
		return value is T;
	}

	public static object ConvertList(object value, Type type)
	{
		var ilist = value as IList;
		var list = ilist.Cast<object>().ToList();
		IList ret = (IList)Activator.CreateInstance(type);
		foreach (var item in list)
		{
			ret.Add(item);
		}
		return ret;
	}
}

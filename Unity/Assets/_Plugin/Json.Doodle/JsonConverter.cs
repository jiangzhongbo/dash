using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;
using System.Linq;
namespace Json.Doodle{
	public static class JsonConverter {
		public static string Serialize(object o){
            string json = MiniJSON.Json.Serialize(o.FromObjectToJsonStructure());
			return json;
		}
		
		public static T Deserialize<T>(string json){
            var o = MiniJSON.Json.Deserialize(json);
			T t = o.FromJsonStructureToObject<T>();
			return t;
		}
	}
}

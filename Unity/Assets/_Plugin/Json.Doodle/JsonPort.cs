﻿using UnityEngine;
using System.Collections;
using System;
namespace Json.Doodle
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class JsonPort : Attribute
    {
        private JsonPortType jsonPortType;

        public JsonPortType JsonPortType
        {
            get
            {
                return jsonPortType;
            }
        }
        public JsonPort(JsonPortType type)
        {
            jsonPortType = type;
        }
    }
}

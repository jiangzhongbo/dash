﻿using UnityEngine;
using System.Collections;

namespace Json.Doodle
{
    public enum JsonPortType
    {
        Field,
        Property
    }

}
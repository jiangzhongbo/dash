using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;
using MiniJSON;
namespace Json.Doodle {
	public static class JsonStructureToObjectExtend {

		public static T FromJsonStructureToObject<T>(this object source){
			if (source == null) ThrowExceptionWhenSourceArgumentIsNull();
            var objs = source.GetType().GetCustomAttributes(typeof(JsonPort), true);
            object ret = null;
            if (objs != null && objs.Length > 0)
            {
                if (objs.Cast<JsonPort>().First().JsonPortType == JsonPortType.Field)
                {
                    ret = translateFields(typeof(T), source);
                }
                else
                {
                    ret = translateProperties(typeof(T), source);
                }
            }
            else
            {
                ret = translateProperties(typeof(T), source);
            }
			if(ConverterUtils.IsList(ret)){
				return (T)ConverterUtils.ConvertList(ret, typeof(T));
			}
			else if(ConverterUtils.IsOfType<T>(ret)){
				return (T)ret;
			}
			else if(ConverterUtils.IsDictionary(ret)){
				return (T)ret;
			}
			throw new NotSupportedException("Not Supported +"+typeof(T));
		}
				
		private static void ThrowExceptionWhenSourceArgumentIsNull()
		{
			throw new ArgumentNullException("source", "The source object is null.");
		}
	
		private static object translateProperties(Type targetType, object obj){
			if(ConverterUtils.IsList(targetType)){
				IList ilist = obj as IList;
				List<object> list = ilist.Cast<object>().ToList();
				List<object> ret = new List<object>();
				for(int i = 0; i < list.Count; i++){
					ret.Add(translateProperties(targetType.GetGenericArguments()[0], list[i]));
				}
				return ret;
			}
			else if(ConverterUtils.IsDictionary(targetType)){
				Dictionary<string, object> dict = obj as Dictionary<string, object>;
				var t = Activator.CreateInstance(targetType) as IDictionary;
				foreach(string k in dict.Keys){
					t[k] = translateProperties(targetType.GetGenericArguments()[1],dict[k]);
				}
				return t;
			}
			else if(ConverterUtils.IsPrimitiveType(obj)){
				return obj;
			}
			else{
				Dictionary<string, object> dict = obj as Dictionary<string, object>;
				var t = Activator.CreateInstance(targetType);
				var properties = targetType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
				for(int i = 0; i < properties.Length; i++){
					if(dict.ContainsKey(properties[i].Name)){
						var v = dict[properties[i].Name];
						v = translateProperties(properties[i].PropertyType, v);
						v = Convert.ChangeType(v, properties[i].PropertyType);
						properties[i].SetValue(t, v, null);
					}else{
						properties[i].SetValue(t, null, null);
					}
				}
				return t;
			}
		}

        private static object translateFields(Type targetType, object obj)
        {
            if (ConverterUtils.IsList(targetType))
            {
                IList ilist = obj as IList;
                List<object> list = ilist.Cast<object>().ToList();
                List<object> ret = new List<object>();
                for (int i = 0; i < list.Count; i++)
                {
                    ret.Add(translateFields(targetType.GetGenericArguments()[0], list[i]));
                }
                return ret;
            }
            else if (ConverterUtils.IsDictionary(targetType))
            {
                Dictionary<string, object> dict = obj as Dictionary<string, object>;
                var t = Activator.CreateInstance(targetType) as IDictionary;
                foreach (string k in dict.Keys)
                {
                    t[k] = translateFields(targetType.GetGenericArguments()[1], dict[k]);
                }
                return t;
            }
            else if (ConverterUtils.IsPrimitiveType(obj))
            {
                return obj;
            }
            else
            {
                Dictionary<string, object> dict = obj as Dictionary<string, object>;
                var t = Activator.CreateInstance(targetType);
                var fields = targetType.GetFields(BindingFlags.Public | BindingFlags.Instance);

                for (int i = 0; i < fields.Length; i++)
                {
                    if (dict.ContainsKey(fields[i].Name))
                    {
                        var v = dict[fields[i].Name];
                        v = translateFields(fields[i].FieldType, v);
                        v = Convert.ChangeType(v, fields[i].FieldType);
                        fields[i].SetValue(t, v);
                    }
                    else
                    {
                        fields[i].SetValue(t, null);
                    }
                }
                return t;
            }
        }

	}
}
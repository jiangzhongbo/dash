using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;
using MiniJSON;
namespace Json.Doodle {
	public static class ObjectToJsonStructureExtend {
		public static object FromObjectToJsonStructure(this object source){
			if (source == null) ThrowExceptionWhenSourceArgumentIsNull();
            var objs = source.GetType().GetCustomAttributes(typeof(JsonPort), true);
            if (objs != null && objs.Length > 0)
            {
                if (objs.Cast<JsonPort>().First().JsonPortType == JsonPortType.Field)
                {
                    return translateFields(source);
                }
                else
                {
                    return translateProperties(source);
                }
            }
			return translateProperties(source);
	    }
	    
		private static void ThrowExceptionWhenSourceArgumentIsNull()
		{
			throw new ArgumentNullException("source", "Unable to convert object to a dictionary. The source object is null.");
		}

		private static object translateProperties(object obj){
			if(ConverterUtils.IsList(obj)){
				IList ilist = obj as IList;
				List<object> list = ilist.Cast<object>().ToList();
				List<object> ret = new List<object>();
				for(int i = 0; i < list.Count; i++){
					ret.Add(translateProperties(list[i]));
				}
				return ret;
			}
			else if(ConverterUtils.IsDictionary(obj)){
				IDictionary idict = obj as IDictionary;
				List<string> keys = idict.Keys.Cast<string>().ToList();
				Dictionary<string, object> ret = new Dictionary<string, object>();
				foreach(string k in keys){
					ret[k] =  translateProperties(idict[k]);
				}
				return ret;
			}
			else if(ConverterUtils.IsPrimitiveType(obj)){
				return obj;
			}
			else{
				var dictionary = new Dictionary<string, object>();
				var properties = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
				for(int i = 0; i < properties.Length; i++){
					dictionary[properties[i].Name] = properties[i].GetValue(obj, null);
				}
				return dictionary;
			}
		}

        private static object translateFields(object obj)
        {
            if (ConverterUtils.IsList(obj))
            {
                IList ilist = obj as IList;
                List<object> list = ilist.Cast<object>().ToList();
                List<object> ret = new List<object>();
                for (int i = 0; i < list.Count; i++)
                {
                    ret.Add(translateFields(list[i]));
                }
                return ret;
            }
            else if (ConverterUtils.IsDictionary(obj))
            {
                IDictionary idict = obj as IDictionary;
                List<string> keys = idict.Keys.Cast<string>().ToList();
                Dictionary<string, object> ret = new Dictionary<string, object>();
                foreach (string k in keys)
                {
                    ret[k] = translateFields(idict[k]);
                }
                return ret;
            }
            else if (ConverterUtils.IsPrimitiveType(obj))
            {
                return obj;
            }
            else
            {
                var dictionary = new Dictionary<string, object>();
                var fields = obj.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
                for (int i = 0; i < fields.Length; i++)
                {
                    dictionary[fields[i].Name] = fields[i].GetValue(obj);
                }
                return dictionary;
            }
        }

	}
}

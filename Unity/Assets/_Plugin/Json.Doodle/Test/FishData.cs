using UnityEngine;
public class FishData : MonoBehaviour 
{
static public string Data = @"
[
{""IsComplete"":1,""UID"":""Fish_46"",""ObjectType"":""Fish"",""No"":46,""Order"":1,""Name"":""Mud Carp"",""Probability"":100,""Grade"":""0_E"",""Weight"":1.05,""Strength"":10,""RecLurePropsPropID"":""BaitLure_3"",""Coins"":10,""XP"":60,""LocationSceneUID"":""Scene_1,Scene_2,Scene_6,""}
,

{""IsComplete"":1,""UID"":""Fish_10"",""ObjectType"":""Fish"",""No"":10,""Order"":2,""Name"":""Branchiostegus Japonicus"",""Probability"":80,""Grade"":""0_E"",""Weight"":2.1,""Strength"":11,""RecLurePropsPropID"":""BaitLure_1"",""Coins"":21,""XP"":110,""LocationSceneUID"":""Scene_1,Scene_2,Scene_3,""}
,

{""IsComplete"":1,""UID"":""Fish_37"",""ObjectType"":""Fish"",""No"":37,""Order"":3,""Name"":""Tripletail"",""Probability"":60,""Grade"":""1_D"",""Weight"":2.94,""Strength"":12,""RecLurePropsPropID"":""BaitLure_4"",""Coins"":29,""XP"":166,""LocationSceneUID"":""Scene_1,""}
,

{""IsComplete"":1,""UID"":""Fish_18"",""ObjectType"":""Fish"",""No"":18,""Order"":4,""Name"":""Pomfret"",""Probability"":40,""Grade"":""1_D"",""Weight"":3.36,""Strength"":12,""RecLurePropsPropID"":""BaitLure_5"",""Coins"":33,""XP"":180,""LocationSceneUID"":""Scene_1,Scene_2,""}
,

{""IsComplete"":1,""UID"":""Fish_1"",""ObjectType"":""Fish"",""No"":1,""Order"":5,""Name"":""Tuna"",""Probability"":20,""Grade"":""2_C"",""Weight"":4.2,""Strength"":13,""RecLurePropsPropID"":""BaitLure_6"",""Coins"":42,""XP"":142,""LocationSceneUID"":""Scene_1,Scene_3,Scene_5,Scene_8,""}
,

{""IsComplete"":1,""UID"":""Fish_5"",""ObjectType"":""Fish"",""No"":5,""Order"":6,""Name"":""Scallop"",""Probability"":100,""Grade"":""0_E"",""Weight"":0.42,""Strength"":14,""RecLurePropsPropID"":""BaitLure_1"",""Coins"":4,""XP"":187,""LocationSceneUID"":""Scene_2,Scene_3,""}
,

{""IsComplete"":1,""UID"":""Fish_32"",""ObjectType"":""Fish"",""No"":32,""Order"":7,""Name"":""Trout"",""Probability"":80,""Grade"":""1_D"",""Weight"":1.05,""Strength"":15,""RecLurePropsPropID"":""BaitLure_10"",""Coins"":10,""XP"":235,""LocationSceneUID"":""Scene_2,Scene_4,""}
,

{""IsComplete"":1,""UID"":""Fish_11"",""ObjectType"":""Fish"",""No"":11,""Order"":8,""Name"":""Hexagrammidae"",""Probability"":60,""Grade"":""1_D"",""Weight"":1.26,""Strength"":15,""RecLurePropsPropID"":""BaitLure_8"",""Coins"":12,""XP"":284,""LocationSceneUID"":""Scene_2,Scene_6,""}
,

{""IsComplete"":1,""UID"":""Fish_3"",""ObjectType"":""Fish"",""No"":3,""Order"":9,""Name"":""Pacific Salmon"",""Probability"":40,""Grade"":""2_C"",""Weight"":2.52,""Strength"":16,""RecLurePropsPropID"":""BaitLure_9"",""Coins"":25,""XP"":335,""LocationSceneUID"":""Scene_2,Scene_4,""}
,

{""IsComplete"":1,""UID"":""Fish_19"",""ObjectType"":""Fish"",""No"":19,""Order"":10,""Name"":""Catfish"",""Probability"":20,""Grade"":""2_C"",""Weight"":5.04,""Strength"":17,""RecLurePropsPropID"":""BaitLure_18"",""Coins"":50,""XP"":388,""LocationSceneUID"":""Scene_2,Scene_9,Scene_8,""}
,

{""IsComplete"":1,""UID"":""Fish_8"",""ObjectType"":""Fish"",""No"":8,""Order"":11,""Name"":""Hillstream Loach"",""Probability"":100,""Grade"":""0_E"",""Weight"":0.42,""Strength"":18,""RecLurePropsPropID"":""BaitLure_1"",""Coins"":4,""XP"":441,""LocationSceneUID"":""Scene_3,""}
,

{""IsComplete"":1,""UID"":""Fish_41"",""ObjectType"":""Fish"",""No"":41,""Order"":12,""Name"":""Puffer"",""Probability"":80,""Grade"":""1_D"",""Weight"":2.94,""Strength"":18,""RecLurePropsPropID"":""BaitLure_7"",""Coins"":29,""XP"":495,""LocationSceneUID"":""Scene_3,Scene_4,Scene_12,""}
,

{""IsComplete"":1,""UID"":""Fish_47"",""ObjectType"":""Fish"",""No"":47,""Order"":13,""Name"":""Coelacanth "",""Probability"":60,""Grade"":""2_C"",""Weight"":5.46,""Strength"":19,""RecLurePropsPropID"":""BaitLure_20"",""Coins"":54,""XP"":550,""LocationSceneUID"":""Scene_3,""}
,

{""IsComplete"":1,""UID"":""Fish_52"",""ObjectType"":""Fish"",""No"":52,""Order"":14,""Name"":""Giant Trevally"",""Probability"":40,""Grade"":""2_C"",""Weight"":5.46,""Strength"":20,""RecLurePropsPropID"":""BaitLure_16"",""Coins"":54,""XP"":500,""LocationSceneUID"":""Scene_3,""}
,

{""IsComplete"":1,""UID"":""Fish_6"",""ObjectType"":""Fish"",""No"":6,""Order"":15,""Name"":""Grouper"",""Probability"":20,""Grade"":""3_B"",""Weight"":5.88,""Strength"":21,""RecLurePropsPropID"":""BaitLure_12"",""Coins"":58,""XP"":472,""LocationSceneUID"":""Scene_3,Scene_5,Scene_12,Scene_8,""}
,

{""IsComplete"":1,""UID"":""Fish_13"",""ObjectType"":""Fish"",""No"":13,""Order"":16,""Name"":""Melanonus"",""Probability"":100,""Grade"":""0_E"",""Weight"":0.42,""Strength"":22,""RecLurePropsPropID"":""BaitLure_1"",""Coins"":4,""XP"":456,""LocationSceneUID"":""Scene_4,""}
,

{""IsComplete"":1,""UID"":""Fish_9"",""ObjectType"":""Fish"",""No"":9,""Order"":17,""Name"":""Common Carp"",""Probability"":80,""Grade"":""1_D"",""Weight"":2.52,""Strength"":22,""RecLurePropsPropID"":""BaitLure_15"",""Coins"":25,""XP"":447,""LocationSceneUID"":""Scene_4,""}
,

{""IsComplete"":1,""UID"":""Fish_25"",""ObjectType"":""Fish"",""No"":25,""Order"":18,""Name"":""Bass"",""Probability"":60,""Grade"":""2_C"",""Weight"":3.78,""Strength"":23,""RecLurePropsPropID"":""BaitLure_19"",""Coins"":37,""XP"":444,""LocationSceneUID"":""Scene_4,Scene_6,""}
,

{""IsComplete"":1,""UID"":""Fish_14"",""ObjectType"":""Fish"",""No"":14,""Order"":19,""Name"":""Flatfish"",""Probability"":40,""Grade"":""3_B"",""Weight"":4.2,""Strength"":24,""RecLurePropsPropID"":""BaitLure_21"",""Coins"":42,""XP"":443,""LocationSceneUID"":""Scene_4,Scene_5,""}
,

{""IsComplete"":1,""UID"":""Fish_15"",""ObjectType"":""Fish"",""No"":15,""Order"":20,""Name"":""Butterfly Fish"",""Probability"":20,""Grade"":""3_B"",""Weight"":0.63,""Strength"":25,""RecLurePropsPropID"":""BaitLure_22"",""Coins"":6,""XP"":445,""LocationSceneUID"":""Scene_4,Scene_9,Scene_8,""}
,

{""IsComplete"":1,""UID"":""Fish_16"",""ObjectType"":""Fish"",""No"":16,""Order"":21,""Name"":""European Pilchard"",""Probability"":100,""Grade"":""0_E"",""Weight"":1.05,""Strength"":25,""RecLurePropsPropID"":""BaitLure_1"",""Coins"":10,""XP"":449,""LocationSceneUID"":""Scene_5,""}
,

{""IsComplete"":1,""UID"":""Fish_42"",""ObjectType"":""Fish"",""No"":42,""Order"":22,""Name"":""Seahorse"",""Probability"":80,""Grade"":""1_D"",""Weight"":1.26,""Strength"":26,""RecLurePropsPropID"":""BaitLure_17"",""Coins"":12,""XP"":454,""LocationSceneUID"":""Scene_5,Scene_10,""}
,

{""IsComplete"":1,""UID"":""Fish_36"",""ObjectType"":""Fish"",""No"":36,""Order"":23,""Name"":""Rooster Fish"",""Probability"":60,""Grade"":""2_C"",""Weight"":5.04,""Strength"":27,""RecLurePropsPropID"":""BaitLure_18"",""Coins"":50,""XP"":460,""LocationSceneUID"":""Scene_5,Scene_10,""}
,

{""IsComplete"":1,""UID"":""Fish_23"",""ObjectType"":""Fish"",""No"":23,""Order"":24,""Name"":""Mola Mola"",""Probability"":40,""Grade"":""3_B"",""Weight"":10.5,""Strength"":28,""RecLurePropsPropID"":""BaitLure_14"",""Coins"":105,""XP"":467,""LocationSceneUID"":""Scene_5,""}
,

{""IsComplete"":1,""UID"":""Fish_2"",""ObjectType"":""Fish"",""No"":2,""Order"":25,""Name"":""Sailfish"",""Probability"":20,""Grade"":""4_A"",""Weight"":8.4,""Strength"":28,""RecLurePropsPropID"":""BaitLure_23"",""Coins"":84,""XP"":474,""LocationSceneUID"":""Scene_5,Scene_8,""}
,

{""IsComplete"":1,""UID"":""Fish_30"",""ObjectType"":""Fish"",""No"":30,""Order"":26,""Name"":""Mackerel"",""Probability"":100,""Grade"":""0_E"",""Weight"":1.05,""Strength"":29,""RecLurePropsPropID"":""BaitLure_1"",""Coins"":10,""XP"":482,""LocationSceneUID"":""Scene_6,Scene_9,""}
,

{""IsComplete"":1,""UID"":""Fish_24"",""ObjectType"":""Fish"",""No"":24,""Order"":27,""Name"":""Anguillidae"",""Probability"":80,""Grade"":""1_D"",""Weight"":1.68,""Strength"":30,""RecLurePropsPropID"":""BaitLure_11"",""Coins"":16,""XP"":491,""LocationSceneUID"":""Scene_6,""}
,

{""IsComplete"":1,""UID"":""Fish_29"",""ObjectType"":""Fish"",""No"":29,""Order"":28,""Name"":""Croaker"",""Probability"":60,""Grade"":""2_C"",""Weight"":1.68,""Strength"":31,""RecLurePropsPropID"":""BaitLure_20"",""Coins"":16,""XP"":500,""LocationSceneUID"":""Scene_6,""}
,

{""IsComplete"":1,""UID"":""Fish_22"",""ObjectType"":""Fish"",""No"":22,""Order"":29,""Name"":""Bighead Carp"",""Probability"":40,""Grade"":""3_B"",""Weight"":5.04,""Strength"":32,""RecLurePropsPropID"":""BaitLure_12"",""Coins"":50,""XP"":509,""LocationSceneUID"":""Scene_6,""}
,

{""IsComplete"":1,""UID"":""Fish_45"",""ObjectType"":""Fish"",""No"":45,""Order"":30,""Name"":""Guitarfish "",""Probability"":20,""Grade"":""4_A"",""Weight"":8.4,""Strength"":32,""RecLurePropsPropID"":""BaitLure_13"",""Coins"":84,""XP"":518,""LocationSceneUID"":""Scene_6,Scene_10,Scene_8,""}
,

{""IsComplete"":1,""UID"":""Fish_40"",""ObjectType"":""Fish"",""No"":40,""Order"":31,""Name"":""Lepomis"",""Probability"":100,""Grade"":""0_E"",""Weight"":1.26,""Strength"":33,""RecLurePropsPropID"":""BaitLure_1"",""Coins"":12,""XP"":528,""LocationSceneUID"":""Scene_9,""}
,

{""IsComplete"":1,""UID"":""Fish_20"",""ObjectType"":""Fish"",""No"":20,""Order"":32,""Name"":""Crucian Carp"",""Probability"":80,""Grade"":""1_D"",""Weight"":1.68,""Strength"":34,""RecLurePropsPropID"":""BaitLure_7"",""Coins"":16,""XP"":538,""LocationSceneUID"":""Scene_9,""}
,

{""IsComplete"":1,""UID"":""Fish_12"",""ObjectType"":""Fish"",""No"":12,""Order"":33,""Name"":""Snapper"",""Probability"":60,""Grade"":""2_C"",""Weight"":3.36,""Strength"":35,""RecLurePropsPropID"":""BaitLure_16"",""Coins"":33,""XP"":548,""LocationSceneUID"":""Scene_9,""}
,

{""IsComplete"":1,""UID"":""Fish_26"",""ObjectType"":""Fish"",""No"":26,""Order"":34,""Name"":""Northern Snakehead"",""Probability"":40,""Grade"":""3_B"",""Weight"":5.04,""Strength"":35,""RecLurePropsPropID"":""BaitLure_21"",""Coins"":50,""XP"":558,""LocationSceneUID"":""Scene_9,Scene_12,""}
,

{""IsComplete"":1,""UID"":""Fish_21"",""ObjectType"":""Fish"",""No"":21,""Order"":35,""Name"":""Siniperca Obscura"",""Probability"":20,""Grade"":""4_A"",""Weight"":5.04,""Strength"":36,""RecLurePropsPropID"":""BaitLure_24"",""Coins"":50,""XP"":568,""LocationSceneUID"":""Scene_9,Scene_8,""}
,

{""IsComplete"":1,""UID"":""Fish_4"",""ObjectType"":""Fish"",""No"":4,""Order"":36,""Name"":""Fiddler Crab"",""Probability"":100,""Grade"":""0_E"",""Weight"":0.63,""Strength"":37,""RecLurePropsPropID"":""BaitLure_1"",""Coins"":6,""XP"":579,""LocationSceneUID"":""Scene_10,""}
,

{""IsComplete"":1,""UID"":""Fish_44"",""ObjectType"":""Fish"",""No"":44,""Order"":37,""Name"":""Squirrelfish"",""Probability"":80,""Grade"":""1_D"",""Weight"":1.05,""Strength"":38,""RecLurePropsPropID"":""BaitLure_15"",""Coins"":10,""XP"":590,""LocationSceneUID"":""Scene_10,""}
,

{""IsComplete"":1,""UID"":""Fish_28"",""ObjectType"":""Fish"",""No"":28,""Order"":38,""Name"":""Buffalo Fish"",""Probability"":60,""Grade"":""2_C"",""Weight"":4.2,""Strength"":38,""RecLurePropsPropID"":""BaitLure_19"",""Coins"":42,""XP"":600,""LocationSceneUID"":""Scene_10,""}
,

{""IsComplete"":1,""UID"":""Fish_43"",""ObjectType"":""Fish"",""No"":43,""Order"":39,""Name"":""Killer Whale"",""Probability"":40,""Grade"":""3_B"",""Weight"":33.6,""Strength"":39,""RecLurePropsPropID"":""BaitLure_22"",""Coins"":336,""XP"":611,""LocationSceneUID"":""Scene_10,""}
,

{""IsComplete"":1,""UID"":""Fish_17"",""ObjectType"":""Fish"",""No"":17,""Order"":40,""Name"":""Shark"",""Probability"":20,""Grade"":""4_A"",""Weight"":37.8,""Strength"":40,""RecLurePropsPropID"":""BaitLure_25"",""Coins"":378,""XP"":622,""LocationSceneUID"":""Scene_10,Scene_8,""}
,

{""IsComplete"":1,""UID"":""Fish_50"",""ObjectType"":""Fish"",""No"":50,""Order"":41,""Name"":""Goby Fish "",""Probability"":100,""Grade"":""0_E"",""Weight"":1.05,""Strength"":41,""RecLurePropsPropID"":""BaitLure_1"",""Coins"":10,""XP"":633,""LocationSceneUID"":""Scene_12,""}
,

{""IsComplete"":1,""UID"":""Fish_33"",""ObjectType"":""Fish"",""No"":33,""Order"":42,""Name"":""Peacock Bass"",""Probability"":80,""Grade"":""1_D"",""Weight"":2.94,""Strength"":42,""RecLurePropsPropID"":""BaitLure_17"",""Coins"":29,""XP"":644,""LocationSceneUID"":""Scene_12,Scene_8,""}
,

{""IsComplete"":1,""UID"":""Fish_35"",""ObjectType"":""Fish"",""No"":35,""Order"":43,""Name"":""Bicuda"",""Probability"":60,""Grade"":""2_C"",""Weight"":4.2,""Strength"":42,""RecLurePropsPropID"":""BaitLure_16"",""Coins"":42,""XP"":655,""LocationSceneUID"":""Scene_12,""}
,

{""IsComplete"":1,""UID"":""Fish_34"",""ObjectType"":""Fish"",""No"":34,""Order"":44,""Name"":""Arapaima"",""Probability"":40,""Grade"":""3_B"",""Weight"":6.72,""Strength"":43,""RecLurePropsPropID"":""BaitLure_14"",""Coins"":67,""XP"":666,""LocationSceneUID"":""Scene_12,Scene_8,""}
,

{""IsComplete"":1,""UID"":""Fish_48"",""ObjectType"":""Fish"",""No"":48,""Order"":45,""Name"":""Angelfish "",""Probability"":20,""Grade"":""4_A"",""Weight"":0.42,""Strength"":44,""RecLurePropsPropID"":""BaitLure_24"",""Coins"":4,""XP"":677,""LocationSceneUID"":""Scene_12,Scene_8,""}
,

{""IsComplete"":1,""UID"":""Fish_31"",""ObjectType"":""Fish"",""No"":31,""Order"":46,""Name"":""Milk Fish"",""Probability"":100,""Grade"":""0_E"",""Weight"":5.46,""Strength"":45,""RecLurePropsPropID"":""BaitLure_1"",""Coins"":54,""XP"":688,""LocationSceneUID"":""Scene_8,""}
]
";
}


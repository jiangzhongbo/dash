public class JsonFish {
	public int IsComplete {
		get; set;
	}
	public string UID {
		get; set;
	}
	public string ObjectType {
		get; set;
	}
	public int No {
		get; set;
	}
	public int Order{
		get; set;
	}
	public string Name{
		get; set;
	}
	public float Probability{
		get; set;
	}
	public string Grade {
		get; set;
	}
	public float Weight {
		get; set;
	}
	public int Strength {
		get; set;
	}
	public string RecLurePropsPropID {
		get; set;
	}
	public int Coins {
		get; set;
	}
	public int XP {
		get; set;
	}
	public string LocationSceneUID{
		get; set;
	}
	public override string ToString ()
	{
		return string.Format ("[JsonFish: IsComplete={0}, UID={1}, ObjectType={2}, No={3}, Order={4}, Name={5}, Probability={6}, Grade={7}, Weight={8}, Strength={9}, RecLurePropsPropID={10}, Coins={11}, XP={12}, LocationSceneUID={13}]", IsComplete, UID, ObjectType, No, Order, Name, Probability, Grade, Weight, Strength, RecLurePropsPropID, Coins, XP, LocationSceneUID);
	}
	
}

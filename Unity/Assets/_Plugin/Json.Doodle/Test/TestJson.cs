using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Json.Doodle;
using System.Linq;
using System;
using MiniJSON;
public class Scene{
	public string Name{
		get;set;
	}
	public int SceneID{
		get;set;
	}
	public Info Info{
		get;set;
	}

	public override string ToString ()
	{
		return string.Format ("[Scene: Name={0}, SceneID={1}, Info={2}]", Name, SceneID, Info);
	}
}

public class Info{
	public string Msg{
		get;set;
	}
	public override string ToString ()
	{
		return string.Format ("[Info: Msg={0}]", Msg);
	}
	
}

public class Infos{
	public Info Info1{
		get;set;
	}
	public Info Info2{
		get;set;
	}
	public override string ToString ()
	{
		return string.Format ("[Infos: Info1={0}, Info2={1}]", Info1, Info2);
	}
	
}

public class TestJson : MonoBehaviour {

	void Start () {
		testJsonDoodleDeserialize();
		testJsonDoodleSerialize();
		testSerialize();
		testDeserialize();
	}

	void testJsonDoodleDeserialize(){
        Debug.Log("######## test Json.Doodle Deserialize");
		DateTime start = DateTime.Now;
		for(int i = 0; i < 1; i++){
			List<JsonFish> fishs = Json.Doodle.JsonConverter.Deserialize<List<JsonFish>>(FishData.Data);
		}
		DateTime end = DateTime.Now;
		Debug.Log("testJsonDoodleDeserialize json.length:"+FishData.Data.Count());
        Debug.Log("testJsonDoodleDeserialize cost:" + (end - start).TotalMilliseconds);
	}
	
	void testJsonDoodleSerialize(){
        Debug.Log("######## test Json.Doodle Serialize");
		List<JsonFish> fishs = Json.Doodle.JsonConverter.Deserialize<List<JsonFish>>(FishData.Data);
		DateTime start = DateTime.Now;
		for(int i = 0; i < 1; i++){
			Json.Doodle.JsonConverter.Serialize(fishs);
		}
		DateTime end = DateTime.Now;
        Debug.Log("testJsonDoodleSerialize json.length:" + FishData.Data.Count());
        Debug.Log("testJsonDoodleSerialize cost:" + (end - start).TotalMilliseconds);
	}

	object testCostTime(Func<object> func){
		DateTime start = DateTime.Now;
		object obj = func();
		DateTime end = DateTime.Now;
        Debug.Log("test " + func.Method.Name + " cost:" + (end - start).TotalMilliseconds);
		return obj;
	}

	string json = "[{\"Name\":\"scene Name one\",\"SceneID\":\"1\", \"Info\":{\"Msg\" : \"msg\"}},{\"Name\":\"scene Name two\",\"SceneID\":\"2\", \"Info\":{\"Msg\" : \"msg\"}}]";
	string json1 = "{\"Name\":\"scene Name one\",\"SceneID\":\"1\", \"Info\":{\"Msg\" : \"msg\"}}";
	string json2 = "{\"Info1\":{\"Msg\" : \"msg1\"}, \"Info2\":{\"Msg\" : \"msg2\"}}";


	void  testDeserializeList(){
        Debug.Log("######## test  Deserialize List");
		List<Scene> scenes = testCostTime(
			() => {
				return JsonConverter.Deserialize<List<Scene>>(json);
			}
			) as List<Scene>;
		scenes.ForEach(e => {
            Debug.Log("#e:" + e.ToString());
		});
	}

	void  testDeserializeObject(){
        Debug.Log("######## test  Deserialize Object");
		Scene scene = testCostTime(
			() => {
			return JsonConverter.Deserialize<Scene>(json1);
			}
		) as Scene;
        Debug.Log("#scene:" + scene.ToString());
	}

	void  testDeserializeDictionaryOrObject(){
        Debug.Log("######## test  Deserialize Same Json to Dictionary or Object");
		Dictionary<string, Info> dict = testCostTime(
			() => {
			return JsonConverter.Deserialize<Dictionary<string, Info>>(json2);
			}
		) as Dictionary<string, Info>;
		dict.ToList().ForEach(e => {
            Debug.Log("#e.k:" + e.Key);
            Debug.Log("#e.TempV:" + e.Value.ToString());
		});

		Infos infos = testCostTime(
			() => {
			return JsonConverter.Deserialize<Infos>(json2);
			}
		) as Infos;
        Debug.Log("#infos:" + infos.ToString());
	}

	void testDeserialize(){
		testDeserializeList();
		testDeserializeObject();
		testDeserializeDictionaryOrObject();
	}

	void testSerialize(){
		List<object> misc = new List<object>();
		misc.Add(1);
		misc.Add("fuck");
		List<Scene> scenes = new List<Scene>();
		Scene s1 = new Scene();
		s1.Name = "s1";
		s1.SceneID = 1;
		s1.Info = new Info();
		s1.Info.Msg = "s1 msg";
		Scene s2 = new Scene();
		s2.Name = "s2";
		s2.SceneID = 2;
		s2.Info = new Info();
		s2.Info.Msg = "s2 msg";
		scenes.Add(s1);
		scenes.Add(s2);
		misc.Add(scenes);

		Dictionary<string, Scene> sceneDictionary = new Dictionary<string, Scene>();
		sceneDictionary.Add("s1", s2);

        Debug.Log("######## test Serialize List of  Scene");
		string scenesJson = JsonConverter.Serialize(scenes);
        Debug.Log("scenesJson:" + scenesJson);

        Debug.Log("######## test Serialize List of  Object");
		string miscJson =  JsonConverter.Serialize(misc);
        Debug.Log("miscJson:" + miscJson);

        Debug.Log("######## test Serialize Dictionary");
		string dictionaryJson =  JsonConverter.Serialize(sceneDictionary);
        Debug.Log("dictionaryJson:" + dictionaryJson);

	}

}

Shader "Kirnu/Marvelous/CustomLightingSimple2" {

	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_LayoutTexture ("Layout Texture", 2D) = "white" {}
		_LayoutTexturePower ("Layout Texture Power", Float) = 1
		_LightMaxDistance ("Light Max Distance", Float) = 10
 		_MainColor ("Main Color", Color) = (1,0.73,0.117,0)
 		_TopLight ("Top Light", Float) = 1
 		_RightLight ("Right Light", Float) = 1
 		_FrontLight ("Front Light", Float) = 1
		_RimColor ("Rim Color", Color) = (0,0,0,0)
		_RimPower ("Rim Power", Float) = 0.0
		
 		_LightTint ("Light Tint", Color) = (1,1,1,0)
 		_AmbientColor ("Ambient Color", Color) = (0.5,0.1,0.2,0.0)
 		_AmbientPower ("Ambient Power", Float) = 0.0
		_LightmapColor ("Lightmap Tint", Color) = (0,0,0,0)
		_LightmapPower ("Lightmap Power", Float) = 1
		_ShadowPower ("Shadow Light", Float) = 0
		_UseLightMap ("Lightmap Enabled", Float) = 0
	}
	SubShader {
		Tags { "QUEUE"="Geometry" "RenderType"="Opaque" }
		LOD 200
		
		Pass {

		Tags { "LIGHTMODE"="ForwardBase" "QUEUE"="Geometry" "RenderType"="Opaque" }
			CGPROGRAM
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma multi_compile_fwdbase

				#include "AutoLight.cginc"
				#include "UnityCG.cginc"
				#define USE_REALTIME_SHADOWS;

				#pragma vertex vert
				#pragma fragment frag

				sampler2D unity_Lightmap;
				float4 unity_LightmapST;

				struct CL_IN{
					half4 vertex : POSITION;
					half3 normal : NORMAL;
					half3 color : COLOR;
					half4 texcoord : TEXCOORD0;
					half4 texcoord1 : TEXCOORD1;
				};

				struct CL_OUT_WPOS {
					half4 pos : SV_POSITION;
					half2 lightmap_uv : TEXCOORD1;
					half3 lighting : TEXCOORD2;
					half4 wpos: TEXCOORD3;
					SHADOW_COORDS(6)
					half3 color : TEXCOORD7;
				};

				uniform half4 _MainColor;
				uniform half _TopLight;
				uniform half _RightLight;
				uniform half _FrontLight;
				uniform half3 _RimColor;
				uniform half _RimPower;
				
				uniform half3 _AmbientColor;
				uniform half _AmbientPower;
				uniform half _UseLightMap;
				
				uniform half _LightmapPower;
				uniform half4 _LightTint;
				uniform half4 _LightmapColor;
				uniform half _ShadowPower;
				
				uniform float _LayoutTexturePower;
				uniform float _LightMaxDistance;
				
				//#include "Marvelous.cginc"
				
				CL_OUT_WPOS vert(CL_IN v) {
					CL_OUT_WPOS o;
					o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
					o.wpos = mul( _Object2World, half4(v.vertex.xyz,1) );
					half3 normal =  normalize(mul(_Object2World,half4(v.normal,0))).xyz;
					o.color=v.color;
					half f_d = acos(clamp(dot(half3(0,0,-1),half3(0,0,normal.z)),-1,1))/1.5708;
					half r_d = acos(clamp(dot(half3(1,0,0),half3(normal.x,0,0)),-1,1))/1.5708;
					half t_d = acos(clamp(dot(half3(0,1,0),half3(0,normal.y,0)),-1,1))/1.5708;

					f_d = lerp(0,1-f_d,half(normal.z<1));
					r_d = lerp(0,1-r_d,half(normal.x>0));
					t_d = lerp(0,1-t_d,half(normal.y>0));


					half rim = 1-(f_d+r_d+t_d);

					o.lighting = (_FrontLight*f_d) + (_RightLight*r_d) + (_TopLight*t_d)+(_RimColor*rim*_RimPower);
					o.lightmap_uv = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
					TRANSFER_SHADOW(o);
					o.lighting = _MainColor*o.lighting+(_AmbientColor*_AmbientPower);

					return o;
				}
				
				fixed shadowAttenuation(CL_OUT_WPOS v){
					#ifdef USE_REALTIME_SHADOWS
					return SHADOW_ATTENUATION(v);
					#else
					return 1;
					#endif
				}

				fixed4 frag(CL_OUT_WPOS v) : COLOR {
					fixed4 outColor = fixed4(0.0, 0.0, 0.0, 0.0);

					half4 textureColor = half4((_LightTint * v.lighting),1);
	

					float attenuation = shadowAttenuation(v);
					fixed3 c = lerp(_LightmapColor.xyz * textureColor.xyz,textureColor.xyz, attenuation * attenuation);
		
					outColor = fixed4(c.xyz,1);
					outColor *= fixed4(DecodeLightmap(tex2D(unity_Lightmap, v.lightmap_uv)).xyz,1);
					return outColor;
				}

			ENDCG
		}
	}
	FallBack "Diffuse"
}

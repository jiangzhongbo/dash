Shader "Kirnu/Marvelous/Test" {

	Properties {
 		_MainColor ("Main Color", Color) = (1,0.73,0.117,0)
 		_TopLight ("Top Light", Float) = 1
 		_RightLight ("Right Light", Float) = 1
 		_FrontLight ("Front Light", Float) = 1
		
 		_LightTint ("Light Tint", Color) = (1,1,1,0)
 		_AmbientColor ("Ambient Color", Color) = (0.5,0.1,0.2,0.0)
 		_AmbientPower ("Ambient Power", Float) = 0.0

		_AOColor ("AO Color", Color) = (0,0,0,1)
		_AOIntensity ("AO Intensity", Range(0, 1)) = 1.0
		_AOPower ("AO Power", Range(1, 10)) = 1.0
	}
	SubShader {
		Tags { "QUEUE"="Geometry" "RenderType"="Opaque" }
		LOD 200
		
		Pass {

		Tags { "LIGHTMODE"="ForwardBase" "QUEUE"="Geometry" "RenderType"="Opaque" }
			CGPROGRAM

				#pragma fragmentoption ARB_precision_hint_fastest
				#include "UnityCG.cginc"
				#include "AutoLight.cginc"

				#define USE_LAYOUT_TEXTURE;
				
				#pragma vertex vert
				#pragma fragment frag

				uniform half3 _MainColor;
				uniform half _TopLight;
				uniform half _RightLight;
				uniform half _FrontLight;
				
				uniform half3 _AmbientColor;
				uniform half _AmbientPower;
				uniform half _UseLightMap;
				
				uniform half3 _LightTint;
				
				half4 _AOColor;
				float _AOIntensity;
				float _AOPower;

				struct CL_IN{
					half4 vertex : POSITION;
					half3 normal : NORMAL;
					half4 color : COLOR;
					half4 texcoord : TEXCOORD0;

				};

				struct CL_OUT_WPOS {
					half4 pos : SV_POSITION;
				#ifdef USE_MAIN_TEX
					half2 main_uv : TEXCOORD0;
				#endif	
					half2 lightmap_uv : TEXCOORD1;
					half3 lighting : TEXCOORD2;
					half4 wpos: TEXCOORD3;
					half4 color : TEXCOORD7;
				};

				CL_OUT_WPOS vert(CL_IN v) {
						
					CL_OUT_WPOS o;
					o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
					o.wpos = mul( _Object2World, half4(v.vertex.xyz,1) );
					half3 normal =  normalize(mul(_Object2World,half4(v.normal,0))).xyz;
				
					o.color=v.color;
					half f_d = acos(clamp(dot(half3(0,0,-1),half3(0,0,normal.z)),-1,1))/1.5708;
					half r_d = acos(clamp(dot(half3(1,0,0),half3(normal.x,0,0)),-1,1))/1.5708;
					half t_d = acos(clamp(dot(half3(0,1,0),half3(0,normal.y,0)),-1,1))/1.5708;

					f_d = lerp(0,1-f_d,half(normal.z<1));
					r_d = lerp(0,1-r_d,half(normal.x>0));
					t_d = lerp(0,1-t_d,half(normal.y>0));

					o.lighting = (_FrontLight*f_d) + (_RightLight*r_d) + (_TopLight*t_d);
					o.lighting = _MainColor*o.lighting+(_AmbientColor*_AmbientPower);
					return o;
				}
				
				fixed4 frag(CL_OUT_WPOS v) : COLOR {
					fixed4 outColor = fixed4(0.0, 0.0, 0.0, 0.0);
					half4 textureColor = half4((_LightTint * v.lighting),1);
					half ao = pow((1-v.color.a)*_AOIntensity, _AOPower );
					outColor = textureColor;
					outColor.xyz*=v.color;
					outColor.rgb = lerp(outColor.rgb, _AOColor.rgb, ao);
					return outColor;
				}
			ENDCG
		}
	}
	FallBack "Diffuse"
}

//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2014 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
/// <summary>
/// This script makes it possible for a scroll view to wrap its content, creating endless scroll views.
/// Usage: simply attach this script underneath your scroll view where you would normally place a UIGrid:
/// 
/// + Scroll View
/// |- UIWrappedContent
/// |-- Item 1
/// |-- Item 2
/// |-- Item 3
/// </summary>

[AddComponentMenu("NGUI/Interaction/Wrap Content")]
public class UIWrapContent : MonoBehaviour
{
	public delegate void OnInitializeItem (GameObject go, int wrapIndex, int realIndex);

	/// <summary>
	/// Minimum allowed index for items. If "min" is equal to "max" then there is no limit.
	/// For vertical scroll views indices increment with the Y position (towards top of the screen).
	/// </summary>

	public int minIndex = 0;

	/// <summary>
	/// Maximum allowed index for items. If "min" is equal to "max" then there is no limit.
	/// For vertical scroll views indices increment with the Y position (towards top of the screen).
	/// </summary>

	public int maxIndex = 0;

	/// <summary>
	/// Callback that will be called every time an item needs to have its content updated.
	/// The 'wrapIndex' is the index within the child list, and 'realIndex' is the index using position logic.
	/// </summary>

	public OnInitializeItem onInitializeItem;

	public int showNum = 4;

	Transform mTrans;
	UIPanel mPanel;
	UIScrollView mScroll;
	bool mHorizontal = false;
	bool mFirstTime = true;
	bool mStart = false;
	bool mOpen = false;
	public bool changeNameToRealIndex = false;
	List<Transform> mChildren = new List<Transform>();
	List<Transform> mChildrenShow = new List<Transform>();
	private Vector3 mPanelPosition;
	private Vector2 mPanelClipOffset;
	/// <summary>
	/// ViewDataInitialize everything and register a callback with the UIPanel to be notified when the clipping region moves.
	/// </summary>
	protected virtual void Start ()
	{
		if(mStart) return;
        HandlCelleRate();
		mStart = true;
		//SortBasedOnScrollMovement();
        SortAlphabetically();
		//WrapContent();
		if (mScroll != null){
			mScroll.GetComponent<UIPanel>().onClipMove += OnMove;
		}
		mFirstTime = false;
		mScroll.MoveRelative(new Vector3(1, 0, 0));
	}



	public void Refresh(Vector3 panelPosition, Vector2 clipOffset, bool isResetPosition = true){
		
		mFirstTime = true;
		mChildrenShow.Clear();
		//mOpen = true;
		mPanel.transform.localPosition = panelPosition;
		mPanel.clipOffset = clipOffset;
		WrapContent();
		if(mScroll && isResetPosition) mScroll.ResetPosition();
		mFirstTime = false;
	}

	public void Refresh(bool isRestore = true, bool isResetPositionAndSort = true){
		CacheScrollView();
		mFirstTime = true;
		mChildrenShow.Clear();
		//mOpen = true;
		WrapContent();
		if(mScroll && isResetPositionAndSort){
			SortAlphabetically();
			mScroll.ResetPosition();
		}
		mFirstTime = false;
	}

	void OnDisable(){
	}

	public void ResetChilds(){

	}

	public List<GameObject> GetShowedChildren(){
		CacheScrollView();
		Vector3[] corners = mPanel.worldCorners;
		Vector3 center = Vector3.Lerp(corners[0], corners[2], 0.5f);
		float halfShowSize = 0;
		float halfItemSize = 0;
		
		if(mHorizontal){
			halfShowSize = mScroll.panel.GetViewSize().x / 2;			
		}else{
			halfShowSize = mScroll.panel.GetViewSize().y / 2;
		}

		return mChildren.Where(t => {
			float distance = 0;
			float itemBorderToCenterDistance = 0;
			if(mHorizontal){
				distance = t.localPosition.x - center.x;
			}else{
				distance = t.localPosition.y - center.y;
			}
			if(distance < 0){
				itemBorderToCenterDistance = distance + halfItemSize;
			}
			else if(distance > 0){
				itemBorderToCenterDistance = distance - halfItemSize;
			}
			return Mathf.Abs(itemBorderToCenterDistance) > halfItemSize;
		})
			.Select(e => e.gameObject).Where(e => e.active).ToList();
	}

	public List<GameObject> GetChildren(){
		return mChildren.Select(e => e.gameObject).ToList();
	}

	/// <summary>
	/// Callback triggered by the UIPanel when its clipping region moves (for example when it's being scrolled).
	/// </summary>

	protected virtual void OnMove (UIPanel panel) {
		if(!mOpen) return;
		WrapContent(); 
	}

	/// <summary>
	/// Immediately reposition all children.
	/// </summary>

	[ContextMenu("Sort Based on Scroll Movement")]
	public void SortBasedOnScrollMovement ()
	{
		if (!CacheScrollView()) return;

		// Cache all children and place them in order
		mChildren.Clear();
		for (int i = 0; i < mTrans.childCount; ++i)
			mChildren.Add(mTrans.GetChild(i));

		// Sort the list of children so that they are in order
		if (mHorizontal) mChildren.Sort(UIGrid.SortHorizontal);
		else mChildren.Sort(UIGrid.SortVertical);

		ResetPosition(mChildren);

	}

	/// <summary>
	/// Immediately reposition all children, sorting them alphabetically.
	/// </summary>

	[ContextMenu("Sort Alphabetically")]
	public void SortAlphabetically ()
	{
		if (!CacheScrollView()) return;

		// Cache all children and place them in order
		mChildren.Clear();
		for (int i = 0; i < mTrans.childCount; ++i)
			mChildren.Add(mTrans.GetChild(i));

		// Sort the list of children so that they are in order
		mChildren.Sort(UIGrid.SortByName);
        //mChildren.Sort((a, b) => int.Parse(a.Name) - int.Parse(b.Name));
		ResetPosition(mChildren);
	}

	/// <summary>
	/// Cache the scroll view and return 'false' if the scroll view is not found.
	/// </summary>

	protected bool CacheScrollView ()
	{
		mTrans = transform;
		mPanel = NGUITools.FindInParents<UIPanel>(gameObject);
		mScroll = mPanel.GetComponent<UIScrollView>();
		if (mScroll == null) return false;
		if (mScroll.movement == UIScrollView.Movement.Horizontal) mHorizontal = true;
		else if (mScroll.movement == UIScrollView.Movement.Vertical) mHorizontal = false;
		else return false;
		return true;
	}

	/// <summary>
	/// Wrap all content, repositioning all children as needed.
	/// </summary>

	public void WrapContent ()
	{
		int limited = 0;
		if(maxPerLine > 0){
			limited = maxPerLine;
		}else{
			limited = 1;	
		}
		int childrenNum = mChildren.Where(e => e.gameObject.active).Count();
		float extents = 0;
		if(mHorizontal){
			extents = cellWidth * Mathf.CeilToInt(childrenNum * 1.0f / limited) * 0.5f;
		}
		else{
			extents = cellHeight * Mathf.CeilToInt(childrenNum * 1.0f / limited) * 0.5f;
		}
        //_.Log("********************************");
        //_.Log("******cellHeight:" + cellHeight);
        //_.Log("******extents:" + extents);
        //_.Log("******childrenNum:" + childrenNum);
		Vector3[] corners = mPanel.worldCorners;
		
		for (int i = 0; i < 4; ++i)
		{
			Vector3 v = corners[i];
			v = mTrans.InverseTransformPoint(v);
			corners[i] = v;
		}
		Vector3 center = Vector3.Lerp(corners[0], corners[2], 0.5f);
		bool allWithinRange = true;
		float ext2 = 0;
		float halfItemSize = 0;
		float halfShowSize = 0;
		
		int childWidth = (int)(gameObject.transform.GetChild(0) != null ? gameObject.transform.GetChild(0).gameObject.GetComponent<UIWidget>().width : cellWidth);
        int childHeight = (int)(gameObject.transform.GetChild(0) != null ? gameObject.transform.GetChild(0).gameObject.GetComponent<UIWidget>().height : cellHeight);
		if(mHorizontal){
			ext2 = extents * 2f;
			halfItemSize = childWidth * 0.5f;
			halfShowSize = mScroll.panel.GetViewSize().x / 2;
		}else{
			ext2 = extents * 2f;
			halfItemSize = childHeight * 0.5f;
			halfShowSize = mScroll.panel.GetViewSize().y / 2;
		}
        //_.Log("******ext2:" + ext2);
        //_.Log("******halfItemSize:" + halfItemSize);
        //_.Log("******halfShowSize:" + halfShowSize);
        //_.Log("******childWidth:" + childWidth);
        //_.Log("******childHeight:" + childHeight);
        //_.Log("******center:" + center);
        //_.Log("********************************");
		for (int i = 0, imax = mChildren.Count; i < imax; ++i)
		{
//			_.Log("********************************"+i+"********************************");
			if(i > maxIndex - minIndex){
				//NGUITools.SetActive(mChildren[i].gameObject, false);
				mChildren[i].gameObject.active = false;
				childrenNum--;
				if(mHorizontal){
					extents = cellWidth * Mathf.CeilToInt(childrenNum * 1.0f / limited) * 0.5f;
				}
				else{
					extents = cellHeight * Mathf.CeilToInt(childrenNum * 1.0f / limited) * 0.5f;
				}
				ext2 = extents * 2;
				continue;
			}else if(i <= maxIndex - minIndex && !mChildren[i].gameObject.active){
				//NGUITools.SetActive(mChildren[i].gameObject, true);
				mChildren[i].gameObject.active = true;
				childrenNum++;
				if(mHorizontal){
					extents = cellWidth * Mathf.CeilToInt(childrenNum * 1.0f / limited) * 0.5f;
				}
				else{
					extents = cellHeight * Mathf.CeilToInt(childrenNum * 1.0f / limited) * 0.5f;
				}
				ext2 = extents * 2;
			}
			Transform t = mChildren[i];
			float distance = 0;
			if(mHorizontal){
				distance = t.localPosition.x - center.x;
			}
			else{
				distance = t.localPosition.y - center.y;
			}
//			_.Log("********************************"+distance+"********************************");
			float itemBorderToCenterDistance = 0;
			if(distance < 0){
				itemBorderToCenterDistance = distance + halfItemSize;
			}
			else if(distance > 0){
				itemBorderToCenterDistance = distance - halfItemSize;
			}
			if (distance < -extents && childrenNum >= showNum)
			{
				Vector3 pos = t.localPosition;
				if(mHorizontal){
					pos.x += ext2;
					distance = pos.x - center.x;
				}
				else{
					pos.y += ext2;
					distance = pos.y - center.y;
				}
				int realIndex = getRealIndex(pos, i);
				if ((minIndex <= realIndex && realIndex <= maxIndex))
				{
					t.localPosition = pos;
				}
				else allWithinRange = false;
			}
			else if (distance > extents && childrenNum >= showNum)
			{
				Vector3 pos = t.localPosition;
				
				if(mHorizontal){
					pos.x -= ext2;
					distance = pos.x - center.x;
				}else{
					pos.y -= ext2;
					distance = pos.y - center.y;
				}
				int realIndex = getRealIndex(pos, i);
				if ((minIndex <= realIndex && realIndex <= maxIndex))
				{
					t.localPosition = pos;
				}
				else allWithinRange = false;
			}

//            _.Log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^childrenNum:" + childrenNum);
//            _.Log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^showNum:" + showNum);
			if(childrenNum <= showNum){
				allWithinRange = false;
			}
//			_.Log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^allWithinRange:"+allWithinRange);
			if(itemBorderToCenterDistance > 0 && itemBorderToCenterDistance <= halfShowSize && !mChildrenShow.Contains(t)){
				mChildrenShow.Add(t);
				int realIndex = getRealIndex(t.localPosition, i);
				if ((minIndex <= realIndex && realIndex <= maxIndex))
				{
					UpdateItem(t, i, realIndex);
					if(changeNameToRealIndex) t.name = realIndex.ToString();
				}
			}
			else if(itemBorderToCenterDistance < 0 && itemBorderToCenterDistance >= -halfShowSize && !mChildrenShow.Contains(t)){
				mChildrenShow.Add(t);
				int realIndex = getRealIndex(t.localPosition, i);
				if ((minIndex <= realIndex && realIndex <= maxIndex))
				{
					UpdateItem(t, i, realIndex);
					if(changeNameToRealIndex)  t.name = realIndex.ToString();
				}
			}
			else if(mFirstTime){
				int realIndex = getRealIndex(t.localPosition, i);
				if ((minIndex <= realIndex && realIndex <= maxIndex)){
					UpdateItem(t, i, realIndex);	
				}
			}
			else if(itemBorderToCenterDistance > 0 && itemBorderToCenterDistance > halfShowSize &&  mChildrenShow.Contains(t)){
				mChildrenShow.Remove(t);
			}
			else if(itemBorderToCenterDistance < 0 && itemBorderToCenterDistance < -halfShowSize &&  mChildrenShow.Contains(t)){
				mChildrenShow.Remove(t);
			}
		}
		mScroll.restrictWithinPanel = !allWithinRange;
	}

	/// <summary>
	/// Sanity checks.
	/// </summary>



	void OnValidate ()
	{
		if (maxIndex < minIndex)
			maxIndex = minIndex;
		if (minIndex > maxIndex)
			maxIndex = minIndex;
	}

	/// <summary>
	/// Want to update the content of items as they are scrolled? Override this function.
	/// </summary>

	protected virtual void UpdateItem (Transform item, int index, int realIndex)
	{
		if (onInitializeItem != null)
		{
			//int realIndex = getRealIndex(item.localPosition, index);
			onInitializeItem(item.gameObject, index, realIndex);
		}
	}
	
	int getRealIndex(Vector3 pos, int index){
		int realIndex = (mScroll.movement == UIScrollView.Movement.Vertical) ?
			Mathf.CeilToInt(pos.y / cellHeight) :
			Mathf.CeilToInt(pos.x / cellWidth);
//		_.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!index:"+index);
//		_.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!realIndex:"+realIndex);
//		_.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!cellHeight:"+cellHeight);
//		_.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!pos.y:"+pos.y);
		int limited = 0;
		if(maxPerLine > 0){
			limited = maxPerLine;
		}else{
			limited = 1;	
		}
		if(limited > 1) realIndex = realIndex - (-realIndex - 1) * (limited - 1) - index % limited - 1;
		return realIndex;
	}

	public int maxPerLine = 0;
    public bool UseRate = true;
    public float cellWidthRate = 0.1f;
    public float cellHeightRate = 0.1f;
	public float cellWidth = 200f;
	public float cellHeight = 200f;

    [ContextMenu("HandlCelleRate")]
    public void HandlCelleRate()
    {
        if (UseRate)
        {
            //_.Log("Screen.height:" + Screen.height);
            //_.Log("Screen.width:" + Screen.width);
            cellHeight = Screen.height * cellHeightRate;
            cellWidth = Screen.width * cellWidthRate;
        }
    }

	protected void ResetPosition (List<Transform> list)
	{

		int x = 0;
		int y = 0;
		int maxX = 0;
		int maxY = 0;
		Transform myTrans = transform;

		// Re-add the children in the same order we have them in and position them accordingly
		for (int i = 0, imax = list.Count; i < imax; ++i)
		{
			Transform t = list[i];

			float depth = t.localPosition.z;
			Vector3 pos = (mScroll.movement == UIScrollView.Movement.Vertical) ?
				new Vector3(cellWidth * x, -cellHeight * y, depth) :
				new Vector3(cellWidth * y, -cellHeight * x, depth);

			 t.localPosition = pos;

			maxX = Mathf.Max(maxX, x);
			maxY = Mathf.Max(maxY, y);

			if (++x >= maxPerLine && maxPerLine > 0)
			{
				x = 0;
				++y;
			}
		}
		
		// Apply the origin offset
		if (mScroll.contentPivot != UIWidget.Pivot.TopLeft)
		{
			Vector2 po = NGUIMath.GetPivotOffset(mScroll.contentPivot);

			float fx, fy;

			if (mScroll.movement == UIScrollView.Movement.Vertical)
			{
				fx = Mathf.Lerp(0f, maxX * cellWidth, po.x);
				fy = Mathf.Lerp(-maxY * cellHeight, 0f, po.y);
			}
			else
			{
				fx = Mathf.Lerp(0f, maxY * cellWidth, po.x);
				fy = Mathf.Lerp(-maxX * cellHeight, 0f, po.y);
			}

			for (int i = 0; i < myTrans.childCount; ++i)
			{
				Transform t = myTrans.GetChild(i);
				Vector3 pos = t.localPosition;
				pos.x -= fx;
				pos.y -= fy;
				t.localPosition = pos;
			}
		}
	}
}

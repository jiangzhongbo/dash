﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NavMesh.NavMesh;
public class DynamicNavMeshAgent : MonoBehaviour
{
    public float speed;
    public float stoppingDistance;
    private List<Vector3> path;
    private Vector3 target;
    private NavMeshData _navMeshData;
    private List<NavTriangle> navTriangles;
    public NavMeshData navMeshData
    {
        get{
            return _navMeshData;
        }
        set
        {
            _navMeshData = value;
            List<NavTriangle> lst = new List<NavTriangle>();
            var data = _navMeshData.GetData();
            foreach (Triangle item in data)
            {
                NavTriangle navTri = item.CloneNavTriangle();
                lst.Add(navTri);
            }
            navTriangles = lst;
        }
    }
    public bool IsShowPath = false;
    public Color color = Color.blue;
    private float Frequency = 20;
    private float lastFireTime = 0;
    public bool IsOut = true;
    public bool IsSlowRotation = false;
    bool isRun = false;
    public void Start()
    {
        StopAllCoroutines();
        StartCoroutine(MainUpdate());
    }

    bool isPointIn(Vector3 v)
    {
        var v2 = new Vector2(v.x, v.z);
        for (int i = 0; i < navTriangles.Count; i++)
        {
            if (navTriangles[i].IsPointIn(v2))
            {
                return true;
            }
        }
        return false;
    }

    IEnumerator MainUpdate()
    {
        while (true)
        {
            if (path != null && path.Count > 1 && Time.timeScale != 0)
            {
                var point = path[1];
                if (IsSlowRotation)
                {
                    var e1 = transform.eulerAngles;
                    transform.LookAt(point);
                    var e2 = transform.eulerAngles;
                    if (Mathf.Abs(e1.y - e2.y) < 10)
                    {
                        transform.LookAt(point);
                        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
                    }
                    else
                    {
                        transform.eulerAngles = e1;
                        float t = 0;
                        while(t < 1)
                        {
                            t += 0.005f;
                            transform.eulerAngles = new Vector3(0, Mathf.LerpAngle(e1.y, e2.y, t), 0);
                            yield return null;
                        }
                        transform.eulerAngles = new Vector3(0, e2.y, 0);
                    }

                }
                else
                {
                    transform.LookAt(point);
                    transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
                }

                if ((point - transform.position).magnitude < 0.5f)
                {
                    var p1 = transform.position;
                    var p2 = p1 + transform.forward;
                    float t = 0;
                    while (t < 1 && path != null && path.Count > 1 && Time.timeScale != 0)
                    {
                        t += Time.deltaTime * speed;
                        transform.position = Vector3.Lerp(p1, p2, t);
                        yield return null;
                    }
                }
                else
                {
                    transform.position += transform.forward * speed * Time.deltaTime;
                }
                //while (IsOut)
                //{
                //    var p1 = transform.position;
                //    var p2 = p1 + transform.forward;
                //    float t = 0;
                //    while (t < 1)
                //    {
                //        t += Time.deltaTime * speed;
                //        transform.position = Vector3.Lerp(p1, p2, t);
                //        yield return null;
                //    }
                //    yield return null;
                //}
            }
            yield return null;
        }

    }
    public void Stop()
    {
        path = null;
    }

    public void SetDestination(Vector3 target)
    {
        if (Time.time > lastFireTime + 1.0f / Frequency)
        {
            if (!isPointIn(target))
            {
                path = null;
                IsOut = true;
            }
            else
            {
                path = Seek(transform.position, target);
                IsOut = path.Count() == 0;
            }
            this.target = target;
            lastFireTime = Time.time;
        }
    }

    public bool PointIsOut(Vector3 target)
    {
        var p = Seek(transform.position, target);
        if (p.Count() == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public List<Vector3> Seek(Vector3 sPos, Vector3 ePos)
    {
        if (!navMeshData)
        {
            return null;
        }
        Seeker.GetInstance().NavMeshData = navTriangles;
        List<Vector2> lstpath;
        List<Vector3> result = new List<Vector3>();
        Vector2 ssPos = new Vector2(sPos.x, sPos.z);
        Vector2 eePos = new Vector2(ePos.x, ePos.z);
        Seeker.GetInstance().Seek(ssPos, eePos, out lstpath, -1);
        for (int i = 0; i < lstpath.Count; i++)
        {
            result.Add(new Vector3(lstpath[i].x, 0, lstpath[i].y));
        }
        return result;
    }

    void OnDrawGizmos()
    {
        DrawFath();
    }

    void DrawFath()
    {
        if (IsShowPath && path != null && path.Count > 1)
        {
            for (int i = 0; i < path.Count - 1; i++)
            {
                Gizmos.color = color;
                Gizmos.DrawLine(path[i], path[i+1]);
            }
        }
    }
}

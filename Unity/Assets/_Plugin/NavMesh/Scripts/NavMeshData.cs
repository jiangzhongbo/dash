﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NavMesh.NavMesh;
using System.Linq;
using System;
public class NavMeshData : MonoBehaviour
{
    public List<Triangle> Triangles;
    public bool IsShowNavMesh = false;
    public Color LineColor = Color.white;
    private Vector3 prevPosition = Vector3.zero;
    private Vector3 prevEulerAngles = Vector3.zero;
    private Vector3 prevLossyScale = Vector3.one;
    private List<Triangle> trianglesCache;
    public List<Triangle> GetData()
    {
        if (prevPosition != transform.position || prevEulerAngles != transform.eulerAngles || prevLossyScale != transform.lossyScale)
        {
            var trs = Triangles.Select(e => e.CloneTriangle()).ToList();
            trianglesCache = trs.Select(e =>
            {
                e.m_vecPoints = e.m_vecPoints.Select(p =>
                {
                    Vector3 v1 = new Vector3(p.x, 0, p.y);
                    Vector3 v2 = transform.TransformPoint(v1);
                    Vector3 v3 = new Vector3(v2.x * transform.localScale.x, 0, v2.z * transform.localScale.z);
                    return new Vector2(v3.x, v3.z);
                }).ToArray();
                return e;
            }).ToList();
            prevPosition = transform.position;
            prevEulerAngles = transform.eulerAngles;
            prevLossyScale = transform.lossyScale;
            return trianglesCache;
        }
        else if (trianglesCache != null && trianglesCache.Count != 0)
        {
            return trianglesCache;
        }
        else
        {
            return Triangles;
        }

    }

    [ContextMenu("Resetting")]
    void Resetting()
    {
        LineColor = Color.white;
        prevPosition = Vector3.zero;
        prevEulerAngles = Vector3.zero;
        prevLossyScale = Vector3.one;
        trianglesCache = null;
    }

    void OnDrawGizmos()
    {
        DrawNavMesh();
    }


    //======================= draw NavMesh ======================================

    /// <summary>
    /// Draws the nav mesh.
    /// </summary>
    private void DrawNavMesh()
    {
        if (IsShowNavMesh)
        {
            var Triangles = GetData();
            if (Triangles.Count != 0)
            {
                foreach (Triangle tri in Triangles)
                {
                    Gizmos.color = LineColor;

                    Vector3 p1 = new Vector3(tri.GetPoint(0).x, transform.position.y + 0.01f, tri.GetPoint(0).y);
                    Vector3 p2 = new Vector3(tri.GetPoint(1).x, transform.position.y + 0.01f, tri.GetPoint(1).y);
                    Vector3 p3 = new Vector3(tri.GetPoint(2).x, transform.position.y + 0.01f, tri.GetPoint(2).y);
                    Gizmos.DrawLine(p1, p2);
                    Gizmos.DrawLine(p2, p3);
                    Gizmos.DrawLine(p3, p1);
                }
            }
        }
    }

}

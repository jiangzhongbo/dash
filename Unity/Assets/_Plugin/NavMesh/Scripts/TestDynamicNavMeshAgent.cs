﻿using UnityEngine;
using System.Collections;

public class TestDynamicNavMeshAgent : MonoBehaviour
{
    public GameObject target;
    private DynamicNavMeshAgent agent;
    void Start()
    {
        agent = GetComponent<DynamicNavMeshAgent>();
    }

    void Update()
    {
        if (target)
        {
            agent.SetDestination(target.transform.position);
        }
    }
}

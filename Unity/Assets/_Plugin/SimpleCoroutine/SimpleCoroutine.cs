﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

[System.Serializable]
public class SimpleCoroutine
{
    public SimpleCoroutine Prev
    {
        get;
        set;
    }
    public IEnumerator Enumerator
    {
        get;
        set;
    }
}




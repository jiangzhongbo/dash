﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class SimpleCoroutineDispatcher : MonoBehaviour 
{
    private List<SimpleCoroutine> simpleCoroutines = new List<SimpleCoroutine>();
    Queue<SimpleCoroutine> cache = new Queue<SimpleCoroutine>();
    public bool ignoreTimeScale = true;

    IEnumerator Start()
    {
        while (true)
        {
            for (int i = 0; i < simpleCoroutines.Count; i++)
            {
                handleSimpleCoroutine(simpleCoroutines[i]);
            }
            yield return null;

        }
    }


    void handleSimpleCoroutine(SimpleCoroutine simpleCoroutine)
    {

        if (simpleCoroutine.Enumerator.Current is SimpleYieldInstruction)
        {
            SimpleYieldInstruction yieldInstruction = simpleCoroutine.Enumerator.Current as SimpleYieldInstruction;
            if (!yieldInstruction.IsDone())
            {
                return;
            }
        }
        if (!simpleCoroutine.Enumerator.MoveNext())
        {
            if (simpleCoroutine.Prev != null)
            {
                simpleCoroutines[simpleCoroutines.IndexOf(simpleCoroutine)] = simpleCoroutine.Prev;
                simpleCoroutine = simpleCoroutine.Prev;
                handleSimpleCoroutine(simpleCoroutine);
            }
            else
            {
                simpleCoroutines.Remove(simpleCoroutine);
                cache.Enqueue(simpleCoroutine);
            }
            return;
        }
        if (simpleCoroutine.Enumerator.Current is SimpleCoroutine)
        {
            SimpleCoroutine childCoroutine = (SimpleCoroutine)simpleCoroutine.Enumerator.Current;
            childCoroutine.Prev = simpleCoroutine;
            simpleCoroutines.Remove(childCoroutine);
            simpleCoroutines[simpleCoroutines.IndexOf(simpleCoroutine)] = childCoroutine;
            handleSimpleCoroutine(childCoroutine);
        }
        else if (simpleCoroutine.Enumerator.Current is int && (int)simpleCoroutine.Enumerator.Current == 0)
        {
            handleSimpleCoroutine(simpleCoroutine);
        }
    }

    SimpleCoroutine getSimpleCoroutine()
    {
        if (cache.Count > 0)
        {
            return cache.Dequeue();
        }
        return new SimpleCoroutine();
    }

    public SimpleCoroutine StartSimpleCoroutine(IEnumerator routine, bool ignoreTimeScale = true)
    {
        this.ignoreTimeScale = ignoreTimeScale;
        var r = getSimpleCoroutine();
        r.Enumerator = routine;
        simpleCoroutines.Add(r);
        return r;
    }

}


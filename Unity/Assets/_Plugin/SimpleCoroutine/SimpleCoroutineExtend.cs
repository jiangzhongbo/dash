﻿using UnityEngine;
using System.Collections;
using System;
public static class SimpleCoroutineExtend
{
    public static SimpleCoroutine StartSimpleCoroutine(this MonoBehaviour self, IEnumerator routine, bool ignoreTimeScale = true)
    {
        if (!self.gameObject.GetComponent<SimpleCoroutineDispatcher>())
        {
            self.gameObject.AddComponent<SimpleCoroutineDispatcher>();
        }

        SimpleCoroutineDispatcher dispatcher = self.gameObject.GetComponent<SimpleCoroutineDispatcher>();
        return dispatcher.StartSimpleCoroutine(routine, ignoreTimeScale);
    }
}

﻿using UnityEngine;
using System.Collections;

public class SimpleCoroutineTest : MonoBehaviour
{
    void Start()
    {
        //_.Log("Start begin at frameCount : " + Time.frameCount);
        //this.StartSimpleCoroutine(SelfCoroutine());
        this.StartSimpleCoroutine(OnUpdate());
    }

    IEnumerator SelfCoroutine()
    {
        Debug.Log("1 coroutine begin at frameCount : " + Time.frameCount);
        yield return new SimpleWaitForSeconds(1);
        Debug.Log("1 coroutine end at frameCount : " + Time.frameCount);
    }

    //IEnumerator InnerSelfCoroutine()
    //{
    //    Debug.Log("2 Self Coroutine begin at frameCount : " + Time.frameCount);
    //    yield return this.StartSimpleCoroutine(DeepSelfCoroutine());
    //    Debug.Log("2 Self Coroutine end at frameCount : " + Time.frameCount);
    //}

    //IEnumerator DeepSelfCoroutine()
    //{
    //    Debug.Log("3 Deep Coroutine begin at frameCount : " + Time.frameCount);
    //    yield return new SimpleWaitForSeconds(1);
    //    Debug.Log("3 Deep Coroutine end at frameCount : " + Time.frameCount);
    //}

    IEnumerator OnUpdate()
    {
        while (true)
        {
            Debug.Log("aaa1");
            yield return null;
            Debug.Log("aaa2");
        }
    }

}

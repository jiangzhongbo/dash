﻿using UnityEngine;
using System.Collections;

public class SimpleWaitForSeconds : SimpleYieldInstruction
{
    float m_waitTime;
    float m_startTime;
    public SimpleWaitForSeconds(float waitTime)
    {
        m_waitTime = waitTime;
        m_startTime = -1;
    }

    public override bool IsDone()
    {
        // NOTE: a little tricky here
        if (m_startTime < 0)
        {
            m_startTime = Time.realtimeSinceStartup;
        }
        // check elapsed time
        return (Time.realtimeSinceStartup - m_startTime) >= m_waitTime;

    }

}
using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
public static class GameObjectExtensions {
	
	public static GameObject FindChild( this GameObject parent, string name){
		Transform[] childs;
		childs = parent.GetComponentsInChildren<Transform>(true);
		foreach(Transform child in childs){
			if(child.gameObject.name.Equals(name)){
				return child.gameObject;
			}
		}
		return null;
	}
	
	public static string GetPath(this GameObject obj)
	{
	    string path = "/" + obj.name;
	    while (obj.transform.parent != null)
	    {
	        obj = obj.transform.parent.gameObject;
			if(obj.transform.parent == null){
				path = obj.name + path;
			}else{
				path = "/" + obj.name + path;
			}
	        
	    }
	    return path;
	}
	
	public static UILabel UILabel(this GameObject obj){
        return obj.GetCachedComponent<UILabel>();
	}
	
	public static UISprite UISprite(this GameObject obj){
        return obj.GetCachedComponent<UISprite>();
	}

    public static UIWidget UIWidget(this GameObject obj)
    {
        return obj.GetCachedComponent<UIWidget>();
    }
	
	public static UIEventListener UIEventListener(this GameObject obj){
        return obj.GetCachedComponent<UIEventListener>();
	}
	
	public static UIPanel UIPanel(this GameObject obj){
        return obj.GetCachedComponent<UIPanel>();
	}
	
	public static UIGrid UIGrid(this GameObject obj){
        return obj.GetCachedComponent<UIGrid>();
	}
	
	public static void Hide(this GameObject obj){
		obj.SetActiveRecursively(false);
	}
	
	public static void Show(this GameObject obj){
		obj.SetActiveRecursively(true);
	}
	
	public static GameObject parent(this GameObject obj){
		return obj.transform.parent.gameObject;
	}

	public static string toTitleCase(this string name){
		string str1 = name.Substring(0, 1); 
		string str2 = name.Substring(1, name.Length - 1); 
		return str1.ToUpper() + str2;
	}

	public static string toLowerFirst(this string name){
		string str1 = name.Substring(0, 1); 
		string str2 = name.Substring(1, name.Length - 1); 
		return str1.ToLower() + str2;
	}
	
	public static GameObject FindChildFirstLevel(this GameObject parent, string name){
		Transform[] childs;
		childs = parent.GetComponentsInChildren<Transform>(true);
		foreach(Transform child in childs){
			if(child.parent == parent.transform &&  child.gameObject.name.Equals(name)){
				return child.gameObject;	
			}
		}
		return null;
	}
	
	public static List<GameObject> GetAllFirstLevelChilds(this GameObject parent){
		return parent.GetComponentsInChildren<Transform>(true).Where(e => e.transform.parent == parent.transform).Select(e => e.gameObject).ToList();
	}

	public static void OnClick(this GameObject go, UIEventListener.VoidDelegate callback){
		if(!go){
			return;
		}
		UIEventListener l = go.UIEventListener();
		if(l){
			l.onClick = callback;
		}
	}

    public static T[] GetComponentsInChildrenOfAsset<T>(this GameObject go) where T : Component
    {
        List<Transform> tfs = new List<Transform>();
        CollectChildren(tfs, go.transform);
        List<T> all = new List<T>();
        for (int i = 0; i < tfs.Count; i++)
            all.AddRange(tfs[i].gameObject.GetComponents<T>());
        return all.ToArray();
    }

    static void CollectChildren(List<Transform> transforms, Transform tf)
    {
        transforms.Add(tf);
        foreach (Transform child in tf)
        {
            CollectChildren(transforms, child);
        }
    }

    public static GameObject FindChildOfAsset(this GameObject go, string name)
    {
        List<Transform> tfs = new List<Transform>();
        CollectChildren(tfs, go.transform);
        for (int i = 0; i < tfs.Count; i++)
        {
            if (tfs[i].name == name)
            {
                return tfs[i].gameObject;
            }
        }
        return null;
    }

    private static Dictionary<GameObject, Dictionary<string, List<Component>>> cacheComponents = new Dictionary<GameObject, Dictionary<string, List<Component>>>(500);
    private static Dictionary<Type, string> cacheTypeToName = new Dictionary<Type, string>(500);
    public static Component GetCachedComponent(this GameObject go, Type type)
    {
        if (!cacheTypeToName.ContainsKey(type))
        {
            cacheTypeToName[type] = type.Name;
        }

        return GetCachedComponent(go, cacheTypeToName[type]);
    }

    public static T GetCachedComponent<T>(this GameObject go) where T : Component
    {
        return GetCachedComponent(go, typeof(T)) as T;
    }

    public static Component GetCachedComponent(this GameObject go, string name)
    {
        if (go && !string.IsNullOrEmpty(name))
        {
            if (cacheComponents.ContainsKey(go))
            {
                if (cacheComponents[go].ContainsKey(name))
                {
                    return cacheComponents[go][name][0];
                }
                else
                {
                    var c = go.GetComponent(name);
                    if (c)
                    {
                        cacheComponents[go][name] = new List<Component>();
                        cacheComponents[go][name].Add(c);
                        return cacheComponents[go][name][0];
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                cacheComponents[go] = new Dictionary<string, List<Component>>();
                var c = go.GetComponent(name);
                if (c)
                {
                    cacheComponents[go][name] = new List<Component>();
                    cacheComponents[go][name].Add(c);
                    return cacheComponents[go][name][0];
                }
                else
                {
                    return null;
                }
            }
        }
        return null;
    }

    public static Component[] GetCachedComponents(this GameObject go, string name)
    {
        if (go && !string.IsNullOrEmpty(name))
        {
            if (cacheComponents.ContainsKey(go))
            {
                if (cacheComponents[go].ContainsKey(name))
                {
                    return cacheComponents[go][name].ToArray();
                }
                else
                {
                    var c = go.GetComponent(name);
                    if (c)
                    {
                        cacheComponents[go][name] = new List<Component>();
                        cacheComponents[go][name].Add(c);
                        return cacheComponents[go][name].ToArray();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                cacheComponents[go] = new Dictionary<string, List<Component>>();
                var c = go.GetComponent(name);
                if (c)
                {
                    cacheComponents[go][name] = new List<Component>();
                    cacheComponents[go][name].Add(c);
                    return cacheComponents[go][name].ToArray();
                }
                else
                {
                    return null;
                }
            }
        }
        return null;
    }

    public static Component GetCachedComponentInChildren(this GameObject go, Type type)
    {
        return go.GetComponentInChildren(type);
    }

    public static T GetCachedComponentInChildren<T>(this GameObject go) where T : Component
    {
        return GetCachedComponentInChildren(go, typeof(T)) as T;
    }
}

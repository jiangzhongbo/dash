using UnityEngine;
using System.Collections;

public static class VDPass {

	static public VertexSample vertexSample = new VertexSample();
    static public int sampleWidth = 64;
    static public int sampleHeight = 64;
    static public float samplingBias = 0.001f;
    static public float samplingDistance = 100;
    static public float samplingAngle = 100;
    static public bool edgeSmooth = false;
    static public bool invertNormals = false;
    static public float edgeSmoothBias = 0.001f;
    static public CameraClearFlags skyMode = CameraClearFlags.SolidColor;
    static public bool disableOccluders = false;
    static public Color skyColor = Color.white;
    static public Color globalOccluderColor = Color.black;
    static public float indirectIntensity = 1.0f;
    static public float indirectContrast = 1.0f;
    static public string occluderShader = VDSHADER.AMBIENTOCCLUSION;
    static public Material skyCube;

}
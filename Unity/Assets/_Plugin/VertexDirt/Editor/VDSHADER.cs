public static class VDSHADER {
	public static string AMBIENTOCCLUSION = "Hidden/VD-AMBIENTOCCLUSION";
    public static string INDIRECTLIGHTING = "Hidden/VD-INDIRECTLIGHTING";
    public static string THICKNESS = "Hidden/VD-THICKNESS";
    public static string AMBIENTCUBE = "Hidden/VD-AMBIENTCUBE";
}

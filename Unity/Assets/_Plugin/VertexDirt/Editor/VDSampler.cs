using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class VDSampler : MonoBehaviour {

    void OnPostRender () {

	    var tex = new Texture2D (VertexDirt.sampleWidth, VertexDirt.sampleHeight, TextureFormat.RGB24, true);
	    tex.ReadPixels (new Rect(0, 0, VertexDirt.sampleWidth, VertexDirt.sampleHeight), 0, 0);
	    var lum = tex.GetPixels32(tex.mipmapCount-1);
	    DestroyImmediate (tex);
	    VertexDirt.SetColorSample(lum[0]);
	
    }
}

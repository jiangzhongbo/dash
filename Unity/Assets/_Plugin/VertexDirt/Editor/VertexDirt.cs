using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
/*
	class: VertexDirt

	Main Vertex dirt class. VertexDirt is a vertex colour generator plug-in. Use this to generate Ambient occlusion or other advanced lightings.
*/
public static class VertexDirt {

	// private variables for mesh merging and vertex sampling
    static private List<MeshFilter> o = new List<MeshFilter>();
    static private Vector3[] v;
    static private Vector3[] n;
    static private Color32[] c;
	
	/*
		variable: vertexSample
		
		public variable, but this is used by the baking.
	*/
    static public VertexSample vertexSample = new VertexSample();
	
	/*
		integer: sampleWidth

		Vertical resolution of the sample. The default value of 64 should be fine in all circumstances. 
		Lower values could cause visual artefacts.
	*/
    static public int sampleWidth = 64;

	/*
		integer: sampleHeight

		Horizontal resolution of the sample. The default value of 64 should be fine in all circumstances. 
		Lower values could cause visual artefacts.
	*/
    static public int sampleHeight = 64;
	
	/*
		float: samplingBias

		The near clip plane of the sampling camera.
	*/
    static public float samplingBias = 0.001f;
	
	/*
		float: samplingDistance

		The far clip plane of the sampling camera.
	*/
    static public float samplingDistance = 100;

	/*
		float: samplingAngle

		The FOV of the sampling camera. Please note that this value normally should be between 100-160.
	*/
    static public float samplingAngle = 100;
	
	/*
		boolean: edgeSmooth

		Enable to smoothing out hard edges. Basically just averages the normals of the vertices in the same position.
	*/
    static public bool edgeSmooth = false;

	/*
		boolean: invertNormals

		Set true if you want to render the inside of the objects. Use this parameter enabled to render thickness.
	*/
    static public bool invertNormals = false;

	/*
		float: edgeSmoothBias

		The range of edge smoothing. The normals of vertices closer than this value will be averaged.
	*/
    static public float edgeSmoothBias = 0.001f;

	/*
		enumeration: skyMode

		Parameter for sampling camera backdrop. 
	*/
    static public CameraClearFlags skyMode = CameraClearFlags.SolidColor;

	/*
		boolean: disableOccluders

		Set true if you only want to bake the background colour/cubeMap to the vertex colours.
		
	*/
    static public bool disableOccluders = false;

	/*
		variable: skyColor

		The colour of the Sky.
	*/
    static public Color skyColor = Color.white;
	
	/*
		variable: globalOccluderColor

		Colour tint for the occluders. This property is designed for the VDOccluder shader.
	*/
    static public Color globalOccluderColor = Color.black;

	/*
		string: occluderShader

		The shader used on occluders during the bake. The default VD shader is "Hidden/VDOccluder AO"
		If this string is empty or the shader is not exist, then the occluder objects will use their original shaders. 

	*/
    static public string occluderShader = VDSHADER.AMBIENTOCCLUSION;
		
	/*
		variable: skyCube

		The cubeMap of the sampling camera's sky.
	*/
    static public Material skyCube;

    static public void Dirt(Transform[] sels)
    {
	
 		if (sels.Length > 0) {
			//vertex camera
			var camGO = new GameObject("VDSamplerCamera");
            var cam = camGO.AddComponent<Camera>();
			camGO.AddComponent<VDSampler>();
			RenderTexture.active = null;
			cam.renderingPath = RenderingPath.Forward;
			cam.pixelRect = new Rect(0,0,sampleWidth, sampleHeight);
			cam.aspect = 1.0f;	
			cam.nearClipPlane = samplingBias;
			cam.farClipPlane = samplingDistance;
			cam.fieldOfView = Mathf.Clamp ( samplingAngle, 5, 160 );
			cam.clearFlags = skyMode;
			cam.backgroundColor = skyColor;
			var tempSkybox = RenderSettings.skybox;
			if (skyMode == CameraClearFlags.Skybox) { RenderSettings.skybox = skyCube; }
			UpdateShaderVariables();
			cam.SetReplacementShader(Shader.Find(occluderShader), disableOccluders ? "ibl-only" : "");
			CombineVertices(sels);
			ResetColors();
			SmoothVertices();
			CalcColors(camGO, cam);
			ApplyColors();
			RenderSettings.skybox = tempSkybox;
			GameObject.DestroyImmediate(camGO);
            VDColorHandlerBase[] handlers = UnityEngine.Object.FindObjectsOfType(typeof(VDColorHandlerBase)).Cast<VDColorHandlerBase>().ToArray();

			foreach (var handler in handlers) {
				if (handler.originalMesh && !handler.coloredMesh) {
					handler.coloredMesh = UnityEngine.Object.Instantiate(handler.originalMesh) as Mesh;
                    handler.gameObject.GetComponent<MeshFilter>().mesh = handler.coloredMesh;
				}
			}
		}
 
    }

	// function to update shader properties
    static public void UpdateShaderVariables()
    {
	
		Shader.SetGlobalColor("_VDOccluderColor", globalOccluderColor);

	}
	/* 
		function: SetPreset
		
		Set preset for VertexDirt. Presets are for batch change common VertexDirt parameters.
	*/
    static public void SetPreset(VDPRESET v)
    {
	
		switch (v) {
		
			case VDPRESET.AMBIENTOCCLUSION :
			
				invertNormals = false;
				skyMode = CameraClearFlags.SolidColor;
				occluderShader = VDSHADER.AMBIENTOCCLUSION;
			
			break;

			case VDPRESET.INDIRECTLIGHTING :
			
				invertNormals = false;
				skyMode = CameraClearFlags.SolidColor;
				occluderShader = VDSHADER.INDIRECTLIGHTING;
			
			break;

			case VDPRESET.AMBIENTCUBE :
			
				invertNormals = false;
				skyMode = CameraClearFlags.Skybox;
				occluderShader = VDSHADER.AMBIENTCUBE;
			
			break;

			case VDPRESET.THICKNESS :
			
				invertNormals = false;
				edgeSmooth = true;
				skyMode = CameraClearFlags.SolidColor;
				disableOccluders = false;
				skyColor = Color.white;
				globalOccluderColor = Color.black;
				occluderShader = VDSHADER.THICKNESS;
			
			break;
			
		}
	
	}

	/* 
		function: ResetSettings
		
		Reset every VertexDirt parameters to defaults.
	*/
    static public void ResetSettings()
    {
			
		sampleWidth  = 64;
		sampleHeight = 64;
		samplingBias = 0.001f;
		samplingDistance = 100;
		samplingAngle = 100;
		edgeSmooth = false;
		invertNormals = false;
		edgeSmoothBias = 0.001f;
		skyMode = CameraClearFlags.SolidColor;
		disableOccluders = false;
		skyColor = Color.white;
		globalOccluderColor = Color.black;
		occluderShader = "Hidden/VDOccluder AO";
		skyCube = null;

	}

    static private void CombineVertices(Transform[] sel)
    {
	
		int vertexCount = 0;
		o.Clear();
		v = new Vector3[0];
		n = new Vector3[0];
		c = new Color32[0];

        foreach (Transform go in sel)
        {
            if (go.gameObject.GetComponent<MeshFilter>())
            {
                if (!go.gameObject.GetComponent<VDColorHandlerBase>())
                {
					go.gameObject.AddComponent<VDColorHandler>();
				}
                var v0 = go.gameObject.GetComponent<MeshFilter>().sharedMesh.vertices;
                var n0 = go.gameObject.GetComponent<MeshFilter>().sharedMesh.normals;
				
				for (int t = 0; t < v0.Length; t++) {
					v0[t] = go.TransformPoint(v0[t]);
                    n0[t] = Vector3.Normalize(go.TransformDirection(n0[t]));
				}
				
				vertexCount += v0.Length;
                o.Add(go.gameObject.GetComponent<MeshFilter>());
				v = MergeVector3 (v, v0);
				n = MergeVector3 (n, n0);
								
			}
		}
	
	}

    static public void ResetColors()
    {

        c = new Color32[v.Length];		
		
	}

    //function ResetColors(Color32) {

    //    c = new Color32[TempV.Length];		
		
    //}

    static private void SmoothVertices()
    {
	
		if (edgeSmooth) {
            for (int a = 0; a < v.Length; a++)
            {
                for (int d = a; d < v.Length; d++)
                {
					if (Vector3.Distance(v[a],v[d]) < edgeSmoothBias) {
						n[a] = Vector3.Normalize(n[a] + n[d]);
						n[d] = n[a];
					}
				}
			}
            for (var k = 0; k < c.Length; k++)
            {
				c[k] = new Color32 (255,255,255,255);
			}
		}

	}

    static private void CalcColors(GameObject camGO, Camera cam)
    {

        for (int vv = 0; vv < v.Length; vv++)
        {
			camGO.transform.position = v[vv];

			if (invertNormals) {
				camGO.transform.LookAt(v[vv] - n[vv]);
			}
			else {
				camGO.transform.LookAt(v[vv] + n[vv]);
			}
			
			vertexSample.index = vv;
			vertexSample.isCalulated = false;
			cam.Render();
			while (!vertexSample.isCalulated) {}
			c[vv] = vertexSample.color; // * Color(lum,lum,lum,1);
            EditorUtility.DisplayProgressBar(
                "CalcColors",
                "Calculating...(" + vv + "/" + v.Length + ")",
                (float)vv / (float)v.Length
            );
		}
        EditorUtility.ClearProgressBar();
	}

    static public void SetColorSample(Color32 c)
    {
	
		vertexSample.color = c;
		vertexSample.isCalulated = true;
	
	}

    static private void ApplyColors()
    {
	
		int count = 0;

        foreach (MeshFilter m in o)
        {
            Color32[] tc = new Color32[m.gameObject.GetComponent<VDColorHandlerBase>().originalMesh.vertices.Length];

            for (int c0 = 0; c0 < m.gameObject.GetComponent<VDColorHandlerBase>().originalMesh.vertices.Length; c0++)
            {
				tc[c0] = c[count];
				count++;
			}
            m.gameObject.GetComponent<VDColorHandlerBase>().colors = tc;
            m.gameObject.GetComponent<VDColorHandlerBase>().Refresh();
		}	
	
	}


    static private Vector3[] MergeVector3(Vector3[] v1, Vector3[] v2)
    {

        var v3 = new Vector3[v1.Length + v2.Length];
        System.Array.Copy(v1, v3, v1.Length);
        System.Array.Copy(v2, 0, v3, v1.Length, v2.Length);
		return v3;
		
	}
	
}

//	Class for passing samples from sampler camera to the VertexDirt class. For internal use only

public class VertexSample {

    public Color32 color = Color.white;
    public int index = 0;
    public bool isCalulated = false;

}
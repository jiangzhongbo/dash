using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Linq;

public class VDAmbientOcclusionWindow : EditorWindow
{
    [MenuItem ("Tools/VertexDirt/Ambient Occlusion")]
	static void ShowWindow () {

		VDAmbientOcclusionWindow window = ScriptableObject.CreateInstance<VDAmbientOcclusionWindow>();
		
		window.position = new Rect(100,100, 260,400);
        //window.minSize = new Vector2(260, 400);
        //window.maxSize = new Vector2(260, 400);
		window.title = "VD Ambient Occlusion";
		window.ShowUtility();
		VertexDirt.SetPreset (VDPRESET.AMBIENTOCCLUSION);

	}
	
    void OnGUI() {

        GUILayout.Label("Occlusion distance: 100, The far clip plane of the sampling camera.");
        VertexDirt.samplingDistance = EditorGUILayout.FloatField(VertexDirt.samplingDistance);
		GUILayout.Space(10);
		GUILayout.BeginHorizontal();
        GUILayout.Label("Edge smooth enabled: false");
		GUILayout.FlexibleSpace();
        VertexDirt.edgeSmooth = EditorGUILayout.Toggle(VertexDirt.edgeSmooth);
		GUILayout.EndHorizontal();
		GUILayout.Space(10);
        GUILayout.Label("Sampling angle: 100, The FOV of the sampling camera. Please note that this value normally should be between 100-160.");
        VertexDirt.samplingAngle = EditorGUILayout.FloatField(VertexDirt.samplingAngle);
		GUILayout.Space(10);
		GUILayout.Label ("Sky color");
		VertexDirt.skyColor = EditorGUILayout.ColorField(VertexDirt.skyColor);
		GUILayout.Space(10);
		GUILayout.Label ("Shadow color");
		VertexDirt.globalOccluderColor = EditorGUILayout.ColorField(VertexDirt.globalOccluderColor);
        GUILayout.Space(10);
        GUILayout.Label("Sample Width: 64, Vertical resolution of the sample. The default value of 64 should be fine in all circumstances. Lower values could cause visual artefacts.");
        VertexDirt.sampleWidth = EditorGUILayout.IntField(VertexDirt.sampleWidth);
        GUILayout.Space(10);
        GUILayout.Label("Sample Height: 64, Horizontal resolution of the sample. The default value of 64 should be fine in all circumstances. Lower values could cause visual artefacts.");
        VertexDirt.sampleHeight = EditorGUILayout.IntField(VertexDirt.sampleHeight);
        GUILayout.Space(10);
        GUILayout.Label("Sample Bias: 0.001f, The near clip plane of the sampling camera.");
        VertexDirt.samplingBias = EditorGUILayout.FloatField(VertexDirt.samplingBias);
        GUILayout.Space(10);
        GUILayout.Label("Invert Normals: false, Set true if you want to render the inside of the objects. Use this parameter enabled to render thickness.");
        VertexDirt.invertNormals = EditorGUILayout.Toggle(VertexDirt.invertNormals);
        GUILayout.Space(10);
        GUILayout.Label("Edge Smooth Bias: 0.001f, The range of edge smoothing. The normals of vertices closer than this value will be averaged.");
        VertexDirt.edgeSmoothBias = EditorGUILayout.FloatField(VertexDirt.edgeSmoothBias);
        GUILayout.Space(10);
        GUILayout.Label("Disable Occluders: false, Set true if you only want to bake the background colour/cubeMap to the vertex colours.");
        VertexDirt.disableOccluders = EditorGUILayout.Toggle(VertexDirt.disableOccluders);
        GUILayout.Space(10);
 		if (Selection.gameObjects != null) {
		
			if (GUI.Button(new Rect(10,550,240,40),"Start") ) {

				double tempTime = EditorApplication.timeSinceStartup;
				VertexDirt.Dirt(Selection.GetFiltered(typeof(Transform), SelectionMode.Deep).Cast<Transform>().ToArray());
				Debug.Log ("Dirt time: " + (EditorApplication.timeSinceStartup - tempTime));
		
			}
			
		}
 
    }
}

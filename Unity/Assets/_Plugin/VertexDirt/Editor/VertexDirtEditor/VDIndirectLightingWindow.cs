using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Linq;

public class VDIndirectLightingWindow : EditorWindow
{
[MenuItem ("Tools/VertexDirt/Indirect Lighting")]
	static void ShowWindow () {

		VDIndirectLightingWindow window = ScriptableObject.CreateInstance<VDIndirectLightingWindow>();
		window.autoRepaintOnSceneChange = true;
		window.position = new Rect(100,100, 260,400);
		window.minSize = new Vector2 (260,400);
		window.maxSize = new Vector2 (260,400);
		window.title = "VD Indirect Lighting";
		window.ShowUtility();

	}
	
    void OnGUI() {
	
		VertexDirt.UpdateShaderVariables();
		GUILayout.Label ("Indirect lighting distance");
		VertexDirt.samplingDistance = EditorGUILayout.Slider(VertexDirt.samplingDistance,0.01f, 1000.0f);
		GUILayout.Space(10);
		GUILayout.BeginHorizontal();
		GUILayout.Label ("Edge smooth enabled");
		GUILayout.FlexibleSpace();
		VertexDirt.edgeSmooth = GUILayout.Toggle(VertexDirt.edgeSmooth, "");
		GUILayout.EndHorizontal();
		GUILayout.Space(10);
		GUILayout.Label ("Sampling angle");
		VertexDirt.samplingAngle = EditorGUILayout.Slider(VertexDirt.samplingAngle,100, 150);
		//		VertexDirt.skyCube = EditorGUILayout.ObjectField(VertexDirt.skyCube, Material, true);
		GUILayout.Space(10);
		GUILayout.Label ("Sky color");
		VertexDirt.skyColor = EditorGUILayout.ColorField(VertexDirt.skyColor);
		GUILayout.Space(10);
		GUILayout.Label ("Intensity");
		//VertexDirt.indirectIntensity = EditorGUILayout.Slider(VertexDirt.indirectIntensity,0.0, 2.0);
		GUILayout.Space(10);
		GUILayout.Label ("Color bleed");
		//VertexDirt.indirectContrast = EditorGUILayout.Slider(VertexDirt.indirectContrast,0.0, 2.0);
		
 		if (Selection.gameObjects != null) {
		
			if (GUI.Button(new Rect(10,350,240,40),"Start") ) {

				VertexDirt.SetPreset (VDPRESET.INDIRECTLIGHTING);
				double tempTime = EditorApplication.timeSinceStartup;
				VertexDirt.Dirt(Selection.GetFiltered(typeof(Transform), SelectionMode.Deep).Cast<Transform>().ToArray());
				Debug.Log ("Dirt time: " + (EditorApplication.timeSinceStartup - tempTime));
		
			}
			
		}
		
		Repaint();
 
    }
}

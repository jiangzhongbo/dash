using UnityEngine;
using System.Collections;
using UnityEditor;
public class VDSaveVertexColoredMeshes : EditorWindow {

	private string path = "Plugins/VertexDirt/Saved meshes";
	
	[MenuItem("Tools/VertexDirt/Save Meshes", false, 20)]
	static void Init() {
	
		var window  = EditorWindow.GetWindow(typeof(VDSaveVertexColoredMeshes), false, "Save meshes");
		window.position = new Rect(600,200, 400,50);
		window.autoRepaintOnSceneChange = true;
		window.Show();
		
    }
     
    void OnGUI() {
	
		GUILayout.Label("Use this tool after colors generated.");
		GUILayout.Label("Select single GameObject.");
		path = EditorGUILayout.TextField("Asset path for saving: ", path);
		if (GUILayout.Button("Save meshes of children.", GUILayout.Height(40))) {

			//Debug.Log (GetPathName(Selection.activeTransform, ""));
		
			var gos = Selection.activeTransform.GetComponentsInChildren<Transform>(); 
		
 			foreach (var t in gos) {
			
				if (t.gameObject.GetComponent<VDColorHandlerBase>() && t.gameObject.GetComponent<MeshFilter>()) {
				
					try {
					
						AssetDatabase.CreateAsset( 
						
							t.gameObject.GetComponent<MeshFilter>().sharedMesh, "Assets/"+path+"/"+GetPathName(t, "") +".asset" 
							
						);
						
					}
					catch(UnityException e) {
					
						Debug.Log ("This asset already saved. If you have multiple gameobjects at the same hierarchy and with the same Name, please give them uniqe names.");
					
					}
						
					AssetDatabase.SaveAssets();
					t.gameObject.GetComponent<VDColorHandlerBase>().coloredMesh =
						t.gameObject.GetComponent<MeshFilter>().sharedMesh;
				
				}
				
			}

			AssetDatabase.Refresh();
			
		}
		
		Repaint();
		
	}
	
	public string GetPathName(Transform t, string s) {
	
		s = t.name + s;
	
		if (t.parent != null) {
			s = "--" + s;
			s = GetPathName(t.parent, s);
			
		}
			
		return s;
	
	}
}

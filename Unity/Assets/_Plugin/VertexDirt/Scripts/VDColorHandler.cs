using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class VDColorHandler : VDColorHandlerBase {

	public override void Refresh() {

        var meshFilter = gameObject.GetComponent<MeshFilter>();
		//DestroyImmediate(coloredMesh);
		coloredMesh =  Instantiate(originalMesh) as Mesh;
		meshFilter.mesh = coloredMesh;
		coloredMesh.colors32 = colors;
        gameObject.GetComponent<MeshFilter>().mesh = coloredMesh;			

	}
	
	void OnDisable() {

        gameObject.GetComponent<MeshFilter>().mesh = originalMesh;
	
	}

	void OnEnable() {

        MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();

		if (!originalMesh) {
		
			originalMesh = meshFilter.sharedMesh;
		
		}
		
		if (coloredMesh) {
		
			meshFilter.mesh = coloredMesh;
		
		}
			
	}

    [ContextMenu("IntoColor")]
    public void IntoColor()
    {
        coloredMesh.colors32 = colors;
    }

}
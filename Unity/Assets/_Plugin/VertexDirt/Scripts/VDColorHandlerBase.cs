using UnityEngine;
using System.Collections;

public abstract class VDColorHandlerBase : MonoBehaviour {

	//[HideInInspector]
	public Color32[] colors;
    public Mesh coloredMesh;
    public Mesh originalMesh;

    public abstract void Refresh();

}
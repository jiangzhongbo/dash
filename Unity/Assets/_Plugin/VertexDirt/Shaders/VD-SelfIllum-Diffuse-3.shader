
Shader "VD Vertex Color/Self-Illumin Diffuse 3" {

	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Illum ("Illumin (A)", 2D) = "white" {}
		_EmissionLM ("Emission (Lightmapper)", Float) = 0
		_AOIntensity ("AO Intensity", Range(0, 1)) = 1.0
		_AOPower ("AO Power", Range(1, 10)) = 1.0
		_AOColor ("AO Color", Color) = (0,0,0,1)
		_TopLight ("Top Light", Float) = 1
 		_RightLight ("Right Light", Float) = 1
 		_FrontLight ("Front Light", Float) = 1

	}
	
	SubShader {
	
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
	
		#pragma surface surf Lambert vertex:vert
		sampler2D _MainTex;
		sampler2D _Illum;
		half4 _Color;

		half4 _AOColor;
		float _AOIntensity;
		float _AOPower;

		uniform half _TopLight;
		uniform half _RightLight;
		uniform half _FrontLight;

#if defined(UNITY_COMPILER_HLSL)
#define UNITY_INITIALIZE_OUTPUT(type,name) name = (type)0;
#else
#define UNITY_INITIALIZE_OUTPUT(type,name)
#endif
		struct Input {
		
			half2 uv_MainTex;
			half2 uv_Illum;
			half4 color;
			half3 lighting;
		};

		void vert (inout appdata_full v, out Input o) { 
		
			UNITY_INITIALIZE_OUTPUT(Input,o);
			o.color = v.color;


			half3 normal =  normalize(mul(_Object2World,half4(v.normal,0))).xyz;
			half f_d = acos(clamp(dot(half3(0,0,-1),half3(0,0,normal.z)),-1,1))/1.5708;
			half r_d = acos(clamp(dot(half3(1,0,0),half3(normal.x,0,0)),-1,1))/1.5708;
			half t_d = acos(clamp(dot(half3(0,1,0),half3(0,normal.y,0)),-1,1))/1.5708;

			f_d = lerp(0,1-f_d,half(normal.z<1));
			r_d = lerp(0,1-r_d,half(normal.x>0));
			t_d = lerp(0,1-t_d,half(normal.y>0));

			//half3 d = step(0, normal.zxy) * ()
			o.lighting = (_FrontLight*f_d) + (_RightLight*r_d) + (_TopLight*t_d);
			o.lighting = _Color.xyz*o.lighting;
		}
		
		void surf (Input IN, inout SurfaceOutput o) {
		
			half4 t = tex2D(_MainTex, IN.uv_MainTex);
			half4 c = t * _Color;
			half ao = pow((1-IN.color.r)*_AOIntensity, _AOPower );
			//o.Albedo = c.rgb * IN.color;
			o.Albedo = lerp(c.rgb * IN.color * IN.lighting , _AOColor, ao);
			//o.Albedo = c.rgb * IN.color * IN.lighting;
			o.Emission = c.rgb * tex2D(_Illum, IN.uv_Illum).a;
			o.Alpha = c.a;
			
		}
		
		ENDCG
		
	}

	Fallback "Self-Illumin/VertexLit"
	
}

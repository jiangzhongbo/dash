Shader "Doodle/Monument"
{
	Properties
	{
		_MainTex ("Base (RGB), Alpha (A)", 2D) = "black" {}
		_LightColour0 ("_LightColour0", Color) = (1,1,1,1)
		_LightColour1 ("_LightColour0", Color) = (1,1,1,1)
		_LightColour2 ("_LightColour0", Color) = (1,1,1,1)
		_LightColour3 ("_LightColour0", Color) = (1,1,1,1)
		_AmbientColour1 ("_AmbientColour1", Color) = (1,1,1,1)
		_ShadowRamp ("_ShadowRamp", float) = 0
		_ShadowColour ("_ShadowColour", Color) = (1,1,1,1)
		_LightTint ("_LightTint", Color) = (1,1,1,1)
	}

	SubShader
	{
		LOD 200

		Tags
		{
			"RenderType" = "Opaque"
		}
		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			half4 _MainTex_ST;

			uniform half4 _LightColour0;
			uniform half4 _LightColour1;
			uniform half4 _LightColour2;
			uniform half4 _LightColour3;
			uniform half4 _AmbientColour1;

			uniform float _ShadowRamp;
			uniform half4 _ShadowColour;
			uniform half4 _LightTint;

			uniform float4 unity_LightmapST;
			uniform sampler2D unity_Lightmap;

			struct appdata_t
			{
				float4 vertex : POSITION;
				half3 normal : NORMAL;
				float2 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				half4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
				float3 texcoord2 : TEXCOORD2;
			};

			v2f vert (appdata_t v)
			{
				v2f o;
				half3 flatWorldNormal_1;
				half4 vertex  = mul(UNITY_MATRIX_MVP, v.vertex);
				half2 texcoord = ((v.texcoord.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
				half2 texcoord1 = ((v.texcoord1.xy * unity_LightmapST.xy) + unity_LightmapST.zw);
				half4 normal;
				normal.w = 0;
				normal.xyz = normalize(v.normal);
				half3 worldSpaceNormal  = normalize(mul(_Object2World, normal).xyz);
				flatWorldNormal_1.xy = worldSpaceNormal.xy;
				flatWorldNormal_1.z = 0;
				half tmpvar_7 = sqrt(dot (flatWorldNormal_1, flatWorldNormal_1));
				if ((tmpvar_7 > 0.0001)) {
					flatWorldNormal_1 = normalize(flatWorldNormal_1);
				}
				half tmpvar_8 = clamp (-(flatWorldNormal_1).x, -1.0, 1.0);
				half tmpvar_9 = ((1.5708 - (sign(tmpvar_8) * 
					(1.5708 - (sqrt((1.0 - abs(tmpvar_8))) * (1.5708 + (abs(tmpvar_8)* (-0.214602 + (abs(tmpvar_8) * (0.0865667 + (abs(tmpvar_8)* -0.0310296)))))))))) / 1.5708);
					half tmpvar_10 = clamp (-(worldSpaceNormal ).z, -1.0, 1.0);
					half tmpvar_11 = (1.0 - ((1.5708 - (sign(tmpvar_10) * (1.5708 - (sqrt((1.0 - abs(tmpvar_10))) * (1.5708 + (abs(tmpvar_10) * (-0.214602 + (abs(tmpvar_10) * (0.0865667 + (abs(tmpvar_10) * -0.0310296)))))))))) / 1.5708));
				o.vertex = vertex;
				o.texcoord = texcoord;
				o.texcoord1 = texcoord1;
				o.texcoord2 = (
					lerp (
						lerp (
							lerp (
								_LightColour0.xyz,
								_LightColour3.xyz, 
								half3(float((flatWorldNormal_1.x >= 0.0)))
							)
							, 
							lerp (
								_LightColour1.xyz, 
								_LightColour3.xyz, 
								half3(float((0.0 >= flatWorldNormal_1.y)))
								)
							,
							half3(
								lerp (
									tmpvar_9, 
									(2.0 - tmpvar_9), 
									float((flatWorldNormal_1.x >= 0.0))
								)
							)
						)
						,
						lerp (
							_LightColour2.xyz,
							_LightColour3.xyz,
							half3(float((0.0 >= -(worldSpaceNormal .z))))
						)
						, 
						half3(lerp (tmpvar_11, -(tmpvar_11), float((0.0 >= tmpvar_11))))
					) + _AmbientColour1.xyz);

				return o;
			}

			half4 frag (v2f IN) : COLOR
			{
				float4 color;
				float3 tmpvar_2 = (2.0 * tex2D (unity_Lightmap, IN.texcoord1).xyz);
				float4 texColor = tex2D (_MainTex, IN.texcoord);
				//float3 tmpvar_4 = lerp (((_LightTint.xyz * IN.texcoord2) * texColor.xyz), _ShadowColour.xyz, half3(clamp((_ShadowRamp * (1.0 - tmpvar_2)).x, 0.001, 0.999)));
				float3 tmpvar_4 = lerp (
					((_LightTint.xyz * IN.texcoord2) * texColor.xyz)
					,
					_ShadowColour.xyz
					, 
					half3(clamp(_ShadowRamp, 0.001, 0.999))
				);

				color.xyz = tmpvar_4;
				color.w = 1.0;
				return color;
			}
			ENDCG
		}
	}
	Fallback Off
}

﻿using UnityEngine;
using System.Collections;
using System;
public enum SKU_ID
{
    golds1_99 = 1,
    golds2_99 = 2,
    golds3_99 = 3,
    golds4_99 = 4,
    golds9_99 = 5,
    golds19_99 = 6,
    golds49_99 = 7,
    golds99_99 = 8,
}
public static class AndroidJavaManager
{
    public static AndroidJavaObject GetActivity()
    {
        AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = jc.GetStatic<AndroidJavaObject>("currentActivity");
        return activity;
    }

    public static void MoreGames()
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            activity.Call("MoreGames");
        }
    }

    public static void Rate()
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            activity.Call("Rate");
        }
    }

    public static void ShowFeatureViewSmall()
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            activity.Call("ShowFeatureViewSmall");
        }
    }

    public static void ShowFeatureView()
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            activity.Call("ShowFeatureView");
        }
    }

    public static void CloseFeatureView()
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            activity.Call("CloseFeatureView");
        }
    }

    public static void ShowFullScreenSmall()
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            activity.Call("ShowFullScreenSmall");
        }
    }

    public static void CloseFullScreenSmall()
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            activity.Call("CloseFullScreenSmall");
        }
    }

    public static void ShowFullScreenSmallExit()
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            activity.Call("ShowFullScreenSmallExit");
        }
    }

    public static void CloseFullScreenSmallExit()
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            activity.Call("CloseFullScreenSmallExit");
        }
    }

    public static bool IsFullScreenSmallShowing()
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            return activity.Call<bool>("IsFullScreenSmallShowing");
        }
    }

    public static bool IsFullScreenSmallIsReady()
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            bool b = activity.Call<bool>("IsFullScreenSmallIsReady");
            return b;
        }
    }

    public static void ShowNotification()
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            activity.Call("ShowNotification");
        }
    }



    public static void Purchase(SKU_ID id)
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            activity.Call("purchase", (int)id);
        }
    }

    public static int GetDPI()
    {
        try
        {
            using (AndroidJavaObject activity = GetActivity())
            {
                return activity.Call<int>("GetDPI");
            }
        }
        catch(Exception e){
            _.Log(e);
            return Mathf.RoundToInt(Screen.dpi);
        }
    }

    public static void GetServerTime()
    {
        using (AndroidJavaObject activity = GetActivity())
        {
            activity.Call("GetServerTime");
        }
    }

    public static void CallOnQuit()
    {
        PlayerPrefs.Save();
        using (AndroidJavaObject activity = GetActivity())
        {
            activity.Call("CallOnQuit");
        }
    }
}

using UnityEngine;
using System.Collections;
using System.Linq;
using System;
public class Back : MonoBehaviour {
	void Update () {
		try
        {
			if(Input.GetKeyDown(KeyCode.Escape))
            {
                if(AndroidJavaManager.IsFullScreenSmallShowing()){
					AndroidJavaManager.CloseFullScreenSmall();
	                return;
	            }
            }
		}catch(Exception  e){
			_.Log(e);
		}
	}
	
	public void CloseAdInQuitGame()
    {
        
    }

    public void ServerTimeCallback(string servertime)
    {
        long time = long.Parse(servertime);
        Debug.Log("1############### servertime:" + servertime);
        if (time == -1)
        {
            return;
        }
    }

    public static long UnixTimeNow()
    {
        var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
        return (long)timeSpan.TotalSeconds;
    }

    [ContextMenu("FakeServerTimeCallback")]
    public void FakeServerTimeCallback()
    {
        long timestamp = UnixTimeNow();
        ServerTimeCallback(timestamp + "");
    }

}

﻿using UnityEngine;
using System.Collections;

public class BgManager : MonoBehaviour
{
    float x = 0;
    float h = 0;
    void Start()
    {

    }
    private static BgManager instance;
    public static BgManager Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
    }

    public void Play()
    {
        isPlaying = true;
    }

    public void Stop()
    {
        isPlaying = false;
    }

    bool isPlaying = false;

    void Update()
    {
        if (!isPlaying) return;
        x -= Time.deltaTime / 50;
        renderer.materials[0].mainTextureOffset = new Vector2(x, 0);

        float r = Mathf.Abs(Mathf.Sin(Time.timeSinceLevelLoad));
        float g = Mathf.Abs(Mathf.Sin(Time.timeSinceLevelLoad * 0.45f));
        float b = Mathf.Abs(Mathf.Sin(Time.timeSinceLevelLoad * 1.2f));
        Color newColor = new Color(r, g, b);
        renderer.materials[0].color = newColor;

        //renderer.materials[0].color = new HSBColor(h, 100f / 255f, 1, 1).ToColor();
        //h += 0.01f;
        //if (h > 1)
        //{
        //    h = 0;
        //}
    }
}

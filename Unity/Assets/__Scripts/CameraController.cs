﻿using UnityEngine;
using System.Collections;



public class CameraController : MonoBehaviour
{

    public Transform target;

    public float Damping = 10.0f;

    Vector3 initPos;
    Vector3 targetPos = Vector3.zero;
    Vector3 targetInitPos = Vector3.zero;
    public int i = 0;
    void Start()
    {
        initPos = transform.position;
        targetInitPos = target.position;
    }

    //void LateUpdate()
    //{
    //    if (!target)
    //        return;
    //    Vector3 TempV = transform.position;
    //    float d = target.position.y - 0.5f;
    //    i = (int)d / 8;
    //    transform.LookAt(targetInitPos + Vector3.up * 3 - i * Vector3.up);
    //}

    void Update()
    {
        float d = target.position.y - 0.5f;
        i = (int)d / 5;

        var lookpos = targetInitPos + Vector3.up * 4 + i * Vector3.up * 3 - transform.position;
        var rotation = Quaternion.LookRotation(lookpos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * Damping);


    }

}
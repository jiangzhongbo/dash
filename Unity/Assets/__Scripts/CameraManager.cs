﻿using UnityEngine;
using System.Collections;
public enum GameScene
{
    GameScene1 = 1 << 27,
    GameScene2 = 1 << 28,
    GameScene3 = 1 << 29,
    GameScene4 = 1 << 30,
}
public class CameraManager : MonoBehaviour
{
    private static CameraManager instance;

    private Vector3 targetPos;
    private Vector3 targetEuler;

    public static CameraManager Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
        //Positive();
    }

    [ContextMenu("Positive")]
    public void Positive()
    {
        targetPos = new Vector3(23, 8.7f, -19);
        targetEuler = new Vector3(6, 315, 1.7f);
        transform.position = Vector3.Lerp(transform.position, targetPos, 1);
        transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, targetEuler, 1);

    }

    [ContextMenu("Reverse")]
    public void Reverse()
    {
        targetPos = new Vector3(-23, 8.7f, -19);
        targetEuler = new Vector3(6, 45, 1.7f);
        transform.position = Vector3.Lerp(transform.position, targetPos, 1);
        transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, targetEuler, 1);
    }

    public void SelectGameScene(GameScene gs)
    {
        int mask = camera.cullingMask;
        var newMask = mask & ~(int)GameScene.GameScene1 & ~(int)GameScene.GameScene2 & ~(int)GameScene.GameScene3 & ~(int)GameScene.GameScene4 | (int)gs;
        camera.cullingMask = newMask;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public enum GamePattern
{
    SimulateGravity,
    UFO,
    Bird,
    Arrow,
}
public enum GameState
{
    Normal,
    Success,
    Fail,
    Pause,
}
public class CubeManager : MonoBehaviour
{
    public GamePattern GP = GamePattern.SimulateGravity;
    public GameState State = GameState.Normal;
    public float ZSpeed = 1;
    public bool IsPlaying = false;
    public bool IsPractice = false;
    public delegate void FailedDelegate();
    public FailedDelegate Failed;
    public delegate void SuccessDelegate();
    public SuccessDelegate Success;
    public GameObject CurrentMap;
    private static CubeManager instance;
    public static CubeManager Instance
    {
        get
        {
            if (!instance && !Application.isPlaying)
            {
                instance = GameObject.Find("Cube").GetComponent<CubeManager>();
            }
            return instance;
        }
    }
    public GameObject p1;
    public GameObject p2;
    public GameObject core;
    public GameObject propeller;
    public GameObject ufoModel;
    public GameObject Cube;

    IGamePattern simulateGravity;
    IGamePattern ufo;
    IGamePattern bird;
    IGamePattern arrow;

    void Awake()
    {
        simulateGravity = GetComponent<SimulateGravity>();
        ufo = GetComponent<UFO>();
        bird = GetComponent<Bird>();
        arrow = GetComponent<Arrow>();

        instance = this;
        p1 = gameObject.FindChild("p1");
        p2 = gameObject.FindChild("p2");

        core = gameObject.FindChild("core");
        propeller = gameObject.FindChild("propeller");
        ufoModel = gameObject.FindChild("UFO");

        simulateGravity.SetP1(p1);
        simulateGravity.SetP2(p2);
        simulateGravity.SetCore(core);

        ufo.SetP1(p1);
        ufo.SetP2(p2);
        ufo.SetCore(core);

        bird.SetP1(p1);
        bird.SetP2(p2);
        bird.SetCore(core);

        arrow.SetP1(p1);
        arrow.SetP2(p2);
        arrow.SetCore(core);

        propeller.Hide();
        ufoModel.Hide();
    }

    public void ChangeGP(GamePattern gp)
    {
        propeller.Hide();
        ufoModel.Hide();
        GP = gp;
        if (GP == GamePattern.SimulateGravity)
        {
            ufo.Stop();
            bird.Stop();
            arrow.Stop();

            ufo.SetEnable(false);
            bird.SetEnable(false);
            arrow.SetEnable(false);

            simulateGravity.SetEnable(true);
            simulateGravity.Play();
        }
        else if(GP == GamePattern.UFO)
        {
            simulateGravity.Stop();
            bird.Stop();
            arrow.Stop();

            simulateGravity.SetEnable(false);
            bird.SetEnable(false);
            arrow.SetEnable(false);

            ufo.SetEnable(true);
            ufo.Play();

            ufoModel.Show();
        }
        else if (GP == GamePattern.Bird)
        {
            ufo.Stop();
            simulateGravity.Stop();
            arrow.Stop();

            ufo.SetEnable(false);
            simulateGravity.SetEnable(false);
            arrow.SetEnable(false);

            bird.SetEnable(true);
            bird.Play();
            propeller.renderer.enabled = true;
            
        }
        else if (GP == GamePattern.Arrow)
        {
            ufo.Stop();
            bird.Stop();
            simulateGravity.Stop();

            ufo.SetEnable(false);
            bird.SetEnable(false);
            simulateGravity.SetEnable(false);

            arrow.SetEnable(true);
            arrow.Play();
        }
    }

    public IGamePattern GetCurrentPattern()
    {
        if (GP == GamePattern.SimulateGravity)
        {
            return simulateGravity;
        }
        else if (GP == GamePattern.UFO)
        {
            return ufo;
        }
        else if (GP == GamePattern.Bird)
        {
            return bird;
        }
        else if (GP == GamePattern.Arrow)
        {
            return arrow;
        }
        return null;
    }

    void Start()
    {
        StartCoroutine(loopGenRevivePoint());
    }

    public void Play()
    {
        if (!Cube || Cube.name != GameContext.Instance.CubeId)
        {
            if (Cube)
            {
                GameObject.DestroyImmediate(Cube);
            }
            GameObject prefab = Resources.Load("Cube/" + GameContext.Instance.CubeId) as GameObject;
            Cube = GameObject.Instantiate(prefab) as GameObject;
            Cube.transform.parent = core.transform;
            Cube.transform.localPosition = Vector3.zero;
            Cube.transform.localEulerAngles = Vector3.zero;
            Cube.transform.localScale = Vector3.one;
            Cube.layer = LayerMask.NameToLayer("Cube");
        }
        if (State == GameState.Pause)
        {
        }
        else if (State == GameState.Fail)
        {
            if (revive)
            {
                ChangeGP(GP);
                revive = false;
                transform.eulerAngles = Vector3.zero;
                IsPlaying = true;
                CurrentRevivePoint();
            }
            else
            {
                GP = GamePattern.SimulateGravity;
                Reset();
                ChangeGP(GP);
                transform.eulerAngles = Vector3.zero;
                IsPlaying = true;

                RevivePoints.ToList().ForEach(e =>
                {
                    GameObject.DestroyImmediate(e.Point);
                });
                RevivePoints.Clear();

            }
        }
        else if (State == GameState.Success || State == GameState.Normal)
        {
            GP = GamePattern.SimulateGravity;
            Reset();
            ChangeGP(GP);
            transform.eulerAngles = Vector3.zero;
            IsPlaying = true;

            RevivePoints.ToList().ForEach(e =>
            {
                GameObject.DestroyImmediate(e.Point);
            });
            RevivePoints.Clear();
        }
        

        State = GameState.Normal;
        if (GameObject.Find("_Maps"))
        {
            CurrentMap = GameObject.Find("_Maps");
        }
        if (CurrentMap)
        {
            if (CurrentMap.GetComponent<MapInfo>().Reverse)
            {
                CameraManager.Instance.Reverse();
            }
            else
            {
                CameraManager.Instance.Positive();
            }
        }
        GetComponent<Follow>().Begin();
    }

    private bool revive = false;


    public void SetRevive()
    {
        revive = true;
    }

    public void DoSuccess()
    {
        State = GameState.Success;
        if (Success != null) Success();
    }

    public void Stop()
    {
        GetCurrentPattern().Stop();
        propeller.Hide();
        ufoModel.Hide();

        IsPlaying = false;
        core.Hide();
        
    }

    public void Reset()
    {
        transform.position = Vector3.up * 0.5f;
    }

    public void Jump()
    {
        GetCurrentPattern().Jump();
    }

    public void Pause()
    {
        State = GameState.Pause;
    }

    void MainUpdate()
    {
        if (!IsPlaying)
        {
        }
        else
        {
            RaycastHit hitinfo;
            Vector3 offset = Vector3.forward * -1 + Vector3.up * 0.5f;
            bool hit = Physics.Raycast(p1.transform.position + offset, Vector3.forward, out hitinfo, 10f, 1 << LayerMask.NameToLayer("Block"));
            if (hit)
            {
                if (hitinfo.distance <= 1f)
                {
                    _.Log2(1, hitinfo.transform.name);
                    CallFailed();
                }
            }

                

            if (GP == GamePattern.SimulateGravity || GP == GamePattern.Arrow)
            {
                RaycastHit hitinfo3;
                RaycastHit hitinfo4;
                bool hit3 = Physics.Raycast(p1.transform.position + Vector3.up * 0.5f, Vector3.up, out hitinfo3, 100, 1 << LayerMask.NameToLayer("Block"));
                bool hit4 = Physics.Raycast(p2.transform.position + Vector3.up * 0.5f, Vector3.up, out hitinfo4, 100, 1 << LayerMask.NameToLayer("Block"));
                if (hit3)
                {
                    if (hitinfo3.distance < 0.5f && !hitinfo3.transform.gameObject.GetComponent<PartInfo>().IsTopFloor)
                    {
                        _.Log(2);
                        CallFailed();
                    }
                }
                if (hit4)
                {
                    if (hitinfo4.distance < 0.5f && !hitinfo4.transform.gameObject.GetComponent<PartInfo>().IsTopFloor)
                    {
                        _.Log(3);
                        CallFailed();
                    }
                }
            }
        }
            
    }

    void OnTriggerEnter(Collider other)
    {
        
        if (other.GetComponent<PartInfo>() && other.GetComponent<PartInfo>().IsFloor)
        {
            return;
        }
        if (other.GetComponent<PartInfo>() && other.GetComponent<PartInfo>().IsObstacle)
        {
            _.Log2(4, other.name);
            CallFailed();
        }
    }

    public void PrevRevivePoint()
    {
        if (RevivePoints.Count > 1 && RevivePoints.Peek().Point)
        {
            RevicePoint p = RevivePoints.Pop();
            transform.position = RevivePoints.Peek().Point.transform.position;
            ChangeGP(RevivePoints.Peek().GP);
            DestroyImmediate(p.Point);
        }
        else
        {
            if (RevivePoints.Count > 0)
            {
                RevicePoint p = RevivePoints.Pop();
                DestroyImmediate(p.Point);
            } 
            transform.position = new Vector3(0, 0.5f, 0);
            ChangeGP(GamePattern.SimulateGravity);
        }
    }

    public void CurrentRevivePoint()
    {
        if (RevivePoints.Count > 0 && RevivePoints.Peek().Point)
        {
            RevicePoint p = RevivePoints.Peek();
            transform.position = RevivePoints.Peek().Point.transform.position;
            ChangeGP(RevivePoints.Peek().GP);
        }
        else if (State == GameState.Fail)
        {
            transform.position = new Vector3(0, 0.5f, 0);
            ChangeGP(GamePattern.SimulateGravity);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            PrevRevivePoint();

        }
        if (!IsPlaying) return;
        transform.position += Vector3.forward * Time.deltaTime * ZSpeed;
        if (
            simulateGravity.IsGrounded()
            && simulateGravity.GetGroundedPart()
            && simulateGravity.GetGroundedPart().GetComponent<PartInfo>().IsObstacle)
        {
            _.Log(5);
            CallFailed();
        }
        _FixedUpdate();
    }

    void _FixedUpdate()
    {
        MainUpdate();
        if (simulateGravity != null) simulateGravity.MainUpdate();
        if (bird != null) bird.MainUpdate();
        if (ufo != null) ufo.MainUpdate();
        if (arrow != null) arrow.MainUpdate();
    }

    public void StopPattern()
    {
        GetCurrentPattern().Stop();
        IsPlaying = false;
    }



    void CallFailed()
    {
        SoundManager.Instance.PlayOneShot(SoundManager.Instance.fx_broke);
        Stop();
        State = GameState.Fail;
        if (Failed != null) Failed();
    }

    public Stack<RevicePoint> RevivePoints = new Stack<RevicePoint>();
    public GameObject RevivePointParent;
    IEnumerator loopGenRevivePoint()
    {
        RevivePoints.Clear();
        if(RevivePointParent)
        {
            DestroyImmediate(RevivePointParent);
        }
        RevivePointParent = new GameObject();
        RevivePointParent.name = "_RevivePointParent";
        RevivePointParent.transform.position = Vector3.zero;
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (IsPlaying && IsPractice)
            {
                RevivePoints.Push(RevicePoint.Gen(GP, transform.position ,RevivePointParent));
            }
            
        }
    }

}

public class RevicePoint
{
    public GameObject Point;
    public GamePattern GP;

    public static int i = 0;
    public static RevicePoint Gen(GamePattern gp, Vector3 pos, GameObject parent)
    {
        var rp = new RevicePoint();
        rp.GP = gp;
        GameObject p = Resources.Load("Effect/RevivePoint") as GameObject;
        GameObject go = GameObject.Instantiate(p) as GameObject;
        go.transform.position = pos;
        go.transform.parent = parent.transform;
        go.name = "RevivePoint_" + i;
        i++;
        rp.Point = go;
        return rp;
    }
}

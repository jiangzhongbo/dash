﻿using UnityEngine;
using UnityEditor;
using System.Linq;
public class DoodleMobileEditor
{
    [MenuItem("DoodleMobile/Remove All Animation")]
    static void DoIt()
    {
        GameObject go = Selection.activeGameObject;
        go.GetComponentsInChildren<Animation>().ToList().ForEach(e =>
        {
            GameObject.DestroyImmediate(e);
        });
    }
}
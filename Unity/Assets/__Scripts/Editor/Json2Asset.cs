﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Json.Doodle;
using System.Text.RegularExpressions;

public class Json2Asset
{
    public class Table
    {
        public string table_name { get; set; }
        public int rows { get; set; }
        public int cols { get; set; }
        public Dictionary<string, object> cells { get; set; }
    }

    [MenuItem("DoodleMobile/E2C/Json2Asset")]
    public static void GenAsset()
    {
        new Json2Asset().Parse();
    }

    public void Parse()
    {
        TextAsset ta = AssetDatabase.LoadAssetAtPath("Assets/__Json/excels.txt", typeof(TextAsset)) as TextAsset;
        List<Table> tables = JsonConverter.Deserialize<List<Table>>(ta.text);
        for (int sheetIndex = 0; sheetIndex < tables.Count; sheetIndex++)
        {
            try
            {
                var sheet = tables[sheetIndex];
                if (IsUsedSheet(sheet.table_name))
                {
                    var modelType = Type.GetType(GetClassName(sheet.table_name) + "Part, Assembly-CSharp");
                    //_.Log("table_name:" + sheet.table_name);
                    //_.Log("modelType:" + modelType);
                    var dataType = Type.GetType(GetClassName(sheet.table_name) + ", Assembly-CSharp");
                    //_.Log("dataType:" + dataType);

                    var listInstance = (IList)typeof(List<>).MakeGenericType(modelType).GetConstructor(Type.EmptyTypes).Invoke(null);

                    for (int rowIndex = 3; rowIndex < sheet.rows; rowIndex++)
                    {
                        try
                        {
                            //_.Log("rowIndex:" + rowIndex);
                            var modelInstance = Activator.CreateInstance(modelType);
                            for (int columnIndex = 0; columnIndex < sheet.cols; columnIndex++)
                            {
                                if (!IsUsedColumn(sheet.cells[2 + "_" + columnIndex]))
                                {
                                    continue;
                                }
                                //Debug.Log("sheet:" + sheet.table_name + ", column:" + columnIndex + ", rowIndex:" + rowIndex);
                                var name = OnlyLetter(GetCellLabel(sheet.cells[0 + "_" + columnIndex])).Trim();
                                var value = GetCellLabel(sheet.cells[rowIndex + "_" + columnIndex]).Trim();

                                var type = GetCellLabel(sheet.cells[1 + "_" + columnIndex]).Trim();
                                //Debug.Log("Name:" + Name + ", value:" + value + ", _type:" + _type);
                                object newValue = null;
                                if (type.Trim() == "string")
                                {
                                    newValue = value;
                                }
                                else if (type.Trim() == "bool")
                                {
                                    newValue = value == "1";
                                }
                                else if (type.Trim() == "int")
                                {
                                    try
                                    {
                                        newValue = int.Parse(value);
                                    }
                                    catch (Exception e)
                                    {
                                        _.Log(e);
                                    }
                                }
                                else if (type.Trim() == "float")
                                {
                                    try
                                    {
                                        newValue = float.Parse(value);

                                    }
                                    catch (Exception e)
                                    {
                                        try
                                        {
                                            newValue = (float)double.Parse(value);
                                        }
                                        catch (Exception e2)
                                        {
                                            _.Log(e2);
                                        }
                                    }
                                }
                                else if (type.Trim() == "Vector3")
                                {
                                    if (value.Contains(","))
                                    {
                                        Vector3 v = new Vector3();
                                        float[] f3 = value.Split(',').Select(e => float.Parse(e)).ToArray();
                                        v.x = f3[0];
                                        v.y = f3[1];
                                        v.z = f3[2];
                                        newValue = v;
                                    }
                                    else
                                    {
                                        newValue = Vector3.zero;
                                    }

                                }
                                modelType.GetField(name).SetValue(modelInstance, newValue);
                            }
                            listInstance.Add(modelInstance);
                        }
                        catch (Exception e)
                        {
                            _.Log(e);
                        }
                    }
                    var load = dataType.BaseType.GetMethod("Load", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.FlattenHierarchy);
                    var dataInstance = load.Invoke(null, null);
                    dataType.GetField("Infos").SetValue(dataInstance, listInstance);
                    dataType.GetMethod("Save", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public).Invoke(dataInstance, null);
                }
            }
            catch (Exception e)
            {
                _.Log(e);
            }
        }
        AssetDatabase.Refresh();
    }

    protected bool IsUsedSheet(string sheetName)
    {
        return string.IsNullOrEmpty(sheetName) ? false : sheetName.StartsWith("#");
    }

    protected bool IsUsedColumn(object obj)
    {
        if (obj.ToString().StartsWith("1"))
        {
            return true;
        }
        return false;
    }

    protected string GetClassName(string sheetName)
    {
        return sheetName.Substring(1);
    }

    protected string GetCellLabel(object obj)
    {
        if (obj == null)
        {
            return "";
        }
        return obj.ToString();
    }


    protected string OnlyLetter(string str)
    {
        string result = Regex.Replace(str, @"[^1-9a-zA-Z_]", "");
        return result;
    }

}

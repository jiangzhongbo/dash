﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(MapManager))]
public class MapManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        MapManager mm = target as MapManager;
        mm.Maps = EditorGUILayout.TextArea(mm.Maps, GUI.skin.textArea, GUILayout.Height(500f));
        DrawDefaultInspector();
        if (GUILayout.Button("Gen"))
        {
            mm.Gen();
        }
        if (GUILayout.Button("GenList"))
        {
            mm.GenList();
        }
        if (GUILayout.Button("UpdateTimeScale"))
        {
            mm.UpdateTimeScale();
        }
    }
}
﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;


[CustomEditor(typeof(_Debug))]
public class _DebugEditor : Editor
{
    private List<PropertyInfo> properties;
    private List<FieldInfo> fields;
    public override void OnPreviewSettings()
    {
        base.OnPreviewSettings();
    }

    void OnEnable()
    {
    }

    public _DebugEditor()
    {
        properties = typeof(GameContext).GetProperties(BindingFlags.Instance | BindingFlags.Public).ToList();
        fields = typeof(GameContext).GetFields(BindingFlags.Instance | BindingFlags.Public).ToList();
    }

    public override void OnInspectorGUI()
    {


        foreach (PropertyInfo p in properties)
        {
            if (p.PropertyType == typeof(int))
            {
                p.SetValue(GameContext.Instance, EditorGUILayout.IntField(p.Name, (int)p.GetValue(GameContext.Instance, null)), null);
            }
            else if (p.PropertyType == typeof(string))
            {
                p.SetValue(GameContext.Instance, EditorGUILayout.TextField(p.Name, (string)p.GetValue(GameContext.Instance, null)), null);
            }
            else if (p.PropertyType == typeof(bool))
            {
                p.SetValue(GameContext.Instance, EditorGUILayout.Toggle(p.Name, (bool)p.GetValue(GameContext.Instance, null)), null);
            }
            else if (p.PropertyType == typeof(float))
            {
                p.SetValue(GameContext.Instance, EditorGUILayout.FloatField(p.Name, (float)p.GetValue(GameContext.Instance, null)), null);
            }
        }
        foreach (FieldInfo p in fields)
        {
            if (p.FieldType == typeof(int))
            {
                p.SetValue(GameContext.Instance, EditorGUILayout.IntField(p.Name, (int)p.GetValue(GameContext.Instance)));
            }
            else if (p.FieldType == typeof(string))
            {
                p.SetValue(GameContext.Instance, EditorGUILayout.TextField(p.Name, (string)p.GetValue(GameContext.Instance)));
            }
            else if (p.FieldType == typeof(bool))
            {
                p.SetValue(GameContext.Instance, EditorGUILayout.Toggle(p.Name, (bool)p.GetValue(GameContext.Instance)));
            }
            else if (p.FieldType == typeof(float))
            {
                p.SetValue(GameContext.Instance, EditorGUILayout.FloatField(p.Name, (float)p.GetValue(GameContext.Instance)));
            }
        }

        if (GUILayout.Button("Save", GUILayout.Width(200)))
        {
            if (GameContext.Instance != null)
            {
                GameContext.Instance.Save();
            }
        }
    }
}
﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

public class FloorManager : MonoBehaviour
{
    public GameObject parent;
    private List<GameObject> parts = new List<GameObject>();
    private List<GameObject> parts2 = new List<GameObject>();
    public float ShowLength = 200;
    public float Speed = 1;
    private GameObject prev;
    private GameObject prev2;
    public float h = 0;
    public float TopY = 12;
    private static FloorManager instance;
    public static FloorManager Instance
    {
        get
        {
            return instance;
        }
    }
    
    void Awake()
    {
        instance = this;
        parent = GameObject.Find("_Floors");
        
    }



    [ContextMenu("Gen")]
    void Gen()
    {
        parent = GameObject.Find("_Floors");
        DestroyImmediate(parent);
        parent = new GameObject("_Floors");
        for (int i = 0; i < 5; i++)
        {
            var go = genMap();
            go.transform.parent = parent.transform;
            go.transform.eulerAngles = Vector3.zero;
            go.name = "down_"+i;
            if (prev)
            {
                go.transform.localPosition = prev.transform.localPosition + Vector3.forward * 213;
            }
            else
            {
                go.transform.localPosition = new Vector3(20, -30, -20);
            }
            parts.Add(go);
            prev = go;
        }
    }

    void changeColor(GameObject go)
    {
        MeshRenderer mr = go.GetComponent<MeshRenderer>();
        Material m = mr.material;
        m.color = new HSBColor(h, 100f / 255f, 1, 1).ToColor();
        h += 0.1f;
        if (h > 1)
        {
            h = 0;
        }
    }

    GameObject genMap()
    {
        string path = "Floor/city"+Random.Range(1,3);
        GameObject prefab = Resources.Load(path) as GameObject;
        var go = GameObject.Instantiate(prefab) as GameObject;
        go.transform.name = "floor";
        go.transform.eulerAngles = Vector3.zero;
        return go;
    }

}

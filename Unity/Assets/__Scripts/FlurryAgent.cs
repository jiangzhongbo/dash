using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class FlurryAgent : IDisposable
{
	private static FlurryAgent _instance;
	public static FlurryAgent Instance
	{
		get
		{
			if(_instance == null) _instance = new FlurryAgent();
			return _instance;
		}
	}
	

	private AndroidJavaClass cls_FlurryAgent = new AndroidJavaClass("com.flurry.android.FlurryAgent");
	
	public void onStartSession(string apiKey)
	{
		
		Debug.Log ("****** onStartSession " +apiKey);
		using(AndroidJavaClass cls_UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) 
		{
			using(AndroidJavaObject obj_Activity = cls_UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity")) 
			{
				cls_FlurryAgent.CallStatic("onStartSession", obj_Activity, apiKey);
			}
		}
	}
	
	public void onEndSession()
	{
		_.Log ("****** onEndSession ");
		using(AndroidJavaClass cls_UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) 
		{
			using(AndroidJavaObject obj_Activity = cls_UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity")) 
			{
				cls_FlurryAgent.CallStatic("onEndSession", obj_Activity);
			}
		}
	}
	
	public void logEvent(string eventId)
	{
        //Debug.Log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@FlurryAgent: " + eventId);
		cls_FlurryAgent.CallStatic("logEvent", eventId);
	}
	
	public void setContinueSessionMillis(long milliseconds)
	{
		cls_FlurryAgent.CallStatic("setContinueSessionMillis", milliseconds);
	}
	
	public void onError(string errorId, string message, string errorClass)
	{
		cls_FlurryAgent.CallStatic("onError", errorId, message, errorClass);
	}
	
	public void onPageView()
	{
		cls_FlurryAgent.CallStatic("onPageView");
	}
	
	public void setLogEnabled(bool enabled)
	{
		cls_FlurryAgent.CallStatic("setLogEnabled", enabled);
	}
	
	public void setUserID(string userId)
	{
		cls_FlurryAgent.CallStatic("setUserID", userId);
	}
	
	public void setAge(int age)
	{
		cls_FlurryAgent.CallStatic("setAge", age);
	}
		
	public void setReportLocation(bool reportLocation)
	{
		cls_FlurryAgent.CallStatic("setReportLocation", reportLocation);
	}

	private void log(string eventId, Dictionary<string, string> parameters){
        //Debug.Log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@FlurryAgent start->" + eventId + ":");
        //Debug.Log("parameters:" + parameters);
        //parameters.ToList().ForEach(e => {
        //    Debug.Log(e.Key + ":" + e.Value);
        //});
        //Debug.Log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@FlurryAgent end->" + eventId + ":");
	}

	public void logEvent(string eventId, Dictionary<string, string> parameters)
	{
		log(eventId, parameters);
		foreach(string key in parameters.Keys){
			if(parameters[key] == null){
				parameters[key] = "";
			}
		}
		using(AndroidJavaObject obj_HashMap = new AndroidJavaObject("java.util.HashMap")) 
		{
			// Call 'put' via the JNI instead of using helper classes to avoid:
			// 	"JNI: Init'd AndroidJavaObject with null ptr!"
			IntPtr method_Put = AndroidJNIHelper.GetMethodID(obj_HashMap.GetRawClass(), "put", 
			                                                 "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
			
			object[] args = new object[2];
			foreach(KeyValuePair<string, string> kvp in parameters)
			{
				using(AndroidJavaObject k = new AndroidJavaObject("java.lang.String", kvp.Key))
				{
					using(AndroidJavaObject v = new AndroidJavaObject("java.lang.String", kvp.Value))
					{
						args[0] = k;
						args[1] = v;
						AndroidJNI.CallObjectMethod(obj_HashMap.GetRawObject(), 
						                            method_Put, AndroidJNIHelper.CreateJNIArgArray(args));
					}
				}
			}
			cls_FlurryAgent.CallStatic("logEvent", eventId, obj_HashMap);
		}
	}
	
	public void Dispose()
	{
		cls_FlurryAgent.Dispose();
	}
};
using System;
using System.Collections;
using System.Collections.Generic;

public class FlurryLogger {

    private static FlurryLogger instance;

    public static FlurryLogger Instance
    {
        get
        {
            if (instance == null) instance = new FlurryLogger();
            return instance;
        }
    }

    //1
    public void Golds_Consume(string consume_gold_type)
    {
        Dictionary<string, string> values = new Dictionary<string, string>();
        values.Add("consume_gold_type", consume_gold_type.ToString());
        FlurryAgent.Instance.logEvent("Golds_Consume", values);
    }

    //2
    public void First_Golds_Consume(string consume_gold_type)
    {
        Dictionary<string, string> values = new Dictionary<string, string>();
        values.Add("consume_gold_type", consume_gold_type.ToString());
        FlurryAgent.Instance.logEvent("First_Golds_Consume", values);
    }

    //3
    public void Golds_Purchased(string purchase_gold_type, int dollors_type)
    {
        Dictionary<string, string> values = new Dictionary<string, string>(); 
        values.Add("purchase_gold_type", purchase_gold_type.ToString());
        values.Add("dollors_type", dollors_type.ToString());
        FlurryAgent.Instance.logEvent("Golds_Purchased", values);
    }

    //4
    public void First_Golds_Purchased(string purchase_gold_type, int dollors_type)
    {
        Dictionary<string, string> values = new Dictionary<string, string>(); 
        values.Add("purchase_gold_type", purchase_gold_type.ToString());
        values.Add("dollors_type", dollors_type.ToString());
        FlurryAgent.Instance.logEvent("First_Golds_Purchased", values);
    }


    //5
    public void Achievement_Complete(string achieve_name) {
        Dictionary<string, string> values = new Dictionary<string, string>(); 
        values.Add("achieve_name", achieve_name.ToString());
        FlurryAgent.Instance.logEvent("Achievement_Complete", values);
    }


    //6
    public void Tutorial_start(string tutorial_type) {
        Dictionary<string, string> values = new Dictionary<string, string>(); 
        values.Add("tutorial_type", tutorial_type.ToString());
        FlurryAgent.Instance.logEvent("Tutorial_start", values);
    }

    //7
    public void Tutorial_Complete(string tutorial_type) {
        Dictionary<string, string> values = new Dictionary<string, string>(); 
        values.Add("tutorial_type", tutorial_type.ToString());
        FlurryAgent.Instance.logEvent("Tutorial_Complete", values);
    }

    //8
    public void DailyBonus_Open(int daily_day) {
        Dictionary<string, string> values = new Dictionary<string, string>(); 
        values.Add("daily_day", daily_day.ToString());
        FlurryAgent.Instance.logEvent("DailyBonus_Open", values);
    }

    //10
    public void Level_Play_First(string level_id) {
        Dictionary<string, string> values = new Dictionary<string, string>(); 
        values.Add("level_id", level_id.ToString());
        FlurryAgent.Instance.logEvent("Level_Play_First", values);
    }


    //11
    public void Level_Complete_First(string level_id) {
        Dictionary<string, string> values = new Dictionary<string, string>(); 
        values.Add("level_id", level_id.ToString());
        FlurryAgent.Instance.logEvent("Level_Complete_First", values);
    }

    //12
    public void Practice_Complete_First(string level_id)
    {
        Dictionary<string, string> values = new Dictionary<string, string>();
        values.Add("level_id", level_id.ToString());
        FlurryAgent.Instance.logEvent("Practice_Complete_First", values);
    }

    //13
    public void Level_Failed(string level_id) {
        Dictionary<string, string> values = new Dictionary<string, string>(); 
        values.Add("level_id", level_id.ToString());
        FlurryAgent.Instance.logEvent("Level_Failed", values);
    }

    //14
    public void Level_Replay(string level_id) {
        Dictionary<string, string> values = new Dictionary<string, string>(); 
        values.Add("level_id", level_id.ToString());
        FlurryAgent.Instance.logEvent("Level_Replay", values);
    }

    //15
    public void Share_Pressed()
    {
        Dictionary<string, string> values = new Dictionary<string, string>();
        FlurryAgent.Instance.logEvent("Share_Pressed", values);
    }

    public FlurryLogger() {
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Follow : MonoBehaviour
{
    public GameObject target;
    public List<GameObject> DefaultFollows;
    public List<GameObject> Follows;
    public List<float> diff = new List<float>();

    void Start()
    {
        Init();
    }

    public void Init()
    {
        for (int i = 0; i < DefaultFollows.Count; i++)
        {
            if (!DefaultFollows[i]) continue;
            if (Follows.Contains(DefaultFollows[i])) continue;
            Follows.Add(DefaultFollows[i]);
        }
        diff.Clear();
        for (int i = 0; i < Follows.Count; i++)
        {
            if (!Follows[i]) continue;
            diff.Add(Follows[i].transform.position.z - target.transform.position.z);
        }
        Begin();
    }

    public void Add(List<GameObject> follows)
    {
        for (int i = 0; i < follows.Count; i++)
        {
            if (!follows[i]) continue;
            if (Follows.Contains(follows[i])) continue;
            Follows.Add(follows[i]);
        }
    }

    void LateUpdate()
    {
        if (!following) return;
        for (int i = 0; i < Follows.Count; i++)
        {
            if (!Follows[i]) continue;
            var v = Follows[i].transform.position;
            v.z = target.transform.position.z + diff[i];
            Follows[i].transform.position = v;
        }
    }

    public bool following = false;

    public void Stop()
    {
        following = false;
    }

    public void Begin()
    {
        following = true;
    }
}

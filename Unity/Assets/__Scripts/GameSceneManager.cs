﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
public class GameSceneManager : MonoBehaviour
{
    private static GameSceneManager instance;

    public static GameSceneManager Instance
    {
        get
        {
            return instance;
        }
    }
    public List<GameObject> GameScenes;

    void Awake()
    {
        instance = this;
    }

    public void Select(int i)
    {
        switch (i)
        {
            case 1:
                CameraManager.Instance.SelectGameScene(GameScene.GameScene1);
                RenderSettings.fogColor = new Color(168f / 255f, 104f/ 255f, 221f / 255f);
                RenderSettings.ambientLight = new Color(139f / 255f, 139f / 255f, 139f / 255f);
                RenderSettings.fogDensity = 0.0024f;
                break;
            case 2:
                CameraManager.Instance.SelectGameScene(GameScene.GameScene2);
                RenderSettings.fogColor = new Color(143f / 255f, 199f / 255f, 240f / 255f);
                RenderSettings.ambientLight = new Color(204f / 255f, 204f / 255f, 204f / 255f);
                RenderSettings.fogDensity = 0.0022f;
                break;
            case 3:
                CameraManager.Instance.SelectGameScene(GameScene.GameScene3);
                RenderSettings.fogColor = new Color(7f / 255f, 116f / 255f, 162f / 255f);
                RenderSettings.ambientLight = new Color(173f / 255f, 173f / 255f, 173f / 255f);
                RenderSettings.fogDensity = 0.003f;
                break;
            case 4:
                CameraManager.Instance.SelectGameScene(GameScene.GameScene4);
                RenderSettings.fogColor = new Color(0f / 255f, 0f / 255f, 0f / 255f);
                RenderSettings.ambientLight = new Color(0 / 255f, 0 / 255f, 0 / 255f);
                RenderSettings.fogDensity = 0.0000f;
                break;
        }
        _.Log(i);
        CubeManager.Instance.GetComponent<Follow>().Add(GameScenes[i - 1].GetComponent<GameSceneInfo>().Follows);
        CubeManager.Instance.GetComponent<Follow>().Init();
        GameScenes.ForEach(e => e.Hide());
        GameScenes[i - 1].Show();
        CubeManager.Instance.CurrentMap = LevelManager.Instance.GetCurrentMap();
    }
}

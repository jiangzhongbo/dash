﻿using UnityEngine;
using System.Collections;

public interface IGamePattern
{
    void SetP1(GameObject p1);
    void SetP2(GameObject p2);
    void SetCore(GameObject core);
    void Play();
    void Stop();
    void Jump();
    bool IsGrounded();
    GameObject GetGroundedPart();

    void SetEnable(bool enable);

    void MainUpdate();
}

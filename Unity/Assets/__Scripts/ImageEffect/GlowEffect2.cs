﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
[AddComponentMenu("Image Effects/Glow2")]
public class GlowEffect2 : MonoBehaviour
{
//    public int downsampleSteps = 2;


    public int iterations = 3;

    public float blurSpread = 0.6f;

    public float innerStrength = 1f;

    //public float outerStrength = 1f;

    public float boostStrength = 1f;

    //public BlendMode blendMode = BlendMode.Additive;

    //public DownsampleBlendMode downsampleBlendMode = DownsampleBlendMode.Max;


    public Shader composeShader;
    Material m_ComposeMaterial = null;

    protected Material compoeMaterial
    {
        get
        {
            if (m_ComposeMaterial == null)
            {
                m_ComposeMaterial = new Material(composeShader);
                m_ComposeMaterial.hideFlags = HideFlags.HideAndDontSave;
            }
            return m_ComposeMaterial;
        }
    }


    public Shader composeMaxShader;
    Material m_ComposeMaxMaterial = null;

    protected Material composeMaxMaterial
    {
        get
        {
            if (m_ComposeMaxMaterial == null)
            {
                m_ComposeMaxMaterial = new Material(composeMaxShader);
                m_ComposeMaxMaterial.hideFlags = HideFlags.HideAndDontSave;
            }
            return m_ComposeMaxMaterial;
        }
    }


    public Shader blurShader;
    Material m_BlurMaterial = null;
    protected Material blurMaterial
    {
        get
        {
            if (m_BlurMaterial == null)
            {
                m_BlurMaterial = new Material(blurShader);
                m_BlurMaterial.hideFlags = HideFlags.HideAndDontSave;
            }
            return m_BlurMaterial;
        }
    }

    public Shader downsampleShader;
    Material m_DownsampleMaterial = null;
    protected Material downsampleMaterial
    {
        get
        {
            if (m_DownsampleMaterial == null)
            {
                m_DownsampleMaterial = new Material(downsampleShader);
                m_DownsampleMaterial.hideFlags = HideFlags.HideAndDontSave;
            }
            return m_DownsampleMaterial;
        }
    }

    protected void OnDisable()
    {
        if (m_ComposeMaterial)
        {
            DestroyImmediate(m_ComposeMaterial);
        }
        if (m_BlurMaterial)
        {
            DestroyImmediate(m_BlurMaterial);
        }
        if (m_DownsampleMaterial)
            DestroyImmediate(m_DownsampleMaterial);
    }

    protected void Start()
    {
        if (!SystemInfo.supportsImageEffects)
        {
            enabled = false;
            return;
        }

        if (downsampleShader == null)
        {
            Debug.Log("No downsample shader assigned! Disabling glow.");
            enabled = false;
        }
        else
        {
            if (!blurMaterial.shader.isSupported)
                enabled = false;
            if (!compoeMaterial.shader.isSupported)
                enabled = false;
            if (!downsampleMaterial.shader.isSupported)
                enabled = false;
        }
    }

    public void FourTapCone(RenderTexture source, RenderTexture dest, int iteration)
    {
        float off = 0.5f + iteration * blurSpread;
        Graphics.BlitMultiTap(source, dest, blurMaterial,
            new Vector2(off, off),
            new Vector2(-off, off),
            new Vector2(off, -off),
            new Vector2(-off, -off)
        );
    }


    //void OnRenderImage(RenderTexture source, RenderTexture destination)
    //{
    //    RenderTexture[] array = new RenderTexture[downsampleSteps * 2];
    //    RenderTextureFormat renderTextureFormat = RenderTextureFormat.Default;
    //    this.downsampleMaterial.SetFloat("_Strength", innerStrength / 4);
    //    RenderTexture temporary = RenderTexture.GetTemporary(source.width / 4, source.height / 4, 0, renderTextureFormat);
    //    RenderTexture temporary2 = RenderTexture.GetTemporary(temporary.width, temporary.height, 0, renderTextureFormat);
    //    Graphics.Blit(source, temporary, this.downsampleMaterial, 0);
    //    this.downsampleMaterial.SetFloat("_Strength", innerStrength / 4);
    //    RenderTexture renderTexture = temporary;
    //    for (int i = 0; i < downsampleSteps; i++)
    //    {
    //        int num = renderTexture.width / 4;
    //        int num2 = renderTexture.height / 4;
    //        if (num == 0 || num2 == 0)
    //        {
    //            break;
    //        }
    //        array[i * 2] = RenderTexture.GetTemporary(num, num2, 0, renderTextureFormat);
    //        array[i * 2 + 1] = RenderTexture.GetTemporary(num, num2, 0, renderTextureFormat);
    //        Graphics.Blit(renderTexture, array[i * 2], this.downsampleMaterial, 0);
    //        renderTexture = array[i * 2];
    //    }
    //    for (int j = downsampleSteps - 1; j >= 0; j--)
    //    {
    //        if (!(array[j * 2] == null))
    //        {
    //            BlurBuffer(array[j * 2], array[j * 2 + 1]);
    //            RenderTexture renderTexture2 = (j <= 0) ? temporary : array[(j - 1) * 2];
    //            if (downsampleBlendMode == DownsampleBlendMode.Max)
    //            {
    //                this.composeMaxMaterial.SetFloat("_Strength", outerStrength / ((float)j / 2f + 1f));
    //                Graphics.Blit(array[j * 2], renderTexture2, this.composeMaxMaterial);
    //            }
    //            else
    //            {
    //                this.compositeMaterial.SetFloat("_Strength", outerStrength / ((float)j / 2f + 1f));
    //                Graphics.Blit(array[j * 2], renderTexture2, this.compositeMaterial, (int)downsampleBlendMode);
    //            }
    //        }
    //    }
    //    BlurBuffer(temporary, temporary2);
    //    this.compositeMaterial.SetFloat("_Strength", boostStrength);
    //    //Graphics.Blit(temporary, destination, this.compositeMaterial, (int)blendMode);
    //    Graphics.Blit(temporary2, destination);
    //    RenderTexture.ReleaseTemporary(temporary);
    //    RenderTexture.ReleaseTemporary(temporary2);
    //    for (int k = 0; k < downsampleSteps; k++)
    //    {
    //        RenderTexture.ReleaseTemporary(array[k * 2]);
    //        RenderTexture.ReleaseTemporary(array[k * 2 + 1]);
    //    }
    //}

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        this.blurMaterial.SetFloat("_Strength", innerStrength / 4f);
        this.compoeMaterial.SetFloat("_Strength", boostStrength);
        RenderTexture temporary = RenderTexture.GetTemporary(source.width / 4, source.height / 4, 0, RenderTextureFormat.Default);
        RenderTexture temporary2 = RenderTexture.GetTemporary(source.width / 4, source.height / 4, 0, RenderTextureFormat.Default);
        this.DownSample4x(source, temporary);
        bool flag = true;
        for (int i = 0; i < iterations; i++)
        {
            if (flag)
            {
                this.FourTapCone(temporary, temporary2, i, blurSpread);
            }
            else
            {
                this.FourTapCone(temporary2, temporary, i, blurSpread);
            }
            flag = !flag;
        }
        if (flag)
        {
            Graphics.Blit(temporary, destination, this.compoeMaterial);
        }
        else
        {
            Graphics.Blit(temporary2, destination, this.compoeMaterial);
        }
        RenderTexture.ReleaseTemporary(temporary);
        RenderTexture.ReleaseTemporary(temporary2);
    }

    private bool glMode = true;

    protected Vector2[] blurOffsetsHorizontal = new Vector2[]
		{
			new Vector2(0f, 0f),
			new Vector2(-1.38461542f, 0f),
			new Vector2(1.38461542f, 0f),
			new Vector2(-3.23076916f, 0f),
			new Vector2(3.23076916f, 0f)
		};

    protected Vector2[] blurOffsetsVertical = new Vector2[]
		{
			new Vector2(0f, 0f),
			new Vector2(0f, -1.38461542f),
			new Vector2(0f, 1.38461542f),
			new Vector2(0f, -3.23076916f),
			new Vector2(0f, 3.23076916f)
		};

    protected void BlurBuffer(RenderTexture buffer, RenderTexture buffer2)
    {
        if (this.glMode)
        {
            Graphics.BlitMultiTap(buffer, buffer2, this.blurMaterial, this.blurOffsetsHorizontal);
            Graphics.BlitMultiTap(buffer2, buffer, this.blurMaterial, this.blurOffsetsVertical);
        }
        else
        {
            this.blurMaterial.SetFloat("_offset1", 1.38461542f);
            this.blurMaterial.SetFloat("_offset2", 3.23076916f);
            Graphics.Blit(buffer, buffer2, this.blurMaterial, 0);
            Graphics.Blit(buffer2, buffer, this.blurMaterial, 1);
        }
    }


    private void DownSample4x(RenderTexture source, RenderTexture dest)
    {
        float num = 1f;
        Graphics.BlitMultiTap(source, dest, this.blurMaterial, new Vector2[]
			{
				new Vector2(-num, -num),
				new Vector2(-num, num),
				new Vector2(num, num),
				new Vector2(num, -num)
			});
    }

    private void FourTapCone(RenderTexture source, RenderTexture dest, int iteration, float blurSpread)
    {
        float num = 0.5f + (float)iteration * blurSpread;
        Graphics.BlitMultiTap(source, dest, this.blurMaterial, new Vector2[]
			{
				new Vector2(-num, -num),
				new Vector2(-num, num),
				new Vector2(num, num),
				new Vector2(num, -num)
			});
    }
}

﻿using UnityEngine;
using System.Collections;

public class GlowEffectHigh : MonoBehaviour
{
    public Resolution resolution = Resolution.Full;
    public int iterations = 3;

    public float innerStrength = 1f;

    public BlendMode blendMode = BlendMode.Additive;

    public float boostStrength = 1f;

    protected Material blurMaterial = null;

    protected Material composeMaterial = null;

    protected Material downsampleMaterial = null;

    private bool glMode = true;
    void Awake()
    {
        Shader shader = Shader.Find("Hidden/Glow 11/Compose");
        Shader shader2 = Shader.Find("Hidden/Glow 11/Blur GL");
        if (!shader2.isSupported)
        {
            this.glMode = false;
            shader2 = Shader.Find("Hidden/Glow 11/Blur");
        }
        Shader shader3 = Shader.Find("Hidden/Glow 11/Downsample");
        this.composeMaterial = new Material(shader);
        this.composeMaterial.hideFlags = HideFlags.DontSave;
        this.blurMaterial = new Material(shader2);
        this.blurMaterial.hideFlags = HideFlags.DontSave;
        this.downsampleMaterial = new Material(shader3);
        this.downsampleMaterial.hideFlags = HideFlags.DontSave;
    }

    protected Vector2[] blurOffsetsHorizontal = new Vector2[]
		{
			new Vector2(0f, 0f),
			new Vector2(-1.38461542f, 0f),
			new Vector2(1.38461542f, 0f),
			new Vector2(-3.23076916f, 0f),
			new Vector2(3.23076916f, 0f)
		};

    protected Vector2[] blurOffsetsVertical = new Vector2[]
		{
			new Vector2(0f, 0f),
			new Vector2(0f, -1.38461542f),
			new Vector2(0f, 1.38461542f),
			new Vector2(0f, -3.23076916f),
			new Vector2(0f, 3.23076916f)
		};

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        bool highPrecision = false;
        int baseResolution = (int)resolution;
        this.downsampleMaterial.SetFloat("_Strength", innerStrength / (float)((baseResolution != 4) ? 1 : 4));
        this.composeMaterial.SetFloat("_Strength", boostStrength);
        RenderTexture temporary = RenderTexture.GetTemporary(source.width / baseResolution, source.height / baseResolution, 0, (!highPrecision) ? RenderTextureFormat.Default : RenderTextureFormat.ARGBHalf);
        RenderTexture temporary2 = RenderTexture.GetTemporary(temporary.width, temporary.height, 0, (!highPrecision) ? RenderTextureFormat.Default : RenderTextureFormat.ARGBHalf);
        Graphics.Blit(source, temporary, this.downsampleMaterial, (baseResolution != 4) ? 1 : 0);
        for (int i = 0; i < iterations; i++)
        {
            BlurBuffer(temporary, temporary2);
        }
        Graphics.Blit(temporary, destination, this.composeMaterial, (int)blendMode);
        RenderTexture.ReleaseTemporary(temporary);
        RenderTexture.ReleaseTemporary(temporary2);
    }

    protected void BlurBuffer(RenderTexture buffer, RenderTexture buffer2)
    {
        if (this.glMode)
        {
            Graphics.BlitMultiTap(buffer, buffer2, this.blurMaterial, this.blurOffsetsHorizontal);
            Graphics.BlitMultiTap(buffer2, buffer, this.blurMaterial, this.blurOffsetsVertical);
        }
        else
        {
            this.blurMaterial.SetFloat("_offset1", 1.38461542f);
            this.blurMaterial.SetFloat("_offset2", 3.23076916f);
            Graphics.Blit(buffer, buffer2, this.blurMaterial, 0);
            Graphics.Blit(buffer2, buffer, this.blurMaterial, 1);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
[RequireComponent (typeof(Camera))]
[AddComponentMenu ("Image Effects/Bloom (HDR, Lens Flares)")]
public class _BloomAndLensFlares : _PostEffectsBase
{
	public _BloomScreenBlendMode  screenBlendMode = _BloomScreenBlendMode.Add;
	
	public HDRBloomMode hdr = HDRBloomMode.Auto;
	private bool doHdr = false;
	public float sepBlurSpread = 1.5f;
	public float useSrcAlphaAsMask = 0.5f;
	
	public float bloomIntensity = 1.0f;
	public float bloomThreshhold = 0.5f;
	public int bloomBlurIterations = 2;	
						
	public Shader separableBlurShader;
	private Material separableBlurMaterial;

	public Shader screenBlendShader;
	private Material screenBlend;
	
	public Shader brightPassFilterShader;
	private Material brightPassFilterMaterial;
	
    void OnDisable()
    {
		if (screenBlend) 
		    DestroyImmediate(screenBlend);

		if (separableBlurMaterial) 
		    DestroyImmediate(separableBlurMaterial);

		if (brightPassFilterMaterial) 
		    DestroyImmediate(brightPassFilterMaterial);
    }
	bool CheckResources () {
		CheckSupport (false);
		
		screenBlend = CheckShaderAndCreateMaterial (screenBlendShader, screenBlend);
		separableBlurMaterial = CheckShaderAndCreateMaterial(separableBlurShader,separableBlurMaterial);
		brightPassFilterMaterial = CheckShaderAndCreateMaterial(brightPassFilterShader, brightPassFilterMaterial);
		
		if(!isSupported)
			ReportAutoDisable ();
		return isSupported;
	}
	
	void OnRenderImage (RenderTexture source, RenderTexture destination) {			
		if(CheckResources()==false) {
			Graphics.Blit (source, destination);
			return;
		}
				
		// screen blend is not supported when HDR is enabled (will cap values)
		
		doHdr = false;
		if(hdr == HDRBloomMode.Auto)
			doHdr = source.format == RenderTextureFormat.ARGBHalf && camera.hdr;
		else {
			doHdr = hdr == HDRBloomMode.On;
		}
		
		doHdr = doHdr && supportHDRTextures;
		
		_BloomScreenBlendMode  realBlendMode = screenBlendMode;
		if(doHdr)
			realBlendMode = _BloomScreenBlendMode.Add;
		
		var rtFormat = (doHdr) ? RenderTextureFormat.ARGBHalf : RenderTextureFormat.Default;
		var halfRezColor = RenderTexture.GetTemporary (source.width / 2, source.height / 2, 0, rtFormat);			
		var quarterRezColor = RenderTexture.GetTemporary (source.width / 4, source.height / 4, 0, rtFormat);	
		var secondQuarterRezColor = RenderTexture.GetTemporary (source.width / 4, source.height / 4, 0, rtFormat);	
		var thirdQuarterRezColor = RenderTexture.GetTemporary (source.width / 4, source.height / 4, 0, rtFormat);	
		
		var widthOverHeight = (1.0f * source.width) / (1.0f * source.height);
		var oneOverBaseSize = 1.0f / 512.0f;
		
		// downsample
		 
		Graphics.Blit (source, halfRezColor, screenBlend, 2); // <- 2 is stable downsample
		Graphics.Blit (halfRezColor, quarterRezColor, screenBlend, 2); // <- 2 is stable downsample	
		
		RenderTexture.ReleaseTemporary (halfRezColor);			

		// cut colors (threshholding)			
		
		BrightFilter (bloomThreshhold, useSrcAlphaAsMask, quarterRezColor, secondQuarterRezColor);		
				
		// blurring
		
		if (bloomBlurIterations < 1) bloomBlurIterations = 1;	
				        
		for (var iter = 0; iter < bloomBlurIterations; iter++ ) {
			var spreadForPass = (1.0f + (iter * 0.5f)) * sepBlurSpread;
			separableBlurMaterial.SetVector ("offsets", new Vector4 (0.0f, spreadForPass * oneOverBaseSize, 0.0f, 0.0f));	
			Graphics.Blit (iter == 0 ? secondQuarterRezColor : quarterRezColor, thirdQuarterRezColor, separableBlurMaterial); 
			separableBlurMaterial.SetVector ("offsets", new Vector4 ((spreadForPass / widthOverHeight) * oneOverBaseSize, 0.0f, 0.0f, 0.0f));	
			Graphics.Blit (thirdQuarterRezColor, quarterRezColor, separableBlurMaterial);		
		}


		// screen blend bloom results to color buffer
		
		screenBlend.SetFloat ("_Intensity", bloomIntensity);
		screenBlend.SetTexture ("_ColorBuffer", source);
		Graphics.Blit (quarterRezColor, destination, screenBlend, (int)realBlendMode);		
		
		RenderTexture.ReleaseTemporary (quarterRezColor);	
		RenderTexture.ReleaseTemporary (secondQuarterRezColor);	
		RenderTexture.ReleaseTemporary (thirdQuarterRezColor);		
	}


	private void  BrightFilter (float thresh, float useAlphaAsMask, RenderTexture from, RenderTexture to) {
		if(doHdr)
			brightPassFilterMaterial.SetVector ("threshhold", new Vector4 (thresh, 1.0f, 0.0f, 0.0f));
		else
			brightPassFilterMaterial.SetVector ("threshhold", new Vector4 (thresh, 1.0f / (1.0f-thresh), 0.0f, 0.0f));
		brightPassFilterMaterial.SetFloat ("useSrcAlphaAsMask", useAlphaAsMask);
		Graphics.Blit (from, to, brightPassFilterMaterial);			
	}

}


public enum _LensflareStyle34
{
    Ghosting = 0,
    Anamorphic = 1,
    Combined = 2,
}

public enum _TweakMode34
{
    Basic = 0,
    Complex = 1,
}

public enum _HDRBloomMode
{
    Auto = 0,
    On = 1,
    Off = 2,
}

public enum _BloomScreenBlendMode
{
    Screen = 0,
    Add = 1,
}
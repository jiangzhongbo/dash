﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class AchInfoPart
{
    public string id;
    public string name;
    public string description;
    public string gc_key;
    public int gc_value;
}

public class AchInfo : StaticStore<AchInfo>
{
    public List<AchInfoPart> Infos;
}
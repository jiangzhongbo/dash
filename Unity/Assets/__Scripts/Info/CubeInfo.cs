﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
[Serializable]
public class CubeInfoPart
{
    public string id;
    public string name;
    public string rank;
    public string buy_price;
    public string model;
    public int mode;
}

public class CubeInfo : StaticStore<CubeInfo>
{
    public List<CubeInfoPart> Infos;
}
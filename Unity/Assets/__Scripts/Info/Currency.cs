﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
[Serializable]
public class CurrencyPart
{
    public string id;
    public float price_dollars;
    public int count;

}

public class Currency : StaticStore<Currency>
{
    public List<CurrencyPart> Infos;
}
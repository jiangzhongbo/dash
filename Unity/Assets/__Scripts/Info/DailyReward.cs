﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class DailyRewardPart
{
    public string id;
    public string name;
    public string description;
    public int reward_count;
}

public class DailyReward : StaticStore<DailyReward>
{
    public List<DailyRewardPart> Infos;
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class LevelInfoPart
{
    public string id;
    public string name;
    public int locked;
    public string scene;
    public int coin_count;
}

public class LevelInfo : StaticStore<LevelInfo>
{
    public List<LevelInfoPart> Infos;
}
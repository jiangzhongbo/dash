﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class MapPart
{
    public string id;
    public string difficulty;
    public int length;
    public string data;
}

public class Map : StaticStore<Map>
{
    public List<MapPart> Infos;
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class LevelManager : MonoBehaviour
{
    public GameObject Gold;
    private static LevelManager instance;
    public int MAX
    {
        get
        {
            return levels.Count;
        }
    }
    public static LevelManager Instance
    {
        get
        {
            if (!instance)
            {
                instance = GameObject.Find("_Level").GetComponent<LevelManager>();
            }
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
    }


    public int Level = 1;
    public List<GameObject> levels = new List<GameObject>();

    public LevelInfoPart GetLevelInfo()
    {
        return LevelInfo.Load().Infos[Level - 1];
    }

    public GameObject GetCurrentMap()
    {
        return levels[Level - 1];
    }
    
    public int Next()
    {
        LevelManager.Instance.Level++;
        if (Level > MAX) Level = MAX;
        return Level;
    }

    public int Prev()
    {
        LevelManager.Instance.Level--;
        if (Level < 1) Level = 1;
        return Level;
    }

    [ContextMenu("Select")]
    public void Select()
    {
        SelectLevel(Level);
    }

    public void SelectLevel(int i)
    {
        HideAll();
        levels[i - 1].Show();
        GenGolds2(GetLevelInfo().coin_count);
    }

    public void HideAll()
    {
        levels.ForEach(e =>
        {
            e.Hide();
        });
    }

    public void GenGolds(int num)
    {
        List<Vector3> golds = GetCurrentMap().GetComponent<MapInfo>().Golds;
        List<int> cache = new List<int>();
        if (num > golds.Count / 2) num = golds.Count / 2;
        for (int i = 0; i < num; i++ )
        {
            float v = Random.value;
            v = Mathf.Pow(v, 1.0f / 4);
            int v1 = Mathf.CeilToInt(golds.Count * v);
            if (!cache.Contains(v1))
            {
                cache.Add(v1);
            }
            else
            {
                i--;
            }
        }

        cache.ForEach(e =>
        {
            var go = GameObject.Instantiate(Gold) as GameObject;
            go.transform.position = golds[e];
        });
    }

    public void GenGolds2(int num)
    {
        GameObject goldsGo = GameObject.Find("_Golds");
        if (goldsGo)
        {
            GameObject.DestroyImmediate(goldsGo);
        }
        goldsGo = new GameObject("_Golds");
        goldsGo.transform.position = Vector3.zero;
        if (num > 30) num = 30;
        List<Vector3> golds = GetCurrentMap().GetComponent<MapInfo>().Golds;

        int offset = golds.Count / 30;
        List<int> cache = new List<int>();
        for (int i = 0; i < num; i++)
        {
            int v = golds.Count - i * offset - Random.Range(0, (i+num) * offset);
            if (v > 0 && !cache.Contains(v))
            {
                cache.Add(v);
            }
            else
            {
                i--;
            }
        }
        cache.ForEach(e =>
        {
            var go = GameObject.Instantiate(Gold) as GameObject;
            go.transform.position = golds[e];
            go.transform.parent = goldsGo.transform;
        });
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using SimpleLambdaParser;

/// <summary>
/// 解析代码
/// </summary>
public class MapCodeParser
{
    /// <summary>
    /// 当前读取的索引位置
    /// </summary>
    public int Index { get; private set; }

    /// <summary>
    /// 当前读取的字符长度
    /// </summary>
    public int Length { get; private set; }

    /// <summary>
    /// 整个传入的代码内容
    /// </summary>
    public string Content { get; private set; }

    /// <summary>
    /// 分析".."或@".."所定义的字符串
    /// </summary>
    public string DefineString { get; private set; }

    public MapCodeParser(string content)
    {
        this.Content = content;
    }

    /// <summary>
    /// 往下读取字符串。(此方法是Read()方法的封装)
    /// </summary>
    /// <returns></returns>
    public string ReadString()
    {
        return ReadString(true);
    }

    /// <summary>
    /// 往下读取字符串。(此方法是Read()方法的封装)
    /// </summary>
    /// <param Name="isIgnoreWhiteSpace">是否忽略空格</param>
    /// <returns></returns>
    public string ReadString(bool isIgnoreWhiteSpace)
    {
        if (Read(true, isIgnoreWhiteSpace))
        {
            return this.Content.Substring(this.Index, this.Length);
        }
        return null;
    }

    /// <summary>
    /// 读取接下来的符号
    /// </summary>
    /// <param Name="symbol"></param>
    /// <returns></returns>
    public bool ReadSymbol(string symbol)
    {
        return ReadSymbol(symbol, true);
    }

    /// <summary>
    /// 读取接下来的符号
    /// </summary>
    /// <param Name="symbol"></param>
    /// <param Name="throwExceptionIfError"></param>
    /// <returns></returns>
    public bool ReadSymbol(string symbol, bool throwExceptionIfError)
    {
        // 跳过空格
        while (char.IsWhiteSpace(this.Content[this.Index + this.Length]))
        {
            this.Length++;
        }

        if (throwExceptionIfError)
        {
            ParseException.Assert(this.Content.Substring(this.Index + this.Length, symbol.Length), symbol, this.Index);
        }
        else if (this.Content.Substring(this.Index + this.Length, symbol.Length) != symbol)
        {
            return false;
        }
        this.Index += this.Length;
        this.Length = symbol.Length;
        return true;
    }

    /// <summary>
    /// 获取下一个字符串而不改变当前位置
    /// </summary>
    /// <returns></returns>
    public string PeekString()
    {
        int index = this.Index;
        int length = this.Length;

        string str = ReadString(true);

        this.Index = index;
        this.Length = length;

        return str;
    }

    #region private 方法

    /// <summary>
    /// 往下读取。通过Index和Length指示当前位置。
    /// </summary>
    /// <param Name="isBuildDefineString">遇到代码中的字符串常量时是否将字符串常量解析到DefineString成员。</param>
    /// <param Name="isIgnoreWhiteSpace">是否忽略空格</param>
    /// <returns></returns>
    private bool Read(bool isBuildDefineString, bool isIgnoreWhiteSpace)
    {
        this.Index += this.Length;
        this.Length = 1;

        // 超过了末尾，则返回0
        if (this.Index == this.Content.Length)
        {
            this.Index = 0;
            return false;
        }

        // 检查到空白字符，则跳过，继续
        if (isIgnoreWhiteSpace && char.IsWhiteSpace(this.Content, this.Index))
        {
            return Read(isBuildDefineString, isIgnoreWhiteSpace);
        }

        // 获取当前字母
        char c = this.Content[this.Index];

        // _
        if (c == '_')
        {
            return true;
        }

        // 字母开头
        if (char.IsLetter(c))
        {
            // 找下去
            for (this.Length = 1; (this.Length + this.Index) < this.Content.Length; this.Length++)
            {
                char cInner = this.Content[this.Index + this.Length];

                // 当char不是字母、数字时返回
                if ((!char.IsLetterOrDigit(cInner)))
                {
                    return true;
                }
            }

            return true;
        }

        // 数字开头
        if (char.IsDigit(c))
        {
            // 找下去
            for (this.Length = 1; (this.Length + this.Index) < this.Content.Length; this.Length++)
            {
                char cInner = this.Content[this.Index + this.Length];

                // 当char是点时，判断其后面的字符是否数字，若不是数字，则返回
                if (cInner == '.')
                {
                    char nextChar = this.Content[this.Index + this.Length + 1];
                    if (!char.IsDigit(nextChar))
                    {
                        return true;
                    }
                }

                // 当char不是数字时返回
                if (!char.IsDigit(cInner))
                {
                    return true;
                }
            }

            return true;
        }

        // 获取下一个char
        char nextInner;
        if (!TryGetNextChar(false, out nextInner))
        {
            // 到尾了，直接返回
            return true;
        }

        // 是否已知符号，某些做处理
        switch (c)
        {
            #region case .....
            case ',':
            case '(':
            case ')':
            case '#':
            case '!':
                break;
            #endregion
            default:
                throw new ParseUnknownException(c.ToString(), this.Index);
        }

        return true;
    }

    /// <summary>
    /// 在this.Content获取指定字符串，返回-1表示没找到
    /// </summary>
    private int GetStringIndex(string str, int startIndex)
    {
        for (int i = startIndex; i < this.Content.Length; i++)
        {
            if (string.Compare(this.Content, i, str, 0, str.Length, StringComparison.Ordinal)
                == 0)
            {
                return i;
            }
        }

        return -1;
    }

    /// <summary>
    /// 尝试获取下个字符，若已到结尾没有下个字符则返回false
    /// </summary>
    private bool TryGetNextChar(bool ignoreWhiteSpace, out char cNext)
    {
        cNext = '\0';
        for (int i = 0; i < int.MaxValue; i++)
        {
            if (this.Index + this.Length + i >= this.Content.Length)
            {
                return false;
            }
            cNext = this.Content[this.Index + this.Length];
            if ((!ignoreWhiteSpace) || (!char.IsWhiteSpace(cNext)))
            {
                break;
            }
        }
        return true;
    }

    #endregion


    #region CodeParserPosition的操作

    /// <summary>
    /// 保存当前位置
    /// </summary>
    /// <returns></returns>
    public CodeParserPosition SavePosition()
    {
        return new MyCodeParserPosition()
        {
            Index = this.Index,
            Length = this.Length
        };
    }

    /// <summary>
    /// 恢复指定的位置
    /// </summary>
    /// <param Name="position"></param>
    public void RevertPosition(CodeParserPosition position)
    {
        MyCodeParserPosition myPosition = (MyCodeParserPosition)position;
        this.Index = myPosition.Index;
        this.Length = myPosition.Length;
    }

    /// <summary>
    /// 恢复到初始状态
    /// </summary>
    public void RevertPosition()
    {
        RevertPosition(new MyCodeParserPosition());
    }

    private class MyCodeParserPosition : CodeParserPosition
    {
        public int Index { get; set; }
        public int Length { get; set; }
    }

    #endregion
}

/// <summary>
/// CodeParser保存的位置点，用来还原
/// </summary>
abstract public class CodeParserPosition
{
}

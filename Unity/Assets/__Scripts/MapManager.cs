﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

[System.Serializable]
public class Part
{
    public int Index;
    public float X;
    public float Y;
    public string Name;
    public bool IsPositive = true;
    public int StringPos;
    public int Length;
}
public class MapManager : MonoBehaviour
{
    public float TopY = 15;
    public GameObject parent;
    public string Prefix = "_,_,_,_,_,_,_,_,_,_,_,_";
    public string Suffix = "_,_,_,_,_,_,_,_,_,_,_,_,E01";
    public float TimeScale = 1;
    public float Speed = 1;
    public float ShowLength = 30;
    public int ShowErrorStringLen = 10;
    public List<Part> Parts;
    public List<Part> Golds;
    public List<Vector3> goldsPos;
    public string Maps = "_,_,A01#1!1.1|A01#1!2,_";
    private List<GameObject> parts = new List<GameObject>();
    private bool isPlaying = false;
    private GameObject current;
    public List<GameObject> tops = new List<GameObject>();
    private int stringPos = 0;
    private static MapManager instance;
    public static MapManager Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
        parent = GameObject.Find("_Maps");
    }

    [ContextMenu("Gen")]
    public void Gen()
    {
        parent = GameObject.Find("_Maps");
        if (parent) DestroyImmediate(parent);
        parent = new GameObject("_Maps");
        parent.AddComponent<MapInfo>();
        Parts.Clear();
        parts.Clear();
        tops.Clear();
        Golds.Clear();
        goldsPos.Clear();
        parseMap();
        genMap();
    }

    public void GenList()
    {
        GameObject _Level = GameObject.Find("_Level");
        if (_Level)
        {
            GameObject.DestroyImmediate(_Level);
            _Level = new GameObject("_Level");
            _Level.AddComponent<LevelManager>();
        }
        Map.Load().Infos.ForEach(e =>
        {
            Maps = e.data;
            Gen();
            GameObject _Maps = GameObject.Find("_Maps");
            _Maps.name = "Level_"+Map.Load().Infos.IndexOf(e);
            _Maps.transform.parent = _Level.transform;
            _Maps.transform.position = Vector3.zero;
            _Maps.transform.eulerAngles = Vector3.zero;
            _Maps.transform.localScale = Vector3.one;
            _Level.GetComponent<LevelManager>().levels.Add(_Maps);
            _Maps.Hide();
            ChangeMats(_Maps, int.Parse(LevelInfo.Load().Infos[Map.Load().Infos.IndexOf(e)].scene.Split('_')[1]));
            _Maps.GetComponent<MapInfo>().Golds.Clear();
            _Maps.GetComponent<MapInfo>().Golds.AddRange(goldsPos);
        });

    }

    Part handlePart(string str, int index)
    {
        char c = str[0];
        int i = 0;
        for (i = 1; i < str.Length; i++)
        {
            char cInner = str[i];

            if ((!char.IsLetterOrDigit(cInner)))
            {
                break;
            }
        }
        Part p = new Part();
        p.Name = str.Substring(0, i);
        p.Index = index;
        if (str.Contains("#"))
        {
            p.X = parseFloat(str.Substring(str.IndexOf("#")));
        }
        if (str.Contains("!"))
        {
            p.Y = parseFloat(str.Substring(str.IndexOf("!")));
            p.IsPositive = true;
        }
        if (str.Contains("?"))
        {
            p.Y = parseFloat(str.Substring(str.IndexOf("?")));
            p.IsPositive = false;
        }
        p.StringPos = stringPos;
        stringPos += str.Length + 1;
        p.Length = str.Length;
        return p;
    }

    float parseFloat(string str)
    {
        int i = 0;
        for (i = 1; i < str.Length; i++)
        {
            char cInner = str[i];

            if (cInner == '.')
            {
                char nextChar = str[i+1];
                if (!char.IsDigit(nextChar))
                {
                    break;
                }
            }

            if (!char.IsDigit(cInner) && cInner != '.')
            {
                break;
            }
        }
        string sub = str.Substring(1, i-1);
        return float.Parse(sub);
    }

    void parseMap()
    {
        if (Maps.Contains("__"))
        {
            _.Log("contains __");
            return;
        }
        else if (Maps.Contains(",,"))
        {
            _.Log("contains ,,");
            return;
        }

        stringPos = 0;
        string maps = Prefix + "," + Maps.Trim() + "," + Suffix;
        List<string> p1 = maps.Split(',').ToList();
        bool isGenGold = true;
        for (int i = 0; i < p1.Count; i++)
        {
            string c = p1[i];
            if (c == "_")
            {
                Part p = new Part();
                p.Name = "_";
                p.X = p.Y = 0;
                p.Index = i;
                p.StringPos = stringPos;
                p.Length = 1;
                Parts.Add(p);
                if (isGenGold) Golds.Add(p);
                stringPos += 2;
            }
            else if (c.Contains("|"))
            {
                List<string> p2 = c.Split('|').ToList();
                List<Part> temp = new List<Part>();
                for (int j = 0; j < p2.Count; j++)
                {
                    var v = handlePart(p2[j], i);
                    if (v.Name.Contains("H"))
                    {
                        isGenGold = !isGenGold;
                    }
                    temp.Add(v);
                }
                Parts.AddRange(temp);
                var t = temp.Where(e => e.IsPositive).OrderByDescending(e => e.Y).FirstOrDefault();
                if (isGenGold && t != null && (t.Name.Contains("A") || t.Name.Contains("B01") || t.Name.Contains("B03"))) Golds.Add(t);
            }
            else if (c == "R")
            {
                parent.GetComponent<MapInfo>().Reverse = true;
            }
            else
            {
                var p = handlePart(c, i);
                if (p.Name.Contains("H"))
                {
                    isGenGold = !isGenGold;
                }
                Parts.Add(p);
                if (isGenGold && p.IsPositive && (p.Name.Contains("A") || p.Name.Contains("B01") || p.Name.Contains("B03")))
                {
                    Golds.Add(p);
                }
            }
        }
    }

    void createGold(int i, Part p)
    {
        //var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        //go.transform.name = "_gold_" + p.Index + "_" + p.Name;
       // go.transform.parent = parent.transform;
        float y = 1;
        switch (p.Name)
        {
            case "A01":
                y = 2;
                break;
            case "A02":
                y = 3;
                break;
            case "A03":
                y = 4;
                break;
            case "A04":
                y = 5;
                break;
            case "A05":
                y = 6;
                break;
            case "A06":
                y = 7;
                break;
            case "A11":
                y = 1.5f;
                break;
            case "B01":
                y = 1.5f;
                break;
            case "B03":
                y = 2;
                break;
        }

        if (i > 1 && (Golds[i - 1].Name.Contains("B01")))
        {
            y += 1f;
        }
        if (i > 1 && (Golds[i - 1].Name.Contains("B03")))
        {
            y += 0.5f;
        }

        //go.transform.localPosition = Vector3.forward * p.Index + Vector3.up * p.Y + Vector3.forward * p.X + Vector3.up * y + Vector3.forward * 0.5f;
        //go.transform.localScale = Vector3.one * 0.2f;
        //go.transform.eulerAngles = Vector3.zero;
        //go.renderer.sharedMaterial.color = Color.red;

        goldsPos.Add(Vector3.forward * p.Index + Vector3.up * p.Y + Vector3.forward * p.X + Vector3.up * (y-0.5f) + Vector3.forward * 0.5f);
    }


    void genMap()
    {
        parent = GameObject.Find("_Maps");

        for (int i = 0; i < Golds.Count; i++)
        {
            Part p = Golds[i];
            createGold(i, p);
        }

        for (int i = 0; i < Parts.Count; i++)
        {
            Part p = Parts[i];
            if (p.Name == "_")
            {

            }
            else
            {
                string path = "Missions/Styles1/" + p.Name;
                GameObject prefab = Resources.Load(path) as GameObject;
                if (!prefab)
                {
                    int l = p.StringPos - Prefix.Length;
                    int showLen = ShowErrorStringLen + p.Length;
                    l -= ShowErrorStringLen + p.Length;
                    l += p.Length;
                    if (l < 1)
                    {
                        l = 1;
                        showLen = p.StringPos - Prefix.Length + 2;
                    }

                    _.Log(p.Name + " is null:", Maps.Trim().Substring(l - 1, showLen));
                    return;
                }

                var go = GameObject.Instantiate(prefab) as GameObject;
                go.transform.name = p.Index + "_" + p.Name + "_" + i;
                go.transform.parent = parent.transform;
                if (p.IsPositive)
                {
                    go.transform.localPosition = Vector3.forward * p.Index + Vector3.up * p.Y + Vector3.forward * p.X;
                    go.transform.localScale = Vector3.one;
                    go.transform.eulerAngles = Vector3.zero;
                }
                else
                {
                    go.transform.localPosition = Vector3.forward * p.Index + Vector3.up * (TopY - p.Y) + Vector3.forward * p.X;
                    go.transform.localScale = new Vector3(-1, 1, 1);
                    go.transform.eulerAngles = new Vector3(0, 0, 180);
                }
                parts.Add(go);

                if (p.Name == "G01" || p.Name == "G02")
                {
                    tops.Add(go);
                }

            }
        }
        
        for (int i = 0; i < tops.Count; i++,i++)
        {
            string path = "Top/upfloor";
            GameObject prefab = Resources.Load(path) as GameObject;
            var go = GameObject.Instantiate(prefab) as GameObject;
            go.transform.parent = parent.transform;
            go.transform.localScale = new Vector3(1, 1, (tops[i].transform.localPosition - tops[i+1].transform.localPosition).magnitude);
            go.transform.localPosition = tops[i].transform.localPosition;
            parts.Add(go);
        }

        string path1 = "Floor/floor";
        GameObject prefab1 = Resources.Load(path1) as GameObject;
        GameObject floorP = new GameObject();
        floorP.transform.parent = parent.transform;
        floorP.name = "floor";
        for (int i = 0; i < 1; i++)
        {
            var floor = GameObject.Instantiate(prefab1) as GameObject;
            floor.transform.name = "floor_"+i;
            floor.transform.parent = floorP.transform;
            floor.transform.localPosition = Vector3.forward * (i - 20);
            floor.transform.localScale = new Vector3(1, 1, 1000);
        }
        
    }

    [ContextMenu("UpdateTimeScale")]
    public void UpdateTimeScale()
    {
        GameContext.Instance.TimeScale = TimeScale;
        Time.timeScale = TimeScale;
    }

    void _Update()
    {
        UpdateTimeScale();
    }

    public void ChangeMats(GameObject map,  int i)
    {
        Material A01 = Resources.Load("Style/ST" + i + "/A01_ST" + i) as Material;
        Material B01 = Resources.Load("Style/ST" + i + "/B01_ST" + i) as Material;
        Material B02 = Resources.Load("Style/ST" + i + "/B02_ST" + i) as Material;
        Material C01 = Resources.Load("Style/ST" + i + "/C01_ST" + i) as Material;
        Material C07 = Resources.Load("Style/ST" + i + "/C07_ST" + i) as Material;
        Material C30 = Resources.Load("Style/ST" + i + "/C30_ST" + i) as Material;
        Material F01 = Resources.Load("Style/ST" + i + "/F01_ST" + i) as Material;
        Material floor = Resources.Load("Style/ST" + i + "/floor_ST" + i) as Material;
        map.GetComponentsInChildren<MeshRenderer>(true).ToList().ForEach(e =>
        {
            if (e.sharedMaterial.name.Contains("A01"))
            {
                e.sharedMaterial = A01;
            }
            else if (e.sharedMaterial.name.Contains("B01"))
            {
                e.sharedMaterial = B01;
            }
            else if (e.sharedMaterial.name.Contains("B02"))
            {
                e.sharedMaterial = B02;
            }
            else if (e.sharedMaterial.name.Contains("C01"))
            {
                e.sharedMaterial = C01;
            }
            else if (e.sharedMaterial.name.Contains("C07"))
            {
                e.sharedMaterial = C07;
            }
            else if (e.sharedMaterial.name.Contains("C30"))
            {
                e.sharedMaterial = C30;
            }
            else if (e.sharedMaterial.name.Contains("F01"))
            {
                e.sharedMaterial = F01;
            }
            else if (e.sharedMaterial.name.Contains("floor"))
            {
                e.sharedMaterial = floor;
            }
        });

    }
}

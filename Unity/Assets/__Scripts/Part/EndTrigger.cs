﻿using UnityEngine;
using System.Collections;

public class EndTrigger : MonoBehaviour
{
    public float r = 1.5f;
    float t = 0;
    float t2 = 0;
    bool isAnim = false;

    private static EndTrigger instance;

    public static EndTrigger Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
    }

    void Update()
    {
        if (!CubeManager.Instance) return;
        if (!SoundManager.Instance) return;
        if (CubeManager.Instance.transform.position.z + 5 >= transform.position.z && CubeManager.Instance.transform.position.z < transform.position.z && !isAnim)
        {
            CubeManager.Instance.StopPattern();
            CubeManager.Instance.GetComponent<Follow>().Stop();
            isAnim = true;
            t = t2 = 0;
            SoundManager.Instance.PlayOneShot(SoundManager.Instance.fx_end);
        }
        if (isAnim)
        {
            CubeManager.Instance.transform.Rotate(Vector3.right * -2);
            CubeManager.Instance.transform.position = 
                Vector3.Lerp(
                CubeManager.Instance.transform.position,
                gameObject.FindChild("mianpian").transform.position + Vector3.forward * 1f,
                t2);
            t += Time.deltaTime / 2;
            t2 = Mathf.Pow(t, 2);
            if (t2 > 1)
            {
                CubeManager.Instance.Stop();
                SoundManager.Instance.StopAll();
                CubeManager.Instance.DoSuccess();
                isAnim = false;
            }
        }
        
    }
}

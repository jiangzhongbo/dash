﻿using UnityEngine;
using System.Collections;
using System;
public class GoldTrigger : MonoBehaviour
{
    public bool IsAnim = false;
    void Update()
    {
        if (!CubeManager.Instance) return;
        if (!IsAnim && Mathf.Abs(CubeManager.Instance.transform.position.z - transform.position.z) < 0.5f && Mathf.Abs(CubeManager.Instance.transform.position.y - transform.position.y) < 0.5f)
        {
            IsAnim = true;
            GameContext.Instance.AddGolds(1);
            Game.Current.PlayAnimGold(WorldToUI(transform.position));
            GameObject.DestroyImmediate(gameObject);
            SoundManager.Instance.PlayOneShot(SoundManager.Instance.fx_coin);
        }
    }

    void LateUpdate()
    {
        
    }

    public Vector3 WorldToUI(Vector3 v)
    {
        Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position);
        screenPos = new Vector2(screenPos.x / Screen.width, screenPos.y / Screen.height);
        return screenPos;
    }
}

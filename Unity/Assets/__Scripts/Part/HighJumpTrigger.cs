﻿using UnityEngine;
using System.Collections;

public class HighJumpTrigger : MonoBehaviour
{

    public float r = 1.5f;
    public float JumpHeight = 8f;

    void Update()
    {
        if (CubeManager.Instance.GP == GamePattern.SimulateGravity)
        {
            if (CubeManager.Instance.p1.transform.position.z - transform.position.z > 0.3f && CubeManager.Instance.p1.transform.position.z - transform.position.z < 1f && Mathf.Abs(CubeManager.Instance.transform.position.y - transform.position.y) <= r)
            {
                CubeManager.Instance.transform.GetComponent<SimulateGravity>().HighJump(JumpHeight);
            }
        }

    }
}

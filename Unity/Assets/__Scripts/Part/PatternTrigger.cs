﻿using UnityEngine;
using System.Collections;

public class PatternTrigger : MonoBehaviour
{
    public GamePattern GP = GamePattern.UFO;

    void Update()
    {
        if (!CubeManager.Instance) return;
        if (Mathf.Abs(CubeManager.Instance.transform.position.z - transform.position.z) < 1)
        {
            if ((CubeManager.Instance.transform.position.z - transform.position.z) > 0)
            {
                CubeManager.Instance.ChangeGP(GP);
            }
        }
    }

}

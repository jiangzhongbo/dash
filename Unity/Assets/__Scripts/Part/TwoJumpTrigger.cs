﻿using UnityEngine;
using System.Collections;

public class TwoJumpTrigger : MonoBehaviour
{
    public float r = 1.5f;
    private float time = 1;
    void Update()
    {
        if (CubeManager.Instance.GP == GamePattern.SimulateGravity)
        {
            if ((CubeManager.Instance.transform.position - transform.position).magnitude < r && time >= 1)
            {
                CubeManager.Instance.transform.GetComponent<SimulateGravity>().SetTwoJump();
                time = 0;
            }
            else
            {
                time += Time.deltaTime;
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class PartInfo : MonoBehaviour
{
    public bool IsObstacle;
    public bool IsFloor;
    public bool IsTopFloor;
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public enum SimulateGravityMode
{
    Rotate,
    Jump,
    Normal,
}

public class SimulateGravity : MonoBehaviour, IGamePattern
{
    public GameObject p1;
    public GameObject p2;
    public GameObject core;
    public SimulateGravityMode Mode = SimulateGravityMode.Rotate;
    public void SetP1(GameObject p1)
    {
        this.p1 = p1;
    }

    public void SetP2(GameObject p2)
    {
        this.p2 = p2;
    }
    public void SetCore(GameObject core)
    {
        this.core = core;
    }

    public bool jumping = false;
    public bool twoJumping = false;
    public bool highJumping = false;
    private float verticalSpeed = 0.0f;
    private float lastJumpButtonTime = -10.0f;
    private float jumpTimeout = 0.05f;
    public float JumpHeight = 2f;
    public float Gravity = 10;
    public float GroundP = 2;
    public bool grounded = false;
    public bool prevGrounded = false;
    public float YOffset = 0.5f;

    public bool isPlaying = false;

    private float av = 0;
    public bool IsHighJump = false;
    private float prevJumpHeight;

    public bool IsTwoJump = false;
    public void SetTwoJump()
    {
        //if (twoJumping) return;
        IsTwoJump = true;
    }
    public void Play()
    {
        isPlaying = true;
        transform.eulerAngles = Vector3.zero;
        core.transform.eulerAngles = Vector3.zero;
        lastJumpButtonTime = -10;
        grounded = false;
        IsTwoJump = false;
        verticalSpeed = 0;

        cubes.ForEach(e => GameObject.DestroyImmediate(e));
        cubes.Clear();
    }

    void Start()
    {
        prevJumpHeight = JumpHeight;
    }

    public void Stop()
    {
        isPlaying = false;
        IsTwoJump = false;
        twoJumping = false;
        highJumping = false;
        jumping = false;
        lastJumpButtonTime = -10;
        grounded = false;
        GroundedPart = null;
        GroundedPoint = Vector3.zero;
    }

    public void SetEnable(bool enable)
    {
        this.enabled = enable;
    }

    float rotateA = 0;
    float endRotateA = 0;
    Vector3 temp = Vector3.zero;
    Vector3 offset = Vector3.up * 2f;
    Quaternion tempQ = Quaternion.identity;
    float tempT = 0;
    public void MainUpdate()
    {
        if (!isPlaying) return;
        if (Time.timeScale == 0) return;
        prevGrounded = grounded;
        grounded = isGrounded();
#if UNITY_EDITOR
        //if (prevGrounded && !grounded)
        //{
        //    FindNextGroundedPoint();
        //}
#endif
        ApplyGravity();

        ApplyJumping();

        float distance = 0;
        RaycastHit hitinfo1;
        RaycastHit hitinfo2;
        bool hit1 = Physics.Raycast(p1.transform.position + offset, Vector3.down, out hitinfo1, 100f, 1 << LayerMask.NameToLayer("Block"));
        bool hit2 = Physics.Raycast(p2.transform.position + offset, Vector3.down, out hitinfo2, 100f, 1 << LayerMask.NameToLayer("Block"));
        bool isHit = hit1 || hit2;
        RaycastHit hitinfo = default(RaycastHit);
        if (hit1 && hit2)
        {
            hitinfo = hitinfo1.distance > hitinfo2.distance ? hitinfo2 : hitinfo1;
            distance = hitinfo.distance;
        }
        else if (hit1)
        {
            hitinfo = hitinfo1;
            distance = hitinfo.distance;
        }
        else if (hit2)
        {
            hitinfo = hitinfo2;
            distance = hitinfo.distance;
        }
        else
        {
            distance = 0;
        }
        if (isHit)
        {
            if (verticalSpeed < 0 && (distance - 2) < -verticalSpeed * Time.deltaTime)
            {
                RaycastHit rh;
                if (Physics.Raycast(p2.transform.position + Vector3.up * 1.5f, Vector3.down, out rh, 2f, 1 << LayerMask.NameToLayer("Block")))
                {
                    move(Vector3.down * (rh.distance - 1.5f));
                }
                else if (Physics.Raycast(p1.transform.position + Vector3.up * 1.5f, Vector3.down, out rh, 2f, 1 << LayerMask.NameToLayer("Block")))
                {
                    move(Vector3.down * (rh.distance - 1.5f));
                }
                else
                {
                    move(Vector3.down * (distance - 2));
                }
            }
            else
            {
                move(new Vector3(0, verticalSpeed * Time.deltaTime, 0));
            }
        }
        else 
        {

        }

        if (Mode == SimulateGravityMode.Normal)
        {

        }
        else if (Mode == SimulateGravityMode.Rotate)
        {
            if (!grounded)
            {
                core.transform.Rotate(new Vector3(10, 0, 0));
            }
            if (!grounded)
            {
                Vector3 realv = getRealEulerangle(core.transform.eulerAngles);
                if (90 > realv.x && realv.x >= 0)
                {
                    rotateA = 90 - realv.x;
                }
                if (180 > realv.x && realv.x >= 90)
                {
                    rotateA = 180 - realv.x;
                }
                if (270 > realv.x && realv.x >= 180)
                {
                    rotateA = 270 - realv.x;
                }
                if (360 >= realv.x && realv.x >= 270)
                {
                    rotateA = 360 - realv.x;
                }

                if (rotateA < 60)
                {
                    endRotateA = realv.x + rotateA;
                }
                else
                {
                    endRotateA = realv.x - (90 - rotateA);
                }
                temp = new Vector3(endRotateA, 0, 0);
            }
            if (grounded && isHit)
            {
                //Vector3 up = Vector3.Lerp(core.transform.up, Quaternion.Euler(temp) * hitinfo.normal, Time.deltaTime * 30);
                //core.transform.up = up;
                Quaternion to = Quaternion.FromToRotation(Vector3.up, hitinfo.normal) * Quaternion.Euler(temp);
                core.transform.rotation = Quaternion.Slerp(tempQ, to, tempT);
                tempT += Time.deltaTime * 30;
                if (tempT > 1)
                {
                    core.transform.rotation = to;
                }
            }
            else
            {
                tempT = 0;
                tempQ = core.transform.rotation;
            }
        }
        else if (Mode == SimulateGravityMode.Jump)
        {

            if (grounded)
            {
                Vector3 e = core.transform.eulerAngles;
                e.x = Mathf.Atan(verticalSpeed / CubeManager.Instance.ZSpeed) * Mathf.Rad2Deg;
                core.transform.eulerAngles = e;
            }
            else
            {
                Vector3 e = core.transform.eulerAngles;
                e.x = -Mathf.Atan(verticalSpeed / CubeManager.Instance.ZSpeed) * Mathf.Rad2Deg;
                core.transform.eulerAngles = e;
            }
        }

        

        



    }

    Vector3 lerpVector3Angle(Vector3 from, Vector3 to, float t)
    {
        return new Vector3(Mathf.LerpAngle(from.x, to.x, t), Mathf.LerpAngle(from.y, to.y, t), Mathf.LerpAngle(from.z, to.z, t));
    }

    Vector3 getRealEulerangle(Vector3 eulurangle)
    {
        if (eulurangle.x <= 90 && eulurangle.y == 0 && eulurangle.z == 0)
        {
            return eulurangle;
        }
        else if (eulurangle.x <= 90 && eulurangle.y == 180 && eulurangle.z == 180)
        {
            return new Vector3(180 - eulurangle.x, 0, 0);
        }
        else if (eulurangle.x >= 270 && eulurangle.y == 180 && eulurangle.z == 180)
        {
            return new Vector3(180 + 360 - eulurangle.x, 0, 0);
        }
        else if (eulurangle.x > 270 && eulurangle.y == 0 && eulurangle.z == 0)
        {
            return eulurangle;
        }
        return eulurangle;
    }

    void move(Vector3 v)
    {
        transform.position += v;
        if (GroundedPart && transform.position.y - 0.5f - GroundedPoint.y < 0.3f)
        {
            transform.position = new Vector3(transform.position.x, GroundedPoint.y + 0.5f, transform.position.z);
        }
    }

    public void Jump()
    {
        if (!isPlaying) return;
        //if (twoJumping) return;
        lastJumpButtonTime = Time.time;
    }

    public void HighJump(float h)
    {
        if (highJumping) return;
        if (!isPlaying) return;
        IsHighJump = true;
        JumpHeight = h;
        lastJumpButtonTime = Time.time;
    }
    public GameObject GroundedPart;
    public Vector3 GroundedPoint = Vector3.zero;
    public bool IsGrounded()
    {
        return grounded;
    }
    public GameObject GetGroundedPart()
    {
        return GroundedPart;
    }

    private bool isGrounded()
    {
        Vector3 offset = Vector3.up * 2f;
        float distance = 0;
        RaycastHit hitinfo1;
        RaycastHit hitinfo2;
        bool hit1 = Physics.Raycast(p1.transform.position + offset, Vector3.down, out hitinfo1, 2.05f, 1 << LayerMask.NameToLayer("Block"));
        bool hit2 = Physics.Raycast(p2.transform.position + offset, Vector3.down, out hitinfo2, 2.05f, 1 << LayerMask.NameToLayer("Block"));
        bool isHit = hit1 || hit2;
        RaycastHit hitinfo = default(RaycastHit);
        if (hit1 && hit2)
        {
            hitinfo = hitinfo1.distance > hitinfo2.distance ? hitinfo2 : hitinfo1;
            distance = hitinfo.distance;
        }
        else if (hit1)
        {
            hitinfo = hitinfo1;
            distance = hitinfo.distance;
        }
        else if (hit2)
        {
            hitinfo = hitinfo2;
            distance = hitinfo.distance;
        }
        else
        {
            distance = 0;
        }
        if (jumping)
        {
            if (verticalSpeed > 0)
            {
                GroundedPart = null;
                return false;
            }
                
        }
        if (!isHit)
        {
            RaycastHit rh;
            if (Physics.Raycast(p2.transform.position + Vector3.up * 1.5f + Vector3.forward * -0.2f, Vector3.down, out rh, 3f, 1 << LayerMask.NameToLayer("Block")))
            {

                if (Mathf.Abs(rh.distance - 1.5f) <= 0.2f)
                {
                    GroundedPart = rh.transform.gameObject;
                    GroundedPoint = rh.point;
                    return true;
                }
            }
            GroundedPart = null;
            return false;
        }
        else if (isHit)
        {
            if (hitinfo.normal.y > 0)
            {
                transform.position = new Vector3(transform.position.x, hitinfo.point.y + YOffset, transform.position.z);
            }
            GroundedPart = hitinfo.transform.gameObject;
            GroundedPoint = hitinfo.point;
            return true;
        }
        GroundedPart = null;
        return true;
    }

    void ApplyJumping()
    {
        if (grounded || IsTwoJump || IsHighJump)
        {

            if (Time.time < lastJumpButtonTime + jumpTimeout)
            {
                verticalSpeed = CalculateJumpVerticalSpeed(JumpHeight);
                JumpHeight = prevJumpHeight;
                av = 360 / (verticalSpeed / Gravity) / 4 / (1 / Time.deltaTime);
                DidJump();
            }
        }
    }

    void DidJump()
    {
        jumping = true;
        lastJumpButtonTime = -10;
        if (IsTwoJump) twoJumping = true;
        if (IsHighJump) highJumping = true;
        IsTwoJump = false;
        IsHighJump = false;
    }

    void ApplyGravity()
    {
        if (grounded)
        {
            verticalSpeed = 0.0f;
            jumping = false;
            twoJumping = false;
            highJumping = false;
        }
        else if (!grounded)
        {
            verticalSpeed -= Gravity * Time.deltaTime;
        }
    }

    float CalculateJumpVerticalSpeed(float targetJumpHeight)
    {
        return Mathf.Sqrt(2 * targetJumpHeight * Gravity);
    }

    Vector3 GetNextGroundedPoint(Vector3 p)
    {
        float vs = verticalSpeed;
        int i = 0;
        RaycastHit hit;
        while (true)
        {
            float dt = 0.0001f;
            while (true)
            {
                float y = vs * dt;
                if (y > 0.01f)
                {
                    dt /= 2;
                }
                else
                {
                    break;
                }
            }
            
            p += new Vector3(0, vs * dt, 0);
            p += Vector3.forward * dt * CubeManager.Instance.ZSpeed;
            bool isHit = Physics.Raycast(p + Vector3.up, Vector3.down, out hit, 1.5f, 1 << LayerMask.NameToLayer("Block"));
            if (vs < 0 && isHit && Mathf.Abs(hit.distance - 1) < 0.2f)
            {
                p = hit.point;
                break;
            }
            if (p.y <= 0)
            {
                break;
            }
            if (i > 10000)
            {
                break;
            }
            vs -= Gravity * dt;
            i++;
        }
        return p;
    }

    int index = 0;
    private List<GameObject> cubes = new List<GameObject>();
    void FindNextGroundedPoint()
    {
        index++;
        Vector3 po1 = GetNextGroundedPoint(p1.transform.position);
        Vector3 po2 = GetNextGroundedPoint(p2.transform.position);

        Vector3 po = Vector3.zero;
        if (po1.y > po2.y)
        {
            po = po1;
        }
        else if (po1.y < po2.y)
        {
            po = po2;
        }
        else
        {
            po = (po1 + po2) / 2;
        }

        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.localScale = new Vector3(0.1f, 2, 0.1f);
        cube.transform.position = po;
        cube.transform.name = "___1cube"+index;
        cube.renderer.material.color = Color.black;
        cubes.Add(cube);

        float z = Mathf.Abs(gameObject.transform.position.z - po.z);
        float t = z / CubeManager.Instance.ZSpeed;
        av = 160 / (t / Time.deltaTime);

        GameObject cube2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube2.transform.localScale = new Vector3(0.1f, 2, 0.1f);
        cube2.transform.position = transform.position;
        cube2.transform.name = "___2cube" + index;
        cube2.renderer.material.color = Color.blue;
        cubes.Add(cube2);

    }

}

﻿using UnityEngine;
using System.Collections;

public class UFO :  MonoBehaviour, IGamePattern
{
    public GameObject p1;
    public GameObject p2;
    public GameObject core;

    public void SetP1(GameObject p1)
    {
        this.p1 = p1;
    }
    public void SetP2(GameObject p2)
    {
        this.p2 = p2;
    }
    public void SetCore(GameObject core)
    {
        this.core = core;
    }

    public void SetEnable(bool enable)
    {
        this.enabled = enable;
    }

    private Vector3 v = Vector3.zero;

    private bool jumping = false;
    private float verticalSpeed = 0.0f;
    private float lastJumpButtonTime = -10.0f;
    public float jumpTimeout = 0.05f;
    public float JumpHeight = 2f;
    public float Gravity = 10;
    public float MaxVerticalSpeed = 5;
    bool grounded = false;
    bool touchedTop = false;
    public float YOffset = 0.5f;

    public bool isPlaying = false;

    private float av = 0;

    public bool IsTwoJump = true;

    public void Play()
    {
        isPlaying = true;
        IsTwoJump = true;
        transform.eulerAngles = Vector3.zero;
        core.transform.eulerAngles = Vector3.zero;
    }

    public void Stop()
    {
        isPlaying = false;
        IsTwoJump = false;
    }

    float rotateA = 0;
    float endRotateA = 0;
    Vector3 temp = Vector3.zero;
    public void MainUpdate()
    {
        if (!isPlaying) return;
        Vector3 offset = Vector3.up * 2f;
        ApplyGravity();

        ApplyJumping();

        float distance = 0;
        RaycastHit hitinfo1;
        RaycastHit hitinfo2;
        bool hit1 = Physics.Raycast(p1.transform.position + offset, Vector3.down, out hitinfo1, 100f, 1 << LayerMask.NameToLayer("Block"));
        bool hit2 = Physics.Raycast(p2.transform.position + offset, Vector3.down, out hitinfo2, 100f, 1 << LayerMask.NameToLayer("Block"));
        bool isHit = hit1 || hit2;
        RaycastHit hitinfo = default(RaycastHit);
        if (hit1 && hit2)
        {
            hitinfo = hitinfo1.distance > hitinfo2.distance ? hitinfo2 : hitinfo1;
            distance = hitinfo.distance;
        }
        else if (hit1)
        {
            hitinfo = hitinfo1;
            distance = hitinfo.distance;
        }
        else if (hit2)
        {
            hitinfo = hitinfo2;
            distance = hitinfo.distance;
        }
        else
        {
            distance = 0;
        }

        if (isHit)
        {
            if (verticalSpeed < 0 && distance < -verticalSpeed * Time.deltaTime)
            {
                move(Vector3.down * (distance - 1));
            }
            else
            {
                move(new Vector3(0, verticalSpeed * Time.deltaTime, 0));
            }
        }
        else
        {
            move(new Vector3(0, verticalSpeed * Time.deltaTime, 0));
        }
        grounded = isGrounded();
        touchedTop = isTouchedTop();
    }


    float getAngle(Vector3 nor)
    {
        return Vector3.Angle(nor, Vector3.up);
    }

    void move(Vector3 v)
    {
        transform.position += v;
    }

    public void Jump()
    {
        lastJumpButtonTime = Time.time;
    }

    public GameObject GroundedPart;
    public bool IsGrounded()
    {
        return grounded;
    }

    public GameObject GetGroundedPart()
    {
        return GroundedPart;
    }

    public bool isTouchedTop()
    {
        Vector3 offset = Vector3.zero;
        float distance = 0;
        RaycastHit hitinfo1;
        RaycastHit hitinfo2;
        bool hit1 = Physics.Raycast(p1.transform.position + offset, Vector3.up, out hitinfo1, 100f, 1 << LayerMask.NameToLayer("Block"));
        bool hit2 = Physics.Raycast(p2.transform.position + offset, Vector3.up, out hitinfo2, 100f, 1 << LayerMask.NameToLayer("Block"));
        bool isHit = hit1 || hit2;
        RaycastHit hitinfo = default(RaycastHit);
        if (hit1 && hit2)
        {
            hitinfo = hitinfo1.distance > hitinfo2.distance ? hitinfo2 : hitinfo1;
            distance = hitinfo.distance;
        }
        else if (hit1)
        {
            hitinfo = hitinfo1;
            distance = hitinfo.distance;
        }
        else if (hit2)
        {
            hitinfo = hitinfo2;
            distance = hitinfo.distance;
        }
        else
        {
            distance = 0;
        }
        if (jumping)
        {
            if (verticalSpeed < 0)
            {
                return false;
            }

        }
        if (isHit)
        {
            if (distance - 1 > 0 && Mathf.Abs(distance - 1) > 0.01f * Gravity / 10)
            {
                return false;
            }
            else
            {
                if (hitinfo.normal.y < 0)
                {
                    transform.position = new Vector3(transform.position.x, hitinfo.point.y - YOffset, transform.position.z);
                }
                return true;
            }
        }
        return true;
    }


    private bool isGrounded()
    {
        Vector3 offset = Vector3.up * 2f;
        float distance = 0;
        RaycastHit hitinfo1;
        RaycastHit hitinfo2;
        bool hit1 = Physics.Raycast(p1.transform.position + offset, Vector3.down, out hitinfo1, 100f, 1 << LayerMask.NameToLayer("Block"));
        bool hit2 = Physics.Raycast(p2.transform.position + offset, Vector3.down, out hitinfo2, 100f, 1 << LayerMask.NameToLayer("Block"));
        bool isHit = hit1 || hit2;
        RaycastHit hitinfo = default(RaycastHit);
        if (hit1 && hit2)
        {
            hitinfo = hitinfo1.distance > hitinfo2.distance ? hitinfo2 : hitinfo1;
            distance = hitinfo.distance;
        }
        else if (hit1)
        {
            hitinfo = hitinfo1;
            distance = hitinfo.distance;
        }
        else if (hit2)
        {
            hitinfo = hitinfo2;
            distance = hitinfo.distance;
        }
        else
        {
            distance = 0;
        }
        if (jumping)
        {
            if (verticalSpeed > 0)
            {
                GroundedPart = null;
                return false;
            }

        }
        if (isHit)
        {
            if (distance - 2 > 0 && Mathf.Abs(distance - 2) > 0.01f * Gravity / 10)
            {
                GroundedPart = null;
                return false;
            }
            else
            {
                if (hitinfo.normal.y > 0)
                {

                    transform.position = new Vector3(transform.position.x, hitinfo.point.y + YOffset, transform.position.z);
                }
                GroundedPart = hitinfo.transform.gameObject;
                return true;
            }
        }
        GroundedPart = null;
        return true;
    }

    void ApplyJumping()
    {
        if (IsTwoJump)
        {
            if (Time.time < lastJumpButtonTime + jumpTimeout)
            {
                verticalSpeed = CalculateJumpVerticalSpeed(JumpHeight);
                av = 360 / (verticalSpeed / Gravity) / 4 / (1 / Time.smoothDeltaTime);
                DidJump();
            }
        }
    }

    void DidJump()
    {
        jumping = true;
        lastJumpButtonTime = -10;
    }

    void ApplyGravity()
    {
        if (grounded)
        {
            verticalSpeed = 0.0f;
            jumping = false;
        }
        else if (!grounded)
        {
            verticalSpeed -= Gravity * Time.deltaTime;
            if (verticalSpeed > MaxVerticalSpeed)
            {
                verticalSpeed = MaxVerticalSpeed;
            }
            else if (verticalSpeed < -MaxVerticalSpeed)
            {
                verticalSpeed = -MaxVerticalSpeed;
            }
        }
    }

    float CalculateJumpVerticalSpeed(float targetJumpHeight)
    {
        return Mathf.Sqrt(2 * targetJumpHeight * Gravity);
    }
}

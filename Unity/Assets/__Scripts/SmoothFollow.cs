﻿using UnityEngine;

public class SmoothFollow : MonoBehaviour
{

    // The target we are following
    [SerializeField]
    private Transform target;
    // The distance in the x-z plane to the target

    [SerializeField]
    private Transform lookAtTarget;
    [SerializeField]
    private Vector3 lookAtOffset;
    //初始向量差 用来保持距离不变
    private Vector3 originVector;
    //记录上一frame的旋转叫
    private Vector3 lastFrameTargetRoation;
    //鼠标旋转视野的速度
    [SerializeField]
    private float mouseTurnedSpeed = 0.3f;
    //鼠标右键控制镜头旋转的代码
    private bool rightButtonDonwed;

    // Use this for initialization
    void Start()
    {
        //获取当前的distance和height
        originVector = new Vector3(target.position.x - transform.position.x, target.position.y - transform.position.y, target.position.z - transform.position.z);
        rightButtonDonwed = false;
        lastFrameTargetRoation = target.rotation.eulerAngles;
    }

    // Pull is called once per frame
    void Update()
    {

        //变动的距离
        float changeDistance = Input.GetAxis("Mouse ScrollWheel");
        //单前的距离
        float currentDistance = originVector.magnitude;
        //单位向量
        Vector3 miniVector = originVector.normalized;
        //获取新的向量
        originVector = miniVector * (currentDistance - changeDistance * Time.deltaTime * 100);


        //记录鼠标右键是否按下的状态
        
        transform.position = target.position - originVector;

        transform.RotateAround(target.position, Vector3.up, target.rotation.eulerAngles.y - lastFrameTargetRoation.y);

        lastFrameTargetRoation = target.rotation.eulerAngles;
        originVector = new Vector3(target.position.x - transform.position.x, target.position.y - transform.position.y, target.position.z - transform.position.z);


    }
}
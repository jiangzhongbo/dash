﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SoundManager : MonoBehaviour
{
    public bool Unload = false;
    private Dictionary<AudioSource, bool> ASUsed = new Dictionary<AudioSource, bool>();

    public AudioClip fx_boxout;
    public AudioClip fx_broke;
    public AudioClip fx_button;
    public AudioClip fx_coin;
    public AudioClip fx_end;
    public AudioClip fx_shake;
    public AudioClip fx_win;
    public AudioClip m_scene1;
    public AudioClip m_scene2;

    private static SoundManager instance;
    public static SoundManager Instance
    {
        get
        {
            return instance;
        }
    }

    public AudioSource[] AS;

    void Awake()
    {
        if (!Unload)
        {
            GetType().GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance).Where(e => e.FieldType == typeof(AudioClip)).ToList().ForEach(e =>
            {
                e.SetValue(this, Resources.Load(string.Concat("Sound/", e.Name), typeof(AudioClip)));
            });
            AS = GetComponents<AudioSource>();
            for (int i = 0; i < AS.Length; i++)
            {
                ASUsed[AS[i]] = false;
            }
           
        }

        instance = this;
    }


    public void StopAll()
    {
        if (Unload) return;
        for (int i = 0; i < AS.Length; i++)
        {
            if (AS[i].clip)
            {
                AS[i].Stop();
                AS[i].clip = null;
                ASUsed[AS[i]] = false;
            }
        }
    }

    public void PlayLoop(AudioClip ac)
    {
        if (Unload)
        {
            return;
        }
        for (int i = 0; i < AS.Length; i++)
        {
            if (ASUsed[AS[i]] && AS[i].clip == ac)
            {
                if (AS[i].isPlaying)
                {
                    break;
                }
                else
                {
                    AS[i].Play();
                    break;
                }
            }
            if (!ASUsed[AS[i]])
            {
                ASUsed[AS[i]] = true;
                AS[i].loop = true;
                AS[i].clip = ac;
                AS[i].Play();
                break;
            }
        }
    }

    public void StopLoop(AudioClip ac)
    {
        if (Unload)
        {
            return;
        }
        for (int i = 0; i < AS.Length; i++)
        {
            if (AS[i].clip == ac)
            {
                AS[i].Stop();
                AS[i].clip = null;
                ASUsed[AS[i]] = false;
                break;
            }
        }
    }

    public void PauseLoop(AudioClip ac)
    {
        if (Unload)
        {
            return;
        }
        for (int i = 0; i < AS.Length; i++)
        {
            if (AS[i].clip == ac)
            {
                AS[i].Pause();
                break;
            }
        }
    }

    public void PlayOneShot(string ac_name)
    {
        if (Unload)
        {
            return;
        }
        AudioClip ac = GetType().GetField(ac_name, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance).GetValue(this) as AudioClip;
        PlayOneShot(ac);
    }

    public void PlayOneShot(AudioClip ac)
    {
        if (Unload)
        {
            return;
        }
        for (int i = 0; i < AS.Length; i++)
        {
            if (!ASUsed[AS[i]])
            {
                ASUsed[AS[i]] = true;
                AS[i].loop = false;
                AS[i].clip = ac;
                AS[i].Play();
                break;
            }
        }
    }

    void Update()
    {
        if (Unload)
        {
            return;
        }
        for (int i = 0; i < AS.Length; i++)
        {
            if (ASUsed.ContainsKey(AS[i]) && ASUsed[AS[i]] && !AS[i].isPlaying)
            {
                ASUsed[AS[i]] = false;
                AS[i].loop = false;
                AS[i].clip = null;
            }
        }
    }
}

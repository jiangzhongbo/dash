﻿using UnityEngine;
using System.Collections;
using ActiveStore;
using System.Linq;

public class AchModel : ASModel<AchModel>
{
    public string ID
    {
        get;
        set;
    }

    public bool Completed
    {
        get;
        set;
    }
}

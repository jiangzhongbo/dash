﻿using UnityEngine;
using System.Collections;
using ActiveStore;
using System.Linq;

public class CubeModel : ASModel<CubeModel>
{
    public string ID
    {
        get;
        set;
    }

    public bool Bought
    {
        get;
        set;
    }
}
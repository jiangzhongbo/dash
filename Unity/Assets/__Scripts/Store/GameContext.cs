﻿using UnityEngine;
using System.Collections;
using ActiveStore;
using System.Linq;
public class GameContext : ASModel<GameContext>
{
    public string CubeId
    {
        get;
        set;
    }
    public float TimeScale = 1;
    private static GameContext instance;
    public static GameContext Instance
    {
        get
        {
            if (instance == null)
            {
                Repository.Pull();
                instance = Repository.FirstOrDefault();
                if (instance == null)
                {
                    instance = new GameContext();
                    instance.CubeId = "cube_01";
                    instance.Save();
                }
            }
            return instance;
        }
    }

    public bool ParcticeMode
    {
        get;
        set;
    }

    public bool DoubleIncome
    {
        get;
        set;
    }

    public int Golds
    {
        get;
        set;
    }

    public int st_time_played
    {
        get;
        set;
    }

    public int st_continuously_5
    {
        get;
        set;
    }

    public int st_finish_tutorial
    {
        get;
        set;
    }

    public int st_game
    {
        get;
        set;
    }

    public int st_practice
    {
        get;
        set;
    }

    public int st_cube_owned
    {
        get;
        set;
    }

    public int st_dead
    {
        get;
        set;
    }

    public int st_brag
    {
        get;
        set;
    }

    public int st_purchase
    {
        get;
        set;
    }

    public void AddGolds(int num)
    {
        Golds += num;
        Save();
        if (ShowMoney.Current) ShowMoney.Current.UpdateCount();
    }

    public void SpendGolds(int num)
    {
        Golds -= num;
        Save();
        if (ShowMoney.Current) ShowMoney.Current.UpdateCount();
    }

    public void SetDoubleIncome(bool v)
    {
        DoubleIncome = v;
        Save();
    }

    public T GetPropertyValueByName<T>(string name)
    {
        return (T)GetType().GetProperty(name, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance).GetValue(this, null);
    }

    public T GetFieldValueByName<T>(string name)
    {
        return (T)GetType().GetField(name, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance).GetValue(this);
    }

    public T GetStaticValueByName<T>(string name)
    {
        return (T)GetType().GetField(name, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static).GetValue(null);
    }

}

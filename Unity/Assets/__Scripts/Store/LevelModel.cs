﻿using UnityEngine;
using System.Collections;
using ActiveStore;
using System.Linq;

public class LevelModel : ASModel<LevelModel>
{
    public bool Bought
    {
        get;
        set;
    }

    public bool IsNotFirsPlay
    {
        get;
        set;
    }

    public float NormalProgress
    {
        get;
        set;
    }

    public float PracticeProgress
    {
        get;
        set;
    }
}
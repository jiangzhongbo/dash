﻿using UnityEngine;
using System.Collections;

public class TestCamera : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        camera.depthTextureMode = DepthTextureMode.DepthNormals;
        camera.SetReplacementShader(Shader.Find("Render Depth"), "RenderType");
    }

    // Pull is called once per frame
    void Update()
    {

    }
}

﻿using UnityEngine;
using System.Collections;

public class TestFPS : MonoBehaviour
{
    public GameObject Label;
    // Attach this to a GUIText to make a frames/second indicator.
    //
    // It calculates frames/second over each updateInterval,
    // so the display does not keep changing wildly.
    //
    // It is also fairly accurate at very low FPS counts (<10).
    // We do this not by simply counting frames per interval, but
    // by accumulating FPS for each frame. This way we end up with
    // correct overall FPS even if the interval renders something like
    // 5.5 frames.

    private float updateInterval = 0.1f;

    private int frames = 0; // Frames drawn over the interval
    private float timeleft; // Left time for current interval
    float deltaTime = 0;
    float prevRealtimeSinceStartup = 0;
    void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = -1;
        prevRealtimeSinceStartup = Time.realtimeSinceStartup;
        timeleft = updateInterval;
        deltaTime = 0;
    }

    void Update()
    {
        deltaTime = Time.realtimeSinceStartup - prevRealtimeSinceStartup;
        timeleft -= deltaTime;
        ++frames;
        Application.targetFrameRate = 60;
        if (timeleft <= 0.0f)
        {
            float fps = frames * (1 / updateInterval);
            string format = System.String.Format("{0:F2} FPS", fps);
            Label.UILabel().text = format;

            if (fps < 20)
                Label.UILabel().color = Color.red;
            else if (fps < 30)
                Label.UILabel().color = Color.yellow;
            else
                Label.UILabel().color = Color.green;
            timeleft = updateInterval;
            frames = 0;
        }
        prevRealtimeSinceStartup = Time.realtimeSinceStartup;
    }
}

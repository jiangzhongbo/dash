﻿using UnityEngine;
using System.Collections;

public class TestUp : MonoBehaviour
{
    void Start()
    {

    }
    void Update()
    {

        Vector3 newup = transform.position;

        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit))
        {

            newup.y = (hit.point + Vector3.up - Vector3.up * 0.5f).y;

        }
        transform.position = newup;
        transform.up = Vector3.Slerp(transform.up, Quaternion.Euler(270, 0, 0) * hit.normal, Time.deltaTime * 10);
    }
}

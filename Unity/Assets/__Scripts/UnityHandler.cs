using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class UnityHandler : MonoBehaviour {
	public Dictionary<SKU_ID, int> SkuID_To_Golds = new Dictionary<SKU_ID, int>{
		{SKU_ID.golds1_99, 10},
		{SKU_ID.golds4_99, 25},
		{SKU_ID.golds9_99, 60},
		{SKU_ID.golds19_99, 125},
		{SKU_ID.golds49_99, 325},
		{SKU_ID.golds99_99, 700},
	};

	public static UnityHandler Instance{
		get{
			return instance;
		}
	}

	private static UnityHandler instance;
	void PurchaseSuccess (string id) {
        PlayerPrefs.SetInt("purchase_id", -1);
		SKU_ID skuId = (SKU_ID)(int.Parse(id));
        if (SkuID_To_Golds.ContainsKey(skuId))
        {
            if (skuId != SKU_ID.golds1_99)
            {
				AndroidJavaManager.CloseFeatureView();
			}
            
        }
	}

	void Awake(){
		instance = this;
        int purchase_id = PlayerPrefs.GetInt("purchase_id", -1);
        Debug.Log("###### c# PurchaseSuccess:"+purchase_id);
        if (purchase_id != -1)
        {
            PurchaseSuccess(purchase_id + "");
        }
	}

}

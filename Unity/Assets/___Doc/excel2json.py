from xlrd import open_workbook
import os
import json
class Arm(object):
	def __init__(self, id, dsp_name, dsp_code, hub_code, pin_code, pptl):
		self.id = id
		self.dsp_name = dsp_name
		self.dsp_code = dsp_code
		self.hub_code = hub_code
		self.pin_code = pin_code
		self.pptl = pptl

	def __str__(self):
		 return("Arm object:\n"
			"  Arm_id = {0}\n"
			"  DSPName = {1}\n"
			"  DSPCode = {2}\n"
			"  HubCode = {3}\n"
			"  PinCode = {4} \n"
			"  PPTL = {5}"
				.format(self.id, self.dsp_name, self.dsp_code,
					self.hub_code, self.pin_code, self.pptl))

def get_rows(sheet):
	number_of_rows = sheet.nrows
	for row in range(0, number_of_rows):
		value = sheet.cell(row,0).value
		if value == 'end':
			return row - 1

def get_cols(sheet):
	number_of_columns = sheet.ncols
	for col in range(0, number_of_columns):
		value = (sheet.cell(0,col).value)
		if value == 'end':
			return col - 1

out = {
	'table_name' : '',
	'rows' : 0,
	'cols' : 0,
	'cells' : {}
}

row_start = 3;


wb = open_workbook('cube_config.xlsx')
jsons = []
for sheet in wb.sheets():
	number_of_rows = sheet.nrows
	number_of_columns = sheet.ncols
	sheet_name = sheet.name
	rows = get_rows(sheet) + 1
	cols = get_cols(sheet) + 1
	j = {}
	j['table_name'] = sheet_name
	j['rows'] = rows - row_start
	j['cols'] = cols
	cells = {}
	print sheet_name, rows, cols
	for row in range(row_start, rows):
		for col in range(cols):
			value  = (sheet.cell(row,col).value)
			cells['%d_%d' % (row - row_start, col)] = value
	j['cells'] = cells
	jsons.append(j)


f_name = 'excels.txt'
path = '../__json/' + f_name
if os.path.isfile(path):
	os.remove(path)
f = open(path, 'w')
str_json = json.dumps(jsons)
f.writelines([str_json])
f.close()
















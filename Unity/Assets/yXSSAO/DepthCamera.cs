using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
[AddComponentMenu("yX/Depth Camera")]
public class DepthCamera : MonoBehaviour
{
    public Camera depthCamera;
    public SSAOCamera ssaoCamera;
    public RenderTexture depthTexture;
    void Awake()
    {
        depthCamera = GetComponent<Camera>();
        depthCamera.SetReplacementShader(Shader.Find("yX/Depth"), "RenderType");
        depthTexture = new RenderTexture(1024, 1024, 8);
        depthCamera.targetTexture = depthTexture;
        ssaoCamera.m_DepthTexture = depthCamera.targetTexture;
    }

    //void Pull()
    //{
    //    depthCamera.RenderWithShader(Shader.Find("yX/Depth"), "RenderType");
    //}

    void OnDestroy()
    {
        if (depthTexture != null)
        {
            depthTexture.Release();
        }
        if (depthCamera != null)
        {
            depthCamera.targetTexture = null;
        }
    }
}

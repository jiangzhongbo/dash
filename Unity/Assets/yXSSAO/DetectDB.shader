Shader "Transparent/DetectDynamicBatchingShader" {
	Properties{
		_Color("Main Color", Color) = (1, 1, 1, 1)
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
	}

		SubShader{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		LOD 200
		Cull Off

		CGPROGRAM
#pragma surface surf Lambert alpha
#pragma target 3.0

		sampler2D _MainTex;
		fixed4 _Color;

		struct Input {
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			float4x4 m = _Object2World;

				if (!(
					m[0][0] == 1 && m[0][1] == 0 && m[0][2] == 0 && m[0][3] == 0
					&& m[1][0] == 0 && m[1][1] == 1 && m[1][2] == 0 && m[1][3] == 0
					&& m[2][0] == 0 && m[2][1] == 0 && m[2][2] == 1 && m[2][3] == 0
					&& m[3][0] == 0 && m[3][1] == 0 && m[3][2] == 0 && m[3][3] == 1)) {

					o.Albedo.r = 1 - o.Albedo.r;
					o.Albedo.g = 1 - o.Albedo.g;
					o.Albedo.b = 1 - o.Albedo.b;

				}
			o.Alpha = c.a;
		}
		ENDCG
	}

	Fallback "Transparent/VertexLit"
}
Shader "Unlit/Texture1" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 150
		Pass{
			#pragma surface surf Lambert
			CGPROGRAM

//#define CM 0
//#if CM
//			COLORMASK 0
//#else
//			COLORMASK RGBA
//#endif
			sampler2D _MainTex;

			struct Input {
				float2 uv_MainTex;
			};

			void surf(Input IN, inout SurfaceOutput o) {
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
				o.Albedo = c.rgb;
				o.Alpha = c.a;
			}
			ENDCG
		}
	}

	Fallback "Mobile/VertexLit"
}

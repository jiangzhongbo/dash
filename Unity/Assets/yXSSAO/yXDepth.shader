Shader "yX/Depth" {
	Properties{
		_MainTex("", 2D) = "white" {}
		_Cutoff("", Float) = 0.5
		_Color("", Color) = (1, 1, 1, 1)
		_DepthParam("_DepthParam", Float) = 10
	}
	Category{
		Fog{ Mode Off }

		SubShader{
			Tags{ "RenderType" = "Opaque" }
			Pass{
				CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"
				//struct v2f {
				//	float4 pos : POSITION;
				//	float2 depth : TEXCOORD0;
				//};
				//v2f vert(appdata_base v) {
				//	v2f o;
				//	o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				//	o.depth = o.pos.zw;
				//	return o;
				//}
				//fixed4 frag(v2f i) : COLOR{
				//	//float d = i.depth.x / i.depth.y;
				//	float d = pow((10.0 - i.depth.x) / 10.0, 2.0);
				//	return EncodeFloatRGBA(d);
				//	//return fixed4(0.0, 0.0, 0.0, 0.0);
				//	//return fixed4(d, d, d, d);
				//}
				float _DepthParam;
					struct v2f {
						float4 pos : POSITION;
						float4 nz : TEXCOORD0;
					};
					v2f vert(appdata_base v) {
						v2f o;
						o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
						o.nz.xyz = COMPUTE_VIEW_NORMAL;
						o.nz.w = COMPUTE_DEPTH_01;
						//o.nz.w = -(mul(UNITY_MATRIX_MV, v.vertex).z * _DepthParam);
						return o;
					}
					fixed4 frag(v2f i) : COLOR{
						return EncodeDepthNormal(i.nz.w, i.nz.xyz);
					}
					ENDCG
				}
			}
		}
		Fallback Off
}

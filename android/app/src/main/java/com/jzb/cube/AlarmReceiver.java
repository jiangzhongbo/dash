package com.jzb.cube;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {

	public static void Register(Context context) {
		Intent intentBroadcast = new Intent(context, AlarmReceiver.class);
		intentBroadcast.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
		PendingIntent alarmSender = PendingIntent.getBroadcast(context, 0,
				intentBroadcast, 0);
		long firstTime = SystemClock.elapsedRealtime();
		AlarmManager am = (AlarmManager) context
				.getSystemService(Activity.ALARM_SERVICE);
		am.cancel(alarmSender);
		am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime,
				1 * 60 * 1000, alarmSender);
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		SharedPreferences sp = context.getSharedPreferences(
				"com.jzb.adventurer", Context.MODE_WORLD_WRITEABLE);
		
		long now = Calendar.getInstance().getTimeInMillis();
		long start_time = sp.getLong("start_time", now);
		if(now - start_time >= 3 * 24 * 60 * 60 * 1000 && !sp.getBoolean("showed", true)){
			sp.edit().putBoolean("showed", true).commit();;
			NotificationHelper.showNotification(context, "Time to kill zombies!");
		}
		
		long crate_finish_time = Long.parseLong(sp.getString("crate_finish_time", "0"));
		Log.d("##########$$$$$$$$$$$$crate_finish_time:", crate_finish_time+"");
		Log.d("##########$$$$$$$$$$$$now:", now+"");
		Log.d("##########$$$$$$$$$$$$open_msg_showed:",""+ sp.getBoolean("open_msg_showed", true));
		Log.d("##########$$$$$$$$$$$$showed:",""+ sp.getBoolean("showed", true));
		if(crate_finish_time != 0){
			crate_finish_time *= 1000;
			if(now >= crate_finish_time && !sp.getBoolean("open_msg_showed", true)){
				sp.edit().putBoolean("open_msg_showed", true).commit();;
				NotificationHelper.showNotification(context, "Your chest is ready to open!");
			}
		}
//		
//		long limited = 10 * 60 * 1000;
//		String energyTimeValue = sp.getString("energyTime", "0");
//		long energyTime = Long.parseLong(energyTimeValue) * 1000;
//		int current = sp.getInt("energyCurrent", 10);
//		int max = sp.getInt("energyMax", 10);
//		Log.d("##########$$$$$$$$$$$$now:", now+"");
//		Log.d("##########$$$$$$$$$$$$limited:", limited+"");
//		Log.d("##########$$$$$$$$$$$$energyTime:", energyTime+"");
//		Log.d("##########$$$$$$$$$$$$current:", current+"");
//		Log.d("##########$$$$$$$$$$$$max:", max+"");
//		if(current == max){
//			return;
//		}
//		if (energyTime != 0) {
//			long ts = now - (energyTime + 5 * 60 * 1000);
//			Log.d("##########$$$$$$$$$$$$ts:", ts+"");
//			if (ts >= limited) {
//				long count = ts / limited;
//				Log.d("##########$$$$$$$$$$$$count:", count+"");
//				if (current + count >= max) {
//					sp.edit().putInt("energyCurrent", max).commit();
//					sp.edit().putString("energyTime", "0").commit();
//					if (sp.getInt("pref_bool_energy", 0) == 1) {
//						NotificationHelper.showNotification(context);
//					}
//				}
//			}
//		}
	}
}

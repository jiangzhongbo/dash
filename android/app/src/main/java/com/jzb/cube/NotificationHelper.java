package com.jzb.cube;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.app.NotificationCompat;

public class NotificationHelper {
	private static NotificationManager nm = null;

	public static void clearNotification() {
		if (nm != null) {
			nm.cancelAll();
		}
	}

	@SuppressWarnings("deprecation")
	public static void showNotification(Context context, String content) {
		SharedPreferences sp = context.getSharedPreferences(
				"com.jzb.adventurer", Context.MODE_WORLD_WRITEABLE);
		
		if(sp.getInt("notification_option", 1) == 0){
			return;
		}
		
		if(sp.getInt("GameLevelType", 1) == 0){
			return;
		}
		
		Intent cIntent = new Intent(context, MainActivity.class);
		if(nm == null){
			nm = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);			
		}

//		Notification n = new Notification();
//		n.flags |= Notification.FLAG_AUTO_CANCEL | Notification.FLAG_ONLY_ALERT_ONCE;
//		n.icon = R.drawable.icon;
//
//		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
//				cIntent, PendingIntent.FLAG_CANCEL_CURRENT);
//		n.contentIntent = pendingIntent;
//		n.setLatestEventInfo(context, content,
//				context.getResources().getString(R.string.app_name),
//				n.contentIntent);
//		nm.notify(R.string.app_name, n);
		
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
				cIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		Bitmap largeIcon = ((BitmapDrawable) context.getResources().getDrawable(R.drawable.icon)).getBitmap();
		builder.setLargeIcon(largeIcon)
	        .setSmallIcon(R.drawable.icon)
	        .setContentTitle("Zombie Overkill")
	        .setContentText(content)
	        .setTicker(content)
	        .setContentIntent(pendingIntent);
		final Notification n = builder.getNotification();
		nm.notify(R.string.app_name, n);

			
		
	}
}

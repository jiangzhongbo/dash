package com.doodlemobile.gamecenter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.doodlemobile.gamecenter.cache.DGlobalPrefences;
import com.doodlemobile.gamecenter.featuregames.FeatureView;
import com.doodlemobile.gamecenter.fullscreen.Resources;
import com.doodlemobile.gamecenter.fullscreen.Resources.AdmobFullCloseListener;
import com.doodlemobile.gamecenter.fullscreen.Resources.FullScreenClickListener;
import com.doodlemobile.gamecenter.fullscreen.Resources.FullScreenCloseListener;
import com.doodlemobile.gamecenter.fullscreen.Resources.FullScreenTaskBeginListener;
import com.doodlemobile.gamecenter.fullscreen.Resources.GetFullScreenResultListener;
import com.doodlemobile.gamecenter.fullscreen.Resources.GetServerTimeListener;

public class DGameTestActivity extends Activity {
	private static Activity mActivity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
//		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectNetwork().penaltyDialog().penaltyLog().build());
//		StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build());
		
		super.onCreate(savedInstanceState);
		
		Platform.setFull_Admob_ID("a15380843ce45b9");
		Platform.setGoogleAnalyticsID("UA-60839069-1");
		Platform.onCreate(this, false);
		
		Platform.setFullScreenTaskBeginListener(new FullScreenTaskBeginListener() {
			
			@Override
			public void onFullScreenTaskBegined() {
				// TODO Auto-generated method stub
				Log.e("FullScreen", "task begin now");
			}
		});
		
		Platform.setGetServerTimeListener(new GetServerTimeListener() {
  			
  			@Override
  			public void onServerTimeRecived(long servertime) {
  				// TODO Auto-generated method stub
  				Log.e("ServerTime", "ServerTime: " + servertime);
  				if(servertime == -1 || servertime < 0) {
  					Log.e("ServerTime", "Get ServerTime failed, you should ues local time instead.");
  				} else {
  					//transform to local time
  					DGlobalPrefences.serverTime = servertime + DGlobalPrefences.GetTimeZoneOffsetSecond();
  					//get login days
  					int login_days = DGlobalPrefences.GetBonusDayCount(DGlobalPrefences.serverTime);
  					//after gain bonus, remember to save login days
  					DGlobalPrefences.SetBonusDay(DGlobalPrefences.serverTime);
  				}
  			}
  		});
		
		Platform.setGetFullScreenResultListener(new GetFullScreenResultListener() {
			
			@Override
			public void onFullScreenResultRecived(String message, int retCode) {
				// TODO Auto-generated method stub
				Log.e("FullScreen", "Result is: " + message);
			}
		});
		
		Platform.setFullScreenCloseListener(new FullScreenCloseListener() {
			
			@Override
			public void onFullSCreenClosed() {
				// TODO Auto-generated method stub
				Log.e("FullScreen", "FullScreen Closed!");
			}
		});
		
		Platform.setFullScreenClickListener(new FullScreenClickListener() {
			
			@Override
			public void onFullScreenClicked() {
				// TODO Auto-generated method stub
				Log.e("FullScreen", "FullScreen Clicked!");
			}
		});
		
		Platform.setAdmobFullCloseListener(new AdmobFullCloseListener() {
			
			@Override
			public void onAdmobFullClosed() {
				// TODO Auto-generated method stub
				Log.e("Admob", "Admob Closed!");
			}
		});
		
		setContentView(R.layout.gamecenter_main);
		mActivity = this;
		
//		Platform.getServerTime();
		Platform.getTestServerTime();

		findViewById(R.id.moregames).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				Platform.getHandler().sendEmptyMessage(Platform.START_MOREGAMES_ACTIVITY);
				Message msg = Platform.getHandler(DGameTestActivity.this).obtainMessage(Platform.SHOW_FULLSCREEN_SMALL, false);
				Platform.getHandler(DGameTestActivity.this).sendMessage(msg);
				
//                t.setScreenName("moregames");
//                t.send(new HitBuilders.AppViewBuilder().build());
			}
		});
		
		findViewById(R.id.moregames_layout).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				Platform.getHandler().sendEmptyMessage(Platform.SHOW_MOREGAMES_LAYOUT);
				Message msg = Platform.getHandler(DGameTestActivity.this).obtainMessage(Platform.SHOW_FULLSCREEN_SMALLEXIT, true);
				Platform.getHandler(DGameTestActivity.this).sendMessage(msg);
			}
		});
		findViewById(R.id.show_all).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Message.obtain(Platform.getHandler(DGameTestActivity.this), Platform.SHOW_FEATURE_VIEW, 
						FeatureView.CENTER_HORIZONTAL, FeatureView.CENTER_VERTICAL,
						new Rect(0, 720, 480, 800)
				).sendToTarget();
			}
		});
		findViewById(R.id.show_next).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Platform.getHandler(DGameTestActivity.this).sendEmptyMessage(Platform.CLOSE_FEATURE_VIEW);
			}
		});
		
	}
	
	final static int MSG_MOREGAMES = 0x00000001;

	public Handler mainHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_MOREGAMES:
//				MoreGamesLayout.showMoreGames(mActivity);
				((Button)findViewById(R.id.show_next)).setText("Moregameschanged!!");
				break;
			}
		}
	};
	
	private void showExitDialog(){
		View featureView = null;
		
		LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		featureView = inflater.inflate(R.layout.example_dialog, null);
		
		AlertDialog.Builder dialg = new AlertDialog.Builder(mActivity)
		.setTitle("Are you sure exit?")
		.setView(featureView)
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						 mActivity.finish();
					}
				})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		
		dialg.show();
	}

	
	@Override
	protected void onStart() {
		super.onStart();
		Platform.onStart();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		Platform.onStop();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Platform.onDestroy();
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(Resources.fullScreen != null && Resources.fullScreen.isShowing()) {
			return Resources.fullScreen.onKeyUp(keyCode, event);
		} else if(Resources.fullScreen_small != null && Resources.fullScreen_small.isShowing()) {
			return Resources.fullScreen_small.onKeyUp(keyCode, event);
		}
		return super.onKeyUp(keyCode, event);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(Resources.fullScreen != null && Resources.fullScreen.isShowing()) {
			return Resources.fullScreen.onKeyDown(keyCode, event);
		} else if(Platform.isFullScreenSmallShowing()) {
			return Resources.fullScreen_small.onKeyDown(keyCode, event);
		} else {
			if(keyCode == KeyEvent.KEYCODE_BACK) {
				showExitDialog();
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
}

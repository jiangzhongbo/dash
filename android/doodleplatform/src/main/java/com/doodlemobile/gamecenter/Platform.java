package com.doodlemobile.gamecenter;

import java.lang.Thread.UncaughtExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewManager;
import android.widget.RelativeLayout;

import com.doodlemobile.gamecenter.DoodleAnalytics.TrackerName;
import com.doodlemobile.gamecenter.event.ExitAppUtils;
import com.doodlemobile.gamecenter.event.PlatformHandler;
import com.doodlemobile.gamecenter.featuregames.FeatureView;
import com.doodlemobile.gamecenter.fullscreen.Resources;
import com.doodlemobile.gamecenter.fullscreen.Resources.AdmobFullCloseListener;
import com.doodlemobile.gamecenter.fullscreen.Resources.FullScreenClickListener;
import com.doodlemobile.gamecenter.fullscreen.Resources.FullScreenCloseListener;
import com.doodlemobile.gamecenter.fullscreen.Resources.FullScreenTaskBeginListener;
import com.doodlemobile.gamecenter.fullscreen.Resources.GetFullScreenResultListener;
import com.doodlemobile.gamecenter.fullscreen.Resources.GetServerTimeListener;
import com.doodlemobile.gamecenter.net.DHttpClient;
import com.doodlemobile.gamecenter.net.DNetworkStatus;
import com.doodlemobile.gamecenter.utils.DGlobalParams;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.ExceptionReporter;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

public class Platform {
	public static boolean fvShouldShow = false;
	public static final long DEFAULT_FULLSCREEN_LAST_TIME = 10000L;
	public static final int CREATE_FULLSCREEN = 0x0;
	public static final int CLOSE_FULLSCREEN = 0x1;
	public static final int START_MOREGAMES_ACTIVITY = 0x2;
	public static final int SHOW_MOREGAMES_LAYOUT = 0x3;
	public static final int CREATE_FEATURE_VIEW = 0x4;
	public static final int SHOW_FEATURE_VIEW = 0x5;
	public static final int CLOSE_FEATURE_VIEW = 0x6;
	public static final int RELEASE_FEATURE_VIEW = 0x7;
	public static final int GOTO_RATE = 0x8;
	public static final int SHOW_FULLSCREEN_SMALL = 0x9;
	public static final int CLOSE_FULLSCREEN_SMALL = 0x10;
	public static final int SHOW_FULLSCREEN_SMALLEXIT = 0x11;
	
	private static Activity activity = null;
	private static Handler handler = null;
	private static int orientation = 0;
	private static DisplayMetrics metrics = null;
	private static boolean isLandSacpe = false;
	
	public static int ShowFullScreenTimes = 0;
	public static String MY_AD_UNIT_ID = null;
	public static String MY_GA_ID = null;
	
	public static void onCreate(Activity mainActivity, boolean landScape){
		if(mainActivity == null){
			throw new RuntimeException("Platform init failed: activity can't be null");
		}
		activity = mainActivity;
		isLandSacpe = landScape;
		ExitAppUtils.getInstance().addActivity(activity);
		handler = new PlatformHandler(activity);
		
		orientation = activity.getResources().getConfiguration().orientation;
		metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
//		Log.e("FullScreen", "Platform init wp: " + metrics.widthPixels + ", Platform hp: " + metrics.heightPixels);
		
		FeatureView.index = 0;
		ShowFullScreenTimes = 0;
		DoodleMobileAnaylise.onCreate(mainActivity); 
		
		String pkgName = activity.getPackageName();
    	Resources.featureViewR = activity.getResources().getIdentifier("dm_featureview", "layout", pkgName);
    	Resources.admobViewR = activity.getResources().getIdentifier("dm_admob", "layout", pkgName);
    	if(Resources.admobViewR == 0) {
    		Resources.admobViewR = Resources.featureViewR;
    	}
		
//		String gameVerName = DoodleMobilePublic.getAppVersionName();
//		int gameVerCode = DoodleMobilePublic.getAppVersionCode();
//		Log.i("activity", "gameVerName = " + gameVerName + "; gameVerCode = " + gameVerCode);
		
		Tracker tracker = ((DoodleAnalytics) activity.getApplication()).getTracker(
                TrackerName.APP_TRACKER);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);
       
        UncaughtExceptionHandler myHandler = new ExceptionReporter(tracker, Thread.getDefaultUncaughtExceptionHandler(), activity);
        Thread.setDefaultUncaughtExceptionHandler(myHandler);
	}
	
	public static void onPause() {
		if(Resources.featureView != null){
			View view = ((RelativeLayout)((RelativeLayout)Resources.featureView).getChildAt(0)).getChildAt(0);
			if(view instanceof AdView) {
				((AdView)view).pause();
			}
		}
	}
	
	public static void onResume() {
		if(Resources.featureView != null){
			View view = ((RelativeLayout)((RelativeLayout)Resources.featureView).getChildAt(0)).getChildAt(0);
			if(view instanceof AdView) {
				((AdView)view).resume();
			}
		}
	}
	
	public static void onStart(){
		try {
			GoogleAnalytics.getInstance(activity.getApplicationContext()).reportActivityStart(activity);
		}catch (Exception e) {
			e.printStackTrace();
			Log.w("GoogleAnalytics", "activity: " + activity);
			if(activity.getApplicationContext() != null) {
				Log.w("GoogleAnalytics", "getApplicationContext is not null ");
			}
			if(GoogleAnalytics.getInstance(activity.getApplicationContext()) != null) {
				Log.w("GoogleAnalytics", "GoogleAnalytics.getInstance is not null ");
			}
		}
		
	}
	
	public static void onStop(){
		try {
			GoogleAnalytics.getInstance(activity.getApplicationContext()).reportActivityStop(activity);
		}catch (Exception e) {
			Log.w("GoogleAnalytics", "activity: " + activity);
			if(activity.getApplicationContext() != null) {
				Log.w("GoogleAnalytics", "getApplicationContext is not null ");
			}
			if(GoogleAnalytics.getInstance(activity.getApplicationContext()) != null) {
				Log.w("GoogleAnalytics", "GoogleAnalytics.getInstance is not null ");
			}
		}
		DoodleMobileAnaylise.onStop(activity);
	}
	
	public static void onDestroy() {
		try {
			fvShouldShow = false;
			clearMessage();
			ExitAppUtils.getInstance().delActivity(activity);
			handler = null;
			activity = null;
			if(Resources.featureView != null){
				View view = ((RelativeLayout)((RelativeLayout)Resources.featureView).getChildAt(0)).getChildAt(0);
				if(view instanceof AdView) {
					((AdView)view).destroy();
				}
				((ViewManager)Resources.featureView.getParent()).removeView(Resources.featureView);
				Resources.featureView = null;
			}
			Resources.featureViewR = 0;
			Resources.admobViewR = 0;
			Resources.getTimeListener = null;
			Resources.fullScreenCloseListener = null;
			//add for fullscreen test
			Resources.fullScreen = null;
			Resources.fullScreen_small = null;
			if(Resources.fullScreenImage != null && !Resources.fullScreenImage.isRecycled()){
				Resources.fullScreenImage.recycle();
			}
			if(Resources.fullScreenImage_small != null && !Resources.fullScreenImage_small.isRecycled()){
				Resources.fullScreenImage_small.recycle();
			}
			Resources.fullScreenImage = null;
			Resources.fullScreenImage_small = null;
			DHttpClient.destroy();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void clearMessage(){
		int[] messages = {
				CREATE_FULLSCREEN, CLOSE_FULLSCREEN,
				CREATE_FEATURE_VIEW, SHOW_FEATURE_VIEW, CLOSE_FEATURE_VIEW,
				SHOW_MOREGAMES_LAYOUT, START_MOREGAMES_ACTIVITY,
		};
		for(int i = 0; i < messages.length; i++){
			if(getHandler(getActivity()) != null)
				getHandler(getActivity()).removeMessages(messages[i]);
		}
	}
	
	public static void setActivity(Activity mainActivity) {
		activity = mainActivity;
	}
	
	public static Activity getActivity(){
		return activity;
	}
	
	public static Handler getHandler(Activity mainActivity){
		if(handler == null){
			handler = new PlatformHandler(mainActivity);
		}
		return handler;
	}

	public static boolean isFullScreenShowing(){
		if(Resources.fullScreen != null && Resources.fullScreen.isShowing())
			return true;
		return false;
	}
	
	public static boolean isFullScreenSmallShowing(){
		if(Resources.fullScreen_small != null && Resources.fullScreen_small.isShowing())
			return true;
		return false;
	}
	
	public static boolean isFullScreenSmallIsReady() {
		return (Resources.fullScreen_small != null);
	}
	
	public static void setGetServerTimeListener(GetServerTimeListener getTimeListener) {
  		Resources.getTimeListener = getTimeListener;
  	}
	
	public static void setGetFullScreenResultListener(GetFullScreenResultListener getFullScreenResultListener) {
		Resources.getFullScreenResultListener = getFullScreenResultListener;
	}
	
	public static void setFullScreenTaskBeginListener(FullScreenTaskBeginListener fullScreenTaskBeginListener) {
		Resources.fullScreenTaskBeginListener = fullScreenTaskBeginListener;
	}
	
	public static void setFullScreenCloseListener(FullScreenCloseListener fullScreenCloseListener) {
		Resources.fullScreenCloseListener = fullScreenCloseListener;
	}
	
	public static void setFullScreenClickListener(FullScreenClickListener fullScreenClickListener) {
		Resources.fullScreenClisckListener = fullScreenClickListener;
	}
	
	public static void setAdmobFullCloseListener(AdmobFullCloseListener admobFullCloseListener) {
		Resources.admobFullCloseListener = admobFullCloseListener;
	}
	
	public static void getServerTime() {
		
		if(activity == null || !DNetworkStatus.isNetworkAvailable(activity)) {
			if(Resources.getTimeListener != null) {
  				Resources.getTimeListener.onServerTimeRecived(-1);
  			}
			return;
		}
		
		new Thread(new Runnable() {
		
			@Override
			public void run() {
				// TODO Auto-generated method stub
				String response = DHttpClient.post(DGlobalParams.SERVER_NEW_FEATUREVIEW_TIME_URL);
				JSONObject json = null;
				if(response != null) {
					try {
						json = new JSONObject(response);
						if(Resources.getTimeListener != null) {
			  				Resources.getTimeListener.onServerTimeRecived(json.getLong("date"));
			  			}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						if(Resources.getTimeListener != null) {
			  				Resources.getTimeListener.onServerTimeRecived(-1);
			  			}
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
	
	public static int getOrientation() {
		if(isLandSacpe)
			orientation = Configuration.ORIENTATION_LANDSCAPE;
		return orientation;
	}
	
	public static DisplayMetrics getMetrics() {
		if(isLandSacpe) {
			if(metrics.widthPixels < metrics.heightPixels) {
				int w = metrics.widthPixels;
				metrics.widthPixels = metrics.heightPixels;
				metrics.heightPixels = w;
			}
		}
		return metrics;
	}
	
	public static void getTestServerTime() {
		
		if(activity == null || !DNetworkStatus.isNetworkAvailable(activity)) {
			if(Resources.getTimeListener != null) {
  				Resources.getTimeListener.onServerTimeRecived(-1);
  			}
			return;
		}
		
		new Thread(new Runnable() {
		
			@Override
			public void run() {
				// TODO Auto-generated method stub
				String response = DHttpClient.post(DGlobalParams.SERVER_TEST_FEATUREVIEW_TIME_URL);
				JSONObject json = null;
				if(response != null) {
					try {
						json = new JSONObject(response);
						if(Resources.getTimeListener != null) {
			  				Resources.getTimeListener.onServerTimeRecived(json.getLong("date"));
			  			}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
	
	public static void setFull_Admob_ID(String admob_id) {
		MY_AD_UNIT_ID = admob_id;
	}
	
	public static void setGoogleAnalyticsID(String GA_id) {
		MY_GA_ID = GA_id;
	}
}

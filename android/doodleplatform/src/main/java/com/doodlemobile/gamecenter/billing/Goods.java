package com.doodlemobile.gamecenter.billing;

public abstract class Goods {
	protected String sku;

	public Goods(String sku){
		this.sku = sku;
	}
	
	public final String getSku(){
		return sku;
	}
	
	public abstract void onPurchaseSuccess();
	
}

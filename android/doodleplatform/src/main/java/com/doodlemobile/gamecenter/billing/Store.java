package com.doodlemobile.gamecenter.billing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.doodlemobile.gamecenter.billing.util.IabHelper;
import com.doodlemobile.gamecenter.billing.util.IabResult;
import com.doodlemobile.gamecenter.billing.util.Inventory;
import com.doodlemobile.gamecenter.billing.util.Purchase;

public class Store {

	// Debug tag, for logging
    private static final String TAG = "DoodleStore"; 
    
    // (arbitrary) request code for the purchase flow
    private static final int RC_REQUEST = 10001;
    private static final String Pref_Location = "store";
	private static final String KEY_HAS_PURCHASED = "has-purchased";

    // The helper object
    IabHelper mHelper;
    String base64EncodedPublicKey;
    Activity mainActivity;
    private boolean supportInAppBilling = false;
    private boolean debugLogging = false;
    private Goods[] goodsArray;
//    List<Goods> goodsList = new ArrayList<Goods>();
    private boolean purchased = false;
    private Handler billingHandler;
    
    public Store(String base64EncodedPublicKey, Goods... items) {
//    	for(Goods item : items)
//    		goodsList.add(item);
    	this.base64EncodedPublicKey = base64EncodedPublicKey;
    	this.goodsArray = items.clone();
    	billingHandler = new Handler(){
    		public void handleMessage(Message msg) {
    			int whichGoods = msg.what;
    			Store.this.buy(goodsArray[whichGoods]);
    		}
    	};
    }
    
    public Handler getBillingHandler() {
    	return billingHandler;
    }
    
    public void setDebugLogging(boolean debugLogging) {
    	this.debugLogging = debugLogging;
    }

    public void onCreate(Activity activity) {
    	this.mainActivity = activity;
    	SharedPreferences sp = activity.getSharedPreferences(Pref_Location, Context.MODE_PRIVATE);
    	purchased = sp.getBoolean(KEY_HAS_PURCHASED, false);
        // Create the helper, passing it our context and the public key to verify signatures with
        Log.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(activity, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(debugLogging);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        Log.d(TAG, "Starting setup.");
        try{
	        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
				public void onIabSetupFinished(IabResult result) {
	                Log.d(TAG, "Setup finished.");
	                
	                if (!result.isSuccess()) {
	                    // Oh noes, there was a problem.
	                    // complain("Problem setting up in-app billing: unavailable, sorry!");
	                    return;
	                }
	
	                // Have we been disposed of in the meantime? If so, quit.
	                if (mHelper == null) return;
	
	                supportInAppBilling = true;
	                // IAB is fully set up. Now, let's get an inventory of stuff we own.
	                Log.d(TAG, "Setup successful. Querying inventory.");
	                try{
	                	mHelper.queryInventoryAsync(mGotInventoryListener);
	                } catch(IllegalStateException e) {
	                	// This exception is for checking setup done, is there an async operation,
	                	e.printStackTrace();
	                }
	            }
	        });
        } catch(IllegalStateException e) {
        	// This exception is for checking setup done, not disposed,
        	e.printStackTrace();
        }
    }

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                Log.d(TAG, "Failed to query inventory: " + result);
                return;
            }

            Log.d(TAG, "Query inventory was successful.");

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */


            // Check for gas delivery -- if we own gas, we should fill up the tank immediately
            for(Goods goods : goodsArray) {
            	Purchase purchase = inventory.getPurchase(goods.getSku());
            	if (purchase != null && verifyDeveloperPayload(purchase)) {
            		Log.d(TAG, "We have gas. Consuming it.");
            		try{
            			mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            		} catch(IllegalStateException e) {
            			// This exception is for checking setup done, not disposed, is there an async operation,
            			e.printStackTrace();
            		}
            		return;
            	}
            }
        }
    };

    // User buy something
    public void buy(Goods goods) {
    	if(!supportInAppBilling) {
    		complain("Not support, sorry!");
    		return;
    	}
        Log.d(TAG, "Buy start.");
        /* TODO: for security, generate your payload here for verification. See the comments on
         *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
         *        an empty string, but on a production app you should carefully generate this. */
        String payload = goods.getSku();
        try {
        	mHelper.launchPurchaseFlow(mainActivity, goods.getSku(), RC_REQUEST,
        			mPurchaseFinishedListener, payload);
        } catch(IllegalStateException e) {
        	// This exception is for checking setup done, not disposed, is there an async operation,
        	e.printStackTrace();
        }
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (requestCode != RC_REQUEST) return false;
        if (mHelper == null) return true;
        
        if(data == null && requestCode == RC_REQUEST) {
	        try{
	        	mHelper.queryInventoryAsync(mGotInventoryListener);
	        } catch(IllegalStateException e) {
	        	// This exception is for checking setup done, is there an async operation,
	        	e.printStackTrace();
	        }
	        return true;
        }
        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            return false;
        } else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
            return true;
        }
    }

    /** Verifies the developer payload of a purchase. */
    boolean verifyDeveloperPayload(Purchase p) {

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */
        
        for(Goods good : goodsArray) {
	        String payload = good.getSku();
	        if(payload.equalsIgnoreCase(p.getDeveloperPayload()))
	        	return true;
        }

        return false;
    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
            	Log.d(TAG, "Error purchasing: " + result);
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                Log.d(TAG, "Error purchasing. Authenticity verification failed.");
                return;
            }

            Log.d(TAG, "Purchase successful.");
            alert("Thank you!");
            
            // use for adFree
            purchased = true;
            SharedPreferences sp = mainActivity.getSharedPreferences(Pref_Location, Context.MODE_PRIVATE);
            sp.edit().putBoolean(KEY_HAS_PURCHASED, purchased).commit();
            
            for(Goods goods : goodsArray) if (purchase.getSku().equals(goods.getSku())) {
                // bought 1/4 tank of gas. So consume it.
                Log.d(TAG, "Purchase is " + goods.getSku() + ". Starting consumption.");
                try{
                	mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                } catch (IllegalStateException e) {
                	// This exception is for checking setup done, is there an async operation,
                	e.printStackTrace();
                }
            }
        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
            	for(Goods goods : goodsArray) if(purchase.getSku().equals(goods.getSku())) {
            		goods.onPurchaseSuccess();
            	}
            } else {
                Log.d(TAG, "Error while consuming: " + result);
            }
            Log.d(TAG, "End consumption flow.");
        }
    };

    // We're being destroyed. It's important to dispose of the helper here!
    public void onDestroy() {
        // very important:
        Log.d(TAG, "Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }
    
    public boolean inAppBillingSupported(){
    	return supportInAppBilling;
    }
    
    public boolean hasPurchased() {
    	return purchased;
    }

    void complain(String message) {
        Log.e(TAG, "**** TrivialDrive Error: " + message);
        alert(message);
    }

    void alert(String message) {
//        AlertDialog.Builder bld = new AlertDialog.Builder(mainActivity);
//        bld.setMessage(message);
//        bld.setNeutralButton("OK", null);
//        Log.d(TAG, "Showing alert dialog: " + message);
//        bld.create().show();
        try {
            if(mainActivity != null) {
                Toast.makeText(mainActivity, message, Toast.LENGTH_SHORT).show();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}

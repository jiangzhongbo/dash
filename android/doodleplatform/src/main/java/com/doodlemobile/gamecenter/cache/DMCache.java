package com.doodlemobile.gamecenter.cache;

import android.os.Environment;

public abstract class DMCache {

	protected boolean hasSDCard() {
		return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
	}
}

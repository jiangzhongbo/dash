package com.doodlemobile.gamecenter.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.os.Environment;

import com.doodlemobile.gamecenter.DoodleMobileAnaylise;

public class ExternalStorageIO implements IFileIO {
	private static final String DIR_PLATFORM_GAMES = ".dmplatform/.dmgames";		// game
	private static final String DIR_PLATFORM_APPS = ".dmplatform/.dmapps";		// app
	
	private String SDCARD_FILE_DIR = "";
	
	public ExternalStorageIO() {
		String sdcardPath = Environment.getExternalStorageDirectory().getPath();		//  "/mnt/sdcard"
		if(DoodleMobileAnaylise.gAppType == DoodleMobileAnaylise.DMTYPE_APPS) {
			SDCARD_FILE_DIR = sdcardPath + File.separator + DIR_PLATFORM_APPS + File.separator;
		} else {
			SDCARD_FILE_DIR = sdcardPath + File.separator + DIR_PLATFORM_GAMES + File.separator;
		}
		
		File dir = new File(SDCARD_FILE_DIR);
		if(!dir.exists()){
			dir.mkdirs();    //create directories included missing parents
		}
	}

	@Override
	public boolean hasFile(String filename) {
		File file = new File(SDCARD_FILE_DIR + filename);
		return file.exists();
	}

	@Override
	public byte[] getFile(String filename) {
		// TODO Auto-generated method stub
		try {
			InputStream reader = new FileInputStream(SDCARD_FILE_DIR + filename);
			byte buf[] = new byte[reader.available()];
			reader.read(buf);
			reader.close();
			return buf;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void saveFile(String filename, byte[] content) {
		try {
			OutputStream writer = new FileOutputStream(SDCARD_FILE_DIR + filename);
			writer.write(content);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteFile(String filename) {
		try {
			File file = new File(SDCARD_FILE_DIR + filename);
			if (file.exists()) {
				file.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

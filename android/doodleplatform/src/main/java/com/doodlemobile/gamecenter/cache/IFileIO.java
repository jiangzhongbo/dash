package com.doodlemobile.gamecenter.cache;


public interface IFileIO {

	public boolean hasFile(String filename);
	
	public byte[] getFile(String filename);
	
	public void saveFile(String filename, byte[] content);
	
	public void deleteFile(String filename);

}

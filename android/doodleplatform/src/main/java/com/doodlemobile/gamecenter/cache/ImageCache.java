package com.doodlemobile.gamecenter.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.doodlemobile.des.DBase64;

public class ImageCache extends DMCache {
	IFileIO fileIO = null;
	
	private static ImageCache imageCache = null;
	public static ImageCache getInstance(Context context){
		if(imageCache == null){
			imageCache = new ImageCache(context);
		}
		return imageCache;
	}
	
	private ImageCache(Context context) {
		if(hasSDCard()){
			fileIO = new ExternalStorageIO();
		} else {
			fileIO = new InternalStorageIO(context);
		}
	}
	
	public boolean isExist(String imageURI){
		String filename = new String(DBase64.encode(imageURI.getBytes()));
		return fileIO.hasFile(filename);
	}
	
	public void put(String imageURI, byte[] content){
		String filename = new String(DBase64.encode(imageURI.getBytes()));
		fileIO.saveFile(filename, content);
	}
	
	public Bitmap get(String imageURI){
		try{
			String filename = new String(DBase64.encode(imageURI.getBytes()));
			byte[] content = fileIO.getFile(filename);
			return BitmapFactory.decodeByteArray(content, 0, content.length);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void delete(String imageURI){
		String filename = new String(DBase64.encode(imageURI.getBytes()));
		fileIO.deleteFile(filename);
	}
}

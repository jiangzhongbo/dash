package com.doodlemobile.gamecenter.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import android.content.Context;

public class InternalStorageIO implements IFileIO {
	private Context context = null;

	public InternalStorageIO(Context context) {
		this.context = context;
	}

	@Override
	public boolean hasFile(String filename) {
		File file = context.getFileStreamPath(filename);
		return file.exists();
	}

	@Override
	public byte[] getFile(String filename) {
		if (hasFile(filename)) {
			try {
				FileInputStream fos = context.openFileInput(filename);
				byte[] buf = new byte[fos.available()];
				fos.read(buf);
				fos.close();
				return buf;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public void saveFile(String filename, byte[] content) {
		try {
			FileOutputStream fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
			fos.write(content);
			fos.flush();
			fos.close();
		} catch (Exception e) {
			deleteFile(filename);
			e.printStackTrace();
		}
	}

	@Override
	public void deleteFile(String filename) {
		try {
			if(hasFile(filename))
				context.deleteFile(filename);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

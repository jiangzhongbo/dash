package com.doodlemobile.gamecenter.cache;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * 一定不会返回null
 * 在使用本缓存之前，一定要先验证key是否存在，否则默认返回值不确定
 * @author passion
 */
public class PreferencesCache {
	private final static String DM_PREFERENCES_FILE = ".dmgames_prefs";
	private final static String DEFAULT_STRING = "none";
	private final static int DEFAULT_INT = 0;
	private final static long DEFAULT_LONG = 0L;
	private final static boolean DEFAULT_BOOLEAN = false;

	private SharedPreferences preferences;
	private SharedPreferences.Editor editor;
	
	private static PreferencesCache instance = null;
	public static PreferencesCache getInstance(Context context){
		if(instance == null){
			instance = new PreferencesCache(context);
		}
		return instance;
	}
	
	private PreferencesCache(Context context){
		preferences = context.getSharedPreferences(DM_PREFERENCES_FILE, Context.MODE_PRIVATE);
		editor = preferences.edit();
	}
	
	public boolean isExist(String key){
		return preferences.contains(key);
	}
	
	public String getString(String key){
		return preferences.getString(key, DEFAULT_STRING);
	}
	
	public int getInt(String key){
		return preferences.getInt(key, DEFAULT_INT);
	}
	
	public long getLong(String key){
		return preferences.getLong(key, DEFAULT_LONG);
	}
	
	public boolean getBoolean(String key){
		return preferences.getBoolean(key, DEFAULT_BOOLEAN);
	}
	
	public void putString(String key, String value){
		editor.putString(key, value);
		editor.commit();
	}
	
	public void putInt(String key, int value){
		editor.putInt(key, value);
		editor.commit();
	}
	
	public void putLong(String key, long value){
		editor.putLong(key, value);
		editor.commit();
	}
	
	public void putBoolean(String key, boolean value){
		editor.putBoolean(key, value);
		editor.commit();
	}
	
	public void delete(String key){
		editor.remove(key).commit();
	}
}

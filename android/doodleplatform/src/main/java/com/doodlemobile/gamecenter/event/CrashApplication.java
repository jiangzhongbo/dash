package com.doodlemobile.gamecenter.event;

import android.app.Application;  

public class CrashApplication extends Application {  
  
    @Override  
    public void onCreate() {  
        super.onCreate();  
        CrashHandler mCustomCrashHandler = CrashHandler.getInstance();  
        mCustomCrashHandler.setCustomCrashHanler(getApplicationContext());  
    }  
  
}  
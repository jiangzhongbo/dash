package com.doodlemobile.gamecenter.event;

import android.os.Handler;
import android.os.HandlerThread;

public class DLogThread extends HandlerThread {

	private final static String LOG_THREAD_NAME = "dm_log_thread";
	private static DLogThread instance = null;
	
	private DLogThread(String name){
		super(name);
	}
	
	private static DLogThread getInstance(){
		if(instance == null){
			instance = new DLogThread(LOG_THREAD_NAME);
		}
		if(instance != null && !instance.isAlive()){
			instance.start();
		}
		return instance;
	}
	
	private static Handler handler = null;
	private static Handler getHandler() {
		if(handler == null) {
			handler = new Handler(getInstance().getLooper());
		}
		return handler;
	}
	
	public static synchronized void post(Runnable r) {
		getHandler().post(r);
	}
	
}

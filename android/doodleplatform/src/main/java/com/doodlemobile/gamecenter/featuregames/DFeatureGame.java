package com.doodlemobile.gamecenter.featuregames;

import android.graphics.Bitmap;

public class DFeatureGame {
	public String mGameName;
	public String mCompanyName;
	public String mMarketUri;
	public Bitmap mBitmap = null;
	public int priority;
	
	public DFeatureGame(String mCompanyName, String mGameName, String mMarketUri, Bitmap mBitmap, int priority) {
		this.mGameName = mGameName;
		this.mCompanyName = mCompanyName;
		this.mMarketUri = mMarketUri;	
		this.mBitmap = mBitmap;
		this.priority = priority;
	}
}

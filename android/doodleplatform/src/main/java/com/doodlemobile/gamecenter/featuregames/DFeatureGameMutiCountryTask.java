package com.doodlemobile.gamecenter.featuregames;

import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.doodlemobile.gamecenter.Platform;
import com.doodlemobile.gamecenter.cache.PreferencesCache;
import com.doodlemobile.gamecenter.fullscreen.Resources;
import com.doodlemobile.gamecenter.net.DHttpClient;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class DFeatureGameMutiCountryTask extends AsyncTask<NameValuePair, Integer, String>{

	private static final String MutiCountryURL = "http://f2.doodlemobile.com/feature_server/geo-ip/test.php";
	private boolean ShowAdmob = false;
	private Activity activity = null;
	
	public DFeatureGameMutiCountryTask() {
		this.activity = Platform.getActivity();
	}
	
	@Override
	protected String doInBackground(NameValuePair... params) {
		String result = null;
		PreferencesCache cache = PreferencesCache.getInstance(activity);
		if(cache.isExist("showadmob") && cache.isExist("showadmob-lastmodified")){
			Long lastModified = cache.getLong("showadmob-lastmodified");
			if(cache.isExist("showadmob-cachecontrol")){
				Long cacheControl = cache.getLong("showadmob-cachecontrol");
				if(System.currentTimeMillis() < lastModified + cacheControl) {
					ShowAdmob = cache.getBoolean("showadmob");
					return null;
				} else {
					int requestTimes = 0;
					do {
						requestTimes++;
						result = DHttpClient.post(MutiCountryURL);
						Log.i("getMutiCountry", "" + result);
					} while(result == null && requestTimes < 2);
				}
			} else {
				int requestTimes = 0;
				do {
					requestTimes++;
					result = DHttpClient.post(MutiCountryURL,
							new BasicNameValuePair("If-Modified-Since", String.valueOf(lastModified)));
					Log.i("getMutiCountry", "" + result);
				} while(result == null && requestTimes < 2);
			}
		} else {
			if(result == null) {
				int requestTimes = 0;
				do {
					requestTimes++;
					result = DHttpClient.post(MutiCountryURL);
					Log.i("getMutiCountry", "" + result);
				} while(result == null && requestTimes < 2);
			}
		}
		
		String userCountry = null;
		if(result != null && result.length() != 0) {
			try {
				JSONObject ret = new JSONObject(result);
				int retcode = ret.getInt("retcode");
				Log.i("getMutiCountry", ret.getString("message"));
				if(retcode != 1) {
					userCountry = ret.getString("country").toString();
				} else {
					userCountry = Locale.getDefault().getCountry();
					Log.i("getMutiCountry", "" + userCountry);
				}
				JSONArray countries = ret.getJSONArray("adMobCountries");
				for(int i = 0; i < countries.length(); i++) {
					if(userCountry != null && userCountry.equalsIgnoreCase(countries.getString(i)))
					{
						ShowAdmob = true;
						break;
					}
				}
				
				if(retcode != 1 && ret.has("Last-Modified")) {
					cache.putBoolean("showadmob", ShowAdmob);
					cache.putLong("showadmob-lastmodified", System.currentTimeMillis());
					if(ret.has("Cache-Control")){
						cache.putLong("showadmob-cachecontrol", ret.getLong("Cache-Control"));
					}
				}
				
				
			} catch (JSONException e) {
				Log.i("getMutiCountry", "error!!");
				e.printStackTrace();
			}
		}
		Log.i("getMutiCountry", "" + ShowAdmob);
		return result;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		
		//no show admob
		ShowAdmob = false;
		
		if(activity == null)
			return;
		try {
			LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if(!ShowAdmob || Resources.admobViewR == Resources.featureViewR) {
				if(Resources.featureViewR == 0)
					return;
				Resources.featureView = inflater.inflate(Resources.featureViewR, null);
				Resources.featureView.setVisibility(Platform.fvShouldShow ? View.VISIBLE : View.GONE);
			} else {
				if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity) != ConnectionResult.SUCCESS) 
				{
					Resources.featureView = inflater.inflate(Resources.featureViewR, null);
					Resources.featureView.setVisibility(Platform.fvShouldShow ? View.VISIBLE : View.GONE);
				} else {
					if(Resources.admobViewR == 0)
						return;
					Resources.featureView = inflater.inflate(Resources.admobViewR, null);
					if(Resources.featureView != null){
						View view = ((RelativeLayout)((RelativeLayout)Resources.featureView).getChildAt(0)).getChildAt(0);
						if(view != null) {
							AdRequest adRequest = new AdRequest.Builder()
					    	.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
					    	.build();
							((AdView)view).setAdListener(new AdListener(){
								public void onAdLoaded() {
									super.onAdLoaded();
									if(Resources.featureView != null)
										Resources.featureView.setVisibility(Platform.fvShouldShow ? View.VISIBLE : View.GONE);
								}
							});
							((AdView)view).loadAd(adRequest);
						}
					}
				}
			}
			activity.addContentView(Resources.featureView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

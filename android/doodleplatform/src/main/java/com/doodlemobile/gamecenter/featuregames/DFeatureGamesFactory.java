package com.doodlemobile.gamecenter.featuregames;

import java.util.ArrayList;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.doodlemobile.gamecenter.cache.ImageCache;
import com.doodlemobile.gamecenter.event.DHandlerThread;
import com.doodlemobile.gamecenter.net.DHttpClient;
import com.doodlemobile.gamecenter.net.DNetworkStatus;
import com.doodlemobile.gamecenter.utils.DGlobalParams;

public class DFeatureGamesFactory {
	private static final String TAG = "DFeatureGamesFactory";

	public ArrayList<DFeatureGame> uninstalledFeatureGames = new ArrayList<DFeatureGame>();
	public ArrayList<FeatureView> mFeatureViewList = new ArrayList<FeatureView>();

	public static DFeatureGamesFactory gInstance = new DFeatureGamesFactory();

	public DFeatureGame getNextUninstalledGame(FeatureView featureView) {
		if (uninstalledFeatureGames.size() <= 0) {
			featureView.featureGameIndex++;
			return null;
		}
		featureView.featureGameIndex = (featureView.featureGameIndex + 1)
				% uninstalledFeatureGames.size();
		return uninstalledFeatureGames.get(featureView.featureGameIndex);
	}

	private boolean parse(String result, Context mContext) {
		if (result == null) {
			Log.w(TAG, "result = null, httpResponse.getEntity() is null");
			return false;
		}

		try {
			uninstalledFeatureGames.clear();
			ImageCache imageCache = ImageCache.getInstance(mContext);
			JSONObject jsonObject = new JSONObject(result);
			JSONArray array = null;
			String arrayString = jsonObject.getString("data"); // data in server, data:"[{},{}...]"
			array = new JSONArray(arrayString);
			if (array == null)
				return false;

			for (int i = 0; i < array.length(); ++i) {
				JSONObject object = (JSONObject) array.get(i);
				String gameName = (String) object.get("pkgname");
				String companyName = (String) object.get("description");
				String marketURI = (String) object.get("marketurl");
				String imageURI = (String) object.get("imageurl");
				int showProirity = (Integer) object.get("priority");

				Bitmap bitmap = null;
				byte[] bytes = null;
				if (!imageCache.isExist(imageURI)) { // image not exist
					bytes = DHttpClient.downloadImage(imageURI);
					if (bytes != null && bytes.length != 0) {
						bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
						imageCache.put(imageURI, bytes);
					}
				} else {
					bitmap = imageCache.get(imageURI);
				}

				/** 2015.06.01
				 * add the first uninstalled featureview
				 * 
				 */
				
				/** Deprecated
				 * add featureview game to uninstalledFeatureGames
				 * here we add two time for No.1 featuregame and one time for No.2 featuregame
				 * if both of these two games are installed, we add the third game to uninstalledFeatureGames 
				 */
				if (bitmap != null && isPackageUnInstall(mContext, DGlobalParams.getPackageName(marketURI))) {
					DFeatureGame dGame = new DFeatureGame(companyName, gameName, marketURI, bitmap, showProirity);
//					switch(showProirity) {
//						case 1:
//							uninstalledFeatureGames.add(dGame);
//							uninstalledFeatureGames.add(dGame);
//							break;
//						case 2:
//							uninstalledFeatureGames.add(dGame);
//							break;
//						case 3:
//							if(uninstalledFeatureGames.isEmpty())
//								uninstalledFeatureGames.add(dGame);
//							break;
//						default:
//							//do nothing
//					}
					uninstalledFeatureGames.add(dGame);
					break;
				}
			}
			return true;
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean isPackageUnInstall(Context context, String packagename) {
		try {
			context.getPackageManager().getPackageInfo(packagename,
					PackageManager.GET_UNINSTALLED_PACKAGES);
			return false;
		} catch (NameNotFoundException e) {
			return true;
		}
	}

	private Handler mainHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == 2) {

				final int size = mFeatureViewList.size();
				Log.i("main handler", "calling reset and feature view size = "
						+ size);
				for (int i = 0; i < size; ++i) {
					mFeatureViewList.get(i).reset();
				}
			}
		}
	};

	private static DMHandler dmHandler = null;

	private class DMHandler extends Handler {
		public DMHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				mainHandler.sendEmptyMessage(2);
			}
		}
	}

	public void getGameList(final Context context) {
		if (!DNetworkStatus.isNetworkAvailable(context)) {
			Log.w(TAG, "no network");
			return;
		}
		try {
			dmHandler = new DMHandler(DHandlerThread.getInstance().getLooper());
			dmHandler.post(new Runnable() {
				@Override
				public void run() {
//					String response = DHttpClient.post(
//							DGlobalParams.SERVER_POP_GAMES_URL,
//							new BasicNameValuePair("language", Locale
//									.getDefault().getLanguage()),
//							new BasicNameValuePair("locale",
//									DoodleMobileAnaylise.getLocale_static()),
//							new BasicNameValuePair("pi", DoodleMobileAnaylise
//									.getAllInstalledDMPkgName()));
					
					String response = DHttpClient.post(DGlobalParams.SERVER_NEW_FEATUREVIEW_URL);
					if (response == null) {
						Log.e("NewFeatureview", "error!"); 
							return;
					}
					Log.i("here", response);
					if (parse(response, context)) {
						dmHandler.sendEmptyMessage(1);
					}
				}
			});
		} catch (Exception e) {
			Log.w(TAG, "getGameList() error" + e.getMessage());
			e.printStackTrace();
		}
	}

	public void registerFeatureView(FeatureView f) {
		try {
			final int size = mFeatureViewList.size();
			for (int i = 0; i < size; ++i) {
				if (mFeatureViewList.get(i).getId() == f.getId()) {
					mFeatureViewList.remove(i);
					break;
				}
			}
			mFeatureViewList.add(f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package com.doodlemobile.gamecenter.fullscreen;

public class FullScreenGame {
	private static final String PRIFIX = "market://details?id=";
	
	public String packageName;
	public String imageURI;
	public int gameIndex;
	public int entireTimes;
	public int timesCounter;
	public long lastModified;
//	public int times;
	
	public FullScreenGame(){
		
	}
	
	public FullScreenGame(String packageName, String imageURI, int gameIndex){
		this.packageName = packageName;
		this.imageURI = imageURI;
		this.gameIndex = gameIndex;
		this.lastModified = -1;
	}
	
	public String getMarketURI(){
		return PRIFIX + packageName;
	}

	public boolean isEntiredAccrodingToTimes() {
		return entireTimes > 0 && timesCounter > entireTimes;
	}
	
	public boolean isEntiredAccrodingToLastModified() {
		if(lastModified < 0) return false;
		long now = System.currentTimeMillis();
		return (now - lastModified) > 24 * 3600 * 1000; // 24 hours * 3600 sec * 1000 millis
	}
}

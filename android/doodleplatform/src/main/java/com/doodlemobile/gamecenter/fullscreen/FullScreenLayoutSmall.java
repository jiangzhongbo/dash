package com.doodlemobile.gamecenter.fullscreen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.doodlemobile.gamecenter.DoodleMobileAnaylise;
import com.doodlemobile.gamecenter.Platform;
import com.doodlemobile.gamecenter.cache.DGlobalPrefences;
import com.doodlemobile.gamecenter.cache.PreferencesCache;

public class FullScreenLayoutSmall extends RelativeLayout{
	private Activity activity;
	private Bitmap image = null;
	private FullScreenGame game;
	private DisplayMetrics metrics;
	private Rect closeRect;
	private Rect downloadRect;
	private boolean hasClicked = false;
	private boolean isExitFullScreen = false; //判断是否是退出的小半屏，要可以点击其他地方

	public FullScreenLayoutSmall(Activity activity, FullScreenGame game, Bitmap image) {
		super(activity);
		this.activity = activity;
		this.game = game;
		this.image = image;
		if(Platform.getMetrics() == null) { 
			this.metrics = new DisplayMetrics();
			activity.getWindowManager().getDefaultDisplay().getMetrics(this.metrics);
		} else
			this.metrics = Platform.getMetrics();

		setFocusableInTouchMode(true);
		requestFocus();
		setVisibility(View.GONE);
		
//		Log.e("FullScreen", "Platform wp: " + Platform.getMetrics().widthPixels + ", Platform hp: " + Platform.getMetrics().heightPixels);
//		Log.e("FullScreen", "wp: " + metrics.widthPixels + ", hp: " + metrics.heightPixels);
	}
	
//	public FullScreenLayoutSmall prepare() {
//		return prepare(80, 85);
//	}
	/**
	 * 
	 * @param img_percentage 全屏图
	 * @param frame_percentage 图框
	 * @param maskflag 遮罩是否开启
	 * @return
	 * 
	 * 在屏幕不是480*800这种比例的分辨率下，为了保持广告图的比例，做插屏85%或60%的时候都会将最低值匹配，所以在竖屏的时候要保持广告框宽不变，高做适配，横屏相反
	 */
	public FullScreenLayoutSmall prepare(int img_percentage, int frame_percentage, boolean maskflag) {
		isExitFullScreen = maskflag;
		int ip = img_percentage;
		int fp = frame_percentage;
		View v = null;
		try {
			removeAllViews();
			String pkgName = activity.getPackageName();
			LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(activity.getResources().getIdentifier("dm_fullscreen_small", "layout", pkgName), null);
			if(isExitFullScreen) {
				ImageView mask = (ImageView) v.findViewById(activity.getResources().getIdentifier("dm_fullscreen_mask", "id", pkgName));
				mask.setVisibility(View.GONE);
			}
			RelativeLayout relativelayout = (RelativeLayout) v.findViewById(activity.getResources().getIdentifier("dm_fullscreen_relativelayout", "id", pkgName));
			ImageView imgView = (ImageView) v.findViewById(activity.getResources().getIdentifier("dm_fullscreen_image", "id", pkgName));
			imgView.setBackgroundDrawable(new BitmapDrawable(image));
			
			
			RelativeLayout imagelayout = (RelativeLayout) v.findViewById(activity.getResources().getIdentifier("dm_fullscreen_imagelayout", "id", pkgName));
			RelativeLayout.LayoutParams lp;
			int a,b;
			float wp = image.getWidth()/(float)metrics.widthPixels;
			float hp = image.getHeight()/(float)metrics.heightPixels;
//			Log.e("metrics", "wp: " + metrics.widthPixels + ", hp: " + metrics.heightPixels + ", w: " + wp + ", h: " + hp);
			
			if(wp != 0 && hp != 0) {
				if(isLanscape()) {
					a = (int) (metrics.widthPixels * ip/100 * wp/hp);
					b = metrics.heightPixels * ip/100;
				} else {
					a = metrics.widthPixels * ip/100;
					b = (int) (metrics.heightPixels * ip/100 * hp/wp);
				}
			}
			else {
				a = metrics.widthPixels * ip/100;
				b = metrics.heightPixels * ip/100;
			}
//			Log.e("image", "a: " + a + ", b: " + b);
			
			lp = new RelativeLayout.LayoutParams(a, b);
			lp.addRule(RelativeLayout.CENTER_IN_PARENT);
			imagelayout.setLayoutParams(lp);
			int dx = (metrics.widthPixels - a)/2;
			int dy = (metrics.heightPixels - b)/2;
			if(isLanscape()){
				closeRect = new Rect(dx + a * 9 / 10, dy, dx + a, dy + b / 5);
//				downloadRect = new Rect(dx + a / 6, dy + b * 2 / 3,  dx + a / 2, dy + b);
				downloadRect = new Rect(dx, dy, dx + a, dy +b);
			} else {
				closeRect = new Rect(dx + a * 4 / 5, dy, dx + a, dy + b / 10);
//				downloadRect = new Rect(dx, dy + b * 4 / 5, dx + a / 2, dy + b);
				downloadRect = new Rect(dx, dy, dx + a, dy + b);
			}
			
			RelativeLayout framelayout = (RelativeLayout) v.findViewById(activity.getResources().getIdentifier("dm_fullscreen_framelayout", "id", pkgName));
			RelativeLayout.LayoutParams lp_f;
			int af,bf;
			if(wp != 0 && hp != 0) {
				if(isLanscape()) {
					af = (int) (metrics.widthPixels * fp/100 * wp/hp);
					bf = metrics.heightPixels * fp/100;
				} else {
					af = metrics.widthPixels * fp/100;
					bf = (int) (metrics.heightPixels * fp/100 * hp/wp);
				}
			}
			else {
				af = metrics.widthPixels * fp/100;
				bf = metrics.heightPixels * fp/100;
			}
			
//			Log.e("full_layout", "af: " + af + ", bf: " + bf);
			lp_f = new RelativeLayout.LayoutParams(af, bf);
			lp_f.addRule(RelativeLayout.CENTER_IN_PARENT);
			framelayout.setLayoutParams(lp_f);
			
			AlphaAnimation alphaAnim = new AlphaAnimation(0.0f, 1.0f);
			ScaleAnimation scaleAnim = new ScaleAnimation(0.1f, 1.0f, 0.1f, 1.0f, Animation.RELATIVE_TO_PARENT, 0.5f, Animation.RELATIVE_TO_PARENT, 0.5f);
			AnimationSet animSet = new AnimationSet(true);
			animSet.addAnimation(alphaAnim);
			animSet.addAnimation(scaleAnim);
			animSet.setDuration(500);
			relativelayout.startAnimation(animSet);
			
//			RotateAnimation rotateAnim = new RotateAnimation(0.0f, 350.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//			rotateAnim.setDuration(500);
//			relativelayout.startAnimation(rotateAnim);
			
//			TranslateAnimation translateAnim = new TranslateAnimation(100, 100, 200, 200);
//			translateAnim.setDuration(1000);
//			translateAnim.setInterpolator(new AccelerateInterpolator());
//			relativelayout.startAnimation(translateAnim);
		} catch(Exception e) {
			e.printStackTrace();
			Log.e("FullScreenSmall", "Exception status: dm_fullscreen_small view=" + v);
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
			Log.e("FullScreenSmall", "OutOfMemoryError status: dm_fullscreen_small view=" + v);
			throw e;
		}
		
		
		addView(v);
		return this;
	}
	
	public void show(){
		if(activity == null) 
			return;

		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				setVisibility(View.VISIBLE);
				FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(metrics.widthPixels, metrics.heightPixels, Gravity.CENTER);
				activity.addContentView(FullScreenLayoutSmall.this, layoutParams);
				DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, game.packageName, "Appear", "FullScreen_Small_" + game.packageName, false);
			}
		});
	}
	
	public boolean isShowing(){
		return getVisibility() == View.VISIBLE;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		//只有在不是小半屏的时候才截获touch
		if(!isExitFullScreen) {
			if(event.getAction() != MotionEvent.ACTION_UP) {
				return true; 
			}
			if (inRect(event, closeRect)) {
				if(Platform.getActivity() != null) {
					Platform.getHandler(activity).sendEmptyMessage(Platform.CLOSE_FULLSCREEN_SMALL);
				}
				return true;
			} else if (inRect(event, downloadRect)) {
				hasClicked = true;
				PreferencesCache cache = PreferencesCache.getInstance(activity);
				cache.putBoolean("hasclicked", hasClicked);
				try {
//					String a = game.getMarketURI() + DGlobalPrefences.FStail + DoodleMobileAnaylise.getPackageName();
//					Log.e("FullScreenSmall", "onTouchEvent: " + a);
					if(Resources.fullScreenClisckListener != null) {
		  				Resources.fullScreenClisckListener.onFullScreenClicked();
		  			}
					Uri uri = Uri.parse(game.getMarketURI() + DGlobalPrefences.FStail + DoodleMobileAnaylise.getPackageName());
					Intent intent = new Intent(Intent.ACTION_VIEW, uri);
					activity.startActivity(intent);
					Platform.getHandler(activity).sendEmptyMessage(Platform.CLOSE_FULLSCREEN_SMALL);
				} catch (Exception e) {
					Toast.makeText(activity, "Open Android Market Failed ... ", Toast.LENGTH_SHORT).show();
					e.printStackTrace();
				}
				DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, game.packageName, "Clicks", "FullScreen_Small_" + game.packageName, false);
				Platform.getHandler(activity).sendEmptyMessage(Platform.CLOSE_FULLSCREEN);
				return true;
			}
		} else if(event.getAction() == MotionEvent.ACTION_DOWN) {
			if (inRect(event, closeRect)) {
				Platform.getHandler(activity).sendEmptyMessage(Platform.CLOSE_FULLSCREEN_SMALL);
				return true;
			} else if (inRect(event, downloadRect)) {
				hasClicked = true;
				PreferencesCache cache = PreferencesCache.getInstance(activity);
				cache.putBoolean("hasclicked", hasClicked);
				try {
					if(Resources.fullScreenClisckListener != null) {
		  				Resources.fullScreenClisckListener.onFullScreenClicked();
		  			}
					Uri uri = Uri.parse(game.getMarketURI() + DGlobalPrefences.FStail + DoodleMobileAnaylise.getPackageName());
					Intent intent = new Intent(Intent.ACTION_VIEW, uri);
					activity.startActivity(intent);
					Platform.getHandler(activity).sendEmptyMessage(Platform.CLOSE_FULLSCREEN_SMALL);
				} catch (Exception e) {
					Toast.makeText(activity, "Open Android Market Failed ... ", Toast.LENGTH_SHORT).show();
					e.printStackTrace();
				}
				DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, game.packageName, "Clicks", "FullScreen_Small_" + game.packageName, false);
				Platform.getHandler(activity).sendEmptyMessage(Platform.CLOSE_FULLSCREEN);
				return true;
			}
		}
		return false;
	}
	
	
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			if(Platform.getActivity() != null) {
				Platform.getHandler(activity).sendEmptyMessage(Platform.CLOSE_FULLSCREEN_SMALL);
				return true;
			}
		}
		return super.onKeyUp(keyCode, event);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK)
			return true;
		return super.onKeyDown(keyCode, event);
	}
	
	private boolean isLanscape() {
		return metrics.widthPixels > metrics.heightPixels;
	}
	
	private boolean inRect(MotionEvent event, Rect rect) {
        if(event.getX() > rect.left && event.getX() < rect.right && 
           event.getY() > rect.top && event.getY() < rect.bottom) 
            return true;
        else
            return false;
    }
}

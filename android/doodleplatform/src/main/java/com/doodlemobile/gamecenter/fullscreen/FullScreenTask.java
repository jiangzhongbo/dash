package com.doodlemobile.gamecenter.fullscreen;

import org.apache.http.NameValuePair;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.doodlemobile.gamecenter.Platform;
import com.doodlemobile.gamecenter.cache.ImageCache;
import com.doodlemobile.gamecenter.fullscreen.strategy.LoadNetworkElsePreference;
import com.doodlemobile.gamecenter.net.DHttpClient;

public class FullScreenTask extends AsyncTask<NameValuePair, Integer, FullScreenGame> {
	private boolean hasCancelled = false;
	private Activity activity = null;
	
	private GetFSGameStrategy strategy;
	
	public FullScreenTask(){
		this.activity = Platform.getActivity();
		
		strategy = new LoadNetworkElsePreference(activity);
	}

	protected FullScreenGame doInBackground(NameValuePair... params) {
		if(activity == null)
			return null;
		
		try{
			// 先获取游戏
			Log.i("FullScreen", "get full screen game");
			FullScreenGame game = strategy.getFSGame();
			if(game == null) return null;
	
			// 再去获取全屏的图像
			Log.i("FullScreen", "get full screen image " + game.imageURI);
			checkOrientation(game);
			ImageCache imageCache = ImageCache.getInstance(activity);
			if (imageCache.isExist(game.imageURI)) {
				Resources.fullScreenImage = imageCache.get(game.imageURI);
				Resources.fullScreenImage_small = imageCache.get(game.imageURI);
			} else {
				Log.d("imageURI", "" +game.imageURI);
				byte[] bytes = DHttpClient.downloadImage(game.imageURI);
				if(bytes != null) {
					imageCache.put(game.imageURI, bytes);
					Resources.fullScreenImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
					Resources.fullScreenImage_small = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
				}
			}
			if (Resources.fullScreenImage == null) {
				Log.w("FullScreen", "image can not be ready");
				return null;
			}
			// 准备就绪
			return game;
		}catch(NullPointerException e) {
			e.printStackTrace();
		}
		return null;
	}

	

	protected void onPostExecute(FullScreenGame game) {
		String message = "DefaultFullScreenResult";
		int retCode = -1;
		if(hasCancelled) {
			message = "FullScreenTask has cancelled!";
		}
		else if (game == null) {
			message = "none display!cache will be deleted";
			strategy.deleteCache();
			return;
		} else {
			message = "FullScreenReady";
			retCode = 0;
			Resources.fullScreen = new FullScreenLayout(activity, game, Resources.fullScreenImage);
			Resources.fullScreen_small = new FullScreenLayoutSmall(activity, game, Resources.fullScreenImage_small);
//			Resources.fullScreen.prepare().show();
//			handler.sendEmptyMessageDelayed(Platform.CLOSE_FULLSCREEN, Platform.DEFAULT_FULLSCREEN_LAST_TIME);
		}
		
		if(Resources.getFullScreenResultListener != null) {
			Resources.getFullScreenResultListener.onFullScreenResultRecived(message, retCode);
		}
	}

	@Override
	protected void onCancelled() {
		Resources.fullScreen = null;
		if(Resources.fullScreenImage != null && !Resources.fullScreenImage.isRecycled()){
			Resources.fullScreenImage.recycle();
		}
		Resources.fullScreenImage = null;
		super.onCancelled();
	}
	
	private void checkOrientation(FullScreenGame game) {
//		Log.e("FullScreen", "orientation: " + Platform.getOrientation());
		if (Configuration.ORIENTATION_LANDSCAPE == Platform.getOrientation()) {
			if (game.imageURI.endsWith(".jpg"))
				game.imageURI = game.imageURI.replaceAll(".jpg", "_l.jpg");
			if (game.imageURI.endsWith(".png"))
				game.imageURI = game.imageURI.replaceAll(".png", "_l.png");
		}
	}
	
	public void cancelTask() {
		this.hasCancelled = true;
		if(getStatus() != Status.FINISHED)
			this.cancel(false);
	}

}

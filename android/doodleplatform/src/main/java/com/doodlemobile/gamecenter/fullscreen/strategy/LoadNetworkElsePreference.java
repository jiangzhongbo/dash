package com.doodlemobile.gamecenter.fullscreen.strategy;

import android.app.Activity;
import android.util.Log;

import com.doodlemobile.gamecenter.cache.PreferencesCache;
import com.doodlemobile.gamecenter.fullscreen.FullScreenGame;
import com.doodlemobile.gamecenter.fullscreen.GetFSGameStrategy;

public class LoadNetworkElsePreference extends GetFSGameStrategy {
	
	enum From {
		Net, Cache, Nowhere
	}

	public LoadNetworkElsePreference(Activity activity) {
		super(activity);
	}

	@Override
	public FullScreenGame getFSGame() {
		From from = From.Nowhere;
		Log.i("FullScreen", "LOAD NET ELSE PREFERENCE");
		String gameJsonStr = getGameJsonStringFromNet();
		if(gameJsonStr == null) {
			Log.i("FullScreen", "return null from net, try to get from preference");
			gameJsonStr = getGameJsonStringFromPreference();
			if(gameJsonStr != null){
				from = From.Cache;
				Log.i("FullScreen", "get content from cache");
			}
		} else if(gameJsonStr.equals("you have installed all fs games.")) {
			return null;
		} else {
			Log.i("FullScreen", "get content from net");
			PreferencesCache.getInstance(mainActivity).delete("hasclicked");
			from = From.Net;
		}
		
		FullScreenGame fsGame = null;
		Log.i("FullScreen", "parse game json string");
		if(from != From.Nowhere){
			fsGame =  parseJsonStringToFSGame(gameJsonStr);
			if(fsGame.isEntiredAccrodingToLastModified())
				return null;
			if(from == From.Net)
				saveFSGameToPreference(fsGame);
		}
		return fsGame;
	}

}

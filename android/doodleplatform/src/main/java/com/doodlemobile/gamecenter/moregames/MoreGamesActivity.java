package com.doodlemobile.gamecenter.moregames;

import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.doodlemobile.gamecenter.DoodleMobileAnaylise;
import com.doodlemobile.gamecenter.cache.DGlobalPrefences;
import com.doodlemobile.gamecenter.net.DNetworkStatus;
import com.doodlemobile.gamecenter.utils.DGlobalParams;

/**
 * @author yanggang
 * @date 2012-08-24
 */

public class MoreGamesActivity extends Activity {
	private static final long CACHE_HOURS = 24L;		
	
	private static WebView mWebView = null;
	private Activity mActivity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.getWindow().requestFeature(Window.FEATURE_PROGRESS);
		this.getWindow().setFeatureInt(Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);

		mActivity = this;

		initMoreGames();
	}
	
	private void initMoreGames() {
		try {
			if(DoodleMobileAnaylise.getIsFirstInitialized() == true) {
				DoodleMobileAnaylise.onCreate(this);
			}
			
			if(DoodleMobileAnaylise.gAppType == DoodleMobileAnaylise.DMTYPE_APPS) {
				DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, "moreapps", "Appear", "MoreGamesActivity", false);
			} else {
				DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, "moregames", "Appear", "MoreGamesActivity", false);
			}
			
			mWebView = new WebView(this);
			
			if (mWebView != null) {
				setContentView(mWebView);		// set view
				
				mWebView.requestFocus();
//				mWebView.addJavascriptInterface(new DoodleJS(), "doodle");		// insert js to html, upload installed pkgs
				mWebView.setWebViewClient(new MyWebViewClient());
				mWebView.setWebChromeClient(new WebChromeClient(){
					@Override
					public void onProgressChanged(WebView view, int progress) {
						MoreGamesActivity.this.setTitle("Loading...");
						MoreGamesActivity.this.setProgress(progress*100);
						if(progress >= 80){
							if(DoodleMobileAnaylise.gAppType == DoodleMobileAnaylise.DMTYPE_APPS) {
								MoreGamesActivity.this.setTitle(" More Apps");
							} else {
								MoreGamesActivity.this.setTitle(" More Games");
							}
						}
					}
				});
				
				mWebView.setOnKeyListener(new View.OnKeyListener() {	// webView can go back
					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						if(keyCode == KeyEvent.KEYCODE_BACK && mWebView.canGoBack()) {
							mWebView.goBack();
							return true;
						}
						return false;
					}
				});

				WebSettings webSettings = mWebView.getSettings();
				if (webSettings != null) {
					webSettings.setDefaultTextEncodingName("utf-8");
					webSettings.setJavaScriptEnabled(true);		// enable javascript for Google Analytics
					setWebSettingsCache(webSettings);
				}

				if(DoodleMobileAnaylise.gAppType == DoodleMobileAnaylise.DMTYPE_APPS){
					mWebView.loadUrl(DGlobalParams.SERVER_MOREAPPS_URL);
				} else {
					mWebView.loadUrl(DGlobalParams.SERVER_MOREGAMES_URL);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	private final class DoodleJS {
		@SuppressWarnings("unused")
		public String getInstalledPackages() {
			StringBuffer mInstalledPkgsBuffer = new StringBuffer();
			try {
				mInstalledPkgsBuffer.append(DoodleMobileAnaylise.getAndroidVersion()).append("=");
				mInstalledPkgsBuffer.append(DoodleMobileAnaylise.getPackageName()).append("=");
				mInstalledPkgsBuffer.append(DoodleMobileAnaylise.getAllInstalledDMPkgName());
			} catch (Exception e) {
				e.printStackTrace();
				mInstalledPkgsBuffer.append("com.threed.bowling");
			}
			return mInstalledPkgsBuffer.toString();
		}

		@SuppressWarnings("unused")
		public void backActivity() {
			new AlertDialog.Builder(mActivity)
			.setTitle("More Apps")
			.setMessage("There is no more apps, please back")
			.setPositiveButton("Ok", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mActivity.finish();
				}
			}).show();
		}
	}
	
	
	private void setWebSettingsCache(WebSettings webSettings){
		try {
			if (DGlobalPrefences.isFirstOpen()) {	// is first open DGlobalPrefences
				new DGlobalPrefences(this);
			}
		
			Date nowDate = new Date();
			long lNowDate = nowDate.getTime();
			long lPreferDate = DGlobalPrefences.getMoreGamesCacheTime();
			long lDays = (lNowDate - lPreferDate) / (3600000);		// 3600 * 1000ms
			
			if(DNetworkStatus.isNetworkAvailable(mActivity) && lDays >= CACHE_HOURS) {	
				mWebView.clearCache(true);
				mWebView.clearHistory();
				DGlobalPrefences.setMoreGamesCacheTime(lNowDate);		
			} else {
				webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** Class of ClientWebView  */
	class MyWebViewClient extends WebViewClient{
		@Override
		public boolean shouldOverrideUrlLoading(WebView webView, String url){
			try {
				Uri uri = Uri.parse(url);
				Intent intentMarket = new Intent(Intent.ACTION_VIEW, uri);		// open with market
				startActivity(intentMarket);
				
				DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, url.split("=")[1], "Clicks", "MoreGamesActivity", false);
			} catch (Exception e) {
				e.printStackTrace();
				
				if(DNetworkStatus.isNetworkAvailable(mActivity)) {
					url = url.replace("market://", "http://play.google.com/store/apps/");	// exception, then open with url
					webView.loadUrl(url);
				} else {
					DNetworkStatus.OpenNetWork(mActivity);
				}
			}
			return true;
		}
		
		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			Toast.makeText(mActivity, "Load url error, " + description, Toast.LENGTH_LONG).show();
			if(view.canGoBack()) {
				view.goBack();
			}
		}
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		if(DoodleMobileAnaylise.getIsFirstInitialized() == false) {
			DoodleMobileAnaylise.onStop(this);
		}
	}
}

package com.doodlemobile.gamecenter.moregames;

import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.doodlemobile.gamecenter.DoodleMobileAnaylise;
import com.doodlemobile.gamecenter.cache.DGlobalPrefences;
import com.doodlemobile.gamecenter.net.DNetworkStatus;
import com.doodlemobile.gamecenter.utils.DGlobalParams;

public class MoreGamesLayout extends RelativeLayout {
	
	private static final String TAG = "MoreGamesLayout";
	private static final int CACHE_HOURS = 24;		
	
	private WebView mWebView = null;
	private ProgressDialog mProgressDialog = null;
	private Activity mActivity;
	
	public MoreGamesLayout(Context context) {
		super(context);
		
		this.mActivity = (Activity)context;
		
		initMoreGames();
		
		setFocusableInTouchMode(true);
		requestFocus();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if( keyCode == KeyEvent.KEYCODE_BACK){
			setVisibility(View.GONE);
			((ViewManager)getParent()).removeView(this);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private void initMoreGames() {
		LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		String pkgName = mActivity.getPackageName();
		
		try {
			int layoutId = mActivity.getResources().getIdentifier("dm_moregames_layout", "layout", pkgName);
			RelativeLayout moregamesLayout = (RelativeLayout) inflater.inflate(layoutId, this);
			mWebView = (WebView)moregamesLayout.findViewById(mActivity.getResources().getIdentifier("dm_moregames_webview", "id", pkgName));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (mWebView != null) {
			mWebView.requestFocus();
			mWebView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
			WebSettings webSettings = mWebView.getSettings();
			if (webSettings != null) {
				webSettings.setDefaultTextEncodingName("utf-8");
				webSettings.setJavaScriptEnabled(true);			// enable javascript for Google Analytics
				setWebSettingsCache(webSettings);
			}
			
			MyWebViewClient myWebViewClient = new MyWebViewClient();		// open google play market
			mWebView.setWebViewClient(myWebViewClient);
			
			// show progress dialog
			mProgressDialog = new ProgressDialog(mActivity);
			if (mProgressDialog != null) {
				mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				mProgressDialog.setMessage("Loading...");
				mProgressDialog.setMax(100);
				mProgressDialog.setCancelable(true);
				mProgressDialog.show();

				mWebView.setWebChromeClient(new WebChromeClient() {
					@Override
					public void onProgressChanged(WebView view, int progress) {
						mProgressDialog.setProgress(progress);
						if(progress >= 50){
							mProgressDialog.dismiss();
						}
					}
				});
			}
			
			// webView can go back
			mWebView.setOnKeyListener(new View.OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					if (keyCode == KeyEvent.KEYCODE_BACK && mWebView.canGoBack()) {
						mWebView.goBack();
						return true;
					}
					return false;
				}
			});

				
			mWebView.addJavascriptInterface(jsInsertObj(), "doodle");		// insert js to html, upload installed pkgs
			if(DoodleMobileAnaylise.gAppType == DoodleMobileAnaylise.DMTYPE_APPS){
				mWebView.loadUrl(DGlobalParams.SERVER_MOREAPPS_URL);
			} else {
				mWebView.loadUrl(DGlobalParams.SERVER_MOREGAMES_URL);
			}
		}
	}
	
	private Object jsInsertObj(){
		Object insertJSObj = new Object(){
			@SuppressWarnings("unused")
			public String getInstalledPackages(){
				StringBuffer mInstalledPkgsBuffer = new StringBuffer();;
				try {
					mInstalledPkgsBuffer.append(DoodleMobileAnaylise.getAndroidVersion()).append("=");
					mInstalledPkgsBuffer.append(DoodleMobileAnaylise.getPackageName()).append("=");
					mInstalledPkgsBuffer.append(DoodleMobileAnaylise.getAllInstalledDMPkgName());
				} catch (Exception e) {
					e.printStackTrace();
					mInstalledPkgsBuffer.append("com.threed.bowling");
				}
				return mInstalledPkgsBuffer.toString();
			}
		};
		return insertJSObj;
	}
	
	private void setWebSettingsCache(WebSettings webSettings){
		if (DGlobalPrefences.isFirstOpen()) {	// is first open DGlobalPrefences
			try {
				new DGlobalPrefences(mActivity);
				DGlobalPrefences.setIsFirstOpen(false);
			} catch (Exception e) {
				e.printStackTrace();
				webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
			}
		}
		
		Date nowDate = new Date();
		long lDays = Long.MAX_VALUE;
		try {
			long lNowDate = nowDate.getTime();
			long lPreferDate = DGlobalPrefences.getMoreGamesCacheTime();
			lDays = (lNowDate - lPreferDate) / (3600000);
			
			if(DNetworkStatus.isNetworkAvailable(mActivity) && lDays >= CACHE_HOURS) {	
				mWebView.clearCache(true);
				mWebView.clearHistory();
				DGlobalPrefences.setMoreGamesCacheTime(lNowDate);		// update time
			} else {							
				webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
			}
		} catch (Exception e) {
			Log.w(TAG, "setWebSettingsCache() error, " + e.getMessage());
		}
	}
	
	/** Class of ClientWebView  */
	class MyWebViewClient extends WebViewClient{
		@Override
		public boolean shouldOverrideUrlLoading(WebView webView, String url){
			try {
				Uri uri = Uri.parse(url);
				Intent intentMarket = new Intent(Intent.ACTION_VIEW, uri);		// open with market
				mActivity.startActivity(intentMarket);
				
				DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, url.split("=")[1], "Clicks", "MoreGamesLayout", false);
			} catch (Exception e) {
				e.printStackTrace();
				
				if(DNetworkStatus.isNetworkAvailable(mActivity)) {
					url = url.replace("market://", "http://play.google.com/store/apps/");	// exception, then open with url
					webView.loadUrl(url);
				} else {
					DNetworkStatus.OpenNetWork(mActivity);
				}
			}
			return true;
		}
		
		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			Toast.makeText(mActivity, "Load url error, " + description, Toast.LENGTH_LONG).show();
			if(view.canGoBack()) {
				view.goBack();
			}
		}
	}
}

package com.doodlemobile.gamecenter.net;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

public class DNetworkStatus {
	private final static String TAG = "DNetWorkStatus";
	
	public static boolean isNetworkAvailable(Context context) {
		if(context == null) {
			return false;
		}
		
		try {
			ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connManager != null && connManager.getActiveNetworkInfo() != null) {
				return connManager.getActiveNetworkInfo().isAvailable();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	
	/** open network dialog  */
	public static void OpenNetWork(final Activity activity){
		if(activity == null) {
			return;
		}
		
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
			builder.setTitle("No network").setMessage("Do you want to setup network?");
			
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = null;
					
					try {
						String sdkVersion = android.os.Build.VERSION.SDK;
						int isdkVersion = Integer.valueOf(sdkVersion);
						if(isdkVersion >= 14){
							intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
						}
						else if(Integer.valueOf(sdkVersion) >= 10) {
							intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
						}else {
							intent = new Intent();
							ComponentName comp = new ComponentName("com.android.settings", "com.android.settings.WirelessSettings");
							intent.setComponent(comp);
							intent.setAction("android.intent.action.VIEW");
						}
						activity.startActivity(intent);
					} catch (Exception e) {
						Log.w(TAG, "open network settings failed, please check...");
						e.printStackTrace();
					}
				}
			}).setNegativeButton("No", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();		
				}
			}).show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package com.doodlemobile.gamecenter.utils;

import android.content.Context;
import android.util.Log;

public class DGlobalParams {
	/** Server_CODE = "code" */
	public static final String Server_CODE = "code";
	/** Server_MSG = "msg" */
	public static final String Server_MSG = "msg";
	/** Server_DATA = "data" */
	public static final String Server_DATA = "data";

	public final static String SERVER_ANALYTICS_URL = "http://data2.doodlemobile.com:8080/dmdata_zmm/ReceiveServlet";
//	public final static String SERVER_ANALYTICS_URL = "http://ec2-184-169-145-230.us-west-1.compute.amazonaws.com:8080/dmdata_test/ReceiveServlet";


//	public final static String SERVER_POP_GAMES_URL = "http://f.doodlemobile.com:8080/feature_server_v3/recommands";
	public final static String SERVER_NEW_FEATUREVIEW_URL = "http://newfeatureview.perfectionholic.com/featureview/getfeatureview/";
	public final static String SERVER_NEW_FEATUREVIEW_TIME_URL = "http://newfeatureview.perfectionholic.com/featureview/gettime/";
	public final static String SERVER_TEST_FEATUREVIEW_TIME_URL = "http://ec2-184-73-77-17.compute-1.amazonaws.com/featureview/gettime/";
	public final static String SERVER_POP_APPS_URL = "http://featured.perfectionholic.com:8080/feature_appserver/recommands";

//	public final static String SERVER_FULLSCREEN_URL = "http://f.doodlemobile.com:8080/feature_server_v3/featurescreen";
//	public final static String SERVER_FULLSCREEN_URL = "http://192.168.1.237:9080/feature_server_v3/featurescreen";
	public final static String SERVER_FULLSCREEN_URL = "http://192.168.1.241:8080/feature_server/featurescreen";
	
	//server for client to send full screen clicks
//	public final static String SERVER_FULLSCREEN_CLICK_URL = "http://192.168.1.83:8080/doodleserver/receivefeaturescreenclick";
	
	//server for client to send user behavior

	public final static String SERVER_USER_BEHAVIOR_URL = "http://192.168.1.241:8080/featureserver/receiveuesraction";
	
	public final static String SERVER_MOREGAMES_URL = "http://featured.perfectionholic.com:8080/moregames/index.html";	
	public final static String SERVER_MOREAPPS_URL = "http://featured.perfectionholic.com:8080/moregames/index_app.html";	
	
	public static boolean isOldGames(Context context) {
		String pkgName = context.getPackageName();
		try {
			return pkgName.equals("com.wordsmobile.slot")
				|| pkgName.equals("com.kiwifruitmobile.sudoku")
				|| pkgName.equals("com.candydroid.breakblock")
				|| pkgName.equals("com.forthblue.pool")
				|| pkgName.equals("com.wordsmobile.hunterville")
				|| pkgName.equals("com.shootbubble.bubbledexlue")
				|| pkgName.equals("com.sniper.activity")
				|| pkgName.equals("com.wordsmobile.rugby")
				|| pkgName.equals("com.sword.game.bubble")
				|| pkgName.equals("com.baileyz.bluppypro")
				|| pkgName.equals("com.threed.bowling")
				|| pkgName.equals("com.doodle.restaurant")
				|| pkgName.equals("com.tapglider")
				|| pkgName.equals("com.leagem.chesslive");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	// xuming fullscreen
	public static boolean isInstalledTheseGames(String packageName) {
		return    packageName.equals("com.tapglider")
		       || packageName.equals("x.JewelsDeluxe")
		       || packageName.equals("com.threed.bowling")
		       || packageName.equals("com.wordsmobile.RollerBall")
		       || packageName.equals("com.a1.game.vszombies")
		       || packageName.equals("com.sniper.activity")
		       || packageName.equals("com.shootbubble.bubbledexlue")
		       || packageName.equals("com.forthblue.pool")
		       || packageName.equals("com.candydroid.breakblock")
		       || packageName.equals("com.junerking.ninjia");
	}
	
	public static String getPackageName(String MarketUri) {
		String packageName = null;
		String re1 = "\\?id=";
		String re = "[^\\.A-Za-z0-9]";
		try {
			String prefix = MarketUri.split(re1)[1];
			if(prefix != null) {
				packageName = prefix.split(re)[0];
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return packageName;
	}
}

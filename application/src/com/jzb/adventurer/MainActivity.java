package com.jzb.adventurer;

import java.util.Calendar;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import com.doodlemobile.gamecenter.Platform;
import com.doodlemobile.gamecenter.billing.Goods;
import com.doodlemobile.gamecenter.billing.Store;
import com.doodlemobile.gamecenter.featuregames.FeatureView;
import com.doodlemobile.gamecenter.fullscreen.Resources.FullScreenCloseListener;
import com.doodlemobile.gamecenter.fullscreen.Resources.GetServerTimeListener;
import com.flurry.android.FlurryAgent;
import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;
import com.tapjoy.TapjoyConnect;

public class MainActivity extends UnityPlayerActivity {
	private String FLURRY_ID = "XZPSVKMH3YGPT8JNSMYV";
	private boolean af;

	String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs8RzQ8lj1xSuEx2jHuaNhLIf3qFM3FB/cH8muhjTtyRPrjdkcsA5EjslJTnCAG/Zkse94ODfNgXznlr+3UmfAcYxxyxqjgJ6Juppn0BwDosZfF5u7TNDg3TIeaw+OwZhxWNhpraSnvrsXlz24EidjMD8edGVJJy0s+VyGzBOhUS3PtmcBQ8YlsNFRDUoydAd4eTYNceFVs3IDUjuWfCB1jTZPPGyg9tgOdOQBubouIXeLCXIIpykt5ztQU4xTXogml3vwjtnCsfEiDMXKJBMCYTgXHtWsvkXksr6RZhMf0sTJb1X5Wlb0vLQlo7mbqhnI3WLdp3WdkMtr/euJUpqjQIDAQAB";

	private static final String SKU_ID[] = { "bucks1_99", "bucks4_99",
			"bucks9_99", "bucks19_99", "bucks49_99", "bucks99_99",

			"golds1_99", "golds4_99", "golds9_99", "golds19_99", "golds49_99",
			"golds99_99" };
	private Store store = new Store(base64EncodedPublicKey,
			new Goods(SKU_ID[0]) {
				@Override
				public void onPurchaseSuccess() {
					MainActivity.this.onPurchaseSuccess(0);
				}
			}, new Goods(SKU_ID[1]) {
				@Override
				public void onPurchaseSuccess() {
					MainActivity.this.onPurchaseSuccess(1);
				}
			}, new Goods(SKU_ID[2]) {
				@Override
				public void onPurchaseSuccess() {
					MainActivity.this.onPurchaseSuccess(2);
				}
			}, new Goods(SKU_ID[3]) {
				@Override
				public void onPurchaseSuccess() {
					MainActivity.this.onPurchaseSuccess(3);
				}
			}, new Goods(SKU_ID[4]) {
				@Override
				public void onPurchaseSuccess() {
					MainActivity.this.onPurchaseSuccess(4);
				}
			}, new Goods(SKU_ID[5]) {
				@Override
				public void onPurchaseSuccess() {
					MainActivity.this.onPurchaseSuccess(5);
				}
			}, new Goods(SKU_ID[6]) {
				@Override
				public void onPurchaseSuccess() {
					MainActivity.this.onPurchaseSuccess(6);
				}
			}, new Goods(SKU_ID[7]) {
				@Override
				public void onPurchaseSuccess() {
					MainActivity.this.onPurchaseSuccess(7);
				}
			}, new Goods(SKU_ID[8]) {
				@Override
				public void onPurchaseSuccess() {
					MainActivity.this.onPurchaseSuccess(8);
				}
			}, new Goods(SKU_ID[9]) {
				@Override
				public void onPurchaseSuccess() {
					MainActivity.this.onPurchaseSuccess(9);
				}
			}, new Goods(SKU_ID[10]) {
				@Override
				public void onPurchaseSuccess() {
					MainActivity.this.onPurchaseSuccess(10);
				}
			}, new Goods(SKU_ID[11]) {
				@Override
				public void onPurchaseSuccess() {
					MainActivity.this.onPurchaseSuccess(11);
				}
			});
	public final Handler billHandler = store.getBillingHandler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);   
		Platform.setFull_Admob_ID("ca-app-pub-3403243588104548/8270448916");
		Platform.setGoogleAnalyticsID("UA-77336629-1");
		Platform.onCreate(this, false);
		af = adFree();
		try {
			store.onCreate(this);
		} catch (Exception e) {
			Log.d("purchase e", e.getMessage());
		}
		if (!af) {
			Platform.setFullScreenCloseListener(new FullScreenCloseListener() {
				@Override
				public void onFullSCreenClosed() {
					Log.e("Unity", "######## FullScreen Closed!");
					// UnityHandler.getInstance().sendMessage("Back",
					// "CloseAdInQuitGame", null);
					UnityPlayer.UnitySendMessage("__Back", "CloseAdInQuitGame",
							"");
				}
			});
		}
		Platform.setGetServerTimeListener(new GetServerTimeListener(){

			@Override
			public void onServerTimeRecived(long servertime) {
				UnityPlayer.UnitySendMessage("__Back", "ServerTimeCallback",
						servertime+"");
			}
			
		});
		AlarmReceiver.Register(this);
		NotificationHelper.clearNotification();
		
		SharedPreferences sp = this.getSharedPreferences(
				"com.jzb.adventurer", Context.MODE_WORLD_WRITEABLE);
		long now = Calendar.getInstance().getTimeInMillis();
		sp.edit().putLong("start_time", now).commit();
		sp.edit().putBoolean("showed", false).commit();
		try {
			TapjoyConnect.requestTapjoyConnect(this,
					"0631ab46-b364-4e06-aa18-37de2dfcad4e",
					"YKBAlEshjpVUdnVyCjUz");
		} catch (Exception e) {
			Log.d("TapjoyConnect e", e.getMessage());
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		Platform.onStart();
		FlurryAgent.onStartSession(this, FLURRY_ID);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Platform.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
		Platform.onStop();
		SharedPreferences sp = this.getSharedPreferences(
				"com.jzb.adventurer", Context.MODE_WORLD_WRITEABLE);
		int crate_is_unlocking = sp.getInt("crate_is_unlocking", 0);
		int crate_storage_all_count = sp.getInt("crate_storage_all_count", 0);
		int notification_option = sp.getInt("notification_option", 1);
		Log.d("###", "###^^^^onStop");
		Log.d("###", "###^^^^crate_is_unlocking:"+ crate_is_unlocking+"");
		Log.d("###", "###^^^^crate_storage_all_count:"+ crate_storage_all_count+"");
		Log.d("###", "###^^^^notification_option:"+ notification_option+"");
		if(crate_is_unlocking == 0 && crate_storage_all_count > 0){
			ShowUnlockNotification();
		}
		sp.edit().putBoolean("open_msg_showed", false).commit();
	}

	@Override
	protected void onResume() {
		super.onResume();
		NotificationHelper.clearNotification();
		Platform.onResume();
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.cancelAll();
		
//		
//		SharedPreferences sp = this.getSharedPreferences(
//				"com.jzb.adventurer", Context.MODE_WORLD_WRITEABLE);
//		sp.edit().putBoolean("open_msg_showed", false).commit();;
	}

	public void CallOnQuit(){
		SharedPreferences sp = this.getSharedPreferences(
				"com.jzb.adventurer", Context.MODE_WORLD_WRITEABLE);
		int crate_is_unlocking = sp.getInt("crate_is_unlocking", 0);
		int crate_storage_all_count = sp.getInt("crate_storage_all_count", 0);
		int notification_option = sp.getInt("notification_option", 1);
		Log.d("###", "###^^^^CallOnQuit");
		Log.d("###", "###^^^^crate_is_unlocking:"+ crate_is_unlocking+"");
		Log.d("###", "###^^^^crate_storage_all_count:"+ crate_storage_all_count+"");
		Log.d("###", "###^^^^notification_option:"+ notification_option+"");
		if(crate_is_unlocking == 0 && crate_storage_all_count > 0){
			ShowUnlockNotification();
		}
		sp.edit().putBoolean("open_msg_showed", false).commit();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		Platform.onDestroy();
		
	}

	public void Rate() {
		final String appPackageName = getPackageName();
		try {
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("market://details?id=" + appPackageName)));
		} catch (android.content.ActivityNotFoundException anfe) {
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://play.google.com/store/apps/details?id="
							+ appPackageName)));
		}
	}

	public void MoreGames() {
		Platform.getHandler(this).sendEmptyMessage(
				Platform.START_MOREGAMES_ACTIVITY);
	}

	public void ShowOpenNotification() {
		NotificationHelper.showNotification(this, "Your chest is ready to open!");
	}
	
	public void ShowUnlockNotification() {
		NotificationHelper.showNotification(this, "You forgot to unlock a chest!");
	}
	
	public void ShowBackNotification() {
		NotificationHelper.showNotification(this, "Zombie is coming again, we need your help!");
	}


	public void ShowFeatureView() {
		if (!af) {
			android.os.Message.obtain(
					Platform.getHandler(this), 
					Platform.SHOW_FEATURE_VIEW, 
		            FeatureView.CENTER_HORIZONTAL, 
		            FeatureView.BOTTOM_VERTICAL,
		            new Rect(150, 410, 650, 480)
				).sendToTarget();
		}
	}
	
	public void ShowFeatureViewSmall() {
		if (!af) {
			android.os.Message.obtain(
					Platform.getHandler(this), 
					Platform.SHOW_FEATURE_VIEW, 
		            FeatureView.CENTER_HORIZONTAL, 
		            FeatureView.BOTTOM_VERTICAL,
		            new Rect(0, 410, 500, 480)
				).sendToTarget();
		}
	}

	public void CloseFeatureView() {
		Platform.getHandler(this).sendEmptyMessage(Platform.CLOSE_FEATURE_VIEW);
	}

	public void ShowFullScreenSmall() {
		if (!af) {
			Platform.getHandler(this).sendEmptyMessage(
					Platform.SHOW_FULLSCREEN_SMALL);
		}
	}

	public void CloseFullScreenSmall() {
		Platform.getHandler(this).sendEmptyMessage(
				Platform.CLOSE_FULLSCREEN_SMALL);
	}

	public void ShowFullScreenSmallExit() {
		Log.i("######", "af:"+af);
		Log.i("######", "ShowFullScreenSmallExit");
		if (!af) {
			Message msg = Platform.getHandler(this).obtainMessage(
					Platform.SHOW_FULLSCREEN_SMALLEXIT, true);
			Platform.getHandler(this).sendMessage(msg);
		}
	}

	public void CloseFullScreenSmallExit() {
		Platform.getHandler(this).sendEmptyMessage(
				Platform.CLOSE_FULLSCREEN_SMALL);
	}

	public boolean IsFullScreenSmallShowing() {
		try {
			return Platform.isFullScreenSmallShowing();
		} catch (Exception e) {
			Log.d("Platform e", e.getMessage());
		}
		return false;
	}

	public boolean IsFullScreenSmallIsReady() {
		try {
			if (af) {
				return false;
			}
			return Platform.isFullScreenSmallIsReady();
		} catch (Exception e) {
			Log.d("Platform e", e.getMessage());
		}
		return false;
	}

	public void onPurchaseSuccess(int id) {
		try {
			Log.d("######", "####onPurchaseSuccess");
			SharedPreferences sp = this.getSharedPreferences(
					"com.jzb.adventurer", Context.MODE_WORLD_WRITEABLE);
			sp.edit().putInt("purchase_id", id).commit();
			Log.d("######", "####purchase_id:"+id);
			UnityHandler.getInstance().onPurchaseSuccess(id);
			if (id != 0 && id != 6) {
				setAdFree(true);
			}
		} catch (Exception e) {
			Log.d("purchase e", e.getMessage());
		}
	}

	public void setAdFree(boolean value) {
		try {
			SharedPreferences sp = this.getSharedPreferences(
					"com.jzb.adventurer", Context.MODE_WORLD_WRITEABLE);
			sp.edit().putInt("adFree", value ? 1 : 0).commit();
			af = value;
		} catch (Exception e) {

		}
	}

	public boolean adFree() {
		try {
			SharedPreferences sp = this.getSharedPreferences(
					"com.jzb.adventurer", Context.MODE_WORLD_WRITEABLE);
			return sp.getInt("adFree", 0) == 1;
		} catch (Exception e) {

		}
		return false;
	}

	public void purchase(int id) {
		try {
			billHandler.sendEmptyMessage(id);
		} catch (Exception e) {
			Log.d("purchase e", e.getMessage());
		}
	}

	public void GetServerTime(){
		try {
			Platform.getServerTime();
		} catch (Exception e) {
			Log.d("purchase e", e.getMessage());
		}
	}
	
	public int GetDPI() {
		DisplayMetrics metrics = this.getResources().getDisplayMetrics();
		return metrics.densityDpi;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Log.d("purchase", "onActivityResult");
		if (!store.onActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
}

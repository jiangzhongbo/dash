package com.jzb.adventurer;


import com.unity3d.player.UnityPlayer;

public class UnityHandler {
	public static String GAMEOBJECT_NAME = "__UnityHandler";
	public static String PURCHASE = "PurchaseSuccess";
	
	private static UnityHandler sUnityHandler;
	public static UnityHandler getInstance() {
		if(sUnityHandler == null) {
			sUnityHandler = new UnityHandler();
		}
		return sUnityHandler;
	}
	
	public void sendMessage(String go, String func, String msg) {
		UnityPlayer.UnitySendMessage(go, func, msg);
	}
	
	public void onPurchaseSuccess(int id) {
		UnityPlayer.UnitySendMessage(GAMEOBJECT_NAME, PURCHASE, "" + id);
	}
}

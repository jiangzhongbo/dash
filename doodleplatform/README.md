# Doodle Platform Client

这是我们的平台，欢迎使用！

* **注意**
# 按照说明文档添加平台不是很麻烦而且不会出错，不要通过随便复制别的项目来添加平台

* **更新**
* **2016-01-19**
  * 平台客户端与支付合并，修复支付中几个NullPointer错误

* **2015-11-23**
  * 平台加入Admob广告回调方法，用来检测是否关闭Admob

* **2015-07-16**
  * 平台加入插屏广告点击回调方法，用来检测是否点击插页广告

* **2015-06-15**
  * 平台加入每日登陆模块
  * 在获取到服务器时间后首先要转换成本地时间DGlobalPrefences.serverTime = servertime + DGlobalPrefences.GetTimeZoneOffsetSecond();
  * 在获取服务器时间的回调函数里面通过调用DGlobalPrefences.GetBonusDayCount(DGlobalPrefences.serverTime)在获取服务器时间的回调函数里面通过调用DGlobalPrefences.SetBonusDay(DGlobalPrefences.serverTime)

* **2015-06-02**
  * 平台加入GA统计功能
  * 在所有国家都展示FeatureView,去掉Banner形式的Admob
  * FeatureView推荐策略去掉轮询，不安装就一直推
  * 插页广告改为每次打开游戏的前两次展示我们的FullScreen，以后展示全屏形式的Admob
  * 在Platform.onCreate()前需要先设置下面两个ID
  * Platform.setFull_Admob_ID(XXXXXXXX);
  * Platform.setGoogleAnalyticsID(XXXXXXXX);
  * 在onStart()里面加入Platform.onStart();


* **2015-05-13**
  * 平台新增测试每日登录接口Platform.getTestServertime()
* **2015-04-01**
  * 平台加入横竖屏设置，来解决广告展示问题
  * Platform.onCreate(this, bool);ture表示横屏，false表示竖屏。

* **2015-03-09**
  * 现在平台提供两种FeatureView layout，想用小尺寸的可以将layout/dm_featureview.xml中的dm:xmlname="dm_featureview_layout"改为dm:xmlname="dm_featureview_layout_small"
  * 增加插屏广告的两个回调函数: 开始请求和结果返回

* **2015-02-25**
  * 删除drawable，加入drawable-xhdpi和drawable-xxhdpi，加入layout\_\*dpi
  * 添加时要将所有icon和dm\_fullscreen\_frame.png加入对应目录

* **2015-01-29**
  * 更改AsyncTask默认的排队顺序，可同时执行
  * 添加Tapjoy和Sponsorpay文档链接，方便添加
  * 去掉手动调用CREATE\_FEATURE\_VIEW

* **2015-01-12**
  * 可以手动获取servertime
  * Platform.getServertime(),返回的结果单位是秒

* **2014-11-20**
  * featureview展示逻辑在本地实现，服务器返回全部结果

* **2014-08-19**
  * 去掉SHOW\_FULLSCREEN的手动调用 


* **2014-08-07 (已经失效，回归一个版本)**
  * 游戏发布需制作两个版本分别支持android:minSdkVersion="9"和android:minSdkVersion="7"
  * 支持level7的版本去掉了google-play-service，并且只推广我们自己的featureview，没有Admob!相应的AndroidMenifest.xml里面不要的东西也删除即可
  * 针对发布的两个版本平台提供一个新的分支[platform-api-level-7]，资源文件也在此目录下

* **2014-07-31**
  * 加入去插屏遮罩功能，适用于退出界面，并且可点击插屏以外区域
  * Message带一个bool参数，ture为去掉遮罩可点击其他区域，false为保留遮罩只能点插屏 
  * 平台增加新接口

```java
  //展示插屏广告
  Message msg = Platform.getHandler(this).obtainMessage(Platform.SHOW_FULLSCREEN_SMALLEXIT, true);
  Platform.getHandler(this).sendMessage(msg);
  
  //插屏广告是否正在展示
  Platform.isFullScreenSmallShowing();

  //插屏广告是否准备就绪
  Platform.isFullScreenSmallIsReady();
```

* **2014-07-30**
  * 加入Advertisting ID
  * 增加插屏广告回调

```java
	//插屏广告关闭回调
	Platform.setFullScreenCloseListener(new FullScreenCloseListener() {
		
		@Override
		public void onFullSCreenClosed() {
			// TODO Auto-generated method stub
			Log.e("FullScreenCallBack", "FullScreen Closed!");
		}
	});
```
* **以前**
  * 删掉大量静态变量，精简平台
  * 工程师不需要自己再在某个地方处理平台的back键的key event，全部由平台内部处理
  * 平台内建Handler供工程师调用，负责管理所有FullScreen，MoreGames，featureView的显示
  * featureView的定位更加完美，更加人性化，支持矩形框指定区域放置，以及其中的盒模型设置

## Before Start
1. **AndroidManifest.xml里面添加权限**
 * (1) manifest节点下添加：

```java
	<uses-permission android:name="android.permission.INTERNET" />
	<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
	<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
	<uses-permission android:name="com.android.vending.BILLING" />
```

 * (2) application节点下添加：

```java
	//application节点内增加android:name
	<application android:name="com.doodlemobile.gamecenter.DoodleAnalytics">
	<activity
        android:name="com.doodlemobile.gamecenter.moregames.MoreGamesActivity"
        android:screenOrientation="portrait"
        android:theme="@android:style/Theme" >
    </activity>
	<activity 
		android:name="com.google.android.gms.ads.AdActivity"
		android:configChanges="keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize"
		android:theme="@android:style/Theme.Translucent">
	</activity>
	<meta-data android:name="doodle_mobile_appid" android:value=XXXXXXXX />
	<meta-data android:name="com.google.android.gms.version" android:value="@integer/google_play_services_version"/>
	<service android:name="com.google.android.gms.analytics.CampaignTrackingService" />
	<receiver android:name="com.google.android.gms.analytics.CampaignTrackingReceiver"
     	android:exported="true">
  		<intent-filter>
    		<action android:name="com.android.vending.INSTALL_REFERRER" />
 	 	</intent-filter>
	</receiver>
```

 * (3) menifest里面格式：

```java
	android:versionCode="1"
    android:versionName="1.0.0"
    android:installLocation="preferExternal" >
    <uses-sdk android:minSdkVersion="7" android:targetSdkVersion="14" />
```

* （4）支付类HintGood

```java
	public class HintGoods extends Goods {

    private Activity mainActivity;
    private int increment;
    private boolean adFree;

    // 构造函数的某些参数可以不需要那么多，根据自身需要量身定制
    public HintGoods(Activity activity, String sku, int increment, boolean adFree){
        super(sku);
        this.mainActivity = activity;
        this.increment = increment;
        this.adFree = adFree;
    }

    @Override
    public final void onPurchaseSuccess() {
        // 根据自己的程序，将数量、adFree加上去，保存，更新UI
        // Settings.adFree = adFree;
        // Settings.save(mainActivity);
        // Stats.hintNum += increment;
        // Stats.save(mainActivity);
        // updateUi();      //自己实现
    }

}
```

 * 说明:以上是平台用到的，其他内容根据游戏自己决定；

## Getting Start

1. **下载jar包和资源文件** - 找zhaomingming@doodlemobile.com索要jar包和res包
2. **申请Appid** - 发游戏名，包名，游戏图标到zhaomingming@doodlemobile.com
3. **申请Flurry id和GA id** - 请向曾毅申请Flurry id，留着在代码中使用
4. **申请AdMob id** - 请向晔哥申请全屏形式的AdMob id
5. **加入gps包** - 以library的形式加入到你的工程中，your project--Properties--Android--Library--Add...
6. **OK，开始使用吧** - 在需要的地方调用内建的handler展示全屏，feature view，more games等等，编译运行
7. **Tapjoy** - http://tech.tapjoy.com/product-overview/integration-advertisers/integrating-the-advertiser-sdk#android, SDK: http://tech.tapjoy.com/product-overview/tapjoy-sdks/sdk
8. **Sponsorpay** - http://developer.fyber.com/content/android/basics/getting-started-sdk/, SDK: http://developer.fyber.com/content/android/basics/downloads/

## Sample

为了最小化工程师添加平台的工作量，也为了规范化平台的添加，我们建议使用继承方式添加平台。以下是一份样例，可以直接拷到项目中适合的地方使用。
样例：

```java
// 由于java的单继承，请使用libgdx引擎的游戏继承AndroidApplication，
// 使用Unity引擎的游戏继承UnityPlayerActivity，
// 使用Android原生开发的游戏继承Android原生的Activity
public class DoodleGame extends Activity/AndroidApplication/UnityPlayerActivity {
	
	//Flurry id请找曾毅申请
	private static final String FLURRY_ID = "XXX";

	//支付初始化
	String base64EncodedPublicKey = "you in app billing key";
    private static final String SKU_ID[] = { "hint_10", "hint_30", "hint_70",
            "hint_160", "hint_450", "hint_999" };
    private static final int SKU_NUM[] = { 10, 30, 70, 160, 450, 999 };
    private Goods[] goodsArray = {
            new HintGoods(this, SKU_ID[0], SKU_NUM[0], false),
            new HintGoods(this, SKU_ID[1], SKU_NUM[1], true),
            new HintGoods(this, SKU_ID[2], SKU_NUM[2], true),
            new HintGoods(this, SKU_ID[3], SKU_NUM[3], true),
            new HintGoods(this, SKU_ID[4], SKU_NUM[4], true),
            new HintGoods(this, SKU_ID[5], SKU_NUM[5], true),
    };
    private Store store = new Store(base64EncodedPublicKey, goodsArray);
    public final Handler billHandler = store.getBillingHandler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//先设置这两个ID
		Platform.setFull_Admob_ID(XXXXXXXX);
		Platform.setGoogleAnalyticsID(XXXXXXXX);
		
		//bool用来控制广告展示横竖屏，true为横屏，false为竖屏
		Platform.onCreate(this, bool);
		store.onCreate(this);
        
        //获取服务器时间的监听器
		Platform.setGetServerTimeListener(new GetServerTimeListener() {
  			
  			@Override
  			public void onServerTimeRecived(long servertime) {
  				// TODO Auto-generated method stub
  				Log.e("ServerTime", "ServerTime: " + servertime);
  				if(servertime == -1 || servertime < 0) {
  					Log.e("ServerTime", "Get ServerTime failed, you should ues local time instead.");
  				} else {
  					//transform to local time
  					DGlobalPrefences.serverTime = servertime + DGlobalPrefences.GetTimeZoneOffsetSecond();
  					//get login days
  					int login_days = DGlobalPrefences.GetBonusDayCount(DGlobalPrefences.serverTime);
  					//after gain bonus, remember to save login days
  					DGlobalPrefences.SetBonusDay(DGlobalPrefences.serverTime);
  				}
  			}
  		});

  		//广告请求开始
  		Platform.setFullScreenTaskBeginListener(new FullScreenTaskBeginListener() {
			
			@Override
			public void onFullScreenTaskBegined() {
				// TODO Auto-generated method stub
				Log.e("FullScreen", "task begin now");
			}
		});

		//平台广告结果返回，这时就知道是否能够显示插屏广告了，或者错误原因
		Platform.setGetFullScreenResultListener(new GetFullScreenResultListener() {
			
			@Override
			public void onFullScreenResultRecived(String message, int retCode) {
				// TODO Auto-generated method stub
				//retCode == 0 means request success!
				Log.e("FullScreen", "Result is: " + message);
			}
		});

		//插屏广告关闭回调
		Platform.setFullScreenCloseListener(new FullScreenCloseListener() {
		
			@Override
			public void onFullSCreenClosed() {
				// TODO Auto-generated method stub
				Log.e("FullScreen", "FullScreen Closed!");
			}
		});

		//插屏广告点击回调
		Platform.setFullScreenClickListener(new FullScreenClickListener() {
			
			@Override
			public void onFullScreenClicked() {
				// TODO Auto-generated method stub
				Log.e("FullScreen", "FullScreen Clicked!");
			}
		});

		Platform.setAdmobFullCloseListener(new AdmobFullCloseListener() {
					
			@Override
			public void onAdmobFullClosed() {
				// TODO Auto-generated method stub
				Log.e("Admob", "Admob Closed!");
			}
		});

	}
	
	//支付回调
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // handle store result
        if(!store.onActivityResult(requestCode, resultCode, data)){
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, FLURRY_ID);
		Platform.onStart();
	}

	// 使用google-play-service后必须添加onPause和onResume生命周期管理
	protected void onPause() {
		super.onPause();
		Platform.onPause();
	}
	
	protected void onResume() {
		super.onResume();
		Platform.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
		Platform.onStop();
	}

	@Override
	protected void onDestroy() {
		// 注意要先调用super.onDestroy()，让该函数先善后，再让平台销毁资源
		super.onDestroy();
		Platform.onDestroy();
		store.onDestroy();
	}
	
}
```

然后让我们自己的主Activity继承这个DoodleGame类即可，不会影响到该类的内容。

```java
public class MyGame extends DoodleGame {
	//You own logic
	...
}
```

## 获取服务器时间

```java
	//测试每日登录专用接口
	Platform.getTestServertime();
```

```java
	Platform.getServertime();
	//返回结果在这个Listener里面
	Platform.setGetServerTimeListener(new GetServerTimeListener() {
      		
  		@Override
  		public void onServerTimeRecived(long servertime) {
  			// TODO Auto-generated method stub
  			Log.e("ServerTime", "ServerTime: " + servertime);
  		}
  	});
```

## 插屏的展示与关闭

插屏广告就是把全屏广告按照一定比例缩小生成的，类似全屏广告逻辑

```java
	//目前比例有两种，直接显示插屏的比例是85%，在退出界面显示的插屏比例是60%
	Platform.getHandler(this).sendEmptyMessage(Platform.SHOW_FULLSCREEN_SMALL);//85%
	Platform.getHandler(this).sendEmptyMessage(Platform.SHOW_FULLSCREEN_SMALLEXIT);//60%
	Platform.getHandler(this).sendEmptyMessage(Platform.CLOSE_FULLSCREEN_SMALL);
```

或者带bool参数, ture为去掉遮罩可点击其他区域，false为保留遮罩只能点插屏 

```java
	Message msg = Platform.getHandler(this).obtainMessage(Platform.SHOW_FULLSCREEN_SMALLEXIT, true);
	Platform.getHandler(this).sendMessage(msg);
```

判断插屏广告是否正在显示

```java
	Platform.isFullScreenSmallShowing();
```

## MoreGames的显示

在需要MoreGames显示的地方，这样调用:

```java
	// 下面这种方式通过Activity显示MoreGames
	Platform.getHandler(this).sendEmptyMessage(Platform.START_MOREGAMES_ACTIVITY);
	// 下面这种方式通过layout显示MoreGames
	Platform.getHandler(this).sendEmptyMessage(Platform.SHOW_MOREGAMES_LAYOUT);
```

**由于layout反应较慢，并不建议使用.**

# FeatureView的显示与关闭  

FeatureView的创建，显示与关闭也封装到了内建的handler里面。现在的平台能够自动搜索layout目录下的dm\_featureview，把它当做featureview的layout，所以请保证不要乱改res下的各个资源，请确保dm\_featureview的存在以及所有dm\_开头的layout的名称不变。平台也会自动搜索dm\_admob并用其展示admob，如果没找到，则会用dm\_featureview代替。
如上，在DoodleGame类里面的onCreate方法中创建featureview。控制FeatureView的2个消息分别为SHOW\_FEATURE\_VIEW、CLOSE\_FEATURE\_VIEW。

```java
	// 显示FeatureView
	Platform.getHandler(this).sendEmptyMessage(Platform.SHOW_FEATURE_VIEW);
	// 关闭FeatureView
	Platform.getHandler(this).sendEmptyMessage(Platform.CLOSE_FEATURE_VIEW);
```

另外，featureview有了更牛逼的定位功能，可以指定矩形区域进行定位，featureview(admob)将会在此矩形区域内展示，还可以规定featureview(admob)在此矩形区域的位置，默认将是在该矩形区域的水平居中，竖直靠底部的位置。
**该矩形区域在竖屏上基于480X800分辨率，在横屏上基于800X400分辨率。
需要注意的是，矩形区域在小屏手机上的区域可能过小会让admob展示不出来。**
使用方式如下：

```java
	// 这是在竖屏手机上，在480*800的相对坐标上，以(0,380)为左上角，以(480, 600)为右下角的矩形区域，
	// 并规定在此矩形区域的水平居中，竖直上部位置展示featureview或admob，用好了，威力很大哦
	Message.obtain(Platform.getHandler(this), Platform.SHOW_FEATURE_VIEW, 
			FeatureView.CENTER_HORIZONTAL, FeatureView.TOP_VERTICAL,
			new Rect(0, 380, 480, 600)
	).sendToTarget();
```

**Admob广告需要320x50dp来显示，那么问题来了，320dp对应不同分辨率手机是多少像素？
竖屏游戏在设置矩形框时直接固定宽度480像素，针对横屏游戏的像素值按如下计算：**

```java
	//计算公式：dp = pixel / density，pixel是实际手机上显示的像素个数，换算方式是矩形设置的宽度* scale，scale 是相对于800*480的一个缩放值，density是屏幕的像素密度
	//以小米2s手机为例，分辨率1280*720，scaleX = 1280/800 = 1.6，density = 2
	于是有 320dp = RectWidthPixel * scaleX / density；即 RectWidthPixel = 320 * density / scaleX = 320 * 2 / 1.5 = 400
	所以在这个手机上面横屏游戏的featureView矩形宽应该最少是400
	//以小Y为例，分辨率320*240， scaleX = 320 / 800 = 0.4，density = 0.75
	RectWidthPixel = 320 * 0.75 / 0.4 = 600
	所以在这个手机上面横屏游戏的featureView矩形宽应该最少是600
	//以三星Ace为例，分辨率480*240，scaleX = 480 / 800 = 0.6，density = 1
	RectWidthPixel = 320 * 1 / 0.6 = 533 
	所以在这个手机上面横屏游戏的featureView矩形宽应该最少是533
	
```


# 谢谢!


# Q&A

Q: **你有哪些想知道的问题?可以把自己想知道的问题告诉我或直接写在下面**
A: 希望不要每一次都犯同样的错误

Q: **游戏包名为什么不能加数字**
A: 因为之前做GA统计的时候匹配包名字符串里只做了字母和点的匹配，考虑到以前很多客户端都使用了这个平台，为了向前兼容之后发的游戏包名同样不能含有数字，虽然这个问题已经在新平台版本中得到了解决。

Q: **游戏退出展示广告时如何判断游戏已经全部安装**
A：如果Resources.fullscreen_small != null说明还有可以展示的游戏广告

Q: **添加GA统计和插屏Admob广告问题**
A: 首先需要设置好这两个ID，Platform.setFull_Admob_ID(XXXXXXXX); Platform.setGoogleAnalyticsID(XXXXXXXX);
插屏展示规则：每次新进入游戏算起，要先展示两次我们自己的广告，之后展示的全部都是Admob。
插屏广告的展示逻辑:每调用平台Platform.SHOW_FULLSCREEN_SMALL这个消息的时候就计数一次，调用两次之后就自动调用Admob，不需要自己单独做计数或者单独处理Admob。但是由于Admob广告每次展示关闭后会重新加载，如果在没有加载成功的时候显示出来的就是我们自己的广告，这属于正常情况。如果不确定有问题的话可以通过命令"adb logcat | grep Ads"来查看log进行判断。
插屏Admob只支持API level9及以上，低版本的只能显示我们的FullScreen。

Q: **平台支付若干问题怎么破**
A: 首先需要保证：
   1.测试版本不是Debug版
   2.添加代码没有遗漏
   3.晔哥已经明确告知可以测试
   4.其他游戏支付没有问题
之后再分析出现的具体问题

Q: **支付之后钱没加上或者支付取消后不能再次进行支付**
A: 检查onActivityResult里面的代码是否添加，是否执行

Q: **FeatureView的框高度默认太高了，改矩形大小会把圆角切掉怎么办？**
A: 现在平台提供两种FeatureView layout，想用小尺寸的可以将layout/dm_featureview.xml中的dm:xmlname="dm_featureview_layout"改为dm:xmlname="dm_featureview_layout_small"

Q: **老游戏升级中FeatureView可以关闭，Admob无法关闭**
A: 去掉手动调用

```java
	Platform.getHandler(activity).sendEmptyMessage(Platform.CREATE_FEATURE_VIEW);
```

Q: **服务器时间获取方法及时机**
A: 服务器时间的获取一个异步过程，通过手动调用Platform.getServertime()向服务器发出请求后，服务器返回结果到回调函数

```java
	Platform.setGetServerTimeListener(new GetServerTimeListener() {
      		
  			@Override
  			public void onServerTimeRecived(long servertime) {
  				// TODO Auto-generated method stub,返回结果的单位是秒
  				Log.e("ServerTime", "ServerTime: " + servertime);
  			}
  		});
```
中得到，返回的时间单位是秒，而非毫秒

Q: **从何时开始计算假loading展示5秒**
A: 最准确的计时开始时间应该是在平台发出请求FullScreen的时刻，由于平台日渐复杂，原来在OnCreate里就开始计时的误差会偏大，为此平台专门提供一个接口用于开始计时

```java
	Platform.setFullScreenTaskBeginListener(new FullScreenTaskBeginListener() {
			
			@Override
			public void onFullScreenTaskBegined() {
				// TODO Auto-generated method stub
				Log.e("FullScreen", "task begin now");
			}
		});
```

Q: **为什么别人加平台没有问题，到我这里就有各种奇怪的现象**
A: 加平台虽不难但是挺烦，稍不留神就有可能出错，原因关键在于不明白为什么要加这些东西。比如要加各种ID，很有可能就有遗漏，添加时一定要看仔细，对于新人更应该切忌急躁，平台有示例，不会的时候多看多问，如果有好的建议也请告诉我

Q: **插屏广告的比例是如何计算的？为什么和游戏的适配不一样？**
A: 在屏幕不是480 * 800这种比例的分辨率下，为了保持广告图的比例，做插屏85%或60%的时候都会将最低值匹配，即保持广告框的最低值是85%或60%，所以在竖屏的时候要保持广告框宽不变，高度做适配，横屏则相反。以320 * 240的手机为例，85%的广告框最后适配结果为306*204，为了保持宽度比例，将高度进行了拉伸才保证广告不会变形。

Q: **部分老游戏升级需要注意的问题有哪些**
A: 很老版本的游戏例如Pool Master里面用到了积分榜，所以还需要保留老平台。新平台以源码的形式加到代码里，有冲突的文件要重命名并确保代码里引用的是新平台的文件。广告featureview展示也要换成新平台的，这样才能确保展示统计的正确。同时新平台里面已经去掉了icon形式的featureview，所以升级的时候这个是否需要保留视情况而定。

[jar-platform]: http://192.168.1.100:8090/doodlemobile-platform/doodle-platform-jar/raw/master/platform/latest/doodlemobile_platform_mini_.jar
[jar-platform+]: http://192.168.1.100:8090/doodlemobile-platform/doodle-platform-jar/raw/master/platform/2014-02-26+google-play-service/doodle-platform-client.jar
[res-link]: http://192.168.1.100:8090/doodlemobile-platform/doodle-platform-jar/raw/master/platform/latest/res_.tar.gz
[jar-admob]: http://192.168.1.100:8090/doodlemobile-platform/doodle-platform-jar/raw/master/admob/GoogleAdMobAdsSdk-6.2.1.jar
[laststable]: http://192.168.1.100:8090/doodlemobile-platform/doodle-platform-client/tree/laststable
[google-play-service-lib]: http://192.168.1.100:8090/JunyongWang/test_lib_project/tree/master
[platform-api-level-7]:http://192.168.1.100:9090/job/doodle-platform-api-level7/

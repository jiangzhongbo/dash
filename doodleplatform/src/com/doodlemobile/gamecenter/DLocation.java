package com.doodlemobile.gamecenter;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

class DLocation {
	Timer timer1;
	LocationManager lm;
	LocationResult locationResult;
	boolean gps_enabled = false;
	boolean network_enabled = false;

	LocationListener locationListenerGps = new LocationListener() {
		public void onLocationChanged(Location location) {
			try {
				if (DLocation.this.timer1 != null) {
					DLocation.this.timer1.cancel();
					DLocation.this.timer1.purge();
					DLocation.this.timer1 = null;
				}
				DLocation.this.locationResult.gotLocation(location);
			} catch (Exception localException) {
			}
			try {
				DLocation.this.lm.removeUpdates(this);
				DLocation.this.lm
						.removeUpdates(DLocation.this.locationListenerNetwork);
			} catch (Exception localException1) {
			}
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};

	LocationListener locationListenerNetwork = new LocationListener() {
		public void onLocationChanged(Location location) {
			try {
				if (DLocation.this.timer1 != null) {
					DLocation.this.timer1.cancel();
					DLocation.this.timer1.purge();
					DLocation.this.timer1 = null;
				}
				DLocation.this.locationResult.gotLocation(location);
			} catch (Exception localException) {
			}
			try {
				DLocation.this.lm.removeUpdates(this);
				DLocation.this.lm
						.removeUpdates(DLocation.this.locationListenerGps);
			} catch (Exception localException1) {
			}
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};

	public boolean getLocation(Context context, LocationResult result) {
		if (this.timer1 != null)
			return true;

		try {
			this.locationResult = result;
			if (this.lm == null)
				this.lm = ((LocationManager) context
						.getSystemService("location"));
			if (this.lm == null) {
				return false;
			}

			if (isProviderSupported("gps"))
				this.gps_enabled = this.lm.isProviderEnabled("gps");
			else
				this.gps_enabled = false;
			if (isProviderSupported("network"))
				this.network_enabled = this.lm.isProviderEnabled("network");
			else
				this.network_enabled = false;
		} catch (Exception localException1) {
		}
		if ((!this.gps_enabled) && (!this.network_enabled))
			return false;
		try {
			if (this.gps_enabled)
				this.lm.requestLocationUpdates("gps", 0L, 0.0F,
						this.locationListenerGps);
			if (this.network_enabled)
				this.lm.requestLocationUpdates("network", 0L, 0.0F,
						this.locationListenerNetwork);
		} catch (Exception e) {
			return false;
		}
		try {
			this.timer1 = new Timer();
			this.timer1.schedule(new GetLastLocation(), 20000L);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public void stopLocation() {
		try {
			if (this.timer1 != null) {
				this.timer1.cancel();
				this.timer1.purge();
				this.timer1 = null;
			}
			if (this.lm != null) {
				if (this.locationListenerGps != null)
					this.lm.removeUpdates(this.locationListenerGps);
				if (this.locationListenerNetwork != null)
					this.lm.removeUpdates(this.locationListenerNetwork);
			}
		} catch (Exception localException) {
		}
	}

	public boolean isProviderSupported(String in_Provider) {
		try {
			List<?> lv_List;
			try {
				lv_List = this.lm.getAllProviders();
			} catch (Throwable e) {
				lv_List = null;
				return false;
			}
			
			for (int lv_N = 0; lv_N < lv_List.size(); lv_N++) {
				if (in_Provider.equals((String) lv_List.get(lv_N))) {
					return true;
				}
			}
			return false;
		} catch (Exception e) {
		}
		return false;
	}

	class GetLastLocation extends TimerTask {
		GetLastLocation() {
		}

		public void run() {
			try {
				DLocation.this.lm
						.removeUpdates(DLocation.this.locationListenerGps);
				DLocation.this.lm
						.removeUpdates(DLocation.this.locationListenerNetwork);
			} catch (Exception localException) {
			}
			try {
				Location net_loc = null;
				Location gps_loc = null;
				if (DLocation.this.gps_enabled)
					gps_loc = DLocation.this.lm
							.getLastKnownLocation("gps");
				if (DLocation.this.network_enabled) {
					net_loc = DLocation.this.lm
							.getLastKnownLocation("network");
				}

				if ((gps_loc != null) && (net_loc != null)) {
					if (gps_loc.getTime() > net_loc.getTime())
						DLocation.this.locationResult
								.gotLocation(gps_loc);
					else
						DLocation.this.locationResult
								.gotLocation(net_loc);
					return;
				}

				if (gps_loc != null) {
					DLocation.this.locationResult.gotLocation(gps_loc);
					return;
				}
				if (net_loc != null) {
					DLocation.this.locationResult.gotLocation(net_loc);
					return;
				}
				DLocation.this.locationResult.gotLocation(null);
			} catch (Exception localException1) {
			}
		}
	}

	public static abstract class LocationResult {
		public abstract void gotLocation(Location paramLocation);
	}
}

package com.doodlemobile.gamecenter;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.Executors;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.location.Criteria;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.doodlemobile.gamecenter.cache.DGlobalPrefences;
import com.doodlemobile.gamecenter.event.DLogThread;
import com.doodlemobile.gamecenter.featuregames.DFeatureGamesFactory;
import com.doodlemobile.gamecenter.net.DNetworkStatus;
import com.doodlemobile.gamecenter.utils.DGlobalParams;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class DoodleMobileAnaylise {
	private static final String TAG = "DoodleMobileAnaylise";

	static final boolean DEBUG = false;

	public static final int LOG_LEVEL_DEBUG = 1;
	public static final int LOG_LEVEL_INFO = 2;
	public static final int LOG_LEVEL_WARN = 4;
	public static final int LOG_LEVEL_ERROR = 8;
	public static final int LOG_LEVEL_FATAL = 16;
	private static final int LOG_LEVEL_MAX = 16;

	public static final int DMTYPE_GAMES = 0x000000;
	public static final int DMTYPE_APPS = 0x000001;
	public static int gAppType = DMTYPE_GAMES;	
	

	/** MC_DIRECTORY = "doodlemobile" */
	private static String MC_DIRECTORY = "doodlemobile";

	/** MC_ANALYTICS_DIRECTORY = "analytics" */
	private static String MC_ANALYTICS_DIRECTORY = "analytics";

	/** MC_MAX_ANALYTICS_FILES = 100 */
	private static int MC_MAX_ANALYTICS_FILES = 100;

	/** MC_MAX_EVENTS_PER_FILE = 5 */
	private static int MC_MAX_EVENTS_PER_FILE = 5;

	private SharedPreferences sharedPrefs = null;

	/** mJsonObj is obj of JSONObject */
	private JSONObject mJsonObj = new JSONObject();

	static final String PREFS_CONFIG = ".DMConfig";

	
	static final int PUSH_MESSAGE_TO_SERVER = 100001;

	public final static int PUSH_MESSAGE_INTERVAL = 60 * 4;
	
	private int idleTimeout = 120000;

	private static File currentFile = null;

	private static boolean fileCreated = false;

	private static int numLinesWritten = 0;

	private static String syncContents = null;

	private boolean mIsOfflineSession = false;

	private boolean mIsInSession = false;

	private boolean mIsTopTask = false;

	private long mSessionStartTime = 0L;

	private long mSessionEndTime = 0L;

	private long mTotalIdleTime = 0L;

	private String androidVersion;

	private String dmAppVersion;

	private String deviceId;

	private String deviceModel;

	private String deviceHardwareModel;

	private String mConnectionType = "null";

	private Handler locationHandler;

	DLocation location = new DLocation();

	private String latitude = "null";

	private String longitude = "null";

	private String mLocale = "null";

	private String mLanguage = "null";

	public String dmAppID = "null";

	private boolean haveLocationPermission = false;

	private Criteria locationCriteria;

	private boolean haveNetworkStatePermission = false;

	private Context context;

	private static final DoodleMobileAnaylise gInstance = new DoodleMobileAnaylise();

	private static boolean gIsFirstInitialized = true;


	private static ApplicationInfo appInfo = null;
	
	private String mPackageName = null;
	private String mInstalledAllDMPkgName = null; 	// installed doodlemobile packages, split with "="
	
	private AsyncTask<String, String, String> ADIDtask = null;

	public static String getPackageName(){
		return gInstance.mPackageName == null ? "null" : gInstance.mPackageName;
	}

	public static String getAllInstalledDMPkgName(){
		return gInstance.mInstalledAllDMPkgName == null ? "null" : gInstance.mInstalledAllDMPkgName;
	}
	
	private String getDMAppId() {
		return gInstance.dmAppID == null ? "null" : gInstance.dmAppID;
	}

	public static String getDMAppVersion() {
		return gInstance.dmAppVersion == null ? "null" : gInstance.dmAppVersion;
	}

	public static String getAndroidVersion() {
		return gInstance.androidVersion == null ? "null" : gInstance.androidVersion;
	}

	String getDeviceId() {
		return this.deviceId == null ? "null" : this.deviceId;
	}
	
	public static String getDeviceId_static() {
		return gInstance.deviceId == null ? "null" : gInstance.deviceId;
	}


	String getDeviceModel() {
		return this.deviceModel == null ? "null" : this.deviceModel;
	}

	String getDeviceHardwareModel() {
		return this.deviceHardwareModel == null ? "null": this.deviceHardwareModel;
	}

	String getConnectionType() {
		return this.mConnectionType == null ? "null" : this.mConnectionType;
	}

	String getLatitude() {
		return this.latitude == null ? "null" : this.latitude;
	}

	String getLongitude() {
		return this.longitude == null ? "null" : this.longitude;
	}

	String getGPS() {
		if ((getLatitude().equals("null")) || (getLongitude().equals("null"))) {
			return "null";
		}
		return getLatitude() + "," + getLongitude();
	}

	String getLanguage() {
		return this.mLanguage == null ? "null" : this.mLanguage;
	}

	public static String getLanguage_static() {
		return gInstance.mLanguage == null ? "null" : gInstance.mLanguage;
	}

	String getLocale() {
		return this.mLocale == null ? "null" : this.mLocale;
	}
	
	public static String getLocale_static() {
		return gInstance.mLocale == null ? "null" : gInstance.mLocale;
	}


	String getMobclixVersion() {
		return "2.3";
	}

	static String getPref(String k) {
		try {
			return gInstance.sharedPrefs.getString(k, "");
		} catch (Exception e) {
		}
		return "";
	}

	static boolean hasPref(String k) {
		try {
			return gInstance.sharedPrefs.contains(k);
		} catch (Exception e) {
		}
		return false;
	}

	static void addPref(String k, String v) {
		try {
			SharedPreferences.Editor spe = gInstance.sharedPrefs.edit();
			spe.putString(k, v);
			spe.commit();
		} catch (Exception localException) {
		}
	}

	static void addPref(Map<String, String> m) {
		try {
			SharedPreferences.Editor spe = gInstance.sharedPrefs.edit();
			for(Entry<String, String> pair : m.entrySet()){
				spe.putString((String) pair.getKey(), (String) pair.getValue());
			}
			spe.commit();
		} catch (Exception localException) {
		}
	}

	static void removePref(String k) {
		try {
			SharedPreferences.Editor spe = gInstance.sharedPrefs.edit();
			spe.remove(k);
			spe.commit();
		} catch (Exception localException) {
		}
	}

	static void clearPref() {
		try {
			SharedPreferences.Editor spe = gInstance.sharedPrefs.edit();
			spe.clear();
			spe.commit();
		} catch (Exception localException) {
		}
	}

	private static String sha1(String string) {
		byte[] shaHash = new byte[40];
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException ex) {
			throw new RuntimeException(ex);
		}

		md.update(string.getBytes(), 0, string.length());
		shaHash = md.digest();
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < shaHash.length; i++) {
			hexString.append(Integer.toHexString(0xFF & shaHash[i]));
		}
		return hexString.toString();
	}

	void updateSession() {
		updateConnectivity();

		if (this.haveLocationPermission) {
			this.locationHandler.sendEmptyMessage(0);
		}
		try {
			this.mJsonObj.put("ts", System.currentTimeMillis());
			String loc = getGPS();
			if (!loc.equals("null")) {
				this.mJsonObj.put("ll", loc);
			} else {
				this.mJsonObj.remove("ll");
			}
			this.mJsonObj.put("g", this.mConnectionType);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void updateConnectivity() {
		try {
			ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService("connectivity");
			String network_type = "u";

			if (this.haveNetworkStatePermission) {
				NetworkInfo network_info = connectivityManager.getActiveNetworkInfo();
				if (network_info != null) {
					network_type = network_info.getTypeName();
				}
			}
			if ((network_type.equalsIgnoreCase("WIFI")) || (network_type.equalsIgnoreCase("WI_FI"))) {
				this.mConnectionType = "wifi";
			} else if (network_type.equalsIgnoreCase("MOBILE")) {
				TelephonyManager telephonyManager = (TelephonyManager) this.context.getSystemService("phone");
				int NetworkType = telephonyManager.getNetworkType();
				this.mConnectionType = Integer.toString(NetworkType);
			} else {
				this.mConnectionType = "null";
			}
			if (this.mConnectionType == null)
				this.mConnectionType = "null";
		} catch (Exception e) {
			this.mConnectionType = "null";
		}
	}

	public static DoodleMobileAnaylise getInstance() {
		return gInstance;
	}
	
	public static final synchronized boolean getIsFirstInitialized(){
		return gIsFirstInitialized;
	}
	
	public static final synchronized void onCreate(final Activity activity, int appType) {
		gAppType = appType;
		DoodleMobileAnaylise.onCreate(activity);
	}

	public static final synchronized void onCreate(final Activity activity) {
		if (activity == null) {
			return;
		}

		new DGlobalPrefences(activity);	// 閸掓繂顬婇崠鏈孉ppPrefences
		
		gInstance.context = activity.getApplicationContext();
		
		if (gIsFirstInitialized) {
			int gameVersionCode = -1;
			String gameVersionName = null;

			try {
				gInstance.mPackageName = activity.getPackageName();		// package name
				
				gameVersionCode = activity.getPackageManager().getPackageInfo(gInstance.mPackageName, PackageManager.PERMISSION_GRANTED).versionCode;
				gameVersionName = activity.getPackageManager().getPackageInfo(gInstance.mPackageName, PackageManager.PERMISSION_GRANTED).versionName;
				
				gInstance.androidVersion = android.os.Build.VERSION.SDK;
			} catch (Exception e) {
				gameVersionCode = -1;
				gameVersionName = "null";
				e.printStackTrace();
			}
	
			if(DGlobalPrefences.getAppVersionCode() == -1 || gameVersionCode > DGlobalPrefences.getAppVersionCode()) {
				DGlobalPrefences.setAppVersionCode(gameVersionCode);
			}
			
			if(DGlobalPrefences.getAppVersionName() == null || gameVersionName.compareToIgnoreCase(DGlobalPrefences.getAppVersionName()) > 0) {
				DGlobalPrefences.setAppVersionName(gameVersionName);
			}
			
//			initDMAppId(activity);
			
			try {
				gInstance.locationHandler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						DoodleMobileAnaylise.gInstance.updateLocation();
					}
				};

				gInstance.updateSession();
				
				gInstance.mLocale = Locale.getDefault().getCountry();
				gInstance.mLanguage = Locale.getDefault().getLanguage();

			} catch (Exception e) {
				Log.w("DoodleMobileAnaylise", "has exception");
				e.printStackTrace();
			}
			gIsFirstInitialized = false;
		}
		
		gInstance.ADIDtask = new AsyncTask<String, String, String>() {
			
			@Override
			protected String doInBackground(String... arg0) {
				// TODO Auto-generated method stub
				Info adInfo = null;
				String id = null;
				try {
					adInfo = AdvertisingIdClient.getAdvertisingIdInfo(gInstance.context);
				    id = adInfo.getId();
					final boolean isLAT = adInfo.isLimitAdTrackingEnabled();
				} catch (Exception e) {
				    // Unrecoverable error connecting to Google Play services (e.g.,
				    // the old version of the service doesn't support getting AdvertisingId).
				}
				return id;
			}
			
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				if(result != null) {
					gInstance.deviceId = result;
				} else {
					try {
						gInstance.deviceId = Settings.Secure.getString(Platform.getActivity().getContentResolver(), "android_id");
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(gInstance.deviceId == null || gInstance.deviceId.equals("") || gInstance.deviceId.equals("9774d56d682e549c")){
						gInstance.deviceId = "_" + UUID.randomUUID();
					}
				}
				Log.e("1234", "userid: " + gInstance.deviceId);
				if(Platform.getActivity() != null) {
					Platform.getHandler(activity).sendEmptyMessage(Platform.CREATE_FULLSCREEN);
					Platform.getHandler(activity).sendEmptyMessage(Platform.CREATE_FEATURE_VIEW);
				}
			}
		};
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			gInstance.ADIDtask.executeOnExecutor(Executors.newCachedThreadPool());
		} else {
			gInstance.ADIDtask.execute();
		}
		
		initDMAppId(activity);
		DFeatureGamesFactory.gInstance.getGameList(activity);
		gInstance.handleSessionStatus(true);
	}

	private void updateLocation() {
		this.location.getLocation(this.context, new DLocation.LocationResult() {
			@Override
			public void gotLocation(Location location) {
				try {
					DoodleMobileAnaylise.this.latitude = Double.toString(location.getLatitude());
					DoodleMobileAnaylise.this.longitude = Double.toString(location.getLongitude());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private static void initDMAppId(Context context) {
		String dmAppID = null;
		
		try {
			String mSharedUserId = null;
			mSharedUserId = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).sharedUserId;
			if(mSharedUserId == null) {
				mSharedUserId = "com.doodlemobile";
			}
			StringBuffer installedPkgNameBuffer = new StringBuffer();
			for(PackageInfo pi : context.getPackageManager().getInstalledPackages(0)) {
				if(mSharedUserId != null && pi != null && pi.sharedUserId != null && pi.sharedUserId.equalsIgnoreCase(mSharedUserId)) {
					installedPkgNameBuffer.append(pi.packageName).append("=");
				}
			}
			gInstance.mInstalledAllDMPkgName = installedPkgNameBuffer.toString();
			Log.e("homer", "sdk = " + getAndroidVersion() + "; installed shareduserid = " + gInstance.mInstalledAllDMPkgName);
			
			appInfo = context.getPackageManager().getApplicationInfo(gInstance.mPackageName, PackageManager.GET_META_DATA);
			dmAppID = appInfo.metaData.getString("doodle_mobile_appid");
			if (dmAppID == null) {		 // "doodle_mobile_appid" 閸忋劋璐熼弫鏉跨摟閺冭绱濋棁锟藉▏閻⑩暎etInt閿涘苯顪�閳ユ竸oodle_mobile_appid = 1234"
				dmAppID = String.valueOf(appInfo.metaData .getInt("doodle_mobile_appid")); // 0 if none
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (dmAppID == null || dmAppID.equals("0")) {		// no dm app id
			throw new Resources.NotFoundException("\"doodle_mobile_appid\" not found in the Android Manifest xml.");
		} else {
			gInstance.dmAppID = dmAppID;

			StringBuffer installedPkgNameBuffer = new StringBuffer();
			ApplicationInfo appInfo;
			for (PackageInfo pi : context.getPackageManager().getInstalledPackages(PackageManager.GET_META_DATA)) {
				
//				if(Integer.parseInt(getAndroidVersion()) <= 8 ){	// 閹绘劕褰嘺ppid閿涘矁袙閸愶拷sdk 8娴犮儰绗呴惃鍕閺堢尨绱�.1, 2.2, 2.2.1)
//					if(!gInstance.mInstalledAllDMPkgName.contains(pi.packageName)) {
//						installedPkgNameBuffer.append(pi.packageName).append("=");
//					}
//					continue;
//				}
				if(pi != null && pi.applicationInfo != null) {
					try {
						appInfo = context.getPackageManager().getApplicationInfo(pi.applicationInfo.packageName, PackageManager.GET_META_DATA);
						if(appInfo != null && appInfo.metaData != null && appInfo.metaData.containsKey("doodle_mobile_appid")) {
							installedPkgNameBuffer.append(pi.packageName).append("=");
						}else if(pi != null && pi.packageName != null && DGlobalParams.isInstalledTheseGames(pi.packageName)) {
							if(!gInstance.mInstalledAllDMPkgName.contains(pi.packageName)) {
								installedPkgNameBuffer.append(pi.packageName).append("=");
							}
						}
					} catch (NameNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
//				if (pi != null && pi.applicationInfo != null && pi.applicationInfo.metaData != null && pi.applicationInfo.metaData.containsKey("doodle_mobile_appid")) {
//					if(!gInstance.mInstalledAllDMPkgName.contains(pi.packageName)) {
//						installedPkgNameBuffer.append(pi.packageName).append("=");
//					}
//				} else if (pi != null && pi.packageName != null && DGlobalParams.isInstalledTheseGames(pi.packageName)) {
//					if(!gInstance.mInstalledAllDMPkgName.contains(pi.packageName)) {
//						installedPkgNameBuffer.append(pi.packageName).append("=");
//					}
//				}
			}
			if (installedPkgNameBuffer.length() > 0) {
				int len = installedPkgNameBuffer.length();
				installedPkgNameBuffer.delete(len - 1, len);
			}
			gInstance.mInstalledAllDMPkgName += installedPkgNameBuffer.toString();
			
			if(gInstance.mInstalledAllDMPkgName.indexOf(getPackageName()) == -1){
				gInstance.mInstalledAllDMPkgName += '=' + getPackageName();
			}
			
			Log.e("homer", "sdk = " + getAndroidVersion() + "; installed appid = " + gInstance.mInstalledAllDMPkgName);
		}
	}
	
	public static final synchronized void onStop(Activity activity) {
		gInstance.handleSessionStatus(false);
	}
	
	public static final synchronized boolean getIsInitialized(){
		return gIsFirstInitialized;
	}

	/** extends from Runnable */
	public class LogEvent implements Runnable {
		JSONObject event;

		public LogEvent() {
		}

		public LogEvent(JSONObject e) {
			this.event = e;
		}

		@Override
		public void run() {
			try {			
				if(!DoodleMobileAnaylise.fileCreated && !DoodleMobileAnaylise.this.OpenAnalyticsFile()){
					Log.i("logEvent", "can not open file");
					return;
				}
				DoodleMobileAnaylise.fileCreated = true;
				DoodleMobileAnaylise.gInstance.updateSession();
				try {
					FileOutputStream fos = new FileOutputStream(DoodleMobileAnaylise.currentFile, true);
					if (DoodleMobileAnaylise.numLinesWritten > 1) {
						fos.write(",".getBytes());
					}
					fos.write(this.event.toString().getBytes());		// event log
					fos.close();

					DoodleMobileAnaylise.numLinesWritten += 1;
					if (DoodleMobileAnaylise.numLinesWritten > DoodleMobileAnaylise.MC_MAX_EVENTS_PER_FILE) {
						DoodleMobileAnaylise.fileCreated = false;
					}
				} catch (Exception localException1) {
					localException1.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static final void logEvent(int eventLogLevel, String processName, String eventName, String description, boolean stopProcess) {
		if (gIsFirstInitialized) {
			Log.v("mobclix-gInstance", "logEvent failed - You must initialize DoodleMobileAnaylise by calling DoodleMobileAnaylise.onCreate(this).");
			return;
		}

		if (eventLogLevel > LOG_LEVEL_MAX) {		// ???
			return;
		}
		
		String logString = processName + ", " + eventName + ": " + description;

		switch (eventLogLevel) {
		case 1:
			Log.d("DoodleMobile", logString);
			break;
		case 2:
			Log.i("DoodleMobile", logString);
			break;
		case 4:
			Log.w("DoodleMobile", logString);
			break;
		case 8:
			Log.e("DoodleMobile", logString);
			break;
		case 16:
			Log.e("DoodleMobile", logString);
		}

		try {
			JSONObject event = new JSONObject(gInstance.mJsonObj, new String[] { "ts", "ll", "g", "id" });
			event.put("el", Integer.toString(eventLogLevel));			// level
			event.put("ep", URLEncoder.encode(processName, "UTF-8"));	// process
			event.put("en", URLEncoder.encode(eventName, "UTF-8"));		// event name
			event.put("ed", URLEncoder.encode(description, "UTF-8"));	// desc
			event.put("et", Long.toString(Thread.currentThread().getId()));		// thread
			event.put("es", stopProcess ? "1" : "0");	// isStop
			
			DoodleMobileAnaylise.LogEvent e1 = gInstance.new LogEvent(event);
			DLogThread.post(e1);
		} catch (Exception localException) {
			localException.printStackTrace();
		}
	}

	private boolean OpenAnalyticsFile() {
		numLinesWritten = 1;
		gInstance.updateSession();
		try {
//			JSONObject header = new JSONObject(gInstance.mJsonObj, new String[] { "ll", "g", "id" });
//			header.put("a", URLEncoder.encode(getDMAppId(), "UTF-8"));
//			header.put("p", "android");
//			header.put("m", URLEncoder.encode(getMobclixVersion()));
//			header.put("v", URLEncoder.encode(getDMAppVersion(), "UTF-8"));
//			header.put("d", URLEncoder.encode(getDeviceId(), "UTF-8"));
//			header.put("dm", URLEncoder.encode(getDeviceModel(), "UTF-8"));
//			header.put("dv", URLEncoder.encode(getAndroidVersion(), "UTF-8"));
//			header.put("hwdm", URLEncoder.encode(getDeviceHardwareModel(), "UTF-8"));
//			header.put("m", URLEncoder.encode("2.3", "UTF-8"));
//			header.put("lg", URLEncoder.encode(getLanguage(), "UTF-8"));
//			header.put("lo", URLEncoder.encode(getLocale(), "UTF-8"));
//			header.put("pn", gInstance.mPackageName);

			File mcDir = gInstance.context.getDir(MC_DIRECTORY, 0);
			File anDir = new File(mcDir.getAbsolutePath() + "/" + MC_ANALYTICS_DIRECTORY);
			anDir.mkdir();
			
			if (anDir != null && anDir.listFiles() != null && anDir.listFiles().length >= MC_MAX_ANALYTICS_FILES) {
				Log.w("DoodleMobileAnaylise", anDir.listFiles().length + " > " + MC_MAX_ANALYTICS_FILES);
				return false;
			}

			currentFile = new File(anDir.getAbsoluteFile() + "/" + System.currentTimeMillis() + ".log");

			currentFile.createNewFile();

			FileOutputStream fos = new FileOutputStream(currentFile);

//			fos.write("[{\"hb\":".getBytes());
//			fos.write(header.toString().getBytes());
//			fos.write(",\"ev\":[".getBytes());
			fos.close();

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	static Handler mSyncHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (PUSH_MESSAGE_TO_SERVER == msg.what) {
				Sync b = gInstance.new Sync();
				DLogThread.post(b);
				mSyncHandler.sendEmptyMessageDelayed(PUSH_MESSAGE_TO_SERVER, 1000 * PUSH_MESSAGE_INTERVAL);
			}
		};
	};

	public static final void sync() {
		if (gIsFirstInitialized) {
			Log.v(TAG, "sync failed... - You must initialize DoodleMobileAnaylise");
			return;
		}

		mSyncHandler.removeMessages(PUSH_MESSAGE_TO_SERVER);
		mSyncHandler.sendEmptyMessage(PUSH_MESSAGE_TO_SERVER);
	}

	private void createNewSession() {
		long ts = System.currentTimeMillis();
		String sessionId = sha1(this.deviceId + ts);

		this.mIsTopTask = true;
		this.mSessionStartTime = ts;
		this.mSessionEndTime = 0L;
		this.mTotalIdleTime = 0L;
		this.mIsInSession = true;
		try {
			this.mJsonObj.put("id", URLEncoder.encode(sessionId, "UTF-8"));
		} catch (Exception e) {
			Log.w(TAG, "createNewSession()  fail...");
			e.printStackTrace();
		}
		Thread remoteConfigThread = new Thread(new FetchRemoteConfig());
		remoteConfigThread.start();
	}

	private synchronized void handleSessionStatus(boolean topTask) {
		try {
			long ts = System.currentTimeMillis();
			if (topTask) {
				if (this.mIsTopTask) {
					return;
				}
				if (!this.mIsInSession) {
					createNewSession();
				} else {
					this.mTotalIdleTime += ts - this.mSessionEndTime;
					this.mIsTopTask = true;
				}

			} else if (!this.mIsTopTask) {
				if ((ts - this.mSessionEndTime > this.idleTimeout) && (this.mIsInSession))
					endSession();
			} else {
				this.mSessionEndTime = ts;
				this.mIsTopTask = false;
				this.location.stopLocation();
			}
		} catch (Exception localException) {
			Log.w("DoodleMobileAnaylise", "handleSessionStatus exception...");
			localException.printStackTrace();
		}
	}

	private void endSession() {
		try {
			if (!this.mIsInSession)
				return;
			long sessionTime = this.mSessionEndTime - this.mSessionStartTime;

			if (hasPref("totalSessionTime"))
				try {
					sessionTime += Long.parseLong(getPref("totalSessionTime"));
				} catch (Exception localException) {
				}
			if (hasPref("mTotalIdleTime"))
				try {
					this.mTotalIdleTime += Long.parseLong(getPref("mTotalIdleTime"));
				} catch (Exception localException1) {
				}
			HashMap<String, String> sessionStats = new HashMap<String, String>();
			sessionStats.put("totalSessionTime", Long.toString(sessionTime));
			sessionStats.put("mTotalIdleTime", Long.toString(this.mTotalIdleTime));

			if (this.mIsOfflineSession) {
				int offlineSessions = 1;
				if (hasPref("offlineSessions"))
					try {
						offlineSessions += Integer.parseInt(getPref("offlineSessions"));
					} catch (Exception e) {
						e.printStackTrace();
					}
				sessionStats.put("offlineSessions", Long.toString(offlineSessions));
			}

			addPref(sessionStats);

			this.mIsInSession = false;
			this.mIsTopTask = false;
			this.mSessionStartTime = 0L;
			this.mSessionEndTime = 0L;
			this.mTotalIdleTime = 0L;
		} catch (Exception localException3) {
		}
	}

	private class FetchRemoteConfig implements Runnable {
		private FetchRemoteConfig() {
		}

		@Override
		public void run() {
			try {
				PackageManager package_manager = DoodleMobileAnaylise.this.context.getPackageManager();
				try {
					DoodleMobileAnaylise.this.dmAppVersion = package_manager.getPackageInfo(gInstance.mPackageName, PackageManager.PERMISSION_GRANTED).versionName;	// PackageManager.PERMISSION_GRANTED = 0
				} catch (PackageManager.NameNotFoundException e) {
					DoodleMobileAnaylise.this.dmAppVersion = "null";
				}
				
				if (package_manager.checkPermission("android.permission.ACCESS_FINE_LOCATION", gInstance.mPackageName) == 0) {	// location
					DoodleMobileAnaylise.this.locationCriteria = new Criteria();
					DoodleMobileAnaylise.this.locationCriteria.setAccuracy(1);
					DoodleMobileAnaylise.this.haveLocationPermission = true;
				} else if (package_manager.checkPermission("android.permission.ACCESS_COARSE_LOCATION", gInstance.mPackageName) == 0) {
					DoodleMobileAnaylise.this.locationCriteria = new Criteria();
					DoodleMobileAnaylise.this.locationCriteria.setAccuracy(2);
					DoodleMobileAnaylise.this.haveLocationPermission = true;
				} else {
					DoodleMobileAnaylise.this.haveLocationPermission = false;
				}
				
				if (package_manager.checkPermission("android.permission.ACCESS_NETWORK_STATE", gInstance.mPackageName) == 0){		// network
					DoodleMobileAnaylise.this.haveNetworkStatePermission = true;
				} else {
					DoodleMobileAnaylise.this.haveNetworkStatePermission = false;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			DoodleMobileAnaylise.sync();
		}
	}

	/** Sync extends from Runnable */
	class Sync implements Runnable {
		@Override
		public synchronized void run() {
			Log.i("Sync to server", "Thread id:" + Thread.currentThread().getId());
			if(!DNetworkStatus.isNetworkAvailable(gInstance.context)) {
				Log.w(TAG, "no network to upload analytics logs");
				return;
			}
			

			try {
				File mcDir = DoodleMobileAnaylise.gInstance.context.getDir(DoodleMobileAnaylise.MC_DIRECTORY, 0);		// mcDir = "/data/data/com.doodlemobile.gamecenter/game_doodlemobile"
				File anDir = new File(mcDir.getAbsolutePath() + "/" + DoodleMobileAnaylise.MC_ANALYTICS_DIRECTORY);		// anDir = "/data/data/com.doodlemobile.gamecenter/game_doodlemobile/analytics"
				anDir.mkdir();
				
				JSONObject header = new JSONObject(gInstance.mJsonObj, new String[] { "ll", "g", "id" });
				header.put("a", URLEncoder.encode(getDMAppId(), "UTF-8"));
				header.put("p", "android");
				header.put("m", URLEncoder.encode(getMobclixVersion()));
				header.put("v", URLEncoder.encode(getDMAppVersion(), "UTF-8"));
				header.put("d", URLEncoder.encode(getDeviceId(), "UTF-8"));
				header.put("dm", URLEncoder.encode(getDeviceModel(), "UTF-8"));
				header.put("dv", URLEncoder.encode(getAndroidVersion(), "UTF-8"));
				header.put("hwdm", URLEncoder.encode(getDeviceHardwareModel(), "UTF-8"));
				header.put("m", URLEncoder.encode("2.3", "UTF-8"));
				header.put("lg", URLEncoder.encode(getLanguage(), "UTF-8"));
				header.put("lo", URLEncoder.encode(getLocale(), "UTF-8"));
				header.put("pn", gInstance.mPackageName);
				
				File[] files = anDir.listFiles();		// files = "[/data/data/com.doodlemobile.gamecenter/game_doodlemobile/analytics/1351079332078.log]"
				if(files == null) {
					return;
				}
				boolean errorOccur = false;
				for (int i = 0; i < files.length; i++) {
					StringBuffer data = new StringBuffer();
					data.append("p=android");
					data.append("&a=").append(URLEncoder.encode(DoodleMobileAnaylise.gInstance.getDMAppId(), "UTF-8"));
					data.append("&m=").append(URLEncoder.encode(DoodleMobileAnaylise.gInstance.getMobclixVersion()));
					data.append("&d=").append(URLEncoder.encode(DoodleMobileAnaylise.gInstance.getDeviceId(), "UTF-8"));
					data.append("&v=").append(URLEncoder.encode(DoodleMobileAnaylise.gInstance.dmAppVersion, "UTF-8"));
					data.append("&j=[{\"hb\":").append(header.toString()).append(",\"ev\":[");

					FileInputStream fis = new FileInputStream(files[i]);
					BufferedInputStream bis = new BufferedInputStream(fis);
					DataInputStream dis = new DataInputStream(bis);

					while (dis.available() != 0) {
						data.append(dis.readLine());
					}
					dis.close();
					bis.close();
					fis.close();

					data.append("]}]");

//					Log.e("zhaoming anaylise", "data.toString: " + data.toString());
					
					String s = data.toString();
					DoodleMobileAnaylise.syncContents = data.toString();
					if(s.contains("}{"))
					{
						DoodleMobileAnaylise.syncContents = s.replaceAll("\\}\\{", "},{");
//						Log.e("zhaoming anaylise", "replace success!");
					}
					
//					Log.e("zhaoming anaylise", "syncContents: " + DoodleMobileAnaylise.syncContents);
					URL url = null;
					HttpURLConnection conn = null;
					try {
						url = new URL(DGlobalParams.SERVER_ANALYTICS_URL);
						conn = (HttpURLConnection) url.openConnection();

						conn.setDoOutput(true);
						conn.setDoInput(true);
						conn.setUseCaches(false);
						conn.setRequestMethod("POST");

						OutputStream ost = conn.getOutputStream();
						PrintWriter pw = new PrintWriter(ost);
						pw.print(DoodleMobileAnaylise.syncContents);

						pw.flush();
						pw.close();

						BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						String line = null;
						String templine;
						while ((templine = rd.readLine()) != null) {
							if (line != null)
								continue;
							line = templine;
						}

						if (!line.equals("1")) {
							errorOccur = true;
						}
						rd.close();
					} catch (Exception e) {
						e.printStackTrace();
						errorOccur = true;
					}

					if (errorOccur) {
						return;
					}
					files[i].delete();
				}
			} catch (Exception localException1) {
				localException1.printStackTrace();
			}
			DoodleMobileAnaylise.syncContents = null;
			DoodleMobileAnaylise.numLinesWritten = 0;
		}
	}
}
package com.doodlemobile.gamecenter;

import com.doodlemobile.gamecenter.cache.DGlobalPrefences;



public class DoodleMobilePublic {

	/** get App Version Code */
	public static int getAppVersionCode() {
		return DGlobalPrefences.getAppVersionCode();
	}

	/** get App Version Name */
	public static String getAppVersionName() {
		return DGlobalPrefences.getAppVersionName();
	}
	
//	/** set FeatureView RefreshTime (s) */
//	public static void setFeatureViewRefreshTime(int refreshtime){
//		FeatureView.setRefreshTime(refreshtime);
//	}
}

package com.doodlemobile.gamecenter.cache;

import java.util.TimeZone;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class DGlobalPrefences {
	private final static String FILE_DM_PREFERENCES = ".dmgames_prefs";
	
	private final static String KEY_ISFIRSTOPEN = "isfirstopen";
	private final static String KEY_APPVERSIONCODE = "appversioncode";		// add by yanggang at 2012-09-28
	private final static String KEY_APPVERSIONNAME = "appversionname";		// add by yanggang at 2012-09-28
	private final static String KEY_MOREGAMESCACHETIME = "moregamescachetime";		// add by yanggang at 2012-11-02
	private final static String KEY_FULLSCREENGAME = "fullscreengame";		// add by yanggang at 2013-01-25
	private final static String KEY_MOREGAMESUPDATE = "moregamesupdatetime"; // add by zhaoming at 2013-06-25
	private final static String KEY_MOREGAMESSHOULDUPDATE = "moregamesshouldupdate"; // add by zhaoming at 2013-06-25
	private static final String KEY_USERCOUNTRY = "usercountry";
	private static final String KEY_USERCOUNTRY_LAST_MODIFIDE = "usercountrylastmodified";
	private static final String KEY_USERCOUNTRY_CACHE_CONTROL = "usercountrycachecontrol";
	
	private static final String KEY_LASTSAVED_LOCALTIME = "lastsavedlocaltime";
	private static final String KEY_LASTSAVED_RUNNINGTIME = "lastsavedrunningtime";
	private static final String KEY_LASTSAVED_SERVERTIME = "lastsavedservertime";
	private static final String KEY_LASTSAVED_NETWORKAVILABLE = "lastsavednetworkavilable";
	
//	private final static String KEY_FULLSCREENSHOWTIMES = "fullscreenshowtimes"; //add by zhaoming at 2013-08-06
	
	public static final String FStail = "&referrer=utm_source%3DFS_";
	public static final String FVtail = "&referrer=utm_source%3DFV_";
	
	private static boolean mFirstOpen = true;
	private static Integer mAppVersionCode = -1;		
	private static String mAppVersionName = null;		
	private static long mMoreGamesCacheTime = -1L;	
	//add by zhaoming
	private static boolean mIsMoreGamesShouldReload = false;
	private static int mFullScreenShowTimes = 0;
	private static String mMoreGamesUpdateTime = null;
	
	private static Context mContext;

	//add by zhaoming 2015.06.15
	private static final int HOUR = 3600 * 1000;
	private static int bonusCount;
	private static long bonusTime;
	public static long serverTime;

	private static final long DAY = 24 * 3600L;
	private static final String BONUS_COUNT = "BonusCount";
	private static final String BONUS_TIME = "BonusTime";
	
	public DGlobalPrefences(Context context) {
		if(context == null) {
			throw new RuntimeException("SharedPreferences init failed: context can't be null");
		}
		mContext = context;
		
		if(isFirstOpen()) {
			setIsFirstOpen(false);
		}

		SharedPreferences preferences = mContext.getSharedPreferences(FILE_DM_PREFERENCES, 0);
		
		mFirstOpen = preferences.getBoolean(KEY_ISFIRSTOPEN , true);
		mAppVersionCode = preferences.getInt(KEY_APPVERSIONCODE, -1);	
		mAppVersionName = preferences.getString(KEY_APPVERSIONNAME, null);	
		
		mMoreGamesCacheTime = preferences.getLong(KEY_MOREGAMESCACHETIME, -1L);
		
		mIsMoreGamesShouldReload = preferences.getBoolean(KEY_MOREGAMESSHOULDUPDATE, false);
		mMoreGamesUpdateTime = preferences.getString(KEY_MOREGAMESUPDATE, null);
		
		bonusCount = preferences.getInt(BONUS_COUNT, 0);
		bonusTime = preferences.getLong(BONUS_TIME, 0);
		
		serverTime = 0;
	}
	
	public static void setIsFirstOpen(boolean firstOpen) {
		SharedPreferences preferences = mContext.getSharedPreferences(FILE_DM_PREFERENCES, 0);
		mFirstOpen = firstOpen;
		preferences.edit().putBoolean(KEY_ISFIRSTOPEN, mFirstOpen).commit();
	}

	public static boolean isFirstOpen() {
		return mFirstOpen;
	}
	
	public static int getAppVersionCode(){
		return mAppVersionCode;
	}
	
	public static void setAppVersionCode(int appversioncode) {
		SharedPreferences preferences = mContext.getSharedPreferences(FILE_DM_PREFERENCES, Context.MODE_PRIVATE);
		mAppVersionCode = appversioncode;
		preferences.edit().putInt(KEY_APPVERSIONCODE, mAppVersionCode).commit();
	}

	public static String getAppVersionName(){
		return mAppVersionName;
	}
	
	public static void setAppVersionName(String appversionname) {
		SharedPreferences preferences = mContext.getSharedPreferences(FILE_DM_PREFERENCES, Context.MODE_PRIVATE);
		mAppVersionName = appversionname;
		preferences.edit().putString(KEY_APPVERSIONNAME, mAppVersionName).commit();
	}
	
	public static long getMoreGamesCacheTime(){
		return mMoreGamesCacheTime;
	}
	
	public static void setMoreGamesCacheTime(long moregamescachetime) {
		SharedPreferences preferences = mContext.getSharedPreferences(FILE_DM_PREFERENCES, Context.MODE_PRIVATE);
		mMoreGamesCacheTime = moregamescachetime;
		preferences.edit().putLong(KEY_MOREGAMESCACHETIME, mMoreGamesCacheTime).commit();
	}
	
	public static void setMoreGamesReload(boolean bool) {
		SharedPreferences preferences = mContext.getSharedPreferences(FILE_DM_PREFERENCES, Context.MODE_PRIVATE);
		preferences.edit().putBoolean(KEY_MOREGAMESSHOULDUPDATE, bool).commit();
		mIsMoreGamesShouldReload = bool;
	}
	
	public static boolean getMoreGamesReload() {
		return mIsMoreGamesShouldReload;
	}
	
	public static void setMoreGamesUpdateTime(String time) {
		SharedPreferences preferences = mContext.getSharedPreferences(FILE_DM_PREFERENCES, Context.MODE_PRIVATE);
		preferences.edit().putString(KEY_MOREGAMESUPDATE, time).commit();
	}
	
	public static String getMoreGamesUpdateTime() {
		return mMoreGamesUpdateTime;
	}
	
	public static void setFullScreenShowTimes(String packagename, int times) {
		SharedPreferences preferences = mContext.getSharedPreferences(FILE_DM_PREFERENCES, Context.MODE_PRIVATE);
		preferences.edit().putInt(packagename, times).commit();
	}
	
	public static int getFullScreenShowTimes(String packagename) {
		SharedPreferences preferences = mContext.getSharedPreferences(FILE_DM_PREFERENCES, 0);
		mFullScreenShowTimes = preferences.getInt(packagename, 0);
		return mFullScreenShowTimes;
	}
	
	// 得到时区信息
	public static int GetTimeZone() {
		TimeZone time_zonex = TimeZone.getDefault();
		int time_zone = time_zonex.getRawOffset() / HOUR;
		System.out.println("time_zone: " + time_zone + "  Unix TimeStamp: " + System.currentTimeMillis());
		return time_zone;
	}

	public static int GetTimeZoneOffsetSecond() {
		TimeZone time_zonex = TimeZone.getDefault();
		return time_zonex.getRawOffset() / 1000;
	}

	public static int GetTimeZoneOffsetMills() {
		TimeZone time_zonex = TimeZone.getDefault();
		return time_zonex.getRawOffset();
	}
	
	// 已经连续登录的天数，9表示连续登录10天
	public static int GetBonusAlreadyGet() {
		return bonusCount;
	}

	// 得到连续登录天数，
	// 返回-1表示领取过或者出错，不要处理
	// 返回0表示第一天登录
	// 返回1表示第二天登录
	// ...
	// 最多返回6，表示第7天登录
	// 如果第8天登录则返回0，但是这个时候可以通过上面那个函数得到总天数，如果需要的话
	public static int GetBonusDayCount(long serverTime) {
		long timeNow = serverTime;
		if (timeNow == -1)
			return -1;
		long daySpan = timeNow / DAY - bonusTime / DAY;
		if (daySpan > bonusCount + 1) {
			return 0;
		} else if (daySpan == bonusCount + 1) {
			return (bonusCount + 1) % 7;
		}
		return -1;
	}

	// 领取dailyBonus之后记得调用一下这个方法
	public static void SetBonusDay(long serverTime) {
		long timeNow = serverTime;
		SharedPreferences preferences = mContext.getSharedPreferences(FILE_DM_PREFERENCES, Context.MODE_PRIVATE);
		if (timeNow == -1)
			return;
		long daySpan = timeNow / DAY - bonusTime / DAY;
		Log.e("123", "daySpan: " + daySpan);
		if (daySpan > bonusCount + 1) {
			bonusCount = 0;
			bonusTime = timeNow;
			preferences.edit().putInt(BONUS_COUNT, bonusCount).commit();
			preferences.edit().putLong(BONUS_TIME, bonusTime).commit();

		} else if (daySpan == bonusCount + 1) {
			bonusCount += 1;
			preferences.edit().putInt(BONUS_COUNT, bonusCount).commit();
			// prefs.edit().putLong(BONUS_TIME, bonusTime).commit();
		}
	}


	
}

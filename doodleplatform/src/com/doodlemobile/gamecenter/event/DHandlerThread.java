package com.doodlemobile.gamecenter.event;

import android.os.HandlerThread;

public class DHandlerThread extends HandlerThread {

	private final static String DMHandlerThread_NAME = "dm_handlerthread";
	private static DHandlerThread gInstance = null;

	public DHandlerThread(String name) {
		super(name);
	}

	public static DHandlerThread getInstance() {
		try {
			if (gInstance == null) {
				gInstance = new DHandlerThread(DMHandlerThread_NAME);
			}

			if (gInstance != null && !gInstance.isAlive()) {
				gInstance.start();
			}

			return gInstance;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}

package com.doodlemobile.gamecenter.event;

import java.lang.reflect.Field;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewManager;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RelativeLayout;

import com.doodlemobile.gamecenter.DoodleMobileAnaylise;
import com.doodlemobile.gamecenter.Platform;
import com.doodlemobile.gamecenter.featuregames.DFeatureGameMutiCountryTask;
import com.doodlemobile.gamecenter.featuregames.FeatureView;
import com.doodlemobile.gamecenter.fullscreen.FullScreenTask;
import com.doodlemobile.gamecenter.fullscreen.Resources;
import com.doodlemobile.gamecenter.moregames.MoreGamesActivity;
import com.doodlemobile.gamecenter.moregames.MoreGamesLayout;
import com.doodlemobile.gamecenter.net.DNetworkStatus;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class PlatformHandler extends Handler {
	private static long lastMoreGameClickTime;
	private Activity activity = null;
	private boolean should_show = true; // default we should show fullscreen
	private float scaleX = 0f;
	private float scaleY = 0f;
	private InterstitialAd interstitial;
	private int sdk_version = 3;
	
	public PlatformHandler(Activity mainActivity){
		activity = mainActivity;
		Platform.setActivity(mainActivity);
		if(activity == null) {
			return;
		}
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay()
				.getMetrics(metrics);
		if(metrics.widthPixels > metrics.heightPixels) {
			scaleX = metrics.widthPixels / 800.0f;
			scaleY = metrics.heightPixels / 480.0f;
		} else {
			scaleX = metrics.widthPixels / 480.0f;
			scaleY = metrics.heightPixels / 800.0f;
		}
		get_sdk();
	}
	
	private void get_sdk() {
		try {
			Class<android.os.Build.VERSION> build_version_class = android.os.Build.VERSION.class;
			Field field = build_version_class.getField("SDK_INT");
			sdk_version = (Integer) field.get(new android.os.Build.VERSION());
		} catch (Exception e) {
		}
	}
	
	public void handleMessage(Message msg) {
		switch(msg.what){
		case Platform.CREATE_FULLSCREEN:
			showFullScreen();
			try {
				if(sdk_version > 8) {
					interstitial = new InterstitialAd(activity);
					if(Platform.MY_AD_UNIT_ID == null) {
		            	throw new RuntimeException("Admob ID can't be null, init MY_AD_UNIT_ID first");
		            }
					if(interstitial != null) {
						interstitial.setAdUnitId(Platform.MY_AD_UNIT_ID);
					    final AdRequest adRequest = new AdRequest.Builder().build();
					    interstitial.loadAd(adRequest);
					    interstitial.setAdListener(new AdListener() {
					    	@Override
					    	public void onAdClosed() {
					    		// TODO Auto-generated method stub
					    		super.onAdClosed();
					    		if(Resources.admobFullCloseListener != null) {
					    			Resources.admobFullCloseListener.onAdmobFullClosed();
					    		}
					    		interstitial.loadAd(adRequest);
					    	}
						});
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
		case Platform.SHOW_FULLSCREEN_SMALL:
			//show FS 2 times, and then show Admob
			try {
				if(sdk_version > 8) {
					if(Platform.ShowFullScreenTimes < 2 || (interstitial != null && !interstitial.isLoaded())) {
						Platform.ShowFullScreenTimes ++;
						if (msg.obj != null && msg.obj instanceof Boolean) {
							showFullScreenSmall((Boolean) msg.obj);
						} else
							showFullScreenSmall(false);
					} else if (interstitial != null && interstitial.isLoaded()) {
						interstitial.show();
					}
				} else {
					if (msg.obj != null && msg.obj instanceof Boolean) {
						showFullScreenSmall((Boolean) msg.obj);
					} else
						showFullScreenSmall(false);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
		case Platform.SHOW_FULLSCREEN_SMALLEXIT:
			if (msg.obj != null && msg.obj instanceof Boolean) {
				showFullScreenSmallExit((Boolean) msg.obj);
			} else
				showFullScreenSmallExit(false);
			break;
		case Platform.CLOSE_FULLSCREEN:
			closeFullScreen();
			break;
		case Platform.CLOSE_FULLSCREEN_SMALL:
			closeFullScreenSmall();
			break;
		case Platform.START_MOREGAMES_ACTIVITY:
			startMoreGamesActivity();
			break;
		case Platform.SHOW_MOREGAMES_LAYOUT:
			showMoreGamesLayout();
			break;
		case Platform.CREATE_FEATURE_VIEW:
			createFeatureView();
			break;
		case Platform.SHOW_FEATURE_VIEW:
			Platform.fvShouldShow = true;
			if(msg.obj == null || !(msg.obj instanceof Rect)) {
				Log.d("DoodleMobile", "no param");
				showFeatureView();
			} else {
				showFeatureView((Rect)msg.obj, msg.arg1, msg.arg2);
			}
			break;
		case Platform.CLOSE_FEATURE_VIEW:
			Platform.fvShouldShow = false;
			closeFeatureView();
			break;
		case Platform.RELEASE_FEATURE_VIEW:
			releaseFeatureView();
			break;
		case Platform.GOTO_RATE:
			gotoRate();
			break;
		}
	}

	private void gotoRate() {
		try {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			String referrer = "market://details?id=" + activity.getPackageName();
			intent.setData(Uri.parse(referrer));
			activity.startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void closeFeatureView() {
		if(Resources.featureView != null) {
			Resources.featureView.setVisibility(View.GONE);
		}
	}

	private void showFeatureView() {
		try{
			if(Resources.featureView != null){
				Resources.featureView.setVisibility(View.VISIBLE);
				View view = ((RelativeLayout)Resources.featureView).getChildAt(0);
				View content = ((RelativeLayout)view).getChildAt(0);
				if(content != null && content instanceof FeatureView) {
						((FeatureView)content).logAppearEvent();
						((FeatureView)content).forceRefreshNewGame();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * @param rect 
	 * @param horizontalPosition 水平位置
	 * @param verticalPosition 垂直位置
	 * 默认水平 CENTER_HORIZONTAL 垂直 BOTTOM_VERTICAL
	 * featureview的高度是固定64dip的，所以看上去矩形设置到太大也没有变化，可变的就是left,top,right
	 */
	private void showFeatureView(Rect rect, int horizontalPosition, int verticalPosition) {
		if(Resources.featureView != null){
			Resources.featureView.setVisibility(View.VISIBLE);
			View view = ((RelativeLayout)Resources.featureView).getChildAt(0);
			
			if(view != null) {
				RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
							(int)((rect.right - rect.left) * scaleX), 
							(int)((rect.bottom - rect.top) * scaleY));
				layoutParams.topMargin = (int) (rect.top * scaleY);
				layoutParams.leftMargin = (int) (rect.left * scaleX);
				view.setLayoutParams(layoutParams);
				
				View content = ((RelativeLayout)view).getChildAt(0);
				if(content != null) {
					RelativeLayout.LayoutParams contentLayoutParams = new RelativeLayout.LayoutParams(
							LayoutParams.WRAP_CONTENT, 
							LayoutParams.WRAP_CONTENT);
					horizontalPosition = horizontalPosition == 0 ? FeatureView.CENTER_HORIZONTAL : horizontalPosition;
					verticalPosition = verticalPosition == 0 ?FeatureView.BOTTOM_VERTICAL : verticalPosition;
					contentLayoutParams.addRule(horizontalPosition);
					contentLayoutParams.addRule(verticalPosition);
					content.setLayoutParams(contentLayoutParams);
				}
				if(content instanceof FeatureView) {
					((FeatureView)content).logAppearEvent();
					((FeatureView)content).forceRefreshNewGame();
				}
			}
		}
	}

	private void createFeatureView() {
		DFeatureGameMutiCountryTask task = new DFeatureGameMutiCountryTask();
		if(sdk_version >= Build.VERSION_CODES.HONEYCOMB) {
			task.executeOnExecutor(Executors.newCachedThreadPool());
		} else {
			task.execute();
		}
	}
	
	private void releaseFeatureView(){
		if(Resources.featureView != null){
			((ViewManager)Resources.featureView.getParent()).removeView(Resources.featureView);
			Resources.featureView = null;
		}
	}

	FullScreenTask task = null;
	private void showFullScreen() {
		Log.e("zhaoming", "handle show_fullscreen");
		if(!DNetworkStatus.isNetworkAvailable(activity)){
			if(Resources.getFullScreenResultListener != null) {
				Resources.getFullScreenResultListener.onFullScreenResultRecived("No network", -1);
			}
			return;
		}
		if(should_show) {
			task = new FullScreenTask();
			if(sdk_version >= Build.VERSION_CODES.HONEYCOMB) {
				task.executeOnExecutor(Executors.newCachedThreadPool());
			} else {
				task.execute();
			}
		}
		if(Resources.fullScreenTaskBeginListener != null) {
			Resources.fullScreenTaskBeginListener.onFullScreenTaskBegined();
		}
	}
	
	private void showFullScreenSmallExit(boolean flag) {
		try {
			if(Resources.fullScreen_small != null && Resources.fullScreenImage_small != null && !Resources.fullScreen_small.isShowing()) {
				Resources.fullScreen_small.prepare(58,60, flag).show();
	 		}
		}catch(Exception e) {
			e.printStackTrace();
		}catch (OutOfMemoryError e) {
			e.printStackTrace();
			throw e;
		}
 		
 	}
	
	private void showFullScreenSmall(boolean flag) {
		try {
			if(Resources.fullScreen_small != null && Resources.fullScreenImage_small != null && !Resources.fullScreen_small.isShowing()) {
				Resources.fullScreen_small.prepare(82,85,flag).show();
	 		}
		} catch (Exception e) {
			e.printStackTrace();
		}catch (OutOfMemoryError e) {
			e.printStackTrace();
			throw e;
		}
 	}
	
	private void closeFullScreen() {
		removeMessages(Platform.CLOSE_FULLSCREEN);
		should_show = false;
		if(task != null){
			task.cancelTask();
		}
		if(Resources.fullScreen != null && Resources.fullScreen.getParent() != null){
			Resources.fullScreen.setVisibility(View.GONE);
			((ViewManager)Resources.fullScreen.getParent()).removeView(Resources.fullScreen);
			Resources.fullScreen = null;
		}
		if(Resources.fullScreenImage != null && !Resources.fullScreenImage.isRecycled()){
			Resources.fullScreenImage.recycle();
		}
		Resources.fullScreenImage = null;
		task = null;
	}
	
	private void closeFullScreenSmall() {
		removeMessages(Platform.CLOSE_FULLSCREEN_SMALL);
		if(Resources.fullScreen_small != null && Resources.fullScreen_small.getParent() != null){
			Resources.fullScreen_small.setVisibility(View.GONE);
			((ViewManager)Resources.fullScreen_small.getParent()).removeView(Resources.fullScreen_small);
			if(Resources.fullScreenCloseListener != null) {
  				Resources.fullScreenCloseListener.onFullSCreenClosed();
  			}
		}
	}
	
	private void startMoreGamesActivity() {
		try {
			Intent intent = new Intent(activity, MoreGamesActivity.class);
			activity.startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	private void showMoreGamesLayout() {
		long curClickTime = System.currentTimeMillis();
		if(curClickTime - lastMoreGameClickTime < 500) {		
			return;
		}
		
		try {
			MoreGamesLayout moreGamesLayout = new MoreGamesLayout(activity);
			LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			activity.addContentView(moreGamesLayout, params);
			moreGamesLayout.setVisibility(View.VISIBLE);
			
			if(DoodleMobileAnaylise.gAppType == DoodleMobileAnaylise.DMTYPE_APPS) {
				DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, "moreapps", "Appear", "MoreGamesLayout", false);
			} else {
				DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, "moregames", "Appear", "MoreGamesLayout", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
			throw e;
		}
		lastMoreGameClickTime = curClickTime;
	}
	
}

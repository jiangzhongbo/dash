package com.doodlemobile.gamecenter.featuregames;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.doodlemobile.gamecenter.DoodleMobileAnaylise;
import com.doodlemobile.gamecenter.cache.DGlobalPrefences;
import com.doodlemobile.gamecenter.net.DNetworkStatus;
import com.doodlemobile.gamecenter.utils.DGlobalParams;

public class FeatureView extends RelativeLayout  {
	
	public static final int LEFT_HORIZONTAL = RelativeLayout.ALIGN_PARENT_LEFT;
	public static final int CENTER_HORIZONTAL = RelativeLayout.CENTER_HORIZONTAL;
	public static final int RIGHT_HORIZONTAL = RelativeLayout.ALIGN_PARENT_RIGHT;
	
	public static final int TOP_VERTICAL = RelativeLayout.ALIGN_PARENT_TOP;
	public static final int CENTER_VERTICAL = RelativeLayout.CENTER_VERTICAL;
	public static final int BOTTOM_VERTICAL = RelativeLayout.ALIGN_PARENT_BOTTOM;

	private final String TYPE_ICON = "icon";
	private final String TYPE_DEFAULT = "default";
	private final String TYPE_ADMOB = "admob";
	
	private static String mFeaturePkg = null;
	public static int index = 0;
	
	public String mFeatureLocation = null;
	private String mFeatureType = null;
	private String mFeatureXmlName = null;
	private int mFeatureRefreshtime = 10;	// featureview refresh time (default)
	private long lastRefreshedTime = 0;
	private RelativeLayout mFeatureLayout = null;	// feature view layout
	private ImageView mFeatureImage = null;			// feature view
	private TextView mFeatureGameText = null;
	private TextView mFeatureCompanyText = null;
	private DFeatureGame dg = null;
	private Intent mFeatureIntent = null;
	private int mRefreshWhat = 10001;
	private Context mContext = null;
	
	public int featureGameIndex = 0;

	public FeatureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		
		mFeatureLocation = attrs.getAttributeValue("dm.featureView.location", "featureLocation");
		mFeatureType = attrs.getAttributeValue("dm.featureView.location", "type");
		mFeatureXmlName = attrs.getAttributeValue("dm.featureView.location", "xmlname");
		mFeatureRefreshtime = attrs.getAttributeIntValue("dm.featureView.location", "refreshtime", 10);
		
		mFeatureLayout = this.getView(mFeatureXmlName);
		mFeatureLayout.setVisibility(View.GONE);
		DFeatureGamesFactory.gInstance.registerFeatureView(this);
		featureGameIndex = FeatureView.index++;
		Log.i("once", "calling reset" + featureGameIndex);
	}

	public void reset() {
		try {
			refreshHandler.removeMessages(mRefreshWhat);
			if (!DNetworkStatus.isNetworkAvailable(mContext)) {
				mFeatureLayout.setVisibility(View.GONE);	
				return;
			}
			dg = DFeatureGamesFactory.gInstance.getNextUninstalledGame(this);
			Log.i("reset", "dg = " + dg);
			if (dg != null && prepareFeatureView(dg)) {
				if (getWindowVisibility() == View.VISIBLE) {
					mFeatureLayout.setVisibility(View.VISIBLE);
					mFeatureImage.setVisibility(View.VISIBLE);
//					DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, dg.mGameName, "Appear", mFeatureLocation, false);
				}
			} else { 
				mFeatureLayout.setVisibility(View.GONE);		// no pop no show
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean prepareFeatureView(DFeatureGame dg) {
		if (dg == null) {
			return false;
		}

		try {
			mFeatureImage.setImageBitmap(dg.mBitmap);	// get featureview bitmap
			if (!mFeatureType.equals(TYPE_ICON)) {
				mFeatureGameText.setText(dg.mGameName);
				mFeatureCompanyText.setText(dg.mCompanyName);
			}
			
			mFeatureIntent = new Intent(Intent.ACTION_VIEW);	// market uri
			mFeatureIntent.setData(Uri.parse(dg.mMarketUri + DGlobalPrefences.FVtail + DoodleMobileAnaylise.getPackageName()));
			//add by zhaoming
//			mFeaturePkg = dg.mMarketUri.split("=")[1];
			mFeaturePkg = DGlobalParams.getPackageName(dg.mMarketUri);
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	protected void onWindowVisibilityChanged(int visibility) {
		super.onWindowVisibilityChanged(visibility);
		if (visibility == View.GONE) {
			refreshHandler.removeMessages(mRefreshWhat);
		} else if (visibility == View.VISIBLE) {
			reset();
			Log.i("onWindowVisibleChanged","here:" + mFeatureLayout.getVisibility() + isShown());
			if (mFeatureLayout.getVisibility() == View.VISIBLE && isShown()) {
				sendRefreshMSG();
			}
		}
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		if (mFeatureLayout.getVisibility() == View.VISIBLE && isShown()) {
			sendRefreshMSG();
		}
	}

	private RelativeLayout getView(String layout) {
		try {
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			String pkgName = mContext.getPackageName();
			
			int layoutId = mContext.getResources().getIdentifier(layout, "layout", pkgName);
			mFeatureLayout = (RelativeLayout) inflater.inflate(layoutId, this);
			
			int fgtId = mContext.getResources().getIdentifier("feature_gamename", "id", pkgName);
			int fctId = mContext.getResources().getIdentifier("feature_companyname", "id", pkgName);
			int featurebarId = mContext.getResources().getIdentifier("featurebar", "id", pkgName);
			
			if (mFeatureType == null || mFeatureType.equals(TYPE_DEFAULT)) {
				mFeatureGameText = (TextView) mFeatureLayout.findViewById(fgtId);
				mFeatureCompanyText = (TextView) mFeatureLayout.findViewById(fctId);

				RelativeLayout mFeatureBackground = null;
				mFeatureBackground = (RelativeLayout) mFeatureLayout.findViewById(featurebarId);
				mFeatureBackground.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mFeatureIntent != null && dg != null) {
							try {
								DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, dg.mGameName, "Clicks", mFeatureLocation + "_" + dg.mGameName, false);
								mContext.startActivity(mFeatureIntent);
//								DUploadUserBehavior.UploadToServer(DUploadUserBehavior.ACTION_CLICK,DUploadUserBehavior.RESOURCE_FEATUREVIEW);
							} catch (Exception e) {
								Toast toast = Toast.makeText(mContext, "Market Not Work", Toast.LENGTH_LONG);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
							}
						}
					}
				});
			} else if (mFeatureType.equals(TYPE_ICON)) {
				mFeatureLayout.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mFeatureIntent != null && dg != null) {
							try {
								DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, dg.mGameName, "Clicks", mFeatureLocation + "_" + dg.mGameName, false);
								mContext.startActivity(mFeatureIntent);
//								DUploadUserBehavior.UploadToServer(DUploadUserBehavior.ACTION_CLICK,DUploadUserBehavior.RESOURCE_FEATUREVIEW);
							} catch (Exception e) {
								Toast toast = Toast.makeText(mContext, "Market Not Work", Toast.LENGTH_LONG);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
							}
							
						}
					}
				});
			} else if (mFeatureType.equals(TYPE_ADMOB)) {
				mFeatureGameText = (TextView) mFeatureLayout.findViewById(fgtId);
				mFeatureCompanyText = (TextView) mFeatureLayout.findViewById(fctId);
				
				mFeatureLayout.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mFeatureIntent != null && dg != null) {
							try {
								DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, dg.mGameName, "Clicks", mFeatureLocation + "_" + dg.mGameName, false);
								mContext.startActivity(mFeatureIntent);
//								DUploadUserBehavior.UploadToServer(DUploadUserBehavior.ACTION_CLICK,DUploadUserBehavior.RESOURCE_FEATUREVIEW);
							} catch (Exception e) {
								Toast toast = Toast.makeText(mContext, "Market Not Work", Toast.LENGTH_LONG);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
							}
						}
					}
				});
			} else if (mFeatureType != null) {
				mFeatureGameText = (TextView) mFeatureLayout.findViewById(fgtId);
				mFeatureCompanyText = (TextView) mFeatureLayout.findViewById(fctId);
				
				mFeatureLayout.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mFeatureIntent != null && dg != null) {
							try {
								DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, dg.mGameName, "Clicks", mFeatureLocation + "_" + dg.mGameName, false);
								mContext.startActivity(mFeatureIntent);
							} catch (Exception e) {
								Toast toast = Toast.makeText(mContext, "Market Not Work", Toast.LENGTH_LONG);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
							}
						}
					}
				});
			}
			
			mFeatureImage = (ImageView) mFeatureLayout.findViewById(mContext.getResources().getIdentifier("feature_image", "id", pkgName));		// feature view
			return mFeatureLayout;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	Handler refreshHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			try {
				long curTime = SystemClock.uptimeMillis();
				boolean forceRefresh = false;
				if(msg != null && msg.obj instanceof Boolean) {
					Log.e("Featureview", "forceRefresh? " + forceRefresh);
					if((Boolean)msg.obj == true) {
						forceRefresh = true;
					}
				}
				if(forceRefresh || (curTime - lastRefreshedTime) > mFeatureRefreshtime*1000) {		// refresh time is larger then set （mFeatureRefreshtime）
					lastRefreshedTime = curTime;
					
					if (mFeatureLayout.getVisibility() == View.VISIBLE) {
						
						DRotate3dAnimation rotation1 = null;
						if (mFeatureType != null && (mFeatureType.equals(TYPE_ICON) || mFeatureType.equals(TYPE_ADMOB))) {
							if (rotation1 == null) {
								rotation1 = new DRotate3dAnimation(0.0F, -90.0F, FeatureView.this.getWidth() / 2f, FeatureView.this.getHeight() / 2f, FeatureView.this.getHeight() * -1f, true);
							}
							rotation1.setDuration(500L);
							rotation1.setFillAfter(true);
							rotation1.setInterpolator(new DecelerateInterpolator());
							rotation1.setAnimationListener(new Animation.AnimationListener() {
								
								@Override
								public void onAnimationStart(Animation animation) {
								}

								@Override
								public void onAnimationEnd(Animation animation) {
									refreshNewGame();
									
									DRotate3dAnimation rotation2 = null;
									if (rotation2 == null) {
										rotation2 = new DRotate3dAnimation(90.0F, 0.0F, FeatureView.this.getWidth() / 2f, FeatureView.this.getHeight() / 2f, FeatureView.this.getHeight() * -1f, false);
									}
									rotation2.setDuration(500L);
									rotation2.setFillAfter(true);
									rotation2.setInterpolator(new DecelerateInterpolator());
									FeatureView.this.startAnimation(rotation2);
								}

								@Override
								public void onAnimationRepeat(Animation animation) {
								}
							});	// end of rotation1.setAnimationListener(...)
							FeatureView.this.startAnimation(rotation1);
						} else {
							refreshNewGame();		// default
						}
					} 
				} // end of refreshedTimeReady
				
				sendRefreshMSG();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private void refreshNewGame() {
		if (mFeatureLayout.getVisibility() == View.VISIBLE && mFeatureLayout.isShown()) {
			dg = DFeatureGamesFactory.gInstance.getNextUninstalledGame(this);
			if (dg != null && prepareFeatureView(dg)) {
				mFeatureImage.setVisibility(View.VISIBLE);
//				DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, dg.mGameName, "Refresh", mFeatureLocation, false);
			}
		}

		sendRefreshMSG();
	}

	private void sendRefreshMSG() {
		refreshHandler.removeMessages(mRefreshWhat);
		refreshHandler.sendEmptyMessageDelayed(mRefreshWhat, mFeatureRefreshtime * 1000L); 	// 10*1000 ms, refresh
	}
	
	//add by zhaoming
	public static String getFeaturePkg() {
		if(mFeaturePkg != null)
			return mFeaturePkg;
		else
			return "null";
	}
	
	public void logAppearEvent() {
		if(dg != null)
		{
			try {
				DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, dg.mGameName, "Appear", mFeatureLocation + "_" + dg.mGameName, false);
//				Toast.makeText(mContext, mFeatureLocation, Toast.LENGTH_SHORT).show();
			} catch (Exception e) {
				Log.i("doodle", "log no network in logAppearEvent");
			}
		}
	}
	
	public void forceRefreshNewGame() {
		if (mFeatureLayout.getVisibility() == View.VISIBLE && mFeatureLayout.isShown()) {
			dg = DFeatureGamesFactory.gInstance.getNextUninstalledGame(this);
			if (dg != null && prepareFeatureView(dg)) {
				mFeatureImage.setVisibility(View.VISIBLE);
			}
		}
		refreshHandler.removeMessages(mRefreshWhat);
		Message msg = refreshHandler.obtainMessage(mRefreshWhat, true);
		refreshHandler.sendMessage(msg);
	}
}

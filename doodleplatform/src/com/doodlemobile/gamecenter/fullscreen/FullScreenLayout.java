package com.doodlemobile.gamecenter.fullscreen;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.doodlemobile.gamecenter.DoodleMobileAnaylise;
import com.doodlemobile.gamecenter.Platform;
import com.doodlemobile.gamecenter.cache.PreferencesCache;

public class FullScreenLayout extends RelativeLayout{
	private Activity activity;
	private Bitmap image = null;
	private FullScreenGame game;
	private DisplayMetrics metrics;
	private Rect closeRect;
	private Rect downloadRect;
	private boolean hasClicked = false;

	public FullScreenLayout(Activity activity, FullScreenGame game, Bitmap image) {
		super(activity);
		this.activity = activity;
		this.game = game;
		this.image = image;
		this.metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay()
				.getMetrics(this.metrics);

		if(isLanscape()){
			closeRect = new Rect(metrics.widthPixels * 9 / 10, 0, metrics.widthPixels, metrics.heightPixels / 5);
			downloadRect = new Rect(metrics.widthPixels / 6, metrics.heightPixels * 2 / 3,  metrics.widthPixels / 2, metrics.heightPixels);
		} else {
			closeRect = new Rect(metrics.widthPixels * 4 / 5, 0, metrics.widthPixels, metrics.heightPixels / 10);
			downloadRect = new Rect(0, metrics.heightPixels * 4 / 5,  metrics.widthPixels / 2, metrics.heightPixels);
		}
		
		setFocusableInTouchMode(true);
		requestFocus();
		setVisibility(View.GONE);
	}
	
	public FullScreenLayout prepare() {
		ImageView imgView = new ImageView(activity);
		imgView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		imgView.setBackgroundDrawable((new BitmapDrawable(image)));
		addView(imgView);
		
		AlphaAnimation alphaAnim = new AlphaAnimation(0.0f, 1.0f);
		alphaAnim.setInterpolator(new AccelerateInterpolator());
		alphaAnim.setDuration(250);
		imgView.startAnimation(alphaAnim);
		
		return this;
	}
	
	public void show(){
		if(activity == null) 
			return;

		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				setVisibility(View.VISIBLE);
				FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(metrics.widthPixels, metrics.heightPixels, Gravity.CENTER);
				activity.addContentView(FullScreenLayout.this, layoutParams);
				DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, game.packageName, "Appear", "FullScreen_" + game.packageName, false);
			}
		});
	}
	
	public boolean isShowing(){
		return getVisibility() == View.VISIBLE;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction() != MotionEvent.ACTION_UP) {
			return true; 
		}
		
		if (inRect(event, closeRect)) {
			if(Platform.getActivity() != null) {
				Platform.getHandler(activity).sendEmptyMessage(Platform.CLOSE_FULLSCREEN);
			}
			return true;
		} else if (inRect(event, downloadRect)) {
			hasClicked = true;
			PreferencesCache cache = PreferencesCache.getInstance(activity);
			cache.putBoolean("hasclicked", hasClicked);
			try {
				Uri uri = Uri.parse(game.getMarketURI());
				Intent intent = new Intent(Intent.ACTION_VIEW, uri);
				activity.startActivity(intent);
				setVisibility(View.GONE);
			} catch (Exception e) {
				Toast.makeText(activity, "Open Android Market Failed ... ", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}
			DoodleMobileAnaylise.logEvent(DoodleMobileAnaylise.LOG_LEVEL_INFO, game.packageName, "Clicks", "FullScreen_" + game.packageName, false);
			Platform.getHandler(activity).sendEmptyMessage(Platform.CLOSE_FULLSCREEN);
			return true;
		}
		return true;
	}
	
	
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			if(Platform.getActivity() != null) {
				Platform.getHandler(activity).sendEmptyMessage(Platform.CLOSE_FULLSCREEN);
				return true;
			}
		}
		return super.onKeyUp(keyCode, event);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK)
			return true;
		return super.onKeyDown(keyCode, event);
	}
	
	private boolean isLanscape() {
		return metrics.widthPixels > metrics.heightPixels;
	}
	
	private boolean inRect(MotionEvent event, Rect rect) {
        if(event.getX() > rect.left && event.getX() < rect.right && 
           event.getY() > rect.top && event.getY() < rect.bottom) 
            return true;
        else
            return false;
    }
}

package com.doodlemobile.gamecenter.fullscreen;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.doodlemobile.gamecenter.DoodleMobileAnaylise;
import com.doodlemobile.gamecenter.cache.PreferencesCache;
import com.doodlemobile.gamecenter.net.DHttpClient;

import android.app.Activity;
import android.util.Log;

public abstract class GetFSGameStrategy {
//	protected static final String GET_FULLSCREEN_HOST = "http://ec2-54-242-231-15.compute-1.amazonaws.com/feature_server/fullScreen/get.php";
	protected static final String GET_FULLSCREEN_HOST = "http://f2.doodlemobile.com/feature_server/fullScreen/get.php";
	protected static class Key {
		public static final String CACHE_ID = "fullscreen";
		public static final String PACKAGE = "package";
		public static final String IMAGEURI = "image";
		public static final String INDEX = "index";
		public static final String TIMES = "times";
		public static final String ENTIRE_TIMES = "entiretimes";
		public static final String LAST_MODIFIED = "lastmodified";
	}
	
	protected Activity mainActivity;
	
	public GetFSGameStrategy(Activity activity) {
		this.mainActivity = activity;
	}
	
	public abstract FullScreenGame getFSGame();
	
	protected FullScreenGame parseJsonStringToFSGame(String gameJsonStr) {
		FullScreenGame fullScreenGame = null;
		if(gameJsonStr != null) {
			Log.i("getFullScreenGame", gameJsonStr);
			try {
				JSONObject jsonGame = new JSONObject(gameJsonStr);
				fullScreenGame = new FullScreenGame();
				fullScreenGame.packageName = jsonGame.getString(Key.PACKAGE);
				fullScreenGame.imageURI = jsonGame.getString(Key.IMAGEURI);
				fullScreenGame.gameIndex = (jsonGame.has(Key.INDEX)) ? jsonGame.getInt(Key.INDEX) : 0;
				fullScreenGame.timesCounter = (jsonGame.has(Key.TIMES)) ? jsonGame.getInt(Key.TIMES) : 1;
				fullScreenGame.entireTimes = (jsonGame.has(Key.ENTIRE_TIMES))? jsonGame.getInt(Key.ENTIRE_TIMES) : 0;
				fullScreenGame.lastModified = (jsonGame.has(Key.LAST_MODIFIED)) ? jsonGame.getLong(Key.LAST_MODIFIED) : System.currentTimeMillis();
			} catch (JSONException e) {
				Log.e("getFullScreenGame", e.getMessage());
				e.printStackTrace();
			}
		}
		return fullScreenGame;
	}
	
	protected String getGameJsonStringFromNet(){
		PreferencesCache cache = PreferencesCache.getInstance(mainActivity);
		boolean hasClicked = false;
		if(cache.isExist("hasclicked")) {
			hasClicked = cache.getBoolean("hasclicked");
		}
		String str = DHttpClient.post(GET_FULLSCREEN_HOST,
				new BasicNameValuePair("clicked", hasClicked ? "1" : "0"),
				new BasicNameValuePair("deviceId", DoodleMobileAnaylise.getDeviceId_static()),
				new BasicNameValuePair("language", DoodleMobileAnaylise.getLanguage_static()),
				new BasicNameValuePair("locale", DoodleMobileAnaylise.getLocale_static()),
				new BasicNameValuePair("sysversion", DoodleMobileAnaylise.getAndroidVersion()),
				new BasicNameValuePair("installedGames", DoodleMobileAnaylise.getAllInstalledDMPkgName()));
		Log.i("FullScreen", "game json str: " + str);
		
		if(str == null) return null;
		
		try {
			JSONObject json = new JSONObject(str);
			if(json.getInt("retcode") < 0) {
				Log.e("FullScreen", json.getString("message"));
				return null;
			}
//			if(Resources.getTimeListener != null) {
//  				Resources.getTimeListener.onServerTimeRecived(json.getLong("date"));
//  			}
			if(json.getString("message").equals("you have installed all fs games.")) {
				return "you have installed all fs games.";
			}
			return json.getJSONObject("game").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	protected String getGameJsonStringFromPreference(){
		String gameJsonStr = null;
		PreferencesCache preferences = PreferencesCache.getInstance(mainActivity);
		if(preferences.isExist(Key.CACHE_ID)) {
			gameJsonStr = preferences.getString(Key.CACHE_ID);
			Log.i("getFullScreenGame", "from SharedPreferences");
		}
		return gameJsonStr;
	}
	
	protected void saveFSGameToPreference(FullScreenGame fullScreenGame) {
		JSONObject jsonGame = new JSONObject();
		String gameStr = null;
		try {
			jsonGame.put(Key.PACKAGE, fullScreenGame.packageName);
			jsonGame.put(Key.IMAGEURI, fullScreenGame.imageURI);
			jsonGame.put(Key.INDEX, fullScreenGame.gameIndex);
			jsonGame.put(Key.ENTIRE_TIMES, fullScreenGame.entireTimes);
			jsonGame.put(Key.TIMES, fullScreenGame.timesCounter);
			jsonGame.put(Key.LAST_MODIFIED, fullScreenGame.lastModified);
			gameStr = jsonGame.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("Platform", "JSONException when saving full screen game:" + e.getMessage());
		}
		if(gameStr != null) {
			PreferencesCache preferences = PreferencesCache.getInstance(mainActivity);
			preferences.putString(Key.CACHE_ID, gameStr);
		}
	}

	public void deleteCache() {
		PreferencesCache preferences = PreferencesCache.getInstance(mainActivity);
		preferences.delete(Key.CACHE_ID);
	}
}

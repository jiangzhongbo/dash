package com.doodlemobile.gamecenter.fullscreen;

import android.graphics.Bitmap;
import android.view.View;


public class Resources {

	public static FullScreenLayout fullScreen = null;
	public static FullScreenLayoutSmall fullScreen_small = null;
	public static Bitmap fullScreenImage = null;
	public static Bitmap fullScreenImage_small = null;
	public static View featureView = null;
	public static int featureViewR = 0;
	public static int admobViewR = 0;
	
	public static GetServerTimeListener getTimeListener = null;
	public static GetFullScreenResultListener getFullScreenResultListener = null;
	public static FullScreenTaskBeginListener fullScreenTaskBeginListener = null;
	public static FullScreenCloseListener fullScreenCloseListener = null;
	public static FullScreenClickListener fullScreenClisckListener = null;
	public static AdmobFullCloseListener admobFullCloseListener = null;
	
	public interface GetServerTimeListener {
		public void onServerTimeRecived(long servertime);
	}
	
	public interface GetFullScreenResultListener {
		public void onFullScreenResultRecived(String message, int retCode);
	}
	
	public interface FullScreenTaskBeginListener {
		public void onFullScreenTaskBegined();
	}
	
	public interface FullScreenCloseListener {
		public void onFullSCreenClosed();
	}
	
	public interface FullScreenClickListener {
		public void onFullScreenClicked();
	}
	
	public interface AdmobFullCloseListener {
		public void onAdmobFullClosed();
	}
}

package com.doodlemobile.gamecenter.net;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.util.Log;

public class DHttpClient {
	private static final int FETCH_TIMEOUT = 1000;
	private static final int CON_TIMEOUT = 5000;
	private static final int REQUEST_TIMEOUT = 0;
	private static final String CHARSET = HTTP.UTF_8;

	private static HttpClient httpClient = null;

	private DHttpClient() {
	}

	private static HttpClient getHttpClient() {
		if (httpClient == null) {
			HttpParams params = new BasicHttpParams();
			// 从连接池中获取连接超时时间
			ConnManagerParams.setTimeout(params, FETCH_TIMEOUT);
			// HttpConnectionParams.setSocketBufferSize(params, 14 * 1024);
			HttpConnectionParams.setConnectionTimeout(params, CON_TIMEOUT);
			HttpConnectionParams.setSoTimeout(params, REQUEST_TIMEOUT);

			HttpProtocolParams.setContentCharset(params, CHARSET);

			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			schemeRegistry.register(new Scheme("https", SSLSocketFactory
					.getSocketFactory(), 443));
//			schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));

			ClientConnectionManager conManager = new ThreadSafeClientConnManager(
					params, schemeRegistry);
			httpClient = new DefaultHttpClient(conManager, params);
		}
		return httpClient;
	}

	public static String post(String url, NameValuePair... params) {
		List<NameValuePair> paramsList = null;
		if (params != null) {
			paramsList = Arrays.asList(params);
		}
		return post(url, paramsList);
	}

	public static String post(String url, List<NameValuePair> paramsList) {
		try {
			HttpPost request = new HttpPost(url);
			if (paramsList != null) {
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(
						paramsList, CHARSET);
				request.setEntity(entity);
			}

			HttpClient httpClient = getHttpClient();
			HttpResponse response = httpClient.execute(request);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				HttpEntity resEntity = response.getEntity();
				return EntityUtils.toString(resEntity);
			} else {
				request.abort();
				Log.w("Http Request", "Response error:" + url);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			Log.w("Http Request", "Connection failed:" + url);
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * It turn out to  be that this way to download image is the most effective. 
	 * And also it's no timeout limit
	 * @param urlStr
	 * @return
	 */
	public static byte[] downloadImage(String urlStr) {
		try {
			Log.i("downloadImage:start", urlStr);
			HttpGet httpGet = new HttpGet(urlStr);
			HttpClient httpclient = new DefaultHttpClient();
			Log.i("downloadImage:excute get request", urlStr);
			HttpResponse response = (HttpResponse) httpclient.execute(httpGet);
			Log.i("downloadImage:wait response", urlStr);
			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				httpGet.abort();
				Log.w("Http Download Image",
						"getImageFromNet(url) failed... url = " + urlStr);
			} else {
				Log.i("downloadImage:get bytes", urlStr);
				byte[] bytes = EntityUtils.toByteArray(response.getEntity());
				Log.i("downloadImage:got bytes", urlStr);
				return bytes;
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			Log.w("Http Download Image", "Connection failed:" + urlStr);
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void destroy() {
		if (httpClient != null) {
			ClientConnectionManager cm = httpClient.getConnectionManager();
			if (cm != null)
				cm.shutdown();
			httpClient = null;
		}
	}
}

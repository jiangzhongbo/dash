package com.doodlemobile.gamecenter.net;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.os.Handler;
import android.util.Log;

import com.doodlemobile.des.DDes;
import com.doodlemobile.gamecenter.DoodleMobileAnaylise;
import com.doodlemobile.gamecenter.event.DHandlerThread;
import com.doodlemobile.gamecenter.featuregames.FeatureView;
import com.doodlemobile.gamecenter.utils.DGlobalParams;

public class DUploadUserBehavior {
	//Behavior
	
	public static String ACTION_CLICK = "CLICK";
	public static String ACTION_CANCEL = "CANCEL";
	public static int RESOURCE_FEATUREVIEW = 1;
	public static int RESOURCE_FULLSCREEN = 2;
	
	public DUploadUserBehavior() {
		
	}
	
	//who(android_id),when(time),where(place),what(action),which(resource)
	private static List<NameValuePair> AssembleRequest(String action, int resource){
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		nameValuePairs.add(new BasicNameValuePair("api", DDes.getEncString("UploadUserBehavior")));
		nameValuePairs.add(new BasicNameValuePair("android_id", DDes.getEncString(DoodleMobileAnaylise.getDeviceId_static())));
		nameValuePairs.add(new BasicNameValuePair("place", DDes.getEncString(DoodleMobileAnaylise.getPackageName())));
		nameValuePairs.add(new BasicNameValuePair("action", DDes.getEncString(action)));
		nameValuePairs.add(new BasicNameValuePair("resource", DDes.getEncString("" +resource)));
//		if(resource == RESOURCE_FULLSCREEN)
//			nameValuePairs.add(new BasicNameValuePair("fullscreen", DDes.getEncString(FullScreen.getFullScreenPkg())));
//		else 
		if(resource == RESOURCE_FEATUREVIEW)
			nameValuePairs.add(new BasicNameValuePair("featureview", DDes.getEncString(FeatureView.getFeaturePkg())));
		else 
			nameValuePairs.add(new BasicNameValuePair("unknown", DDes.getEncString("unknown")));
		 		
		Log.e("zhaoming", "request to Host: " + nameValuePairs.toString());

		return nameValuePairs;
	}
	
	//args:which action on which resource
	public static void UploadToServer(final String action, final int resource) {
		Handler uploadHandler = new Handler(DHandlerThread.getInstance().getLooper());
		uploadHandler.post(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					HttpParams httpParameters = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(httpParameters, 20000);
					HttpConnectionParams.setSoTimeout(httpParameters, 20000);
					
					HttpClient httpclient = new DefaultHttpClient(httpParameters);
					HttpPost httpPost = new HttpPost(DGlobalParams.SERVER_USER_BEHAVIOR_URL);
					httpPost.setEntity(new UrlEncodedFormEntity(AssembleRequest(action, resource), "utf-8"));
					
					httpclient.execute(httpPost);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}

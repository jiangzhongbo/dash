package com.doodlemobile.gamecenter.utils;

import java.util.UUID;

import android.provider.Settings;

import com.doodlemobile.gamecenter.Platform;
import com.doodlemobile.gamecenter.cache.DMCache;
import com.doodlemobile.gamecenter.cache.ExternalStorageIO;
import com.doodlemobile.gamecenter.cache.IFileIO;
import com.doodlemobile.gamecenter.cache.PreferencesCache;

public class AndroidID extends DMCache{

	private static final String ID_FILE_NAME = "UID";
	private String USERID = null;
	private IFileIO fileIO = null;
	
	public AndroidID(){
		if(hasSDCard()){
			fileIO = new ExternalStorageIO();
			if(fileIO.hasFile(ID_FILE_NAME)) {
				byte[] bytes = fileIO.getFile(ID_FILE_NAME);
				USERID = new String(bytes);
			}
		} else {
			PreferencesCache prefs = PreferencesCache.getInstance(Platform.getActivity());
			if(prefs.isExist(ID_FILE_NAME)) {
				USERID = prefs.getString(ID_FILE_NAME);
			}
		}
		
		if(USERID == null) {
			USERID = Settings.Secure.getString(Platform.getActivity().getContentResolver(), "android_id");
			if(USERID == null || USERID.equals("") || USERID.equals("9774d56d682e549c")){
				USERID = "_" + UUID.randomUUID();
			}
			if(fileIO != null){
				fileIO.saveFile(ID_FILE_NAME, USERID.getBytes());
			}
			PreferencesCache prefs = PreferencesCache.getInstance(Platform.getActivity());
			prefs.putString(ID_FILE_NAME, USERID);
		}
	}


	public String getAndroidId(){
		return USERID;
	}
	
}

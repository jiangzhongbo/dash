info face="Arial" size=50 bold=0 italic=0 charset="" unicode=1 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=1,1 outline=0
common lineHeight=50 base=40 scaleW=256 scaleH=256 pages=1 packed=0 alphaChnl=1 redChnl=0 greenChnl=0 blueChnl=0
page id=0 file="fonts_1024_0.png"
chars count=92
char id=32   x=103   y=39    width=3     height=1     xoffset=-1    yoffset=49    xadvance=12    page=0  chnl=15
char id=33   x=127   y=174   width=6     height=32    xoffset=4     yoffset=8     xadvance=14    page=0  chnl=15
char id=34   x=105   y=231   width=13    height=11    xoffset=1     yoffset=8     xadvance=16    page=0  chnl=15
char id=35   x=230   y=67    width=25    height=32    xoffset=-1    yoffset=8     xadvance=24    page=0  chnl=15
char id=36   x=103   y=0     width=22    height=38    xoffset=1     yoffset=6     xadvance=24    page=0  chnl=15
char id=37   x=45    y=42    width=35    height=32    xoffset=2     yoffset=8     xadvance=39    page=0  chnl=15
char id=38   x=227   y=0     width=28    height=32    xoffset=1     yoffset=8     xadvance=29    page=0  chnl=15
char id=39   x=126   y=231   width=6     height=11    xoffset=1     yoffset=8     xadvance=8     page=0  chnl=15
char id=40   x=43    y=0     width=12    height=41    xoffset=2     yoffset=8     xadvance=15    page=0  chnl=15
char id=41   x=56    y=0     width=12    height=41    xoffset=2     yoffset=8     xadvance=15    page=0  chnl=15
char id=42   x=89    y=232   width=15    height=13    xoffset=1     yoffset=8     xadvance=17    page=0  chnl=15
char id=43   x=233   y=191   width=22    height=22    xoffset=2     yoffset=14    xadvance=26    page=0  chnl=15
char id=44   x=119   y=231   width=6     height=11    xoffset=3     yoffset=36    xadvance=12    page=0  chnl=15
char id=45   x=195   y=216   width=13    height=4     xoffset=1     yoffset=27    xadvance=15    page=0  chnl=15
char id=46   x=168   y=198   width=6     height=4     xoffset=3     yoffset=36    xadvance=12    page=0  chnl=15
char id=47   x=98    y=174   width=14    height=32    xoffset=-1    yoffset=8     xadvance=12    page=0  chnl=15
char id=48   x=115   y=141   width=22    height=32    xoffset=1     yoffset=8     xadvance=24    page=0  chnl=15
char id=49   x=113   y=174   width=13    height=32    xoffset=4     yoffset=8     xadvance=24    page=0  chnl=15
char id=50   x=138   y=138   width=22    height=32    xoffset=1     yoffset=8     xadvance=24    page=0  chnl=15
char id=51   x=161   y=134   width=22    height=32    xoffset=1     yoffset=8     xadvance=24    page=0  chnl=15
char id=52   x=185   y=101   width=23    height=32    xoffset=0     yoffset=8     xadvance=24    page=0  chnl=15
char id=53   x=233   y=100   width=22    height=32    xoffset=1     yoffset=8     xadvance=24    page=0  chnl=15
char id=54   x=69    y=141   width=22    height=32    xoffset=1     yoffset=8     xadvance=24    page=0  chnl=15
char id=55   x=0     y=141   width=22    height=32    xoffset=1     yoffset=8     xadvance=24    page=0  chnl=15
char id=56   x=184   y=134   width=22    height=32    xoffset=1     yoffset=8     xadvance=24    page=0  chnl=15
char id=57   x=23    y=141   width=22    height=32    xoffset=1     yoffset=8     xadvance=24    page=0  chnl=15
char id=58   x=226   y=192   width=6     height=23    xoffset=3     yoffset=17    xadvance=12    page=0  chnl=15
char id=59   x=168   y=167   width=6     height=30    xoffset=3     yoffset=17    xadvance=12    page=0  chnl=15
char id=60   x=23    y=232   width=22    height=21    xoffset=2     yoffset=14    xadvance=26    page=0  chnl=15
char id=61   x=66    y=232   width=22    height=13    xoffset=2     yoffset=18    xadvance=26    page=0  chnl=15
char id=62   x=0     y=232   width=22    height=21    xoffset=2     yoffset=14    xadvance=26    page=0  chnl=15
char id=63   x=46    y=141   width=22    height=32    xoffset=1     yoffset=8     xadvance=24    page=0  chnl=15
char id=64   x=0     y=0     width=42    height=41    xoffset=2     yoffset=8     xadvance=45    page=0  chnl=15
char id=65   x=146   y=35    width=31    height=32    xoffset=-1    yoffset=8     xadvance=29    page=0  chnl=15
char id=66   x=108   y=108   width=25    height=32    xoffset=2     yoffset=8     xadvance=29    page=0  chnl=15
char id=67   x=209   y=34    width=30    height=32    xoffset=1     yoffset=8     xadvance=32    page=0  chnl=15
char id=68   x=119   y=72    width=27    height=32    xoffset=3     yoffset=8     xadvance=32    page=0  chnl=15
char id=69   x=160   y=101   width=24    height=32    xoffset=3     yoffset=8     xadvance=29    page=0  chnl=15
char id=70   x=209   y=100   width=23    height=32    xoffset=3     yoffset=8     xadvance=27    page=0  chnl=15
char id=71   x=178   y=34    width=30    height=32    xoffset=2     yoffset=8     xadvance=34    page=0  chnl=15
char id=72   x=0     y=108   width=26    height=32    xoffset=3     yoffset=8     xadvance=32    page=0  chnl=15
char id=73   x=134   y=174   width=6     height=32    xoffset=3     yoffset=8     xadvance=12    page=0  chnl=15
char id=74   x=64    y=174   width=18    height=32    xoffset=1     yoffset=8     xadvance=22    page=0  chnl=15
char id=75   x=147   y=68    width=27    height=32    xoffset=2     yoffset=8     xadvance=29    page=0  chnl=15
char id=76   x=229   y=133   width=21    height=32    xoffset=2     yoffset=8     xadvance=24    page=0  chnl=15
char id=77   x=114   y=39    width=31    height=32    xoffset=3     yoffset=8     xadvance=37    page=0  chnl=15
char id=78   x=203   y=67    width=26    height=32    xoffset=3     yoffset=8     xadvance=32    page=0  chnl=15
char id=79   x=81    y=42    width=32    height=32    xoffset=1     yoffset=8     xadvance=34    page=0  chnl=15
char id=80   x=134   y=105   width=25    height=32    xoffset=3     yoffset=8     xadvance=29    page=0  chnl=15
char id=81   x=126   y=0     width=32    height=34    xoffset=1     yoffset=8     xadvance=34    page=0  chnl=15
char id=82   x=60    y=75    width=29    height=32    xoffset=3     yoffset=8     xadvance=32    page=0  chnl=15
char id=83   x=27    y=108   width=26    height=32    xoffset=1     yoffset=8     xadvance=29    page=0  chnl=15
char id=84   x=54    y=108   width=26    height=32    xoffset=0     yoffset=8     xadvance=26    page=0  chnl=15
char id=85   x=81    y=108   width=26    height=32    xoffset=3     yoffset=8     xadvance=32    page=0  chnl=15
char id=86   x=30    y=75    width=29    height=32    xoffset=0     yoffset=8     xadvance=29    page=0  chnl=15
char id=87   x=0     y=42    width=44    height=32    xoffset=0     yoffset=8     xadvance=44    page=0  chnl=15
char id=88   x=0     y=75    width=29    height=32    xoffset=0     yoffset=8     xadvance=29    page=0  chnl=15
char id=89   x=90    y=75    width=28    height=32    xoffset=0     yoffset=8     xadvance=28    page=0  chnl=15
char id=90   x=175   y=68    width=27    height=32    xoffset=0     yoffset=8     xadvance=27    page=0  chnl=15
char id=91   x=92    y=0     width=10    height=41    xoffset=2     yoffset=8     xadvance=12    page=0  chnl=15
char id=92   x=83    y=174   width=14    height=32    xoffset=-1    yoffset=8     xadvance=12    page=0  chnl=15
char id=93   x=81    y=0     width=10    height=41    xoffset=0     yoffset=8     xadvance=12    page=0  chnl=15
char id=94   x=46    y=232   width=19    height=17    xoffset=0     yoffset=8     xadvance=19    page=0  chnl=15
char id=95   x=168   y=227   width=26    height=4     xoffset=-1    yoffset=45    xadvance=24    page=0  chnl=15
char id=96   x=158   y=231   width=9     height=6     xoffset=2     yoffset=8     xadvance=15    page=0  chnl=15
char id=97   x=210   y=167   width=22    height=24    xoffset=1     yoffset=16    xadvance=24    page=0  chnl=15
char id=98   x=92    y=141   width=22    height=32    xoffset=2     yoffset=8     xadvance=25    page=0  chnl=15
char id=99   x=23    y=207   width=21    height=24    xoffset=1     yoffset=16    xadvance=22    page=0  chnl=15
char id=100  x=207   y=134   width=21    height=32    xoffset=1     yoffset=8     xadvance=24    page=0  chnl=15
char id=101  x=233   y=166   width=22    height=24    xoffset=1     yoffset=16    xadvance=24    page=0  chnl=15
char id=102  x=240   y=33    width=15    height=32    xoffset=0     yoffset=8     xadvance=13    page=0  chnl=15
char id=103  x=205   y=0     width=21    height=33    xoffset=1     yoffset=16    xadvance=25    page=0  chnl=15
char id=104  x=43    y=174   width=20    height=32    xoffset=2     yoffset=8     xadvance=24    page=0  chnl=15
char id=105  x=141   y=171   width=6     height=32    xoffset=2     yoffset=8     xadvance=10    page=0  chnl=15
char id=106  x=69    y=0     width=11    height=41    xoffset=-3    yoffset=8     xadvance=10    page=0  chnl=15
char id=107  x=22    y=174   width=20    height=32    xoffset=2     yoffset=8     xadvance=22    page=0  chnl=15
char id=108  x=148   y=171   width=6     height=32    xoffset=2     yoffset=8     xadvance=10    page=0  chnl=15
char id=109  x=175   y=167   width=34    height=24    xoffset=2     yoffset=16    xadvance=38    page=0  chnl=15
char id=110  x=45    y=207   width=20    height=24    xoffset=2     yoffset=16    xadvance=24    page=0  chnl=15
char id=111  x=0     y=207   width=22    height=24    xoffset=1     yoffset=16    xadvance=24    page=0  chnl=15
char id=112  x=182   y=0     width=22    height=33    xoffset=2     yoffset=16    xadvance=25    page=0  chnl=15
char id=113  x=159   y=0     width=22    height=33    xoffset=1     yoffset=16    xadvance=25    page=0  chnl=15
char id=114  x=87    y=207   width=14    height=24    xoffset=2     yoffset=16    xadvance=15    page=0  chnl=15
char id=115  x=66    y=207   width=20    height=24    xoffset=1     yoffset=16    xadvance=22    page=0  chnl=15
char id=116  x=155   y=171   width=12    height=31    xoffset=0     yoffset=9     xadvance=12    page=0  chnl=15
char id=117  x=205   y=192   width=20    height=23    xoffset=2     yoffset=17    xadvance=24    page=0  chnl=15
char id=118  x=136   y=207   width=22    height=23    xoffset=1     yoffset=17    xadvance=23    page=0  chnl=15
char id=119  x=102   y=207   width=33    height=23    xoffset=0     yoffset=17    xadvance=33    page=0  chnl=15
char id=120  x=159   y=203   width=22    height=23    xoffset=0     yoffset=17    xadvance=22    page=0  chnl=15
char id=121  x=0     y=174   width=21    height=32    xoffset=0     yoffset=17    xadvance=21    page=0  chnl=15
char id=122  x=182   y=192   width=22    height=23    xoffset=0     yoffset=17    xadvance=22    page=0  chnl=15
char id=126  x=133   y=231   width=24    height=8     xoffset=1     yoffset=21    xadvance=26    page=0  chnl=15
kernings count=91
kerning first=32  second=65  amount=-2  
kerning first=32  second=84  amount=-1  
kerning first=32  second=89  amount=-1  
kerning first=121 second=46  amount=-3  
kerning first=121 second=44  amount=-3  
kerning first=119 second=46  amount=-2  
kerning first=119 second=44  amount=-2  
kerning first=118 second=46  amount=-3  
kerning first=118 second=44  amount=-3  
kerning first=114 second=46  amount=-2  
kerning first=49  second=49  amount=-3  
kerning first=65  second=32  amount=-2  
kerning first=65  second=84  amount=-3  
kerning first=65  second=86  amount=-3  
kerning first=65  second=87  amount=-2  
kerning first=65  second=89  amount=-3  
kerning first=65  second=118 amount=-1  
kerning first=65  second=119 amount=-1  
kerning first=65  second=121 amount=-1  
kerning first=114 second=44  amount=-2  
kerning first=70  second=44  amount=-5  
kerning first=70  second=46  amount=-5  
kerning first=70  second=65  amount=-2  
kerning first=76  second=32  amount=-2  
kerning first=76  second=84  amount=-3  
kerning first=76  second=86  amount=-3  
kerning first=76  second=87  amount=-3  
kerning first=76  second=89  amount=-3  
kerning first=76  second=121 amount=-2  
kerning first=102 second=102 amount=-1  
kerning first=80  second=32  amount=-1  
kerning first=80  second=44  amount=-6  
kerning first=80  second=46  amount=-6  
kerning first=80  second=65  amount=-3  
kerning first=82  second=84  amount=-1  
kerning first=82  second=86  amount=-1  
kerning first=82  second=87  amount=-1  
kerning first=82  second=89  amount=-1  
kerning first=84  second=32  amount=-1  
kerning first=84  second=44  amount=-5  
kerning first=84  second=45  amount=-2  
kerning first=84  second=46  amount=-5  
kerning first=84  second=58  amount=-5  
kerning first=89  second=118 amount=-2  
kerning first=84  second=65  amount=-3  
kerning first=84  second=79  amount=-1  
kerning first=84  second=97  amount=-5  
kerning first=84  second=99  amount=-5  
kerning first=84  second=101 amount=-5  
kerning first=84  second=105 amount=-2  
kerning first=84  second=111 amount=-5  
kerning first=84  second=114 amount=-2  
kerning first=84  second=115 amount=-5  
kerning first=84  second=117 amount=-2  
kerning first=84  second=119 amount=-2  
kerning first=84  second=121 amount=-2  
kerning first=86  second=44  amount=-4  
kerning first=86  second=45  amount=-2  
kerning first=86  second=46  amount=-4  
kerning first=86  second=58  amount=-2  
kerning first=89  second=117 amount=-2  
kerning first=86  second=65  amount=-3  
kerning first=86  second=97  amount=-3  
kerning first=86  second=101 amount=-2  
kerning first=86  second=105 amount=-1  
kerning first=86  second=111 amount=-2  
kerning first=86  second=114 amount=-2  
kerning first=86  second=117 amount=-2  
kerning first=86  second=121 amount=-2  
kerning first=87  second=44  amount=-2  
kerning first=87  second=45  amount=-1  
kerning first=87  second=46  amount=-2  
kerning first=87  second=58  amount=-1  
kerning first=89  second=113 amount=-4  
kerning first=87  second=65  amount=-2  
kerning first=87  second=97  amount=-2  
kerning first=87  second=101 amount=-1  
kerning first=89  second=112 amount=-3  
kerning first=87  second=111 amount=-1  
kerning first=87  second=114 amount=-1  
kerning first=87  second=117 amount=-1  
kerning first=89  second=111 amount=-4  
kerning first=89  second=32  amount=-1  
kerning first=89  second=44  amount=-6  
kerning first=89  second=45  amount=-4  
kerning first=89  second=46  amount=-6  
kerning first=89  second=58  amount=-2  
kerning first=89  second=105 amount=-2  
kerning first=89  second=65  amount=-3  
kerning first=89  second=97  amount=-3  
kerning first=89  second=101 amount=-4  
